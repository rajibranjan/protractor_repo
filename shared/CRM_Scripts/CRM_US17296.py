import urllib
import requests
import unittest
import json
import sys
import CRM_US17296_modules
import datetime
import json
import csv
import base64
import traceback
import gnupg
import os
import getpass
from pprint import pprint
import subprocess

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('titanUserName', nargs='?', help='Titan account user name (* contained in single quotes)')
#parser.add_argument('titanPassword', help='Titan account user password (* contained in single quotes)')
parser.add_argument('baseUrl', nargs='?', help='Titan base url to use like https://www.hpondemand.com (* contained in single quotes)')
parser.add_argument('crmFtpUserName', nargs='?', help='CRM FTP site user name')
parser.add_argument('environment', nargs='?', help='Titan environment (prod or test)')
parser.add_argument('titanUserPw', nargs='?', help='None')
parser.add_argument('crmFtpUserPw', nargs='?', help='None')
args = parser.parse_args()

#baseUrl = raw_input('Enter the base URI to get the data from (i.e. https://www.hpondemand.com or https://www-stable.hpfoghorn.com, etc.): ')
titanPassword = ''
titanUserName = args.titanUserName #raw_input('Enter the Titan user email: ') # something like "testuser@smtp.hpfoghorn.com"
if args.titanUserPw:
	titanPassword = args.titanUserPw
else:
	titanPassword = getpass.getpass('Enter the Titan user password: ') #args.titanPassword # something like "User12345678"

# get the CRM ftp site data
crmFtpSiteUserPassword = ''
crmFtpSiteUserName = args.crmFtpUserName #raw_input('Enter the CRM FTP site username: ')
if args.crmFtpUserPw:
	crmFtpSiteUserPassword = args.crmFtpUserPw
else:
	crmFtpSiteUserPassword = getpass.getpass('Enter the CRM FTP site user password: ')

environment = args.environment #raw_input('Enter the environment ("test" or "prod"): ')

baseUrl = args.baseUrl  # something like https://www-testhp.hpfoghorn.com or https://sso.hpondemand.com
#print 'Base URL = ' + baseUrl

validEnvironments = ['prod', 'test']
loginUrl = ''
crmFtpSite = 'ftp.usa.hp.com'

errorDetected = False

#environment = args.environment
values = {}
headers = {}
data = ''
if environment not in validEnvironments:
	print 'Invalid environment - MUST be either "test" or "prod" - exiting'
	sys.exit(1)
else:
	if environment == 'test':
		index = baseUrl.find('stable')
		if index != -1:
			loginUrl = 'https://sso-stable.hpfoghorn.com/v2/authn/login/unamepwd'
		else:
			loginUrl = 'https://sso-testhp.hpfoghorn.com/v2/authn/login/unamepwd'
	else:
		# production  login URL BUT NEED hpadmin credentials
		loginUrl = 'https://sso.hpondemand.com/v2/authn/login/unamepwd'

	base64string = base64.encodestring('%s:%s' % (titanUserName, titanPassword))[:-1]
	authheader =  'Basic %s' % base64string
	headers = {'Accept': 'x-auth-token/header', 'Authorization': authheader}

sessionObj = requests.Session()
print 'Logging into ' + loginUrl
r= sessionObj.get(loginUrl, headers=headers)

print 'Login result status code: ' + str(r.status_code)
if r.status_code != 200 and r.status_code != 204:
	print 'Cannot proceed due to error status code from login of ' + str(r.status_code)
	sys.exit(1)

#print 'response header x-auth-token = '
authTokenHdr = r.headers.get('x-auth-token')
#print authTokenHdr
headers = {'x-auth-token': authTokenHdr}

limit = 200
offset = 0

runDate = datetime.datetime.now().strftime('%Y%m%d%H%M')

#
# Get the I18n localization items for obtaining the offering short and long hames
# to be used in later processing
# get I18n localization items from proxy root for english only
# **********************
proxyRootUrl = 'https://www.hpondemand.com/api/v2/i18n'
values = {'qs' : 'locale=en-US'}
data = urllib.urlencode(values)
print 'Getting proxy root localization items using URL ' + proxyRootUrl
r= sessionObj.get(proxyRootUrl, params=data)
#print 'Proxy root I18n result status code: ' + str(r.status_code)
if r.status_code != 200:
	print 'Cannot proceed due to error status code from Proxy root I18n query of ' + str(r.status_code)
	sys.exit(1)

i18nDict = {}
jsonRes = r.json()
#pprint(jsonRes)
for item in jsonRes['data']:
	i18nData =	item['i18n']
	if 'cDescription' in i18nData:
		i18nDict[item['source'].lower()] = json.loads('{"source" : ' + '"' + item['source'].lower() + '",' + '"title" : ' + '"' + i18nData['cTitle'] + '",' + '"description" : ' + '"' + i18nData['cDescription'] + '"}')
	else:
		i18nDict[item['source'].lower()] = json.loads('{"source" : ' + '"' + item['source'].lower() + '",' + '"title" : ' + '"' + i18nData['cTitle'] + '",' + '"description" : ' + '" "}')

#pprint(i18nDict)

# import the CRM pulic key
gpg = gnupg.GPG(gnupghome='./')
key_data = open('TitanDataMart_PublicKey.gpg').read()
import_result = gpg.import_keys(key_data)
#print 'Import results:'
#pprint(import_result.results)
#for fp in import_result.fingerprints:
#	print 'import results fingerprint: ' + fp

#
# process the company file
# **********************
#
print '******************************'
print '******************************'
print '******************************'
print' WRITING THE COMPANY FILE'
print '******************************'
print '******************************'
print '******************************'
#get account information
acctNextUrl = 'not empty'
offset = 0
getAccountUrl = baseUrl + '/api/v2/accounts?expand=adminContact,reseller&limit=' + str(limit) + '&offset=' + str(offset)

companyFileName = './TITAN_COMPANY_' + runDate + '.txt'

companyColumnHeaders = ['Record Type', 'Company ID', 'Status', 'Created Date', 'Activated Date', 'Suspended Date', 'Is Internal', 'Reseller ID',
'Reseller Name', 'Company Name', 'Primary Contact', 'Company Telephone Nbr', 'Company Telephone Extension', 'Address Line1',
'Address Line2', 'City', 'State_Province', 'Country', 'Postal Code', 'As Of Date', 'Devices Attached Nbr']

# process the company file
companyFile = open(companyFileName, 'ab')
writer = csv.writer(companyFile, dialect='excel-tab', quotechar='', quoting=csv.QUOTE_NONE)
writer.writerow(companyColumnHeaders)
companyFile.close()

while acctNextUrl != '' and not errorDetected: # continue until next account URI is empty/null
	print 'Getting account data using account URL: ' + getAccountUrl

	r = sessionObj.get(getAccountUrl, headers=headers)
	#print 'Account Get result status code: ' + str(r.status_code)

	if r.status_code != 200:
		print 'Cannot proceed on writing company file due to error status code of ' + str(r.status_code)
		acctNextUrl = ''
		errorDetected = True
	else:
		try:
			jsonRes = r.json()
		except:
			print '******'
			print 'Error occurred '
			print '******'
			print 'result = '
			print '*' + r.text + '*'
			traceback.print_exc()
			print '******'
			acctNextUrl = ''
			errorDetected = True
		else:
			#pprint(jsonRes)

			offset = offset + limit
			getAccountUrl = baseUrl + '/api/v2/accounts?expand=adminContact,reseller&limit=' + str(limit) + '&offset=' + str(offset)
			if ('next' in jsonRes):
				acctNextUrl = jsonRes['next']
			else:
				acctNextUrl = ''
			#print 'Account next URL: ' + acctNextUrl

			CRM_US17296_modules.writeTitanCompanyFile(jsonRes, companyFileName, runDate)

		#acctNextUrl = ''

#
# process the users file
# **********************
#
print '******************************'
print '******************************'
print '******************************'
print' WRITING THE USERS FILE'
print '******************************'
print '******************************'
print '******************************'
# get users information
userNextUrl = 'not empty'
offset = 0
getUsersUrl = baseUrl + '/api/v2/users?limit=' + str(limit) + '&offset=' + str(offset)
userFileName = './TITAN_USER_' + runDate + '.txt'
usersColumnHeaders = ['Record Type', 'User ID', 'Status', 'Company ID', 'Role', 'First Name', 'Last Name', 'Email',
'User Telephone Nbr', 'User Telephone Extension', 'Locale & Language', 'OK To Email', 'OK To Track Email', 'Date Last Login']

userFile = open(userFileName, 'ab')
writer = csv.writer(userFile, dialect='excel-tab', quotechar='', quoting=csv.QUOTE_NONE)
writer.writerow(usersColumnHeaders)
userFile.close()

while userNextUrl != '' and not errorDetected: # continue until next users URI is empty/null
	print 'Getting user data using users URL: ' + getUsersUrl

	r = sessionObj.get(getUsersUrl, headers=headers)
	#print 'Users Get status code: ' + str(r.status_code)

	if r.status_code != 200:
		print 'Cannot proceed on writing users file due to error status code of ' + str(r.status_code)
		userNextUrl = ''
		errorDetected = True
	else:
		jsonRes = r.json()
		#pprint(jsonRes)

		offset = offset + limit
		getUsersUrl = baseUrl + '/api/v2/users?limit=' + str(limit) + '&offset=' + str(offset)
		if ('next' in jsonRes):
			userNextUrl = jsonRes['next']
		else:
			userNextUrl = ''
		#print 'Users next URL: ' + userNextUrl

		CRM_US17296_modules.writeTitanUsersFile(jsonRes, userFileName)

		#userNextUrl = ''

#
# process the app file
# **********************
#
print '******************************'
print '******************************'
print '******************************'
print' WRITING THE APP FILE'
print '******************************'
print '******************************'
print '******************************'
# get offerings information
offeringNextUrl = 'not empty'
offset = 0
getOfferingsUrl = baseUrl + '/api/v2/offerings?limit=' + str(limit) + '&offset=' + str(offset)
appFileName = './TITAN_APP_' + runDate + '.txt'
appColumnHeaders = ['Record Type', 'App Short Name', 'App Long Name', 'App Status']

appFile = open(appFileName, 'ab')
writer = csv.writer(appFile, dialect='excel-tab', quotechar='', quoting=csv.QUOTE_NONE)
writer.writerow(appColumnHeaders)
appFile.close()

while offeringNextUrl != '' and not errorDetected: # continue until next offering URI is empty/null
	print 'Getting offering data using URL: ' + getOfferingsUrl

	r = sessionObj.get(getOfferingsUrl, headers=headers)
	#print 'Offerings Get status code: ' + str(r.status_code)

	if r.status_code != 200:
		print 'Cannot proceed on writing app file due to error status code of ' + str(r.status_code)
		offeringNextUrl = ''
		errorDetected = True
	else:
		#pprint(r.json())
		jsonRes = r.json()
		#pprint(jsonRes)

		offset = offset + limit
		getOfferingsUrl = baseUrl + '/api/v2/offerings?limit=' + str(limit) + '&offset=' + str(offset)
		if ('next' in jsonRes):
			offeringNextUrl = jsonRes['next']
		else:
			offeringNextUrl = ''
		#print 'Offerings next URL: ' + offeringNextUrl

		CRM_US17296_modules.writeTitanAppFile(jsonRes, appFileName, i18nDict)

		#offeringNextUrl = ''

#
# process the user app  and user app usage files
# **********************
#
print '******************************'
print '******************************'
print '******************************'
print' WRITING THE USER APP AND USER APP USAGE FILES'
print '******************************'
print '******************************'
print '******************************'
# get entitlements information
userAppNextUrl = 'not empty'
offset = 0
getUserAppUrl = baseUrl + '/api/v2/entitlements?expand=user,license,offering&limit=' + str(limit) + '&offset=' + str(offset)
userAppFileName = './TITAN_USERAPP_' + runDate + '.txt'
userAppColumnHeaders = ['Record Type', 'User ID', 'App Short Name', 'Entitlement State', 'Setup State']

userAppFile = open(userAppFileName, 'ab')
writer = csv.writer(userAppFile, dialect='excel-tab', quotechar='', quoting=csv.QUOTE_NONE)
writer.writerow(userAppColumnHeaders)
userAppFile.close()

userAppUsageFileName = './TITAN_USERAPPUSAGE_' + runDate + '.txt'
userAppUsageColumnHeaders = ['Record Type', 'User ID', 'App Short Name', 'Last Login Date', 'Delta Browser Logins Nbr', 'Delta Device Logins Nbr',
'Delta Nbr Of Jobs Sent', 'Delta Nbr Of Jobs Retrieved', 'Delta Nbr Of Jobs Auto-Deleted', 'Delta Nbr Of Pages Sent', 'Delta Nbr Of Pages Retrieved',
'Delta Nbr Of Pages Auto-Deleted']

userAppUsageFile = open(userAppUsageFileName, 'ab')
writer = csv.writer(userAppUsageFile, dialect='excel-tab', quotechar='', quoting=csv.QUOTE_NONE)
writer.writerow(userAppUsageColumnHeaders)
userAppUsageFile.close()

while userAppNextUrl != '' and not errorDetected: # continue until next entitlements URI is empty/null
	print 'Getting user app data using users URL: ' + getUserAppUrl

	r = sessionObj.get(getUserAppUrl, headers=headers)
	#print 'Entitlements Get status code: ' + str(r.status_code)

	if r.status_code != 200:
		print 'Cannot proceed on writing user app and usage files due to error status code of ' + str(r.status_code)
		userAppNextUrl = ''
		errorDetected = True
	else:
		jsonRes = r.json()
		#pprint(jsonRes)

		offset = offset + limit

		getUserAppUrl = baseUrl + '/api/v2/entitlements?expand=user,license,offering&limit=' + str(limit) + '&offset=' + str(offset)
		if ('next' in jsonRes):
			userAppNextUrl = jsonRes['next']
		else:
			userAppNextUrl = ''
		#print 'User App next URL: ' + userAppNextUrl

		CRM_US17296_modules.writeTitanUserAppAndUsageFiles(jsonRes, userAppFileName, userAppUsageFileName, baseUrl, sessionObj, i18nDict, headers)

		#userAppNextUrl = ''

if not errorDetected:

	# remove duplicates from the files
	print 'Removing duplicates from the company file'
	CRM_US17296_modules.removeDuplicates(companyFileName)

	print 'Removing duplicates from the user file'
	CRM_US17296_modules.removeDuplicates(userFileName)

	print 'Removing duplicates from the app file'
	CRM_US17296_modules.removeDuplicates(appFileName)

	print 'Removing duplicates from the user app file'
	CRM_US17296_modules.removeDuplicates(userAppFileName)

	print 'Removing duplicates from the user app usage file'
	CRM_US17296_modules.removeDuplicates(userAppUsageFileName)

	#encrypt the files
	print 'encrypting the company file'
	with open(companyFileName, 'rb') as f:
		status = gpg.encrypt_file(
    		f, always_trust=True,recipients=import_result.fingerprints,
    		output=companyFileName + '.gpg')
    	print 'ok: ', status.ok
    	#print 'status: ', status.status
    	#print 'stderr: ', status.stderr

	print 'encrypting the user file'
	with open(userFileName, 'rb') as f:
		status = gpg.encrypt_file(
    		f, always_trust=True,recipients=import_result.fingerprints,
    		output=userFileName + '.gpg')
    	print 'ok: ', status.ok
    	#print 'status: ', status.status
    	#print 'stderr: ', status.stderr

	print 'encrypting the app file'
	with open(appFileName, 'rb') as f:
		status = gpg.encrypt_file(
    		f, always_trust=True,recipients=import_result.fingerprints,
    		output=appFileName + '.gpg')
    	print 'ok: ', status.ok
    	#print 'status: ', status.status
    	#print 'stderr: ', status.stderr

	print 'encrypting the user app file'
	with open(userAppFileName, 'rb') as f:
		status = gpg.encrypt_file(
    		f, always_trust=True,recipients=import_result.fingerprints,
    		output=userAppFileName + '.gpg')
    	print 'ok: ', status.ok
    	#print 'status: ', status.status
    	#print 'stderr: ', status.stderr

	print 'encrypting the user app usage file'
	with open(userAppUsageFileName, 'rb') as f:
		status = gpg.encrypt_file(
    		f, always_trust=True,recipients=import_result.fingerprints,
    		output=userAppUsageFileName + '.gpg')
    	print 'ok: ', status.ok
    	#print 'status: ', status.status
    	#print 'stderr: ', status.stderr

	# delete the unencrypted files
	try:
		print 'deleting the unencrypted company file'
		os.remove(companyFileName)
		print 'deleting the unencrypted user file'
		os.remove(userFileName)
		print 'deleting the unencrypted app file'
		os.remove(appFileName)
		print 'deleting the unencrypted user app file'
		os.remove(userAppFileName)
		print 'deleting the unencrypted user app usage file'
		os.remove(userAppUsageFileName)
	except OSError, err:  ## if failed, report it back to the user ##
		print ('Error: %s - %s.' % (err.filename,err.strerror))


	# ftp the files to the CRM site
	print 'FTPing the encrypted company file to the CRM site'
	CRM_US17296_modules.ftpFile(companyFileName + '.gpg', crmFtpSiteUserName, crmFtpSiteUserPassword)

	print 'FTPing the encrypted user file to the CRM site'
	CRM_US17296_modules.ftpFile(userFileName + '.gpg', crmFtpSiteUserName, crmFtpSiteUserPassword)

	print 'FTPing the encrypted app file to the CRM site'
	CRM_US17296_modules.ftpFile(appFileName + '.gpg', crmFtpSiteUserName, crmFtpSiteUserPassword)

	print 'FTPing the encrypted user app file to the CRM site'
	CRM_US17296_modules.ftpFile(userAppFileName + '.gpg', crmFtpSiteUserName, crmFtpSiteUserPassword)

	print 'FTPing the encrypted user app usage file to the CRM site'
	CRM_US17296_modules.ftpFile(userAppUsageFileName + '.gpg', crmFtpSiteUserName, crmFtpSiteUserPassword)
