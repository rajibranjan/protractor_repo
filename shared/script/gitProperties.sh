#~/bin/bash
if [[ -z "$1" ]]; then
	echo "You must specify an output file"
	exit 1
fi
FS_BAK=$IFS
IFS="
"

#(cherry picked from commit 9a1f25e6577d62e5ed2b5edac3601e307f27cc33)
grepParentHashRegex="^.*cherry picked from commit.*$"
sedParentHashRegex="^.*cherry picked from commit ([a-z0-9]{40}?).*$"

getBuildTag () {
	for tag in `git tag --points-at $1`; do
		if [[ $tag == RELEASE-* ]] ;
		then
			echo $tag
			return 0
		fi
	done
	echo $1
	return 0
}



baseHash=`git rev-parse --verify HEAD`
baseVersion=`getBuildTag $baseHash`
cherryPickMessages[0]=
count=0

for gitHash in `git log --reverse --pretty=format:"%H" $baseHash..HEAD`; do
	cherryPickMessage=
	message=$(echo `git log --format=%B -n 1 $gitHash`)
	echo $message | grep -q $grepParentHashRegex
	
	if [ $? -eq 0 ] ;then #This was cherry-picked from Master, we'll use its tag
		cherryPickHash=`echo "$message" | sed -r "s/$sedParentHashRegex/\1/"`
		cherryPickMessage=`getBuildTag $cherryPickHash`
	else #This was not cherry-picked from Master, we'll use its git hash
		cherryPickMessage=$gitHash
	fi
	cherryPickMessages[count]=$cherryPickMessage
	count=`expr $count + 1`
done

#We've constructed up our list of cherry-picked messages, and have our base message. Lets write it out
mkdir -p $(dirname $1)
echo "git.base.hash=$baseHash" > $1
echo "git.base.tag=$baseVersion" >> $1

IFS=',';
echo -n "git.cherrypick.ids=${cherryPickMessages[*]}" >> $1
echo "" >> $1

echo "git.commit.id=`git rev-parse HEAD`" >> $1
echo "git.commit.id.abbrev=`git rev-parse --short=7 HEAD`" >> $1
echo "git.author.time=`git log --pretty=format:"%ad" HEAD~..HEAD`" >> $1
echo "git.author.user.name=`git log --format="%an" HEAD~..HEAD`" >> $1
echo "git.build.time=`date +"%a %b %-d %H:%M:%S %Y %z"`" >> $1
echo "git.build.user.name=`git config user.name`" >> $1

IFS=$IFS_BAK
IFS_BAK=
