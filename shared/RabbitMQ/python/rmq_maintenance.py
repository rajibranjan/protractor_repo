#!/usr/bin/python

import base64
import json
import sets
import sys
import argparse
import os

# Import the requests library which makes HTTP actually possible by humans in Python
from lib.requests import *
import lib.requests as requests

#EXAMPLE BEGINS################################################################################################
#Enter ./rmq_maintenance.py -host http://localhost:15672 -u guest -p guest to access local rabbitMQ management.
#Enter ./rmq_maintenance.py -host https://rmq-moonstone-dev.hpccp.net:15671/ -u hpadmin4jenkins -p riptide@123 to access dev rabbitMQ management.
#EXAMPLE ENDS##################################################################################################


connections_path = '/api/connections'
vhosts_path = '/api/vhosts'
hpadmin_permission_path = '/api/permissions/%s/%s'
host = ''
userName = ''
password = ''
proxies = ''

def GetAllVhosts(host, user, password, proxies):
	# Get the list of all vhosts
	resp = requests.get(host + vhosts_path, auth=(user, password), proxies=proxies)
	print '\nRecieved response code: [' + str(resp.status_code) + '] from: ' + host + vhosts_path
	vhostsJson = resp.json()
	print 'Found [' + str(len(vhostsJson)) + '] virtual hosts'
	return vhostsJson

def DeleteUnusedVhosts(host, user, password, proxies):
	authorization = (user, password)

	# Get the list of active connections.
	resp = requests.get(host + connections_path, auth=authorization, proxies=proxies)
	print 'Recieved response code: [' + str(resp.status_code) + '] from: ' + host + connections_path
	connections = resp.json()

	# Get a set of all the active vhosts.
	uniqueActiveVhosts = set([])
	for connection in connections:
		uniqueActiveVhosts.add(connection['vhost'])
	print 'Found [' + str(len(connections)) + '] active connections connected to a total of [' + str(len(uniqueActiveVhosts)) + '] vhosts.'

	# Get the list of all vhosts
	vhostsJson = GetAllVhosts(host, user, password, proxies)

	# Find which vhosts to actually delete.
	vhostsToDelete = set()
	for vhost in vhostsJson:
		name = vhost['name']
		if(name not in uniqueActiveVhosts):
			vhostsToDelete.add(vhost['name'])

	print '\nDeleting [' + str(len(vhostsToDelete)) + '] vhosts. Vhosts remaining will be: ' + str(len(uniqueActiveVhosts))
	print 'Vhosts that will remain are: '
	for vhost in uniqueActiveVhosts:
		print '\t' + vhost

	i = 0
	totalToDelete = len(vhostsToDelete)

	# Perform the actualy deletions
	for vhost in vhostsToDelete:

		if (vhost == '/'):
			print 'Skipping root vhost'
			i+=1
			continue;

		print '\n' + str(totalToDelete - i) + " deletions remaining out of " + str(totalToDelete)
		print "Deleting vhost: " + vhost

		resp = requests.delete(host + vhosts_path + '/' + vhost, auth=authorization, proxies=proxies)
		print 'Response code: ' + str(resp.status_code)

		i+=1

	print 'Deletion completed!\n'

def DeleteVirtualHost(vhostName, host, user, password, proxies):
	authorization = (user, password)

	# Perform the actual delete
	if (vhostName == '/'):
		print 'Skipping root vhost'

	else :
		print "Deleting vhost: " + vhostName

		resp = requests.delete(host + vhosts_path + '/' + vhostName, auth=authorization, proxies=proxies)
		print 'Response code: ' + str(resp.status_code)

	if(resp.status_code / 100 == 2):
		print 'Successfully deleted vhost ' + vhostName + '!\n'
	
	else:
		print 'Failed to delete vhost ' + vhostName + '!\n'

def HPAdminAccessAllVhosts(host, user, password, proxies):
	vhosts = GetAllVhosts(host, user, password, proxies)
	payload = {'configure':'.*','write':'.*','read':'.*'}
	headers = {'content-type': 'application/json'}
	for vhost in vhosts:
		print 'vhost: ' + vhost['name']
		if(vhost['name'] == '/'):
			vhost['name'] = '%2f'

		path = host + hpadmin_permission_path % (vhost['name'], 'hpadmin')
		resp = requests.put(path, auth=(user, password), data=json.dumps(payload), headers=headers, proxies=proxies)
		print str(resp.status_code) + ' from: ' + path
	print 'Permission insertion completed'

def RMQSummary(host, user, password, proxies):
	print requests.get(host + '/api/overview', auth=(user, password), proxies=proxies).text
	print '\n'

def CloseConnections(host, userName, password, proxies):
	authorization = (userName, password)

	# Get the list of active connections.
	resp = requests.get(host + connections_path, auth=authorization, proxies=proxies)
	connections = resp.json()

	for connection in connections:
		print 'Deleting Connection: ' + connection['name']

		resp = requests.delete(host + connections_path + '/' + connection['name'], auth=authorization, proxies=proxies)
		
		print 'Delete response code: ' + str(resp.status_code)

	print 'All connection deletions complete!\n'

def CloseConnectionsWithLoop(host, userName, password, proxies):
	while(True):
		try:
			print 'Deleting all connections with loop. Press Ctrl + c to terminate this loop.\n'
			CloseConnections(host, userName, password, proxies)
		except KeyboardInterrupt:
			break
		
def PrintOptions():
	print 'Please select an option:'
	print '\t[c]leanUp:\tRemove all virtual hosts with no active connections.'
	print '\t[p]ermission:\tGive HP Admin access to all vhosts.'
	print '\t[s]ummary:\tSummary of the given RabbitMQ server.'
	print '\t[closeCon]:\t\tClose all RabbitMQ connections.'
	print '\t[closeConWithLoop]:\t\tClose all RabbitMQ connections and keep them close with a loop.'
	print '\t[q]uit\tExit.'

def GetProxies(host):

	if('localhost' in host):
		return { 'http':'','https':'' }
	else:
		httpProxy = os.environ.get('http_proxy')
		httpsProxy = os.environ.get('https_proxy')

		if(httpProxy == None):
			httpProxy = os.environ.get('HTTP_PROXY')

		if(httpsProxy == None):
			httpsProxy = os.environ.get('HTTPS_PROXY')

		return { 'http':httpProxy,'https':httpsProxy}

def HandleOption(option):

	if(option == 'c' or option == 'cleanUp'):
		DeleteUnusedVhosts(host, userName, password, proxies)
	elif(option == 'p' or option == 'permission'):
		HPAdminAccessAllVhosts(host, userName, password, proxies)
	elif(option == 's' or option == 'summary'):
		RMQSummary(host, userName, password, proxies)
	elif(option == 'closeCon' or option == 'closeConnection'):
		CloseConnections(host, userName, password, proxies)
	elif(option == 'closeConWithLoop'):
		CloseConnectionsWithLoop(host, userName, password, proxies)
	elif(option == 'q' or option == 'quit'):
		print 'Goodbye'
		sys.exit(0)
	else:
		print '\nInvalid option, please try again. Option can be cleanUp, permission, summary, close all connections, close all connection with loop or quit\n'

def main():
	# Setup command line arguments
	parser = argparse.ArgumentParser(description='RabbitMQ Maintenance Tool. \tHow to use this tool?\nYou can type:\t\"./rmq_maintenance.py -host http://localhost:15672 -u guest -p guest\" to access local rabbitMQ management.\nEnjoy!')
	parser.add_argument('-host', '--host', help='Host of the RabbitMQ Server, such as http://localhost:15672 for local rabbitMQ management', required=True)
	parser.add_argument('-u', '--user', help='Management API Username', required=True)
	parser.add_argument('-p', '--password', help='Management API Password', required=True)
	parser.add_argument('-o', '--option', help='Option can be cleanUp, permission, summary, closeConnection, or quit')
	parser.add_argument('-dvh', '--deleteVirtualHost', help='Specify a virtual host to delete.')

	args = vars(parser.parse_args())

	global host
	host = args['host']

	if host.endswith('/'):
		host = host[:-1]

	global userName
	userName = args['user']

	global password
	password = args['password']

	global proxies
	proxies = GetProxies(args['host'])

	if 'not_authorised' in requests.get(host + '/api/overview', auth=(userName, password), proxies=proxies).text:
		print 'Log in failed. Please check your credentials.\n'
		sys.exit(0)

	if( args['option'] ):
		HandleOption(args['option'])
	elif( args['deleteVirtualHost'] ):
		DeleteVirtualHost(args['deleteVirtualHost'], host, userName, password, proxies)
	else:
		while(True):
			PrintOptions()
			selection = raw_input()

			HandleOption(selection)

main()
