This directory contains common Python scripts for interacting with RabbitMQ. 
Python was chosen since its already installed on our machines and deals
with JSON and HTTP better than BASH. 

The requests folder is for a HTTP library for Python that makes HTTP actually
usable in Python and we'd rather not have to install anything to run these
scripts.