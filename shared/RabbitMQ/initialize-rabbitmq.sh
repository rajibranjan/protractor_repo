#!/bin/bash

# Invoke the setup script that initializes vhost, rabbitUrl, user and password
source ./initialize-rabbitmq-setup.sh

TMPFILE=rabbit.queues.json.`date | sed 's/ //g'`
cp -f rabbit.queues.json $TMPFILE

echo $0 using parameters:
echo "  rabbitUrl=$rabbitUrl"
echo "  TMPFILE=$TMPFILE"
echo "  vhost=$vhost"
sedcmd=s/local/$vhost/g
echo "  sedcmd=$sedcmd"

echo "$0 Set vhost to $vhost in $TMPFILE"
sed -i -e $sedcmd $TMPFILE
sedcmd=s/CHANGEME/$user/g
echo " To change the rabbitmq username in $TMPFILE, use sedcmd=$sedcmd"
sed -i -e $sedcmd $TMPFILE
echo "$0 Upload modified json in $TMPFILE to rabbitmq $rabbitUrl"

echo "Sock options are => $socks_options"

#curl $socks_options -i -u $user:$password -H "content-type:application/json" -XPOST "$rabbitUrl/api/definitions" --data-binary @$TMPFILE
#rm -f $TMPFILE
#exit

outfile=$0.`date | sed 's/ /-/g'`.$RANDOM.out
curl $socks_options -i -u $user:$password -H "content-type:application/json" -w "\\nHTTP_RESPONSE=%{http_code}\\n" -XPOST "$rabbitUrl/api/definitions" --data-binary @$TMPFILE > $outfile
 
response=`cat $outfile | grep HTTP_RESPONSE | sed 's/.*HTTP_RESPONSE=//g'`

rm -f $TMPFILE
echo ""
echo ""

echo "HTTP_RESPONSE=$response"
echo "-----< raw output >-----"
cat $outfile
echo ""
echo "----------"

if [[ $response == 4* || $response == 5* ]] ; then 
  echo $0 FAILURE since HTTP_RESPONSE=$response
  exit 1
fi
if [[ $response != 2* ]] ; then 
  echo $0 UNEXPECTED HTTP_RESPONSE=$response ... assume success
fi
echo $0 SUCCESS
