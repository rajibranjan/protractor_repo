# #################################################################################
# ---------------------------------- PullPrint ---------------------------------- #
# #################################################################################

# ------------------- PullPrint localization resources ---------------------------
PUT $[site.base]/api/v2/i18n json/PullPrint/src/main/assets/i18n/hp-pullprint_en-US.json
PUT $[site.base]/api/v2/i18n json/PullPrint/src/main/assets/i18n/hp-pullprint_fr-CA.json
PUT $[site.base]/api/v2/i18n json/PullPrint/src/main/assets/i18n/hp-pullprint_de-DE.json
PUT $[site.base]/api/v2/i18n json/PullPrint/src/main/assets/i18n/hp-pullprint_es-ES.json
PUT $[site.base]/api/v2/i18n json/PullPrint/src/main/assets/i18n/hp-pullprint_it-IT.json
PUT $[site.base]/api/v2/i18n json/PullPrint/src/main/assets/i18n/hp-pullprint_nl-NL.json
PUT $[site.base]/api/v2/i18n json/PullPrint/src/main/assets/i18n/hp-pullprint_pt-BR.json
PUT $[site.base]/api/v2/i18n json/PullPrint/src/main/assets/i18n/hp-pullprint_zh-CN.json
PUT $[site.base]/api/v2/i18n json/PullPrint/src/main/assets/i18n/hp-pullprint_zh-TW.json


# --------------------------- PullPrint offering Icons ----------------------------
POST $[site.base]/api/v2/offeringIcons json/PullPrint/src/main/assets/offeringIcons/icon_large.json json/PullPrint/src/main/assets/offeringIcons/icon.png
POST $[site.base]/api/v2/offeringIcons json/PullPrint/src/main/assets/offeringIcons/icon_medium.json json/PullPrint/src/main/assets/offeringIcons/icon.png
POST $[site.base]/api/v2/offeringIcons json/PullPrint/src/main/assets/offeringIcons/icon_small.json json/PullPrint/src/main/assets/offeringIcons/icon.png


# --------------------------- PullPrint offering ----------------------------------
PUT $[site.base]/api/v2/offerings json/PullPrint/src/main/assets/offerings/offering.json


# --------------------------- PullPrint product catalog ---------------------------
PUT $[site.base]/api/v2/productCatalogs json/PullPrint/src/main/assets/productCatalogs/productCatalog.json


# ---------------------------- PullPrint products ---------------------------------
PUT $[site.base]/api/v2/products json/PullPrint/src/main/assets/products/product.json









# #################################################################################
# ----------------------------------- ScanSend ---------------------------------- #
# #################################################################################

# ----------------------- ScanSend localization ---------------------------
# This is a temp fr-CA scansend localization file for the test stacks - replace this when there is an actual localized file
PUT $[site.base]/api/v2/i18n json/ScanSend/src/main/assets/i18n/hp-scansend_en-US.json
PUT $[site.base]/api/v2/i18n json/ScanSend/src/main/assets/i18n/hp-scansend_fr-CA.json
PUT $[site.base]/api/v2/i18n json/ScanSend/src/main/assets/i18n/hp-scansend_de-DE.json
PUT $[site.base]/api/v2/i18n json/ScanSend/src/main/assets/i18n/hp-scansend_es-ES.json
PUT $[site.base]/api/v2/i18n json/ScanSend/src/main/assets/i18n/hp-scansend_it-IT.json
PUT $[site.base]/api/v2/i18n json/ScanSend/src/main/assets/i18n/hp-scansend_nl-NL.json
PUT $[site.base]/api/v2/i18n json/ScanSend/src/main/assets/i18n/hp-scansend_pt-BR.json
PUT $[site.base]/api/v2/i18n json/ScanSend/src/main/assets/i18n/hp-scansend_zh-CN.json
PUT $[site.base]/api/v2/i18n json/ScanSend/src/main/assets/i18n/hp-scansend_zh-TW.json


# --------------------------- ScanSend offering Icons ----------------------------
POST $[site.base]/api/v2/offeringIcons json/ScanSend/src/main/assets/offeringIcons/hp-scansend_large.json json/ScanSend/src/main/assets/offeringIcons/icon_large.png
POST $[site.base]/api/v2/offeringIcons json/ScanSend/src/main/assets/offeringIcons/hp-scansend_medium.json json/ScanSend/src/main/assets/offeringIcons/icon_large.png
POST $[site.base]/api/v2/offeringIcons json/ScanSend/src/main/assets/offeringIcons/hp-scansend_small.json json/ScanSend/src/main/assets/offeringIcons/icon_large.png

# --------------------------- ScanSend offering ----------------------------------
PUT $[site.base]/api/v2/offerings json/ScanSend/src/main/assets/offerings/prod/hp-scansend.json


# --------------------------- ScanSend product catalog ---------------------------
PUT $[site.base]/api/v2/productCatalogs json/ScanSend/src/main/assets/productCatalogs/hp-scansend.json


# ---------------------------- ScanSend products ---------------------------------
PUT $[site.base]/api/v2/products json/ScanSend/src/main/assets/products/hp-scansend.json


# ---------------------------- ScanSend cloudVendors -----------------------------
PUT $[scansend.base]/api/v1/cloudVendors json/ScanSend/src/main/assets/cloudVendors/box.json
PUT $[scansend.base]/api/v1/cloudVendors json/ScanSend/src/main/assets/cloudVendors/dropbox.json
PUT $[scansend.base]/api/v1/cloudVendors json/ScanSend/src/main/assets/cloudVendors/googledrive.json
PUT $[scansend.base]/api/v1/cloudVendors json/ScanSend/src/main/assets/cloudVendors/onedrive.json










# #################################################################################
# ---------------------------------- Metering Info ------------------------------ #
# #################################################################################

# ------------------------- Add Metering Info i18n -------------------------------
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/meteringInfo_en-US.json

# --------------------------- Add Metering Info ----------------------------------
PUT $[site.base]/api/v2/meteringInfo json/src/main/assets/meteringInfo/mps.json








# #################################################################################
# ---------------------------------- Downloads ---------------------------------- #
# #################################################################################


# ------------------------ localization resources ---------------------------
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win32_en-US.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win32_fr-CA.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win32_de-DE.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win32_es-ES.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win32_it-IT.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win32_nl-NL.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win32_pt-BR.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win32_zh-CN.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win32_zh-TW.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win64_en-US.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win64_fr-CA.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win64_de-DE.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win64_es-ES.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win64_it-IT.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win64_nl-NL.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win64_pt-BR.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win64_zh-CN.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.win64_zh-TW.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.mac_en-US.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.mac_fr-CA.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.mac_de-DE.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.mac_es-ES.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.mac_it-IT.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.mac_nl-NL.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.mac_pt-BR.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.mac_zh-CN.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.eprintdriver.mac_zh-TW.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.oxp.moreappsinstaller_en-US.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.oxp.moreappsinstaller_fr-CA.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.oxp.moreappsinstaller_de-DE.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.oxp.moreappsinstaller_es-ES.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.oxp.moreappsinstaller_it-IT.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.oxp.moreappsinstaller_nl-NL.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.oxp.moreappsinstaller_pt-BR.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.oxp.moreappsinstaller_zh-CN.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.oxp.moreappsinstaller_zh-TW.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.android_en-US.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.android_fr-CA.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.android_de-DE.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.android_es-ES.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.android_it-IT.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.android_nl-NL.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.android_pt-BR.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.android_zh-CN.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.android_zh-TW.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.ios_en-US.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.ios_fr-CA.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.ios_de-DE.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.ios_es-ES.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.ios_it-IT.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.ios_nl-NL.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.ios_pt-BR.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.ios_zh-CN.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.mobile.ios_zh-TW.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.adupload_en-US.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.adupload_fr-CA.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.adupload_de-DE.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.adupload_es-ES.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.adupload_it-IT.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.adupload_nl-NL.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.adupload_pt-BR.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.adupload_zh-CN.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/com.hp.ondemand.adupload_zh-TW.json


# ---------------------------- downloads resources -------------------------------
PUT $[site.base]/api/v2/downloads json/src/main/assets/downloads/prod/com.hp.eprintdriver.win32.json
PUT $[site.base]/api/v2/downloads json/src/main/assets/downloads/prod/com.hp.eprintdriver.win64.json
PUT $[site.base]/api/v2/downloads json/src/main/assets/downloads/prod/com.hp.eprintdriver.mac.json
PUT $[site.base]/api/v2/downloads json/src/main/assets/downloads/prod/com.hp.ondemand.adupload.json
PUT $[site.base]/api/v2/downloads json/src/main/assets/downloads/com.hp.ondemand.mobile.android.json
PUT $[site.base]/api/v2/downloads json/src/main/assets/downloads/com.hp.ondemand.mobile.ios.json
PUT $[site.base]/api/v2/downloads json/src/main/assets/downloads/prod/com.hp.ondemand.adupload.json










# #################################################################################
# ------------------------------- Titan Platform -------------------------------- #
# #################################################################################

# ----------------------- "platform offering" localization resources -------------
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/hp-platform_en-US.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/hp-platform_fr-CA.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/hp-platform_de-DE.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/hp-platform_es-ES.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/hp-platform_it-IT.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/hp-platform_nl-NL.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/hp-platform_pt-BR.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/hp-platform_zh-CN.json
PUT $[site.base]/api/v2/i18n json/src/main/assets/i18n/hp-platform_zh-TW.json


# ----------------------------- Titan Platform offering  -------------------------------
PUT $[site.base]/api/v2/offerings json/src/main/assets/offerings/hp-platform.json


# ----------------------------- country settings resources --------------------------
PUT $[site.base]/api/v2/country json/src/main/assets/settings/hp-platform_default.json
PUT $[site.base]/api/v2/country json/src/main/assets/settings/hp-platform_CA.json
PUT $[site.base]/api/v2/country json/src/main/assets/settings/hp-platform_US.json
PUT $[site.base]/api/v2/country json/src/main/assets/settings/hp-platform_AT.json
PUT $[site.base]/api/v2/country json/src/main/assets/settings/hp-platform_BE.json
PUT $[site.base]/api/v2/country json/src/main/assets/settings/hp-platform_CH.json
PUT $[site.base]/api/v2/country json/src/main/assets/settings/hp-platform_DE.json
PUT $[site.base]/api/v2/country json/src/main/assets/settings/hp-platform_ES.json
PUT $[site.base]/api/v2/country json/src/main/assets/settings/hp-platform_FR.json
PUT $[site.base]/api/v2/country json/src/main/assets/settings/hp-platform_IE.json
PUT $[site.base]/api/v2/country json/src/main/assets/settings/hp-platform_IT.json
PUT $[site.base]/api/v2/country json/src/main/assets/settings/hp-platform_NL.json
PUT $[site.base]/api/v2/country json/src/main/assets/settings/hp-platform_UK.json
