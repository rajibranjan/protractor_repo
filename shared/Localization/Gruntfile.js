// Generated on 2013-11-14 using generator-angular 0.6.0-rc.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

/*jshint camelcase: false */


module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);

  grunt.initConfig({

    sortJSON: {
      portalUI: {
        src: [
          '../../../../../titan-portal/Clients/Web/src/app/common/i18n/*.json'
        ],
        options: {
          spacing: 2
        }
      },
      framework: {
        src: [
          '../../../../../titan-framework/HPFramework/src/main/resources/localization/**/*.json'
        ],
        options: {
          spacing: 2
        }
      },
      pullPrintUI: {
        src: [
          '../../../../../titan-pullprint/PullPrintUI/src/app/**/i18n/*.json'
        ],
        options: {
          spacing: 2
        }
      },
      pullPrintMFP: {
        src: [
          '../../../../../titan-pullprint/PullPrintService/src/main/oxpmfp/v3/src/resources/i18n/*.json'
        ],
        options: {
          spacing: 2
        }
      },
      pullPrint: {
        src: [
          '../../../../../titan-pullprint/PullPrintService/src/main/resources/i18n/*.json'
        ],
        options: {
          spacing: 2
        }
      },
      proxyRoot: {
        src: [
          "../../../../../titan-proxyroot/ProxyRootService/src/main/resources/i18n/*.properties"
        ],
        options: {
          spacing: 2
        }
      },
      proxyRootMFP: {
        src: [
          "../../../../../titan-proxyroot/ProxyRootService/src/main/oxpmfp/v3/src/resources/i18n/*.json"
        ],
        options: {
          spacing: 2
        }
      },
      scanSendUI: {
        src: [
          '../../../../../titan-scansend/CloudRepoProxy/CloudRepoProxyUI/src/app/**/i18n/*.json'
        ],
        options: {
          spacing: 2
        }
      },
      scanSendFW: {
        src: [
          '../../../../../titan-scansend/CloudRepoProxy/CrpServices/src/main/resources/localization/**/*.json'
        ],
        options: {
          spacing: 2
        }
      }
    },
    deployLocStrings: {
      portalUI: {
        src: [
          'temp_localization/**/portal/common/i18n/*.json'
        ],
        options: {
          dest: '../../../../../titan-portal/Clients/Web/src/app',
          project: 'portal'
        }
      },
      framework: {
        src: [
          'temp_localization/**/framework/**/*.json'
        ],
        options: {
          dest: '../../../../../titan-framework/HPFramework/src/main/resources/localization',
          project: 'framework'
        }
      },
      pullPrintUI: {
        src: [
          'temp_localization/**/pullPrint/home/i18n/*.json'
        ],
        options: {
          dest: '../../../../../titan-pullprint/PullPrintUI/src/app',
          project: 'pullPrint'
        }
      },
      pullPrintMFP: {
        src: [
          'temp_localization/**/pullPrint/oxpmfp/i18n/*.json'
        ],
        options: {
          dest: '../../../../../titan-pullprint/PullPrintService/src/main/oxpmfp/v3/src/resources',
          project: 'pullPrint/oxpmfp'
        }
      },
      pullPrint: {
        src: [
          'temp_localization/**/pullPrint/resources/i18n/*.json'
        ],
        options: {
          dest: '../../../../../titan-pullprint/PullPrintService/src/main',
          project: 'pullPrint'
        }
      },
      proxyRoot: {
        src: [
          'temp_localization/**/proxyroot/resources/i18n/*.properties'
        ],
        options: {
          dest: '../../../../../titan-proxyroot/ProxyRootService/src/main/resources',
          project: 'proxyroot/resources'
        }
      },
      proxyRootMFP: {
        src: [
          'temp_localization/**/proxyroot/oxpmfp/i18n/*.json'
        ],
        options: {
          dest: '../../../../../titan-proxyroot/ProxyRootService/src/main/oxpmfp/v3/src/resources',
          project: 'proxyroot/oxpmfp'
        }
      },
      scanSendUI: {
        src: [
          'temp_localization/**/scanSendUI/i18n/*.json'
        ],
        options: {
          dest: '../../../../../titan-scansend/CloudRepoProxy/CloudRepoProxyUI/src/app',
          project: 'scanSendUI'
        }
      },
      scanSendFW: {
        src: [
          'temp_localization/**/scanSendFW/**/*.json'
        ],
        options: {
          dest: '../../../../../titan-scansend/CloudRepoProxy/CrpServices/src/main/resources/localization',
          project: 'scanSendFW'
        }
      },
      platform: {
        src: [
          'temp_localization/**/platformBootstrap/**/i18n/*.json'
        ],
        options: {
          dest: '../Bootstrap/json/src/main/assets',
          project: 'platformBootstrap'
        }
      },
      scanSend: {
        src: [
          'temp_localization/**/scanSendBootstrap/**/i18n/*.json'
        ],
        options: {
          dest: '../Bootstrap/json/ScanSend/src/main/assets',
          project: 'scanSendBootstrap'
        }
      },
      pullPrint: {
        src: [
          'temp_localization/**/pullPrintBootstrap/**/i18n/*.json'
        ],
        options: {
          dest: '../Bootstrap/json/PullPrint/src/main/assets',
          project: 'pullPrintBootstrap'
        }
      }
    }
  });

  // Load NPM tasks
  grunt.loadNpmTasks('grunt-sort-json');
  grunt.loadNpmTasks('hp-grunt-deploy-localization');


  grunt.registerTask('sort-json', [
    'sortJSON:portalUI',
    'sortJSON:pullPrintUI',
    'sortJSON:framework',
    'sortJSON:scanSendUI',
    'sortJSON:scanSendFW',
    'sortJSON:proxyRootMFP',
    'sortJSON:pullPrintMFP'
  ]);


  grunt.registerTask('localize', [
    'deployLocStrings:portalUI',
    'deployLocStrings:framework',
    'deployLocStrings:platform',
    'deployLocStrings:scanSend',
    'deployLocStrings:pullPrint',
    'deployLocStrings:pullPrintUI',
    'deployLocStrings:pullPrintMFP',
    'deployLocStrings:scanSendUI',
    'deployLocStrings:scanSendFW',
    'deployLocStrings:proxyRoot',
    'deployLocStrings:proxyRootMFP',
    'sortJSON:portalUI',
    'sortJSON:pullPrintMFP',
    'sortJSON:proxyRootMFP',
    'sortJSON:framework',
    'sortJSON:pullPrintUI',
    'sortJSON:scanSendUI',
    'sortJSON:scanSendFW'
  ]);
};
