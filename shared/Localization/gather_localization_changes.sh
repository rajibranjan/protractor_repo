#~/bin/bash

#if in doubt, use this SHA: 23e5b7a332d9a18b95ca84ceb4091266dd4b5ee0

# Add localization resources folders here
platform_bootstrap_i18n_dir="../Bootstrap/json/src/main/assets/i18n"
pullPrint_bootstrap_i18n_dir="../Bootstrap/json/PullPrint/src/main/assets/i18n"
scanSend_bootstrap_i18n_dir="../Bootstrap/json/ScanSend/src/main/assets/i18n"
framework_dir="../../../../../titan-framework/HPFramework/src/main/resources/localization/en-US"
portal_common_i18n_dir="../../../../../titan-portal/Clients/Web/src/app/common/i18n"
pullPrint_i18n_dir="../../../../../titan-pullprint/PullPrintService/src/main/resources/i18n"
pullPrint_home_i18n_dir="../../../../../titan-pullprint/PullPrintUI/src/app/home/i18n"
pullPrint_oxpmfp_i18n_dir="../../../../../titan-pullprint/PullPrintService/src/main/oxpmfp/v3/src/resources/i18n"
proxyroot_home_i18n_dir="../../../../../titan-proxyroot/ProxyRootService/src/main/resources/i18n"
proxyroot_oxpmfp_i18n_dir="../../../../../titan-proxyroot/ProxyRootService/src/main/oxpmfp/v3/src/resources/i18n"
scanSendUI_i18n_dir="../../../../../titan-scansend/CloudRepoProxy/CloudRepoProxyUI/src/app/i18n"
scanSend_i18n_dir="../../../../../titan-scansend/CloudRepoProxy/CrpServices/src/main/resources/localization/en-US"
vm_dir="../../../../../titan-portal/HPServices/src/main/resources/vm/en-US"

# Repo bases
CURRENT_DIR=${PWD}
FRAMEWORK_DIR_BASE="../../../../../titan-framework"
PORTAL_DIR_BASE="../../../../../titan-portal"
PULLPRINT_DIR_BASE="../../../../../titan-pullprint"
PROXYROOT_DIR_BASE="../../../../../titan-proxyroot"
SCANSEND_DIR_BASE="../../../../../titan-scansend"
PARENT_DIR_BASE="../../../../../titan-parent"

LOCALIZATION_FOLDER="localizationDrop_"$(date +"%d%b%Y")

CHANGED_FILES_LIST="changedFiles.txt"
CHANGES_FILE="changes.txt"
EXCLUDES_FILE="string_excludes.txt"
EXCLUDES_ARR=()

#Check args
while getopts r:p:s:f:m:x: option
do
    case "${option}"
    in
        r) PORTAL_SHA=${OPTARG};;
        p) PULLPRINT_SHA=${OPTARG};;
        s) SCANSEND_SHA=${OPTARG};;
        f) FRAMEWORK_SHA=${OPTARG};;
        m) PARENT_SHA=${OPTARG};;
        x) PROXYROOT_SHA=${OPTARG};;
    esac
done

if [ -z "$PORTAL_SHA" ]; then
  echo "ERROR: use -r to set a portal SHA"
  exit 1
fi

if [ -z "$PULLPRINT_SHA" ]; then
  echo "ERROR: use -p to set a pullprint SHA"
  exit 2
fi

if [ -z "$SCANSEND_SHA" ]; then
  echo "ERROR: use -s to set a scansend SHA"
  exit 3
fi

if [ -z "$FRAMEWORK_SHA" ]; then
  echo "ERROR: use -f to set a framework SHA"
  exit 4
fi

if [ -z "$PARENT_SHA" ]; then
  echo "ERROR: use -m to set a parent SHA"
  exit 5
fi

if [ -z "$PROXYROOT_SHA"]; then
  echo "ERROR: use -x to set a proxyroot SHA"
  exit 6
fi

# find the changed localization files
function gatherChanges {

  changedFileList=$temp_dir/$CHANGED_FILES_LIST

  # platform bootstrap resources
  generateChangesFile $platform_bootstrap_i18n_dir $PARENT_SHA $changedFileList
  cd $PARENT_DIR_BASE
  createDiffFiles $changedFileList $PARENT_SHA "platformBootstrap__i18n__"

  # pullprint bootstrap resources
  generateChangesFile $pullPrint_bootstrap_i18n_dir $PARENT_SHA $changedFileList
  cd $PARENT_DIR_BASE
  createDiffFiles $changedFileList $PARENT_SHA "pullPrintBootstrap__i18n__"

  # scansend bootstrap resourc3es
  generateChangesFile $scanSend_bootstrap_i18n_dir $PARENT_SHA $changedFileList
  cd $PARENT_DIR_BASE
  createDiffFiles $changedFileList $PARENT_SHA "scanSendBootstrap__i18n__"

  # framework resources
  generateChangesFile $framework_dir $FRAMEWORK_SHA $changedFileList
  cd $FRAMEWORK_DIR_BASE
  createDiffFiles $changedFileList $FRAMEWORK_SHA "framework__LOCALE__"

  # portal/common resources
  generateChangesFile $portal_common_i18n_dir $PORTAL_SHA $changedFileList
  cd $PORTAL_DIR_BASE
  createDiffFiles $changedFileList $PORTAL_SHA "portal__common__i18n__"

  # pullPrint/home resources
  generateChangesFile $pullPrint_i18n_dir $PULLPRINT_SHA $changedFileList
  cd $PULLPRINT_DIR_BASE
  createDiffFiles $changedFileList $PULLPRINT_SHA "pullPrint__resources__i18n__"

  # pullPrint/home resources
  generateChangesFile $pullPrint_home_i18n_dir $PULLPRINT_SHA $changedFileList
  cd $PULLPRINT_DIR_BASE
  createDiffFiles $changedFileList $PULLPRINT_SHA "pullPrint__home__i18n__"

  # pullPrint/oxpmfp resources
  generateChangesFile $pullPrint_oxpmfp_i18n_dir $PULLPRINT_SHA $changedFileList
  cd $PULLPRINT_DIR_BASE
  createDiffFiles $changedFileList $PULLPRINT_SHA "pullPrint__oxpmfp__i18n__"

  # proxyroot/home resources
  generateChangesFile $proxyroot_home_i18n_dir $PROXYROOT_SHA $changedFileList
  cd $PROXYROOT_DIR_BASE
  createDiffFiles $changedFileList $PROXYROOT_SHA "proxyroot__home__i18n__"

  generateChangesFile $proxyroot_oxpmfp_i18n_dir $PROXYROOT_SHA $changedFileList
  cd $PROXYROOT_DIR_BASE
  createDiffFiles $changedFileList $PROXYROOT_SHA "proxyroot__oxpmfp__i18n__"

  # cloudRepo UI resources
  generateChangesFile $scanSendUI_i18n_dir $SCANSEND_SHA $changedFileList
  cd $SCANSEND_DIR_BASE
  createDiffFiles $changedFileList $SCANSEND_SHA "scanSendUI__i18n__"

  # cloudRepo resources
  generateChangesFile $scanSend_i18n_dir $SCANSEND_SHA $changedFileList
  cd $SCANSEND_DIR_BASE
  createDiffFiles $changedFileList $SCANSEND_SHA "scanSendFW__LOCALE__"

  # vm resources
  generateChangesFile $vm_dir $PORTAL_SHA $changedFileList
  cd $PORTAL_DIR_BASE
  createDiffFiles $changedFileList $PORTAL_SHA "vm__"

}

function generateChangesFile {
  cd $1
  echo "------------------------------------"
  echo "generating change files for ${PWD}"
  if [ -z "$2" ] ; then
    echo "ERROR: did not specify version to diff against"
    return 0
  fi
  if [ -z "$3" ] ; then
    echo "ERROR: did not specify output file"
    return 0
  fi
  if [ -e $changedFileList ] ; then
    rm $changedFileList
  fi

  git diff --name-only $2 ${PWD} > $3
  cd $CURRENT_DIR
}

# parse a file for a list of changed files and generate a diff file for each of them
function createDiffFiles {
  echo "creating diff files"
  if [ ! -e $1 ] ; then
    echo "ERROR: $1 is not a file"
    return 0
  fi
  if [ -z "$2" ] ; then
    echo "ERROR: did not specify version to diff against"
    return 0
  fi
  if [ -z "$3" ] ; then
    echo "ERROR: did not specify localization resource prefix"
    return 0
  fi
  while IFS='' read -r line
  do
    if [[ $line = *en-US* ]]
    then
      fileName=${line##*/}
      targetFile=$drop_dir/$3$fileName
      if [[ $line = *json ]]
      then
        createJSONDiffFile $line $2 $targetFile
      fi
      if [[ $line = *vm ]]
      then
        cp -v -r $line $targetFile
      fi
      if [[ $line = *properties ]]
      then
        git diff -U0 $2  $line > $targetFile
      fi
    fi
  done
  cd $CURRENT_DIR
} < $1

function createJSONDiffFile {
  if [ ! -e $1 ] ; then
    echo "ERROR: $1 is not a file"
    return 0
  fi
  if [ -z "$2" ] ; then
    echo "ERROR: did not specify version to diff against"
    return 1
  fi
  if [ -z "$3" ] ; then
    echo "ERROR: no target specified"
    return 2
  fi

  git diff -U0 $2 $1 | grep '^[+]' | grep -Ev '^(--- a/|\+\+\+ b/)' > $3
  # delete anything prior to the opening quote on each line
  sed -r -i 's/(^[^\"]*)//g' $3
  # delete any commas at the end of the line
  sed -r -i 's/(,$)//g' $3

  # remove any excluded strings
  processExcludes $3
}

function processExcludes {
  if [ ! -e $1 ] ; then
    echo "ERROR: $1 is not a file"
    return 0
  fi
  while IFS='' read -r line
  do
    # check for the exclude
    excludeStr=true
    for i in "${EXCLUDES_ARR[@]}";
    do
      # get the key portion of the json
      match=`expr "$line" : '\(".*":\)'`
      match=${match#*\"}
      match=${match%\"\:}
      if [[ "$match" = "$i" ]] ; then
        excludeStr=false
      fi
    done
    if [[ "$excludeStr" = true ]] ; then
      echo "$line"
    fi
  done < $1 > $1.t
  mv $1{.t,}
}

function populateExcludes {
  if [ ! -e $1 ] ; then
    echo "ERROR: $1 is not a file"
    return 0
  fi
  while IFS='' read -r line
  do
    EXCLUDES_ARR+=($line)
  done

} < $1

#create a temp folder
echo "Creating temp folder"
temp_dir=~/Desktop/Temp
mkdir -p $temp_dir

# create the drop folder
echo "Creating drop folder"
drop_dir=~/Desktop/$LOCALIZATION_FOLDER
mkdir -p $drop_dir

# populate the excludes array
echo "Populating excludes array"
populateExcludes $EXCLUDES_FILE

# generate the drop files
echo "Gathering changes"
gatherChanges

# add an info file with the drop information
echo "PARENT: Past Diff Version: $PARENT_SHA" >> $drop_dir/info.txt
echo "PARENT: Current Diff Version: $(git rev-parse HEAD)" >> $drop_dir/info.txt
cd $FRAMEWORK_DIR_BASE
echo "FRAMEWORK: Past Diff Version: $FRAMEWORK_SHA" >> $drop_dir/info.txt
echo "FRAMEWORK: Current Diff Version: $(git rev-parse HEAD)" >> $drop_dir/info.txt
cd $CURRENT_DIR
cd $PORTAL_DIR_BASE
echo "PORTAL: Past Diff Version: $PORTAL_SHA" >> $drop_dir/info.txt
echo "PORTAL: Current Diff Version: $(git rev-parse HEAD)" >> $drop_dir/info.txt
cd $CURRENT_DIR
cd $PULLPRINT_DIR_BASE
echo "PULLPRINT: Past Diff Version: $PULLPRINT_SHA" >> $drop_dir/info.txt
echo "PULLPRINT: Current Diff Version: $(git rev-parse HEAD)" >> $drop_dir/info.txt
cd $CURRENT_DIR
cd $PROXYROOT_DIR_BASE
echo "PROXYROOT: Past Diff Version: $PROXYROOT_SHA" >> $drop_dir/info.txt
echo "PROXYROOT: Current Diff Version: $(git rev-parse HEAD)" >> $drop_dir/info.txt
cd $CURRENT_DIR
cd $SCANSEND_DIR_BASE
echo "SCANSEND: Past Diff Version: $SCANSEND_SHA" >> $drop_dir/info.txt
echo "SCANSEND: Current Diff Version: $(git rev-parse HEAD)" >> $drop_dir/info.txt
cd $CURRENT_DIR

#rename all .vm files to be .html
cd $drop_dir
for file in *.vm
do
  mv $file "${file/%vm/html}"
done
cd $CURRENT_DIR

# compress the drop folder
cd $drop_dir
zip -r ../$LOCALIZATION_FOLDER *

rm -rf $temp_dir
rm -rf $drop_dir

