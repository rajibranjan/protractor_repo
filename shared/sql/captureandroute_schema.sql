-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: cloudrepoproxy
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CloudVendor`
--

DROP TABLE IF EXISTS `CloudVendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CloudVendor` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `description` longtext,
  `fqName` varchar(255) NOT NULL,
  `iconLargeUrl` longtext,
  `iconMediumUrl` longtext,
  `iconSmallUrl` longtext,
  `name` varchar(255) NOT NULL,
  `vendorVersion` int(11) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `fqName` (`fqName`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EmailJob`
--

DROP TABLE IF EXISTS `EmailJob`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EmailJob` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `completionState` varchar(22) NOT NULL,
  `state` varchar(22) NOT NULL,
  `userId` varchar(36) DEFAULT NULL,
  `body` text CHARACTER SET utf8,
  `from_address` varchar(255) NOT NULL,
  `from_alias` varchar(255) DEFAULT NULL,
  `from_status` varchar(255) DEFAULT NULL,
  `htmlBody` text CHARACTER SET utf8,
  `subject` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Jobs`
--

DROP TABLE IF EXISTS `Jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Jobs` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `completionState` varchar(22) NOT NULL,
  `deviceHostName` varchar(255) DEFAULT NULL,
  `deviceIpAddress` varchar(255) DEFAULT NULL,
  `deviceModelName` varchar(64) DEFAULT NULL,
  `fileNameBase` varchar(255) DEFAULT NULL,
  `fileSize` bigint(20) DEFAULT NULL,
  `fileType` varchar(32) DEFAULT NULL,
  `folderId` varchar(128) DEFAULT NULL,
  `imagesScanned` int(11) NOT NULL,
  `jobDate` varchar(64) NOT NULL,
  `jobTime` varchar(64) NOT NULL,
  `notificationDeliveryStatus` varchar(255) DEFAULT NULL,
  `notificationEmail` varchar(80) DEFAULT NULL,
  `notificationLocale` varchar(8) DEFAULT NULL,
  `notificationUserName` varchar(120) DEFAULT NULL,
  `repositoryProfileId` varchar(36) DEFAULT NULL,
  `state` varchar(22) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RepositoryProfile`
--

DROP TABLE IF EXISTS `RepositoryProfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RepositoryProfile` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `connectorId` varchar(255) DEFAULT NULL,
  `defaultFolder` varchar(255) DEFAULT NULL,
  `defaultFolderId` varchar(255) DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `parentFolderId` varchar(255) DEFAULT NULL,
  `state` varchar(22) NOT NULL,
  `cloudVendor_id` bigint(20) NOT NULL,
  `userProfile_id` bigint(20) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `FK34FF1EDFE6924D36` (`userProfile_id`),
  KEY `FK34FF1EDFC696C516` (`cloudVendor_id`),
  CONSTRAINT `FK34FF1EDFC696C516` FOREIGN KEY (`cloudVendor_id`) REFERENCES `CloudVendor` (`primaryKey`),
  CONSTRAINT `FK34FF1EDFE6924D36` FOREIGN KEY (`userProfile_id`) REFERENCES `UserProfile` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Task`
--

DROP TABLE IF EXISTS `Task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Task` (
  `DTYPE` varchar(31) NOT NULL,
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `name` varchar(40) NOT NULL,
  `referenceId` varchar(36) DEFAULT NULL,
  `status` varchar(15) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserProfile`
--

DROP TABLE IF EXISTS `UserProfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserProfile` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `DefaultRepositoryProfile` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `FK3EFA133EF396A159` (`DefaultRepositoryProfile`),
  CONSTRAINT `FK3EFA133EF396A159` FOREIGN KEY (`DefaultRepositoryProfile`) REFERENCES `RepositoryProfile` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailJob_bcc_destinations`
--

DROP TABLE IF EXISTS `emailJob_bcc_destinations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailJob_bcc_destinations` (
  `emailJob_id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `deliveryStatus` int(11) DEFAULT NULL,
  KEY `FK18659003E6EEE30` (`emailJob_id`),
  CONSTRAINT `FK18659003E6EEE30` FOREIGN KEY (`emailJob_id`) REFERENCES `EmailJob` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailJob_cc_destinations`
--

DROP TABLE IF EXISTS `emailJob_cc_destinations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailJob_cc_destinations` (
  `emailJob_id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `deliveryStatus` int(11) DEFAULT NULL,
  KEY `FK6D54CDE63E6EEE30` (`emailJob_id`),
  CONSTRAINT `FK6D54CDE63E6EEE30` FOREIGN KEY (`emailJob_id`) REFERENCES `EmailJob` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailJob_to_destinations`
--

DROP TABLE IF EXISTS `emailJob_to_destinations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailJob_to_destinations` (
  `emailJob_id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `deliveryStatus` int(11) DEFAULT NULL,
  KEY `FKFB4C4FAB3E6EEE30` (`emailJob_id`),
  CONSTRAINT `FKFB4C4FAB3E6EEE30` FOREIGN KEY (`emailJob_id`) REFERENCES `EmailJob` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_version_contraction`
--

DROP TABLE IF EXISTS `schema_version_contraction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version_contraction` (
  `version_rank` int(11) NOT NULL,
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`),
  KEY `schema_version_contraction_vr_idx` (`version_rank`),
  KEY `schema_version_contraction_ir_idx` (`installed_rank`),
  KEY `schema_version_contraction_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_version_expansion`
--

DROP TABLE IF EXISTS `schema_version_expansion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version_expansion` (
  `version_rank` int(11) NOT NULL,
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`),
  KEY `schema_version_expansion_vr_idx` (`version_rank`),
  KEY `schema_version_expansion_ir_idx` (`installed_rank`),
  KEY `schema_version_expansion_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
