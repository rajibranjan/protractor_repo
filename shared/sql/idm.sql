DROP database if EXISTS ${idm.integration.db};
DROP database if EXISTS ${idm.unitTest.db};

CREATE database ${idm.integration.db};
CREATE database ${idm.unitTest.db};