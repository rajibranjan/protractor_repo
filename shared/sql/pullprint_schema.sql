-- MySQL dump 10.13  Distrib 5.5.42, for Linux (x86_64)
--
-- Host: pxc-titan-dev2.hpccp.net    Database: pullprint_stable
-- ------------------------------------------------------
-- Server version	5.6.20-68.0-56-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ConfigData`
--

DROP TABLE IF EXISTS `ConfigData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConfigData` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `documentExpirationTimeInSeconds` int(11) NOT NULL,
  `entityId` varchar(36) DEFAULT NULL,
  `settingType` varchar(25) DEFAULT NULL,
  `mobilePrintEnabled` tinyint(1) DEFAULT NULL,
  `parentEntity_primaryKey` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `uniqueEntityID` (`entityId`),
  KEY `parentEntityPrimaryKey` (`parentEntity_primaryKey`),
  KEY `FKA85A522CD3D23832` (`parentEntity_primaryKey`),
  CONSTRAINT `parentFK` FOREIGN KEY (`parentEntity_primaryKey`) REFERENCES `ConfigData` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=468093 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_version_contraction`
--

DROP TABLE IF EXISTS `schema_version_contraction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version_contraction` (
  `version_rank` int(11) NOT NULL,
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`),
  KEY `schema_version_contraction_vr_idx` (`version_rank`),
  KEY `schema_version_contraction_ir_idx` (`installed_rank`),
  KEY `schema_version_contraction_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_version_expansion`
--

DROP TABLE IF EXISTS `schema_version_expansion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version_expansion` (
  `version_rank` int(11) NOT NULL,
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`),
  KEY `schema_version_expansion_vr_idx` (`version_rank`),
  KEY `schema_version_expansion_ir_idx` (`installed_rank`),
  KEY `schema_version_expansion_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_version_git`
--

DROP TABLE IF EXISTS `schema_version_git`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version_git` (
  `version_rank` int(11) DEFAULT NULL,
  `git_hash` varchar(7) DEFAULT NULL,
  `migration_type` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-24 21:27:38
