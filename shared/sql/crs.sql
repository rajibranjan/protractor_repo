DROP DATABASE IF EXISTS ${crs.jackrabbit.db.name};
CREATE DATABASE ${crs.jackrabbit.db.name};
GRANT ALL PRIVILEGES ON ${crs.jackrabbit.db.name}.* TO 'jackrabbit'@'%' IDENTIFIED BY 'jackrabbit'
