--
-- Database: `unittest`
--

CREATE DATABASE IF NOT EXISTS `${portal.unitTest.db}`;

DROP TABLE IF EXISTS `${portal.unitTest.db}`.`schema_version_expansion`;
DROP TABLE IF EXISTS `${portal.unitTest.db}`.`schema_version_contraction`;
DROP TABLE IF EXISTS `${portal.unitTest.db}`.`schema_version_git`;
 
-- --------------------------------------------------------
 
--
-- Table structure for table `schema_version_contraction`
--

CREATE TABLE `${portal.unitTest.db}`.`schema_version_contraction` (
  `version_rank` int(11) NOT NULL,
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`),
  KEY `schema_version_contraction_vr_idx` (`version_rank`),
  KEY `schema_version_contraction_ir_idx` (`installed_rank`),
  KEY `schema_version_contraction_s_idx` (`success`)
);

--
-- Table structure for table `schema_version_expansion`
--

CREATE TABLE `${portal.unitTest.db}`.`schema_version_expansion` (
  `version_rank` int(11) NOT NULL,
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`),
  KEY `schema_version_expansion_vr_idx` (`version_rank`),
  KEY `schema_version_expansion_ir_idx` (`installed_rank`),
  KEY `schema_version_expansion_s_idx` (`success`)
);

--
-- Table structure for table `schema_version_git`
--

CREATE TABLE `${portal.unitTest.db}`.`schema_version_git` (
  `version_rank` int(11) DEFAULT NULL,
  `git_hash` varchar(7) DEFAULT NULL,
  `migration_type` varchar(32) DEFAULT NULL
);
