--
-- Database: `riptide`
--

CREATE DATABASE IF NOT EXISTS `${portal.integration.db}`;

DROP TABLE IF EXISTS `${portal.integration.db}`.`acl_entry`;
DROP TABLE IF EXISTS `${portal.integration.db}`.`acl_object_identity`;
DROP TABLE IF EXISTS `${portal.integration.db}`.`acl_class`;
DROP TABLE IF EXISTS `${portal.integration.db}`.`acl_sid`;
 
-- --------------------------------------------------------
 
--
-- Table structure for table `acl_sid`
--
 
CREATE TABLE IF NOT EXISTS `${portal.integration.db}`.`acl_sid` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `principal` tinyint(1) NOT NULL,
  `sid` varchar(36) NOT NULL,
  UNIQUE INDEX `acl_sid_idx_1` (`sid`,`principal`)
);
 
-- --------------------------------------------------------
 
--
-- Table structure for table `acl_class`
--
 
CREATE TABLE IF NOT EXISTS `${portal.integration.db}`.`acl_class` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `class` varchar(255) NOT NULL,
  UNIQUE INDEX `acl_class_idx_1` (`class`)
);

-- --------------------------------------------------------
 
--
-- Table structure for table `acl_object_identity`
--
 
CREATE TABLE IF NOT EXISTS `${portal.integration.db}`.`acl_object_identity` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `object_id_class` bigint(20) UNSIGNED NOT NULL,
  `object_id_identity` bigint(20) NOT NULL,
  `parent_object` bigint(20) UNSIGNED DEFAULT NULL,
  `owner_sid` bigint(20) UNSIGNED DEFAULT NULL,
  `entries_inheriting` tinyint(1) NOT NULL,
  UNIQUE INDEX `acl_object_identity_idx_3` (`object_id_class`,`object_id_identity`),
  FOREIGN KEY (`parent_object`) REFERENCES `${portal.integration.db}`.`acl_object_identity` (`id`),
  FOREIGN KEY (`object_id_class`) REFERENCES `${portal.integration.db}`.`acl_class` (`id`),
  FOREIGN KEY (`owner_sid`) REFERENCES `${portal.integration.db}`.`acl_sid` (`id`)
);
 
-- --------------------------------------------------------
 
--
-- Table structure for table `acl_entry`
--
 
CREATE TABLE IF NOT EXISTS `${portal.integration.db}`.`acl_entry` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `acl_object_identity` bigint(20) UNSIGNED NOT NULL,
  `ace_order` int(11) UNSIGNED NOT NULL,
  `sid` bigint(20) UNSIGNED NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL,
  UNIQUE INDEX `acl_entry_idx_1` (`acl_object_identity`,`ace_order`),
  FOREIGN KEY (`acl_object_identity`) REFERENCES `${portal.integration.db}`.`acl_object_identity` (`id`),
  FOREIGN KEY (`sid`) REFERENCES `${portal.integration.db}`.`acl_sid` (`id`)
);