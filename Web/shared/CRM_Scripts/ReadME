Instructions to execute the CRM script:

The CRM script is meant to be executed within a Linux shell (terminal window).
One executes the CRM script by entering the python command followed by the CRM
script name which is CRM_US17296.py (the US17296 reflects the user story for this
work).  An example execution within the shell is like this (with the the appropriate
input parameters is brackets):

python CRM_US17296.py [titan user email] [base URI] [CRM FTP site user name] [environment (prod or test)]

Example for the prod environment (enter this at Linux shell):
python CRM_US17296.py 'hpadmin@eggnogcookie.com' 'https://www.hpondemand.com/' titan123 prod

Once the CRM script executes, there are 2 more parameters asked for by the script.  These
are associated with passwords, so the user is prompted for these values.  The script
will prompt for:

titan user password (password for the titan user email used)
CRM FTP site password (password for the CRM FTP site user name)

When inputting the values for these 2 prompts, the input is hidden on the console.

When the script executes, errors can happen.  If errors are associated with invalid
parameters or REST endpoint failure codes, the script will stop processing and print
out the error details so the user can fix the error and try again.  If errors are
associated with iterating through the records, the script will print out the record
in error along with what the error is and continue on to the next record to process,
effectively skipping the record in error.  If there are errors during encrypting or
ftp'ing the generated files, the script will stop and print out the error.  The
encrypting and ftp'ing of the files can be done manually, so there is no real impact
to the process or harm to the files.

