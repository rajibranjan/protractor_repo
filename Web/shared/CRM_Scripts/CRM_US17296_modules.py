nonTrackableEmails = ['hpfoghorn.com', 'eggnogcookie.com', 'mailinator.com']

def writeTitanCompanyFile(dataToWrite, fileName, dateRun):
	import csv
	import traceback
	import datetime
	import re
	from pprint import pprint

	global nonTrackableEmails
	with open(fileName, 'ab') as f:

		writer = csv.writer(f, dialect='excel-tab', quotechar='', quoting=csv.QUOTE_NONE)
		numCompaniesToProcess = len(dataToWrite['data'])
		numCompaniesProcessed = 0

		for item in dataToWrite['data']:
			try:
				dataItemList = []

				# check if a primary contact is associated with this account
				if 'adminContact' in item:
					if 'email' not in item['adminContact']:
						print 'Account ' + str(item['name'].encode('utf-8')) + ' does NOT have a primary contact (admin) associated to it: NOT processing account'
						continue # since no primary contact for account

				adminContactData = item['adminContact']
				adminContactLinks = adminContactData['links']
				#pprint(adminContactLinks)
				resellerData = item['reseller']
				resellerLinks = resellerData['links']
				linksData = item['links']
				isInternal = 'FALSE'
				nonTrackable = False

				# exclude any accounts with the following primary contact email suffixes
				emailToCk = adminContactData['email'].lower()
				for nonTrackableEmail in nonTrackableEmails:
					if nonTrackableEmail.lower() in emailToCk:
						print 'Account ' + str(item['name'].encode('utf-8')) + ' is a test/dummy account: NOT processing account'
						nonTrackable = True
						break
					else:
						if '@hp.com' in emailToCk:
							isInternal = 'HP_INTERNAL'
				if nonTrackable:
					continue

				dataItemList.append('CO') # record type
				companyId = 'Company ID here'
				for i, j in enumerate(linksData):
					#print j
					if j['rel'] == 'self':
						selfHref = j['href']
						index = selfHref.find('accounts')
						if index != -1:
							getIndex = index + 9
							companyId = selfHref[getIndex:]
				dataItemList.append(companyId) # company id
				dataItemList.append(item['status']) # status
				if 'createdDate' not in item:
					dataItemList.append(' ') # created date
				else:
					# DB dates are in the format 2014-08-30T19:11:49.000+000 - convert to better format
					tDate = datetime.datetime.strptime(item['createdDate'], '%Y-%m-%dT%H:%M:%S.%f+0000')
					createdDate = tDate.strftime('%Y-%m-%d %H:%M:%S')
					dataItemList.append(createdDate) # created date
				activatedDate = ' '
				if 'activatedDate' in item:
					try:
						tDate = datetime.datetime.strptime(item['activatedDate'], '%Y-%m-%dT%H:%M:%S.%f+0000')
						activatedDate = tDate.strftime('%Y-%m-%d %H:%M:%S')
					except:
						activatedDate = ' '
				dataItemList.append(activatedDate) # activated date
				dataItemList.append(' ') # suspended date
				dataItemList.append(isInternal) # is internal

				resellerId = 'Reseller ID here'
				for i, j in enumerate(resellerLinks):
					#print j
					if j['rel'] == 'self':
						selfHref = j['href']
						index = selfHref.find('resellers')
						if index != -1:
							getIndex = index + 10
							resellerId = selfHref[getIndex:]

				dataItemList.append(resellerId) # reseller id
				dataItemList.append(str(resellerData['name'].encode('utf-8'))) # reseller name
				dataItemList.append(str(item['name'].encode('utf-8'))) # company name

				primaryContactId = 'Primary contact ID here'
				for i, j in enumerate(adminContactLinks):
					#print j
					if j['rel'] == 'self':
						selfHref = j['href']
						index = selfHref.find('users')
						if index != -1:
							getIndex = index + 6
							primaryContactId = selfHref[getIndex:]

				#print primaryContactId

				dataItemList.append(primaryContactId) # primary contact
				if 'createdDate' not in item or item['phoneNumber'] == None:
					dataItemList.append(' ') # company telephone number
				else:
					phoneNbrStripped = re.sub('[^0-9]','', item['phoneNumber'])
					dataItemList.append(phoneNbrStripped) # company telephone number
				if 'phoneExtension' not in item:
					dataItemList.append(' ') # company telephone extension
				else:
					dataItemList.append(item['phoneExtension']) # company telephone extension
				if 'addressLine' not in item or item['addressLine'] == None:
					dataItemList.append(' ') # address line 1
				else:
					dataItemList.append(str(item['addressLine'].encode('utf-8'))) # address line 1
				if 'addressLine2' not in item or item['addressLine2'] == None:
					dataItemList.append(' ') # address line 2
				else:
					dataItemList.append(str(item['addressLine2'].encode('utf-8'))) # address line 2
				if 'city' not in item or item['city'] == None:
					dataItemList.append(' ') # city
				else:
					dataItemList.append(str(item['city'].encode('utf-8'))) # city
				if 'state' not in item or item['state'] == None:
					dataItemList.append(' ') # state/province
				else:
					dataItemList.append(item['state']) # state/province
				if 'country' not in item or item['country'] == None:
					dataItemList.append(' ') # country
				else:
					dataItemList.append(item['country']) # country
				if 'zipcode' not in item or item['zipcode'] == None:
					dataItemList.append(' ') # zip code
				else:
					dataItemList.append(item['zipcode']) # zip code
				tDate = datetime.datetime.strptime(dateRun, '%Y%m%d%H%M')
				asOfDate = tDate.strftime('%Y-%m-%d %H:%M')
				dataItemList.append(asOfDate) # as of date
				dataItemList.append('0') # number of devices attached
				writer.writerow(dataItemList)
				numCompaniesProcessed = numCompaniesProcessed + 1
			except:
				print '******'
				print 'Error occurred - skipping this company data:'
				print '******'
				pprint(item)
				traceback.print_exc()
				print '******'
				continue
			#else:
				#print 'Successfully wrote company ' + item['name'] + ' row to file'

		print '******'
		print '***Wrote ' + str(numCompaniesProcessed) + ' companies out of ' + str(numCompaniesToProcess) + ' companies to process***'
		print '******'


def writeTitanUsersFile(dataToWrite, fileName):
	import csv
	import traceback
	import re
	from pprint import pprint

	global nonTrackableEmails
	with open(fileName, 'ab') as f:
		writer = csv.writer(f, dialect='excel-tab', quotechar='', quoting=csv.QUOTE_NONE)
		numUsersToProcess = len(dataToWrite['data'])
		numUsersProcessed = 0

		for item in dataToWrite['data']:
			try:
				# determine if this a user to process
				roleList = item['roles']
				role = 'UNDEFINED'
				processUser = True
				normalUser = False
				companyAdminUser = False
				for i, j in enumerate(roleList):
					#print j
					if j == 'ROLE_HPADMIN' or j == 'ROLE_RESELLER':
						processUser = False
						break
					elif j == 'ROLE_COMPANY_ADMIN':
						companyAdminUser = True
					elif j == 'ROLE_USER':
						normalUser = True
				if processUser:
					if companyAdminUser:
						role = 'COMPANY_ADMIN'
					elif normalUser:
						role = 'USER'
				else:
					print 'User ' + item['firstName'] + ' ' + item['lastName'] + ' is HP admin or reseller: NOT processing user'
					continue

				#print 'Resulting role is ' + role

				# determine if this a test/dummy user to process
				nonTrackable = False
				emailToCk = item['email'].lower()
				for nonTrackableEmail in nonTrackableEmails:
					if nonTrackableEmail.lower() in emailToCk:
						print 'User ' + item['firstName'] + ' ' + item['lastName'] + ' is a test/dummy user: NOT processing user'
						nonTrackable = True
						break
				if nonTrackable:
					continue

				dataItemList = []
				dataItemList.append('US') # record type

				#pprint(item)

				userLinks = item['links']
				userId = 'User ID here'
				for i, j in enumerate(userLinks):
					#print j
					if j['rel'] == 'self':
						selfHref = j['href']
						index = selfHref.find('users')
						if index != -1:
							getIndex = index + 6
							userId = selfHref[getIndex:]

				#print userId

				dataItemList.append(userId) # user id
				dataItemList.append(item['status']) # status
				if 'accountId' not in item:
					dataItemList.append(' ') # company id
				else:
					dataItemList.append(item['accountId']) # company id
				dataItemList.append(role) # role
				dataItemList.append(str(item['firstName'].encode('utf-8'))) # first name
				dataItemList.append(str(item['lastName'].encode('utf-8'))) # last name
				dataItemList.append(item['email']) # email
				if 'phoneNumber' not in item or item['phoneNumber'] == None:
					dataItemList.append(' ') # user telephone number
				else:
					phoneNbrStripped = re.sub('[^0-9]','', item['phoneNumber'])
					dataItemList.append(phoneNbrStripped) # user telephone number
				if 'phoneExtension' not in item:
					dataItemList.append(' ') # user telephone extension
				else:
					dataItemList.append(item['phoneExtension']) # user telephone extension
				contryCode = ''
				if 'locale' not in item:
					dataItemList.append(' ') # locale & language
				else:
					localeVal = item['locale']
					countryCode = localeVal[-2:]
					dataItemList.append(localeVal) # locale & language
				if item['optedIntoEmail']:
					dataItemList.append('1') # ok to email
				else:
					dataItemList.append('0') # ok to email

				if item['optedIntoEmailTracking']:
					dataItemList.append('1') # ok to track email
				else:
					dataItemList.append('0') # ok to track email

				dataItemList.append(' ') # date last login

				writer.writerow(dataItemList)
				numUsersProcessed = numUsersProcessed + 1
			except:
				print '******'
				print 'Error occurred - skipping this user data:'
				print '******'
				pprint(item)
				traceback.print_exc()
				print '******'
				continue
			#else:
				#print 'Successfully wrote user ' + item['firstName'] + ' ' + item['lastName'] + ' row to file'

		print '******'
		print '***Wrote ' + str(numUsersProcessed) + ' users out of ' + str(numUsersToProcess) + ' users to process***'
		print '******'

def writeTitanAppFile(dataToWrite, appFileName, i18nDict):
	import csv
	import traceback
	import urllib
	import requests
	import json
	from pprint import pprint

	offeringsToIgnore = ['hp-chicken', 'hp-platform']
	#pprint(dataToWrite)
	with open(appFileName, 'ab') as f:
		writer = csv.writer(f, dialect='excel-tab', quotechar='', quoting=csv.QUOTE_NONE)
		numOfferingsToProcess = len(dataToWrite['data'])
		numOfferingsProcessed = 0

		for item in dataToWrite['data']:
			try:
				dataItemList = []

				if item['fqName'] in offeringsToIgnore:
					print 'Offering ' + item['fqName'] + ' not tracked - ignoring'
					continue

				dataItemList.append('AP') # record type

				#pprint(i18nDict)
				found = 0
				for key, value in i18nDict.items():
					if key == item['fqName']:
						dataItemList.append(value['title']) # app short name
						dataItemList.append(value['description']) # app long name
						found = 1
						break
					else:
						continue

				if not found:
					dataItemList.append(' ') # app short name
					dataItemList.append(' ') # app long name

				#**************************
				# NEED to figure out this value - currently in product - need to get - set to FREE
				#**************************
				dataItemList.append('FREE') # app status

				writer.writerow(dataItemList)
				numOfferingsProcessed = numOfferingsProcessed + 1

			except:
				print '******'
				print 'Error occurred - skipping this app data:'
				print '******'
				pprint(item)
				traceback.print_exc()
				print '******'
				continue
			#else:
				#print 'Successfully wrote app data for offering ' + item['fqName'] + ' to file'

		print '******'
		print '***Wrote ' + str(numOfferingsProcessed) + ' apps out of ' + str(numOfferingsToProcess) + ' apps to process***'
		print '******'

def writeTitanUserAppAndUsageFiles(dataToWrite, userAppFileName, userAppUsageFileName, baseUrl, sessionObj, i18nDict, headers):
	import csv
	import traceback
	import urllib
	import requests
	import json
	from pprint import pprint

	offeringsToIgnore = ['hp-chicken']

	# open the user app usage file and setup the writer
	userAppUsageFile = open(userAppUsageFileName, 'ab')
	userAppUsageWriter = csv.writer(userAppUsageFile, dialect='excel-tab', quotechar='', quoting=csv.QUOTE_NONE)

	with open(userAppFileName, 'ab') as f:
		writer = csv.writer(f, dialect='excel-tab', quotechar='', quoting=csv.QUOTE_NONE)
		numUserAppsToProcess = len(dataToWrite['data'])
		numUserAppsProcessed = 0

		for item in dataToWrite['data']:
			try:
				dataItemList = []
				userAppUsageItemList = []
				userData = item['user']
				licenseData = item['license']
				licenseLinks = licenseData['links']

				offeringData = item['offering']

				if offeringData['fqName'] in offeringsToIgnore:
					print 'Offering ' + offeringData['fqName'] + ' not tracked - ignoring'
					continue

				userLinks = userData['links']
				#pprint(userLinks)
				userId = 'User ID here'
				for i, j in enumerate(userLinks):
					#print j
					if j['rel'] == 'self':
						selfHref = j['href']
						index = selfHref.find('users')
						if index != -1:
							getIndex = index + 6
							userId = selfHref[getIndex:]

				dataItemList.append('UA') # record type
				userAppUsageItemList.append('AU')
				dataItemList.append(userId) # user id
				userAppUsageItemList.append(userId)

				#pprint(i18nDict)
				found = 0
				for key, value in i18nDict.items():
					if key == offeringData['fqName']:
						dataItemList.append(value['title']) # app short name
						userAppUsageItemList.append(value['title'])
						found = 1
						break
					else:
						continue

				if not found:
					dataItemList.append(' ') # app short name
					userAppUsageItemList.append(' ')

				if 'status' not in licenseData:
					dataItemList.append(' ') # entitlement state
				else:
					dataItemList.append(licenseData['status']) # entitlement state

				# get setup state from the preferences table
				preferencesUrl = baseUrl + '/api/v2/preferences'

				values = {'qs' : 'userId=' + userId + '&key=setupWizard&type=welcome&source=' + offeringData['fqName'] + '~public'}
				data = urllib.urlencode(values)

				r = sessionObj.get(preferencesUrl, params=data, headers=headers)

				if r.status_code != 200:
					print 'Cannot proceed on writing user apps file for user ' + userId + ' due to error status code from preferences query of ' + str(r.status_code)
					return
				else:
					jsonRes = r.json()
					#pprint(jsonRes)
					preferenceData = jsonRes['data']
					#print 'preference data length = ' + str(len(preferenceData))
					#pprint(preferenceData)
					setupState = 'Not Completed'
					if len(preferenceData) == 0:
						print 'NO preference data for user ' + userId + ' for offering ' + offeringData['fqName'] + ': setting setup state to not completed'
						#continue
					else:
						prefList = preferenceData[0]
						valueList = prefList['value']
						setupStatus = json.loads(valueList)
						#print setupStatus

						if 'complete' in setupStatus:
							completeVal = setupStatus['complete']
							if completeVal:
								setupState = 'Completed'

					dataItemList.append(setupState) # setup state

					writer.writerow(dataItemList)
					numUserAppsProcessed = numUserAppsProcessed + 1

					# write the user app usage data
					userAppUsageItemList.append(' ') # last used date
					userAppUsageItemList.append('0') # delta browser logins nbr
					userAppUsageItemList.append('0') # delta device logins nbr
					userAppUsageItemList.append('0') # delta nbr of jobs sent
					userAppUsageItemList.append('0') # delta nbr of jobs retrieved
					userAppUsageItemList.append('0') # delta nbr of jobs auto-deleted
					userAppUsageItemList.append('0') # delta nbr of pages sent
					userAppUsageItemList.append('0') # delta nbr of pages retrieved
					userAppUsageItemList.append('0') # delta nbr of pages auto-deleted
					userAppUsageWriter.writerow(userAppUsageItemList)

			except:
				print '******'
				print 'Error occurred - skipping this user app data:'
				print '******'
				pprint(item)
				traceback.print_exc()
				print '******'
				continue
			#else:
				#print 'Successfully wrote user app data ' + offeringData['fqName'] + ' for ' + userData['firstName'] + ' ' + userData['lastName'] + ' row to file'

		print '******'
		print '***Wrote ' + str(numUserAppsProcessed) + ' user apps out of ' + str(numUserAppsToProcess) + ' user apps to process***'
		print '******'
		userAppUsageFile.close()

def ftpFile(fileName, ftpUser, ftpPw):
	import subprocess

	try:
		cmd = 'curl -u ' + ftpUser + ':' + ftpPw + ' -T ' + fileName + ' -i -k --ftp-ssl --tlsv1 --ftp-pasv ftp://ftp.usa.hp.com/Inbound/titan/'
		args = cmd.split()
		process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdout, stderr = process.communicate()
		#print('Process return code = ' + str(process.returncode))
		#print('stdout = ' + stdout)
		#print('stderr = ' + stderr)

	except Exception as e:
		print( 'Ftp of file failed for the following reason (MAY have to manually ftp them to server) -> ', e )

def removeDuplicates(fileName):
	try:
		f = open(fileName, "rb")
		linesOfFile = f.readlines()
		f.close()
		#with open(fileName) as f:
   			#sorted_file = sorted(f)

		#save to a file
		#with open('./user_sorted_output', 'wb') as f:
   			#f.writelines(sorted_file)

   		#uniqueRes = removeDups(linesOfFile) #sorted_file)

		# order preserving
		#idFun = None
		#if idfun is None:
		def idfun(x):
			return x
		seen = {}
		result = []
		for item in linesOfFile:
			marker = idfun(item)
			# in old Python versions:
			# if seen.has_key(marker)
			# but in new ones:
			if marker in seen: continue
			seen[marker] = 1
			result.append(item)

		with open(fileName, 'wb') as f1:
			f1.writelines(result)

	except Exception as e:
		print( 'Remove duplicates of file failed for the following reason -> ', e )
