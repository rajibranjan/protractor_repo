# The ESAPI validator does many security checks on input, such as canonicalization
# and whitelist validation. Note that all of these validation rules are applied *after*
# canonicalization. Double-encoded characters (even with different encodings involved,
# are never allowed.
#
# To use:
#
# First set up a pattern below. You can choose any name you want, prefixed by the word
# "Validation." For example:
#   Validation.Email=^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\\.[a-zA-Z]{2,4}$
# 
# Then you can validate in your code against the pattern like this:
#     ESAPI.validator().isValidInput("User Email", input, "Email", maxLength, allowNull);
# Where maxLength and allowNull are set for you needs, respectively.
#
# But note, when you use boolean variants of validation functions, you lose critical 
# canonicalization. It is preferable to use the "get" methods (which throw exceptions) and 
# and use the returned user input which is in canonical form. Consider the following:
#  
# try {
#    someObject.setEmail(ESAPI.validator().getValidInput("User Email", input, "Email", maxLength, allowNull));
#
Validator.SafeString=^[.\\p{Alnum}\\p{Space}]{0,1024}$
Validator.Email=^[A-Za-z0-9._%'-]+@[A-Za-z0-9.-]+\\.[a-zA-Z]{2,4}$
Validator.IPAddress=^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$
Validator.URL=^(ht|f)tp(s?)\\:\\/\\/[0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*(:(0-9)*)*(\\/?)([a-zA-Z0-9\\-\\.\\?\\,\\:\\'\\/\\\\\\+=&amp;%\\$#_]*)?$
Validator.CreditCard=^(\\d{4}[- ]?){3}\\d{4}$
Validator.SSN=^(?!000)([0-6]\\d{2}|7([0-6]\\d|7[012]))([ -]?)(?!00)\\d\\d\\3(?!0000)\\d{4}$

Validator.Alpha=^[\\p{Alnum}]*$
Validator.AlphaNum=^[\\p{Alnum}]*$
Validator.UUID=^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$
Validator.Base64UrlSafe=^[\\p{Alnum}-_]*$
Validator.CookieValue=^[a-zA-Z0-9()\\-=\\*\\.\\?;,+\\/:&_" ]*$
Validator.HeaderValue=^[a-zA-Z0-9()\\-=\\*\\.\\?;,+\\/:&_"% ]*$
Validator.Password=^[a-zA-Z0-9.\\-\\/+=@_!#~$%^&\\*():;,'"<>\\?\\[\\]\\{\\} ]*$

###
# CCP SSO validators
###
Validator.CCP-SSO-AnyString=^.*$
Validator.CCP-SSO-JSON=^([\\s\\S]*)$
Validator.CCP-SSO-Name=[a-zA-Z0-9_-]+
Validator.CCP-SSO-TokenName=(?!(?i)\\b(cookie|set-cookie|comment|discard|expires|max-age|path|secure|version)\\b)([a-zA-Z0-9_-]+)
Validator.CCP-SSO-CookieDomain=^(?=.{1,255}$).?[0-9A-Za-z](?:(?:[0-9A-Za-z]|\\b-){0,61}[0-9A-Za-z])?(?:\\.[0-9A-Za-z](?:(?:[0-9A-Za-z]|\\b-){0,61}[0-9A-Za-z])?)*\\.?$
Validator.CCP-SSO-CookiePath=/[a-zA-Z0-9-/]*
Validator.CCP-SSO-Boolean=(true|false|1|0)
Validator.CCP-SSO-PositiveInteger=^[1-9]\\d*$
Validator.CCP-SSO-Recipient=^https\\:\\/\\/[0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*(:(0-9)*)*(\\/?)([a-zA-Z0-9\\-\\.\\?\\,\\:\\'\\/\\\\\\+=&amp;%\\$#_]*)?$
Validator.CCP-SSO-ContentTypeJSON=^application/json(;.*)?$
Validator.CCP-SSO-ContentTypeForm=^application/x-www-form-urlencoded(;.*)?$
Validator.CCP-SSO-IdmBailiwick=^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$
