-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: pxc-titan-prod.hpccp.net    Database: riptide
-- ------------------------------------------------------
-- Server version	5.6.26-74.0-56-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AMQPGlobalConfiguration`
--

DROP TABLE IF EXISTS `AMQPGlobalConfiguration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AMQPGlobalConfiguration` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `gitHash` varchar(40) NOT NULL,
  `consumersEnabled` bit(1) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `gitHash` (`gitHash`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AMQPMessageGraveyard`
--

DROP TABLE IF EXISTS `AMQPMessageGraveyard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AMQPMessageGraveyard` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `payloadId` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `canonicalName` varchar(255) NOT NULL,
  `jsonPayload` text NOT NULL,
  `queueName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9656 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Account`
--

DROP TABLE IF EXISTS `Account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Account` (
  `DTYPE` varchar(31) NOT NULL,
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `activatedDate` datetime DEFAULT NULL,
  `addressLine` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `addressLine2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `phoneExtension` varchar(7) DEFAULT NULL,
  `phoneNumber` varchar(25) NOT NULL,
  `state` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `billId` varchar(40) DEFAULT NULL,
  `crmId` varchar(40) DEFAULT NULL,
  `crsId` varchar(80) DEFAULT NULL,
  `licensingPolicy` varchar(15) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `poNumber` varchar(80) DEFAULT NULL,
  `registrationId` varchar(80) DEFAULT NULL,
  `serviceRegistrationState` int(11) NOT NULL,
  `status` varchar(15) NOT NULL,
  `locationNumber` varchar(80) DEFAULT NULL,
  `paymentMethod` varchar(16) DEFAULT NULL,
  `customerId` varchar(80) DEFAULT NULL,
  `adminContact_primaryKey` bigint(20) DEFAULT NULL,
  `reseller_primarykey` bigint(20) DEFAULT NULL,
  `referenceId` varchar(100) DEFAULT NULL,
  `isResellerAccount` bit(1) NOT NULL DEFAULT b'0',
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `uniqueReferenceId` (`referenceId`),
  KEY `FK1D0C220DAC76F9F3` (`adminContact_primaryKey`),
  KEY `FK1D0C220DCFF516F9` (`reseller_primarykey`),
  KEY `dTypeIndex` (`DTYPE`),
  CONSTRAINT `__________FK1D0C220DAC76F9F3` FOREIGN KEY (`adminContact_primaryKey`) REFERENCES `User` (`primaryKey`),
  CONSTRAINT `____________FK1D0C220DCFF516F9` FOREIGN KEY (`reseller_primarykey`) REFERENCES `Account` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=385380 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AccountGroup`
--

DROP TABLE IF EXISTS `AccountGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AccountGroup` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_primaryKey` bigint(20) NOT NULL,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `account_group_name_uk` (`account_primaryKey`,`name`),
  UNIQUE KEY `account_group_id_uk` (`id`),
  CONSTRAINT `_acctGrp_account_fk` FOREIGN KEY (`account_primaryKey`) REFERENCES `Account` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AccountGroup_Account`
--

DROP TABLE IF EXISTS `AccountGroup_Account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AccountGroup_Account` (
  `accountGroup_primaryKey` bigint(20) NOT NULL,
  `account_primaryKey` bigint(20) NOT NULL,
  PRIMARY KEY (`accountGroup_primaryKey`,`account_primaryKey`),
  KEY `_acctGrpAcct_account_fk` (`account_primaryKey`),
  CONSTRAINT `_acctGrpAcct_account_fk` FOREIGN KEY (`account_primaryKey`) REFERENCES `Account` (`primaryKey`),
  CONSTRAINT `acctGrpAcct_group_fk` FOREIGN KEY (`accountGroup_primaryKey`) REFERENCES `AccountGroup` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AccountSummary`
--

DROP TABLE IF EXISTS `AccountSummary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AccountSummary` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `userStatus` varchar(15) NOT NULL,
  `userStatusChangeDate` datetime NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `fkAccountSummaryUser` (`user_id`),
  KEY `fkAccountSummaryAccount` (`account_id`),
  CONSTRAINT `______fkAccountSummaryAccount` FOREIGN KEY (`account_id`) REFERENCES `Account` (`primaryKey`),
  CONSTRAINT `____fkAccountSummaryUser` FOREIGN KEY (`user_id`) REFERENCES `User` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=923 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AmqpMessageError`
--

DROP TABLE IF EXISTS `AmqpMessageError`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AmqpMessageError` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `consumerName` varchar(255) DEFAULT NULL,
  `failTime` datetime DEFAULT NULL,
  `payloadID` varchar(36) DEFAULT NULL,
  `errorPayload` text,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `payload_id_index` (`payloadID`)
) ENGINE=InnoDB AUTO_INCREMENT=53898 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Authorization`
--

DROP TABLE IF EXISTS `Authorization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Authorization` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `authDescription` varchar(255) DEFAULT NULL,
  `authorizationId` varchar(64) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `authorizationId` (`authorizationId`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2877606 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AuthorizedProducts`
--

DROP TABLE IF EXISTS `AuthorizedProducts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AuthorizedProducts` (
  `reseller_id` bigint(20) NOT NULL,
  `products` varchar(36) DEFAULT NULL,
  KEY `FKD4011C3F1872A7F7` (`reseller_id`),
  CONSTRAINT `______FKD4011C3F1872A7F7` FOREIGN KEY (`reseller_id`) REFERENCES `Account` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CSVFileUser`
--

DROP TABLE IF EXISTS `CSVFileUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CSVFileUser` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `email` varchar(80) CHARACTER SET utf8 NOT NULL,
  `firstName` varchar(40) CHARACTER SET utf8 NOT NULL,
  `lastName` varchar(80) CHARACTER SET utf8 NOT NULL,
  `locale` varchar(8) DEFAULT NULL,
  `phoneExtension` varchar(7) DEFAULT NULL,
  `phoneNumber` varchar(25) DEFAULT NULL,
  `role` varchar(64) NOT NULL,
  `userCSVUploadTask_primaryKey` bigint(20) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `userCSVUploadTask_Task_Index` (`userCSVUploadTask_primaryKey`),
  CONSTRAINT `_userCSVUploadTask_Task_fk` FOREIGN KEY (`userCSVUploadTask_primaryKey`) REFERENCES `Task` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=9195 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ConfigData`
--

DROP TABLE IF EXISTS `ConfigData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConfigData` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `documentExpirationTimeInSeconds` int(11) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CountryLanguageSettings`
--

DROP TABLE IF EXISTS `CountryLanguageSettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CountryLanguageSettings` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `chatSupportLink` varchar(120) CHARACTER SET utf8 DEFAULT NULL,
  `isCountryDefault` bit(1) NOT NULL,
  `languageCode` varchar(2) CHARACTER SET utf8 NOT NULL,
  `pdrnLink` varchar(120) CHARACTER SET utf8 DEFAULT NULL,
  `countrySettings_id` bigint(20) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `countrySettings_id` (`countrySettings_id`,`languageCode`),
  UNIQUE KEY `id` (`id`),
  CONSTRAINT `_FK_9rs8jbphqyqbw2vlhgmvuq521` FOREIGN KEY (`countrySettings_id`) REFERENCES `CountrySettings` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CountrySettings`
--

DROP TABLE IF EXISTS `CountrySettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CountrySettings` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `code` varchar(2) CHARACTER SET utf8 NOT NULL,
  `emailOptInDefault` bit(1) NOT NULL,
  `emailTrackingDefault` bit(1) NOT NULL,
  `isEmailOptinDisplayed` bit(1) NOT NULL,
  `isEmailTrackingDisplayed` bit(1) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `phoneRegexPattern` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `zipcodeRegexPattern` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `supported` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Download`
--

DROP TABLE IF EXISTS `Download`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Download` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `downloadProvider` varchar(20) DEFAULT NULL,
  `downloadVersion` varchar(16) NOT NULL,
  `fqName` varchar(80) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `path` longtext NOT NULL,
  `subTitle` varchar(128) DEFAULT NULL,
  `title` varchar(64) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `fqName` (`fqName`),
  UNIQUE KEY `id` (`id`),
  KEY `fqNameIndex` (`fqName`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EncryptionKey`
--

DROP TABLE IF EXISTS `EncryptionKey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EncryptionKey` (
  `id` varchar(255) NOT NULL,
  `keyJSON` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Entitlement`
--

DROP TABLE IF EXISTS `Entitlement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Entitlement` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `privacyAcceptedDate` datetime DEFAULT NULL,
  `termsAcceptedDate` datetime DEFAULT NULL,
  `license_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `license_user_constraint` (`license_id`,`user_id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK99F1150D57B263D7` (`user_id`),
  KEY `FK99F1150DAC9A56FD` (`license_id`),
  CONSTRAINT `_____FK99F1150D57B263D7` FOREIGN KEY (`user_id`) REFERENCES `User` (`primaryKey`),
  CONSTRAINT `______FK99F1150DAC9A56FD` FOREIGN KEY (`license_id`) REFERENCES `License` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=537894 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EntitlementRestriction`
--

DROP TABLE IF EXISTS `EntitlementRestriction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EntitlementRestriction` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `restrictionCode` int(11) NOT NULL,
  `entitlement_id` bigint(20) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `uniqueRestrictionCodeKey` (`entitlement_id`,`restrictionCode`),
  UNIQUE KEY `id` (`id`),
  KEY `FK7262EC7FD28F75FD` (`entitlement_id`),
  CONSTRAINT `__FK7262EC7FD28F75FD` FOREIGN KEY (`entitlement_id`) REFERENCES `Entitlement` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=396387 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Error`
--

DROP TABLE IF EXISTS `Error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Error` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `codeVersion` longtext,
  `errorPayload` longtext,
  `failTime` datetime DEFAULT NULL,
  `failedEntity` binary(255) DEFAULT NULL,
  `machineName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ErrorInformation`
--

DROP TABLE IF EXISTS `ErrorInformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ErrorInformation` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `DTYPE` varchar(31) NOT NULL,
  `version` int(11) NOT NULL,
  `serviceCode` int(11) NOT NULL,
  `errorCode` int(11) NOT NULL,
  `details` text,
  `field` varchar(255) DEFAULT NULL,
  `task_primaryKey` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `ErrorInformation_Task_FK` (`task_primaryKey`),
  CONSTRAINT `ErrorInformation_Task_FK` FOREIGN KEY (`task_primaryKey`) REFERENCES `Task` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=288 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Feedback`
--

DROP TABLE IF EXISTS `Feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Feedback` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `language` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `message` text CHARACTER SET utf8,
  `url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `userAgent` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category` varchar(20) DEFAULT NULL,
  `userId` varchar(36) DEFAULT NULL,
  `metadata` text CHARACTER SET utf8,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3474 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GlisProductMapping`
--

DROP TABLE IF EXISTS `GlisProductMapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GlisProductMapping` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `duration` bigint(20) NOT NULL,
  `glisProductNumber` varchar(255) NOT NULL,
  `isTrial` bit(1) NOT NULL,
  `accociatedProduct_primaryKey` bigint(20) NOT NULL,
  `quantity_productAdditionalField_primaryKey` bigint(20) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `glisProductNumber` (`glisProductNumber`),
  UNIQUE KEY `description` (`description`),
  UNIQUE KEY `id` (`id`),
  KEY `GlisProductMapping_productNumber_IDX` (`glisProductNumber`),
  KEY `GlisProductMapping_to_Product_IDX` (`accociatedProduct_primaryKey`),
  KEY `GlisProductMapping_to_ProductAdditionalField_IDX` (`quantity_productAdditionalField_primaryKey`),
  CONSTRAINT `_GlisProductMapping_ProductAdditionalField_FK` FOREIGN KEY (`quantity_productAdditionalField_primaryKey`) REFERENCES `ProductAdditionalField` (`primaryKey`),
  CONSTRAINT `_GlisProductMapping_Product_FK` FOREIGN KEY (`accociatedProduct_primaryKey`) REFERENCES `Product` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `License`
--

DROP TABLE IF EXISTS `License`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `License` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `billId` varchar(40) DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `entitlementPolicy` varchar(20) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `startDate` datetime NOT NULL,
  `status` varchar(15) DEFAULT NULL,
  `product_id` bigint(20) NOT NULL,
  `subscription_id` bigint(20) DEFAULT NULL,
  `eulaPolicy` varchar(64) NOT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `FK6D291C2188AA97F5` (`product_id`),
  KEY `FK6D291C2180D3D97` (`subscription_id`),
  CONSTRAINT `______FK6D291C2180D3D97` FOREIGN KEY (`subscription_id`) REFERENCES `Subscription` (`primaryKey`),
  CONSTRAINT `_________FK6D291C2188AA97F5` FOREIGN KEY (`product_id`) REFERENCES `Product` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=282858 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LicenseRedemption`
--

DROP TABLE IF EXISTS `LicenseRedemption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LicenseRedemption` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `licenseKey` varchar(255) DEFAULT NULL,
  `purchasedDate` datetime DEFAULT NULL,
  `redeemedDate` datetime DEFAULT NULL,
  `license_primaryKey` bigint(20) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `duration` bigint(20) DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `commerceProvider` varchar(50) NOT NULL,
  `productSku` varchar(255) DEFAULT NULL,
  `trial` bit(1) NOT NULL DEFAULT b'0',
  `contractId` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `licenseKey_Unique_Constraint` (`licenseKey`),
  KEY `licenseKeyIndex` (`licenseKey`),
  KEY `_license_redemption_fk` (`license_primaryKey`),
  CONSTRAINT `______________license_redemption_fk` FOREIGN KEY (`license_primaryKey`) REFERENCES `License` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=59244 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LicenseRedemptionAdditionalField`
--

DROP TABLE IF EXISTS `LicenseRedemptionAdditionalField`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LicenseRedemptionAdditionalField` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) NOT NULL,
  `version` int(11) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `redemption_primaryKey` bigint(20) DEFAULT NULL,
  `productAdditionalField_primaryKey` bigint(20) NOT NULL,
  `shoppingCartItem_primaryKey` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `LRAF_LicenseRedemption_primaryKey_IND` (`redemption_primaryKey`),
  KEY `LRAF_ProductAdditionalField_primaryKey_IND` (`productAdditionalField_primaryKey`),
  KEY `___FK_licenseRedemptionAdditionalField_shoppingCartItem` (`shoppingCartItem_primaryKey`),
  CONSTRAINT `___FK_licenseRedemptionAdditionalField_shoppingCartItem` FOREIGN KEY (`shoppingCartItem_primaryKey`) REFERENCES `ShoppingCartItem` (`primaryKey`),
  CONSTRAINT `___LRAF_ProductAdditionalField_primaryKey_FK` FOREIGN KEY (`productAdditionalField_primaryKey`) REFERENCES `ProductAdditionalField` (`primaryKey`),
  CONSTRAINT `____________LRAF_LicenseRedemption_primaryKey_FK` FOREIGN KEY (`redemption_primaryKey`) REFERENCES `LicenseRedemption` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=1239 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaTag`
--

DROP TABLE IF EXISTS `MetaTag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaTag` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaTag_Account`
--

DROP TABLE IF EXISTS `MetaTag_Account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaTag_Account` (
  `account_primaryKey` bigint(20) NOT NULL,
  `metaTag_primaryKey` bigint(20) NOT NULL,
  PRIMARY KEY (`account_primaryKey`,`metaTag_primaryKey`),
  KEY `_join_account_metatag_metatag_key_FK` (`metaTag_primaryKey`),
  CONSTRAINT `___join_account_metatag_account_key_FK` FOREIGN KEY (`account_primaryKey`) REFERENCES `Account` (`primaryKey`),
  CONSTRAINT `_join_account_metatag_metatag_key_FK` FOREIGN KEY (`metaTag_primaryKey`) REFERENCES `MetaTag` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaTag_Offering`
--

DROP TABLE IF EXISTS `MetaTag_Offering`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaTag_Offering` (
  `offering_primaryKey` bigint(20) NOT NULL,
  `metaTag_primaryKey` bigint(20) NOT NULL,
  PRIMARY KEY (`offering_primaryKey`,`metaTag_primaryKey`),
  KEY `_join_offering_metatag_metatag_key_FK` (`metaTag_primaryKey`),
  CONSTRAINT `____join_offering_metatag_offering_key_FK` FOREIGN KEY (`offering_primaryKey`) REFERENCES `Offering` (`primaryKey`),
  CONSTRAINT `_join_offering_metatag_metatag_key_FK` FOREIGN KEY (`metaTag_primaryKey`) REFERENCES `MetaTag` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaTag_Product`
--

DROP TABLE IF EXISTS `MetaTag_Product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaTag_Product` (
  `product_primaryKey` bigint(20) NOT NULL,
  `metaTag_primaryKey` bigint(20) NOT NULL,
  PRIMARY KEY (`product_primaryKey`,`metaTag_primaryKey`),
  KEY `_join_product_metatag_metatag_key_FK` (`metaTag_primaryKey`),
  CONSTRAINT `___join_product_metatag_product_key_FK` FOREIGN KEY (`product_primaryKey`) REFERENCES `Product` (`primaryKey`),
  CONSTRAINT `_join_product_metatag_metatag_key_FK` FOREIGN KEY (`metaTag_primaryKey`) REFERENCES `MetaTag` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Offering`
--

DROP TABLE IF EXISTS `Offering`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Offering` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `fqName` varchar(255) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `offeringVersion` int(11) NOT NULL,
  `privacyPolicy` longtext NOT NULL,
  `shortTitle` varchar(64) NOT NULL,
  `subTitle` varchar(128) NOT NULL,
  `termsOfUse` longtext NOT NULL,
  `title` varchar(64) NOT NULL,
  `isIframe` bit(1) NOT NULL DEFAULT b'0',
  `reverseProxyName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `fqName` (`fqName`),
  UNIQUE KEY `id` (`id`),
  KEY `fqNameIndex` (`fqName`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OfferingAdminPermittedFQName`
--

DROP TABLE IF EXISTS `OfferingAdminPermittedFQName`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OfferingAdminPermittedFQName` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `offeringFqName` varchar(255) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `offeringFqName` (`offeringFqName`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OfferingAdminPermittedFQName_Account`
--

DROP TABLE IF EXISTS `OfferingAdminPermittedFQName_Account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OfferingAdminPermittedFQName_Account` (
  `permittedFQName_primaryKey` bigint(20) NOT NULL,
  `account_primaryKey` bigint(20) NOT NULL,
  PRIMARY KEY (`permittedFQName_primaryKey`,`account_primaryKey`),
  KEY `reseller_primaryKey_IDX` (`account_primaryKey`),
  KEY `permittedFQName_primaryKey_IDX` (`permittedFQName_primaryKey`),
  CONSTRAINT `_reseller_primaryKey_IDX` FOREIGN KEY (`account_primaryKey`) REFERENCES `Account` (`primaryKey`),
  CONSTRAINT `permittedFqName_primaryKey_IDX` FOREIGN KEY (`permittedFQName_primaryKey`) REFERENCES `OfferingAdminPermittedFQName` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OfferingFqNames`
--

DROP TABLE IF EXISTS `OfferingFqNames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OfferingFqNames` (
  `offering_fqName` bigint(20) NOT NULL,
  `offeringFqNames` varchar(128) DEFAULT NULL,
  KEY `FK7976E37D4DDE197` (`offering_fqName`),
  CONSTRAINT `FK7976E37D4DDE197` FOREIGN KEY (`offering_fqName`) REFERENCES `Download` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OfferingIcon`
--

DROP TABLE IF EXISTS `OfferingIcon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OfferingIcon` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `offeringFqName` varchar(255) NOT NULL,
  `iconType` varchar(40) NOT NULL,
  `icon` mediumblob NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `OfferingFqName_IconType_Unique_Constraint` (`offeringFqName`,`iconType`),
  UNIQUE KEY `id` (`id`),
  KEY `OfferingFqName_IDX` (`offeringFqName`)
) ENGINE=InnoDB AUTO_INCREMENT=549 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OperatingSystems`
--

DROP TABLE IF EXISTS `OperatingSystems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OperatingSystems` (
  `download_id` bigint(20) NOT NULL,
  `operatingSystems` varchar(128) DEFAULT NULL,
  KEY `FK4F969FC32FFF399A` (`download_id`),
  CONSTRAINT `FK4F969FC32FFF399A` FOREIGN KEY (`download_id`) REFERENCES `Download` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Preference`
--

DROP TABLE IF EXISTS `Preference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Preference` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `keyName` varchar(40) NOT NULL,
  `preferenceVersion` int(11) DEFAULT NULL,
  `source` varchar(40) NOT NULL,
  `type` varchar(40) NOT NULL,
  `value` longtext NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `FK1FCE98FB57B263D7` (`user_id`),
  KEY `keyNameIndex` (`keyName`),
  CONSTRAINT `_____FK1FCE98FB57B263D7` FOREIGN KEY (`user_id`) REFERENCES `User` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=979485 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Product`
--

DROP TABLE IF EXISTS `Product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `catalogId` varchar(36) NOT NULL,
  `category` varchar(32) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `externalReferenceId` varchar(64) DEFAULT NULL,
  `free` bit(1) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `trialTermDays` int(11) NOT NULL,
  `unitOfMeasure` varchar(32) NOT NULL,
  `units` bigint(20) NOT NULL,
  `solutionId` varchar(36) DEFAULT NULL,
  `productCatalog_id` bigint(20) DEFAULT NULL,
  `provisioningRequired` bit(1) NOT NULL DEFAULT b'0',
  `productType` varchar(32) DEFAULT NULL,
  `maxEntitlementAllowed` int(20) NOT NULL DEFAULT '0',
  `eulaPolicy` varchar(64) NOT NULL,
  `launchBehavior` varchar(32) NOT NULL DEFAULT 'SAME_TAB',
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `FK50C664CFCA890FF0` (`productCatalog_id`),
  CONSTRAINT `__________________________FK50C664CFCA890FF0` FOREIGN KEY (`productCatalog_id`) REFERENCES `ProductCatalog` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProductAdditionalField`
--

DROP TABLE IF EXISTS `ProductAdditionalField`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProductAdditionalField` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `i18nKey` varchar(255) DEFAULT NULL,
  `validationRegex` varchar(255) DEFAULT NULL,
  `product_id` bigint(20) NOT NULL,
  `additionalFieldOperator` varchar(32) NOT NULL DEFAULT 'NONE',
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `i18nKey` (`i18nKey`),
  KEY `ProductAdditionalField_product_id_IDX` (`product_id`),
  CONSTRAINT `__ProductAdditionaField_Product_FK` FOREIGN KEY (`product_id`) REFERENCES `Product` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProductAdditionalField_ApplicableCommerceProvider`
--

DROP TABLE IF EXISTS `ProductAdditionalField_ApplicableCommerceProvider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProductAdditionalField_ApplicableCommerceProvider` (
  `productAdditionalField_primaryKey` bigint(20) NOT NULL,
  `applicableCommerceProviders` varchar(50) NOT NULL,
  KEY `ProductAdditionalField_CommerceProvider_primaryKey_IND` (`productAdditionalField_primaryKey`),
  CONSTRAINT `_ProductAdditionalField_CommerceProvider_primaryKey_FK` FOREIGN KEY (`productAdditionalField_primaryKey`) REFERENCES `ProductAdditionalField` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProductCatalog`
--

DROP TABLE IF EXISTS `ProductCatalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProductCatalog` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `offering_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `productCatalog_unique_offering_id` (`offering_id`),
  KEY `FKDD6C5C4AE66B6F77` (`offering_id`),
  CONSTRAINT `__________________________FKDD6C5C4AE66B6F77` FOREIGN KEY (`offering_id`) REFERENCES `Offering` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Product_CommerceProvider`
--

DROP TABLE IF EXISTS `Product_CommerceProvider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product_CommerceProvider` (
  `product_primaryKey` bigint(20) NOT NULL,
  `supportedCommerceProviders` varchar(50) NOT NULL,
  KEY `Product_CommerceProvider_primaryKey_IND` (`product_primaryKey`),
  CONSTRAINT `_Product_CommerceProvider_primaryKey_FK` FOREIGN KEY (`product_primaryKey`) REFERENCES `Product` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Product_TargetAudiences`
--

DROP TABLE IF EXISTS `Product_TargetAudiences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product_TargetAudiences` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_primaryKey` bigint(20) NOT NULL,
  `targetAudiences` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  KEY `product_primaryKey_IDX` (`product_primaryKey`),
  CONSTRAINT `___productTargetAudience_FK` FOREIGN KEY (`product_primaryKey`) REFERENCES `Product` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Role`
--

DROP TABLE IF EXISTS `Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `groupId` binary(255) NOT NULL,
  `roleDescription` varchar(255) DEFAULT NULL,
  `roleId` varchar(56) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `groupId` (`groupId`),
  UNIQUE KEY `roleId` (`roleId`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SentMessage`
--

DROP TABLE IF EXISTS `SentMessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SentMessage` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `user_primaryKey` bigint(20) NOT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `entityId` varchar(36) DEFAULT NULL,
  `messageType` varchar(255) NOT NULL,
  `sentDate` datetime DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `SentMessage_primaryKey_FK` (`user_primaryKey`),
  CONSTRAINT `SentMessage_primaryKey_FK` FOREIGN KEY (`user_primaryKey`) REFERENCES `User` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ShoppingCart`
--

DROP TABLE IF EXISTS `ShoppingCart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ShoppingCart` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `expireDate` datetime NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `state` varchar(16) NOT NULL,
  `account_primaryKey` bigint(20) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `ShoppingCart_Account_primaryKey_IND` (`account_primaryKey`),
  CONSTRAINT `ShoppingCart_Account_primaryKey_FK` FOREIGN KEY (`account_primaryKey`) REFERENCES `Account` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=267168 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ShoppingCartItem`
--

DROP TABLE IF EXISTS `ShoppingCartItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ShoppingCartItem` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `trial` bit(1) NOT NULL,
  `shoppingCart_id` bigint(20) NOT NULL,
  `contractId` varchar(255) DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `commerceProvider` varchar(50) NOT NULL,
  `licenseKey` varchar(255) DEFAULT NULL,
  `product_primaryKey` bigint(20) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `FKFEC18BFB2D11C8F0` (`shoppingCart_id`),
  KEY `ShoppingCartItem_Product_primaryKey_IND` (`product_primaryKey`),
  CONSTRAINT `ShoppingCartItem_Product_primaryKey_FK` FOREIGN KEY (`product_primaryKey`) REFERENCES `Product` (`primaryKey`),
  CONSTRAINT `______FKFEC18BFB2D11C8F0` FOREIGN KEY (`shoppingCart_id`) REFERENCES `ShoppingCart` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=288900 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Subscription`
--

DROP TABLE IF EXISTS `Subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Subscription` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `billId` varchar(40) DEFAULT NULL,
  `productCatalogId` varchar(36) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `FK1E21AD3D57CCA2B8` (`account_id`),
  CONSTRAINT `_______FK1E21AD3D57CCA2B8` FOREIGN KEY (`account_id`) REFERENCES `Account` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=281487 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Task`
--

DROP TABLE IF EXISTS `Task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Task` (
  `DTYPE` varchar(31) NOT NULL,
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `name` varchar(40) NOT NULL,
  `referenceId` varchar(36) DEFAULT NULL,
  `status` varchar(15) NOT NULL,
  `userId` binary(255) DEFAULT NULL,
  `payload` text,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=967902 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TaskRecord`
--

DROP TABLE IF EXISTS `TaskRecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TaskRecord` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `executionDate` datetime NOT NULL,
  `objectId` varchar(40) NOT NULL,
  `result` int(11) NOT NULL,
  `taskName` varchar(40) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `URIEntity`
--

DROP TABLE IF EXISTS `URIEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `URIEntity` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `value` varchar(1024) NOT NULL,
  `name` varchar(50) NOT NULL,
  `offering_key` bigint(20) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `name` (`name`,`offering_key`),
  UNIQUE KEY `id` (`id`),
  KEY `offering_key_Index` (`offering_key`),
  CONSTRAINT `____________________offering_key_FK` FOREIGN KEY (`offering_key`) REFERENCES `Offering` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=327 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `tenantId` varchar(36) DEFAULT NULL,
  `activatedDate` datetime DEFAULT NULL,
  `addressLine` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `addressLine2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `phoneExtension` varchar(7) DEFAULT NULL,
  `phoneNumber` varchar(25) DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `email` varchar(80) CHARACTER SET utf8 NOT NULL,
  `firstname` varchar(40) CHARACTER SET utf8 NOT NULL,
  `isOptedIntoEmail` bit(1) NOT NULL,
  `lastname` varchar(80) CHARACTER SET utf8 NOT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `password` varchar(80) DEFAULT NULL,
  `serviceState` int(11) NOT NULL,
  `status` varchar(15) NOT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `isOptedIntoEmailTracking` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `id` (`id`),
  KEY `FK285FEB57CCA2B8` (`account_id`),
  KEY `userLastNameIndex` (`lastname`),
  KEY `statusIndex` (`status`),
  CONSTRAINT `__________FK285FEB57CCA2B8` FOREIGN KEY (`account_id`) REFERENCES `Account` (`primaryKey`)
) ENGINE=InnoDB AUTO_INCREMENT=715440 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User_Authorization`
--

DROP TABLE IF EXISTS `User_Authorization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User_Authorization` (
  `User_primaryKey` bigint(20) NOT NULL,
  `authorizations_primaryKey` bigint(20) NOT NULL,
  PRIMARY KEY (`User_primaryKey`,`authorizations_primaryKey`),
  KEY `FK31578B05640EDBD9` (`User_primaryKey`),
  KEY `FK31578B05F27E8E7E` (`authorizations_primaryKey`),
  CONSTRAINT `FK31578B05F27E8E7E` FOREIGN KEY (`authorizations_primaryKey`) REFERENCES `Authorization` (`primaryKey`),
  CONSTRAINT `____FK31578B05640EDBD9` FOREIGN KEY (`User_primaryKey`) REFERENCES `User` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Verification`
--

DROP TABLE IF EXISTS `Verification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Verification` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `data` varchar(36) DEFAULT NULL,
  `dateExpired` datetime DEFAULT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `id` (`id`),
  KEY `dataIndex` (`data`)
) ENGINE=InnoDB AUTO_INCREMENT=719229 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_class`
--

DROP TABLE IF EXISTS `acl_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_class` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `class` (`class`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_entry`
--

DROP TABLE IF EXISTS `acl_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_entry` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ace_order` int(11) unsigned NOT NULL,
  `audit_failure` bit(1) NOT NULL,
  `audit_success` bit(1) NOT NULL,
  `granting` bit(1) NOT NULL,
  `mask` int(11) NOT NULL,
  `acl_object_identity` bigint(20) unsigned NOT NULL,
  `sid` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `acl_entry_idx_1` (`acl_object_identity`,`ace_order`),
  KEY `FK5302D47DE5E70AD3` (`sid`),
  KEY `FK5302D47DDC85CF8F` (`acl_object_identity`),
  CONSTRAINT `FK5302D47DDC85CF8F` FOREIGN KEY (`acl_object_identity`) REFERENCES `acl_object_identity` (`id`),
  CONSTRAINT `FK5302D47DE5E70AD3` FOREIGN KEY (`sid`) REFERENCES `acl_sid` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22237944 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_object_identity`
--

DROP TABLE IF EXISTS `acl_object_identity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_object_identity` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `entries_inheriting` bit(1) NOT NULL,
  `object_id_identity` varchar(255) NOT NULL,
  `object_id_class` bigint(20) unsigned NOT NULL,
  `owner_sid` bigint(20) unsigned DEFAULT NULL,
  `parent_object` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `acl_object_identity_idx_1` (`object_id_class`,`object_id_identity`),
  KEY `FK2A2BB009D0AE83FA` (`parent_object`),
  KEY `FK2A2BB00971752743` (`object_id_class`),
  KEY `FK2A2BB009E6F79B47` (`owner_sid`),
  CONSTRAINT `FK2A2BB00971752743` FOREIGN KEY (`object_id_class`) REFERENCES `acl_class` (`id`),
  CONSTRAINT `FK2A2BB009D0AE83FA` FOREIGN KEY (`parent_object`) REFERENCES `acl_object_identity` (`id`),
  CONSTRAINT `FK2A2BB009E6F79B47` FOREIGN KEY (`owner_sid`) REFERENCES `acl_sid` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2776650 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_sid`
--

DROP TABLE IF EXISTS `acl_sid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_sid` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `principal` bit(1) NOT NULL,
  `sid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `acl_sid_idx_1` (`sid`,`principal`)
) ENGINE=InnoDB AUTO_INCREMENT=2985165 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `i18n`
--

DROP TABLE IF EXISTS `i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `i18n` (
  `primaryKey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(36) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `locale` varchar(12) NOT NULL,
  `createdBy` varchar(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` varchar(36) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `source` varchar(255) NOT NULL,
  PRIMARY KEY (`primaryKey`),
  UNIQUE KEY `UK_8f3fsgpdes46tfxwjlm4qk02` (`source`,`locale`),
  UNIQUE KEY `UK_gvxk44fo5t4rr14r4n0in767p` (`id`),
  KEY `localeIndex` (`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=624 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `i18n_strings`
--

DROP TABLE IF EXISTS `i18n_strings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `i18n_strings` (
  `i18n_primaryKey` bigint(20) NOT NULL,
  `value` longtext CHARACTER SET utf8,
  `strid` varchar(255) NOT NULL,
  PRIMARY KEY (`i18n_primaryKey`,`strid`),
  CONSTRAINT `_FK_p9dv6k728htx6eg5lj5rfqpsj` FOREIGN KEY (`i18n_primaryKey`) REFERENCES `i18n` (`primaryKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_version_contraction`
--

DROP TABLE IF EXISTS `schema_version_contraction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version_contraction` (
  `version_rank` int(11) NOT NULL,
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`),
  KEY `schema_version_contraction_vr_idx` (`version_rank`),
  KEY `schema_version_contraction_ir_idx` (`installed_rank`),
  KEY `schema_version_contraction_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_version_expansion`
--

DROP TABLE IF EXISTS `schema_version_expansion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version_expansion` (
  `version_rank` int(11) NOT NULL,
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`),
  KEY `schema_version_expansion_vr_idx` (`version_rank`),
  KEY `schema_version_expansion_ir_idx` (`installed_rank`),
  KEY `schema_version_expansion_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_version_git`
--

DROP TABLE IF EXISTS `schema_version_git`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version_git` (
  `version_rank` int(11) DEFAULT NULL,
  `git_hash` varchar(7) DEFAULT NULL,
  `migration_type` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-09 19:19:23
