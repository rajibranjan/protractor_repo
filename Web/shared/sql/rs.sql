drop database if exists ${portal.integration.db};
DROP DATABASE IF EXISTS ${portal.integration.audit.db};
drop database if exists ${portal.unitTest.db};
DROP DATABASE IF EXISTS ${portal.unitTest.audit.db};

create database ${portal.integration.db};
create database ${portal.unitTest.db};


CREATE DATABASE ${portal.integration.audit.db};
CREATE TABLE IF NOT EXISTS `${portal.integration.audit.db}`.`LOGS` (
  `AUDIT_STREAM_ID` varchar(36) NOT NULL,
  `RECORD_FORMAT_VERSION` int(11) NOT NULL,
  `EVENT_ID` int(11) NOT NULL,
  `EVENT_GROUP_ID` int(11) NOT NULL,
  `SUBJECT_ID` varchar(255) NOT NULL,
  `SUBJECT_LOCATION` varchar(255) NOT NULL,
  `ACTOR_ID` varchar(255) NOT NULL,
  `OBJECT_ID` varchar(255) NOT NULL,
  `OBJECT_LOCATION` varchar(255) NOT NULL,
  `CONTENT_BEFORE_MODIFICATION` longtext NOT NULL,
  `CONTENT_AFTER_MODIFICATION` longtext NOT NULL,
  `EVENT_TIMESTAMP` varchar(28) NOT NULL,
  `EVENT_TIMEZONE` varchar(50) NOT NULL,
  `RESULT_ID` int(11) NOT NULL,
  `RESULT_DESCRIPTION` text NOT NULL,
  `EVENT_DESCRIPTION` text NOT NULL,
  `AUDIT_TIMESTAMP` datetime NOT NULL,
  `AUDIT_HMAC` varchar(100) NOT NULL,
  `LEVEL` varchar(20) NOT NULL,
  `RAW_EVENT` longtext NOT NULL
);

CREATE TABLE IF NOT EXISTS `${portal.integration.audit.db}`.`HMAC_HISTORY` (
  `AUDIT_STREAM_ID` varchar(36) NOT NULL,
  `LAST_HMAC` varchar(100) NOT NULL
);

CREATE DATABASE ${portal.unitTest.audit.db};
CREATE TABLE IF NOT EXISTS `${portal.unitTest.audit.db}`.`LOGS` (
  `AUDIT_STREAM_ID` varchar(36) NOT NULL,
  `RECORD_FORMAT_VERSION` int(11) NOT NULL,
  `EVENT_ID` int(11) NOT NULL,
  `EVENT_GROUP_ID` int(11) NOT NULL,
  `SUBJECT_ID` varchar(255) NOT NULL,
  `SUBJECT_LOCATION` varchar(255) NOT NULL,
  `ACTOR_ID` varchar(255) NOT NULL,
  `OBJECT_ID` varchar(255) NOT NULL,
  `OBJECT_LOCATION` varchar(255) NOT NULL,
  `CONTENT_BEFORE_MODIFICATION` longtext NOT NULL,
  `CONTENT_AFTER_MODIFICATION` longtext NOT NULL,
  `EVENT_TIMESTAMP` varchar(28) NOT NULL,
  `EVENT_TIMEZONE` varchar(50) NOT NULL,
  `RESULT_ID` int(11) NOT NULL,
  `RESULT_DESCRIPTION` text NOT NULL,
  `EVENT_DESCRIPTION` text NOT NULL,
  `AUDIT_TIMESTAMP` datetime NOT NULL,
  `AUDIT_HMAC` varchar(100) NOT NULL,
  `LEVEL` varchar(20) NOT NULL,
  `RAW_EVENT` longtext NOT NULL
);

CREATE TABLE IF NOT EXISTS `${portal.unitTest.audit.db}`.`HMAC_HISTORY` (
  `AUDIT_STREAM_ID` varchar(36) NOT NULL,
  `LAST_HMAC` varchar(100) NOT NULL
);