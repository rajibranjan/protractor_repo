DROP database if EXISTS ${pullprint.integration.db};
DROP database if EXISTS ${pullprint.unitTest.db};

CREATE database ${pullprint.integration.db};
CREATE database ${pullprint.unitTest.db};