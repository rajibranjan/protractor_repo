#!/bin/bash
echo "STARTING UPLOAD"

# function to extract "id":"uuid value" attribute from given filename
function getID {
  if [ ! -e $1 ] ; then
    echo "ERROR: $1 is not a file" >&2
    return 1
  fi

  id=`cat $1 | sed '/\\"rel\\"\s*:\s*\\"self/,/href/!d' | sed -n 's/.*\(.\{36\}\)"/\1/p'`
  if [ -z "$id" ] ; then
    echo ERROR: no match found >&2
    return 2
  fi
  # echo $0 return id=$id >&2
  echo $id
}

# NOTE: save this for posterity... this is not being used
# function to query the given resource, and parse the response to find the given fqName= or source= match, then parse out and return its uuid
# usage: getUUID [ fqName=<some fqName> | source=<some sourcename> ] HTTP_RESOURCE
function getUUID {
  #echo $0 called with $# arguments >&2
  set +e
  echo $1 | grep "fqName" > /dev/null
  if [ $? -eq 0 ] ; then
    match=`echo $1 | sed 's/fqName=//g'`
  else
    echo $1 | grep "source" > /dev/null
    if [ $? -eq 0 ] ; then
      match=`echo $1 | sed 's/source=//g'`
    else
      return 1
    fi
  fi
  if [ -z "$match" ] ; then
    return 2
  fi
  set -e
  getresult=`curl -k --silent -w "%{http_code}" -b $COOKIES -H "Content-Type: application/json" -X GET $2 --noproxy 127.0.0.1`
  # echo getresult=$getresult >&2
  echo match=$match >&2
  # add carriage-returns to each resource in response, pipe through grep to find pattern match, extract id":", from there to next " is the resource
  # uuid=`echo $getresult | sed 's/},/&\n/g' | grep $match | sed 's/.*id\"\[ ]*:[ ]*\"//g' | sed 's/\"\,\".*//g'`
  uuid=`echo $getresult | sed 's/},/&\n/g' | grep $match | sed 's/.*id\"\:\"//g' | sed 's/\"\,\".*//g'`
  if [ -z "$uuid" ] ; then
    echo ERROR: no match found
    return 3
  fi
  echo return uuid=$uuid >&2
  echo $uuid
}

#Check args
while getopts u:p:s:f:h:a:b:c: option
do
    case "${option}"
    in
        u) USER_ARG=${OPTARG};;
        p) PASS_ARG=${OPTARG};;
        s) SITEBASE_ARG=${OPTARG};;
        f) FILELIST_ARG=${OPTARG};;
        h) SCANSEND_ARG=${OPTARG};;
        a) PULLPRINT_ARG=${OPTARG};;
        b) PUFFIN_ARG=${OPTARG};;
        c) INCATERN_ARG=${OPTARG};;
    esac
done

if [ -z "$USER_ARG" ]; then
    echo "ERROR! Use the '-u' option to set a user"
    exit 1
fi

if [ -z "$PASS_ARG" ]; then
    echo "ERROR! Use the '-p' option to specify password"
    exit 2
fi

if [ -z "$SITEBASE_ARG" ]; then
    echo "ERROR! Use the '-s' option to specify a root URL"
    exit 3
fi

if [ -z "$FILELIST_ARG" ]; then
    echo "ERROR! Use the '-f' option to specify a file list"
    exit 4
fi

echo -e "\n-----------------------------------------------------------"
echo "PullPrint arg: $PULLPRINT_ARG"
echo "ScanSend arg: $SCANSEND_ARG"
echo "Puffin arg: $PUFFIN_ARG"
echo "IncaTern arg: $INCATERN_ARG"
echo -e "-----------------------------------------------------------\n"

#Read the file list
declare -a FILELIST
readarray -t FILELIST < $FILELIST_ARG

#Set a trap to always remove the cookies file
COOKIES=$(mktemp)
removeCookies() {
    rm $COOKIES
}
trap removeCookies EXIT INT TERM

#Log in!
LOGIN_URL="$SITEBASE_ARG/j_spring_security_check"
echo "Logging in to $LOGIN_URL"
curl -k --silent -w "%{http_code}" -c $COOKIES -H "Content-Type: application/x-www-form-urlencoded" -X POST $LOGIN_URL -d "j_username=$USER_ARG&j_password=$PASS_ARG" --noproxy 127.0.0.1
echo -e " RESPONSE\nDone logging in!\n\n"

tempfile="tempfile.json"
for LINE in "${FILELIST[@]}"; do
  IFS=' ' read -a item <<< "$LINE"

  if [ "${item[0]}" = "#" -o "${item[0]}" = "" ] ; then
    echo $LINE
    continue
  fi

  #Create REST url by substituting correct base into url
  REST_URL=${item[1]/\$\[site.base\]/$SITEBASE_ARG}
  REST_URL=${REST_URL/\$\[scansend.base\]/$SCANSEND_ARG}
  REST_URL=${REST_URL/\$\[pullprint.base\]/$PULLPRINT_ARG}
  REST_URL=${REST_URL/\$\[puffin.base\]/$PUFFIN_ARG}
  REST_URL=${REST_URL/\$\[incatern.base\]/$INCATERN_ARG}

  multipart="false"
  if [ "${item[0]}" = "GET" ] ; then
    if [ ! -z "${item[2]}" ] ; then
      echo ASSUME doing a GET using match fqName= or source=
      set +e
      uuid=`getUUID ${item[2]} $REST_URL`
      if [ $? -ne 0 ] ; then
        echo SOME ERROR
        continue
      fi
      set -e
      echo RESOURCE $REST_URL with ${item[2]} has UUID=$uuid
    fi
    file="NA"

  elif [ "${item[0]}" = "POST" ] ; then
    file=${item[2]}
    # Check if multipart data
    if [ ! -z "${item[3]}" ] ; then
      multipart=true
      upload="image=@${item[3]}"
    fi

  elif [ "${item[0]}" = "PUT" ] ; then
    # PUT /resource/path filename ........... extract uuid from filename and PUT filename to /resource/path/<uuid>
    # DELETE /resource/path [ filename ] .... if filename not given, /resource/path is expected to be the fully-qualified path (including uuid)
    if [ -z "${item[1]}" -o -z "${item[2]}" ] ; then
      echo ERROR: ${item[0]} requires 2 parameters: /resource/path and filename
      continue
    fi
    id=`getID ${item[2]}`
    id=`echo $id | sed 's/.* //'`
    if [ -z "$id" ] ; then
      echo ERROR: filename=${item[2]} must contain an \"id\":\"uuid value\" attribute
      continue
    fi
    REST_URL=$REST_URL/$id
    file=${item[2]}

  elif [ "${item[0]}" = "DELETE" ] ; then
    if [ -z "${item[1]}" ] ; then
      echo ERROR: ${item[0]} requires at least 1 parameter: /resource/path
      continue
    fi
    if [ -e "${item[2]}" ] ; then
      # TODO: add support for deleting resources by source= or fqName=
      id=`getID ${item[2]}`
      if [ -z "$id" ] ; then
        echo ERROR: filename=${item[2]} must contain an \"id\":\"uuid value\" attribute
        continue
      fi
      REST_URL=$REST_URL/$id
    fi
    # otherwise, assume REST_URL specifies the path to the resource (including its uuid)
    file="NA"
  else
    echo "ERROR: action='${item[0]}' is not supported."
    continue
  fi

  if [ -z "$file" -o "$file" = "NA" ] ; then
    extraParams=
  else
    sed -e "s;\$\[site.base];$SITEBASE_ARG;g" -e "s;\$\[scansend.base];$SCANSEND_ARG;g" -e "s;\$\[pullprint.base];$PULLPRINT_ARG;g" -e "s;\$\[puffin.base];$PUFFIN_ARG;g" -e "s;\$\[incatern.base];$INCATERN_ARG;g" $file > $tempfile
    if [ "$multipart" = "true" ] ; then
      extraParams="-F entity=<$tempfile"
    else
      extraParams="--data-binary @$tempfile"
    fi
  fi

  outfile=`date | sed 's/ /-/g'`.$RANDOM.out
  # Run curl command for application or multipart data
  echo "HTTP ${item[0]} on $REST_URL - file=$file"
  if [ "$multipart" = "false" ]; then
      http_response=`curl -k --silent -w "%{http_code}" -b $COOKIES -H "Content-Type: application/json" -X ${item[0]} $REST_URL $extraParams --noproxy 127.0.0.1 -o $outfile`
  else
      http_response=`curl -k --silent -w "%{http_code}" -b $COOKIES $extraParams -F $upload -X ${item[0]} $REST_URL --noproxy 127.0.0.1 -o $outfile`
  fi
  echo "     ${item[0]} on $REST_URL HTTP_RESPONSE=$http_response"
  if [ "${http_response:0:1}" = "4" ] ; then
    #echo SOURCE FILE:
    #cat $file
    #echo ""
    echo CURL OUTPUT:
    cat $outfile
  fi
  rm $outfile
  echo ""
done

rm $tempfile
echo "DONE WITH UPLOAD"
