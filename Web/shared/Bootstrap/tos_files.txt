# ----------------------- "platform offering" localization resources -------------
PUT api/v2/i18n json/src/main/assets/i18n/hp-platform_fr-CA.json
PUT api/v2/i18n json/src/main/assets/i18n/hp-platform_en-US.json
PUT api/v2/i18n json/src/main/assets/i18n/hp-platform_de-DE.json
PUT api/v2/i18n json/src/main/assets/i18n/hp-platform_es-ES.json
PUT api/v2/i18n json/src/main/assets/i18n/hp-platform_it-IT.json
PUT api/v2/i18n json/src/main/assets/i18n/hp-platform_nl-NL.json
