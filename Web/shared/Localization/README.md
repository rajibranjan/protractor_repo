
LOCALIZATION TOOLS
==================

HOW TO...
-----------
# You need to have all the repos (titan-parent, titan-framework, titan-portal, titan-pullprint, titan-scansend, titan-proxyroot) up to date and contained in the same folder
# All commands are run from the titan-parent/TitanParent/TitanSharedResources/shared/Localization directory

GENERATE LOCALIZATION DROPS
---------------------------
# all drop scripts will create a localizationDrop_<date>.zip file on the desktop

* Generate a full localization drop of all titan localization resources

    > ./gather_all_localization_resources.sh

* Generate a localization drop of changed resources from a particular git SHA (for each microservice)
# the drop zip should contain an info.txt file which contains the SHA for the versions of each microservice used for the diff

    > ./gather_localization_changes.sh -r <PORTAL_SHA> -p <PULLPRINT_SHA> -s <SCANSEND_SHA>  -f <FRAMEWORK_SHA> -m <PARENT_SHA> -x <PROXYROOT_SHA>


DEPLOY LOCALIZATION DELIVERY FILES
----------------------------------

# NOTE:  deploy_localization-delivery-OLD.sh is deprecated and no longer used

* initial grunt setup required for deploy script (without this the script will fail when it tries to run grunt)

    > sudo npm install

* Deploy a localization delivery .zip into the titan project

    remove any prior delivery.zip files from [Localization/localization_delivery] folder
    copy the new delivery.zip file into the [Localization/localization_delivery] folder

    > deploy_localization_delivery.sh


EXCLUDE STRINGS FROM LOCALIZATION
---------------------------------

The [string_excludes.txt] file is used to exclude json strings from being included in localization drops.
To add a json string to the excludes list edit the file and add the json key for the string on it's own line.
