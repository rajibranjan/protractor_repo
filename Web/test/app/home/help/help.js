'use strict';

describe('[Module: hp.portal.home.help]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.help'));

  describe('controller:HelpCtrl', function () {

    var RolesHelper = {
      selfHasRole: function (arg) {
        return arg;
      }
    };

    var self = {
      locale: 'en-US',
      account: {
        country: 'US'
      },

      getLocale: function () {
        return 'en-US';
      }
    };

    var mockGlobals = {
      permissions: {}
    };

    var scope, HelpCtrl, controller;
    var Roles = {};


    var initController = function (newPermissions) {
      mockGlobals.permissions = newPermissions;
      HelpCtrl = controller('HelpCtrl', {
        $scope: scope,
        RolesHelper: RolesHelper,
        Roles: Roles,
        helpLinksAdmin: 'admin',
        helpLinksUser: 'user',
        Self: self,
        Globals: mockGlobals
      });
    };

    beforeEach(inject(function ($rootScope, $controller) {
      scope = $rootScope.$new();
      controller = $controller;
    }));

    it('Should assign helpLinksAdmin constant if self has COMPANY_ADMIN role', function () {
      var permissions = {
        isCompanyAdmin: true
      };
      initController(permissions);
      expect(scope.helpLinkList).toBe('admin');
    });
    it('Should assign helpLinksUser constant with no role', function () {
      var permissions = {};
      initController(permissions);
      expect(scope.helpLinkList).toBe('user');
    });

    it('Should assign helpLinksUser constant if self has USER role', function () {
      var permissions = {
        isUser: true
      };
      initController(permissions);
      expect(scope.helpLinkList).toBe('user');
    });
    it('Should assign helpLinkAdmin constant if self has USER and COMPANY_ADMIN roles', function () {
      var permissions = {
        isCompanyAdmin: true,
        isUser: true
      };
      initController(permissions);
      expect(scope.helpLinkList).toBe('admin');
    });
    it('Should assign helpLinkAdmin constant if self has USER and HP_ADMIN roles', function () {
      var permissions = {
        isHPAdmin: true,
        isUser: true
      };
      initController(permissions);
      expect(scope.helpLinkList).toBe('admin');
    });
    it('Should assign helpLinkUser constant if self has USER and RESELLER roles', function () {
      var permissions = {
        isReseller: true,
        isUser: true
      };
      initController(permissions);
      expect(scope.helpLinkList).toBe('user');
    });
  });


});
