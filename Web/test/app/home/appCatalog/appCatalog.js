'use strict';

describe('[Module: hp.portal.home.appCatalog]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.appCatalog'));

  var mockEntitledOffering = {
    offering: {
      fqName: 'hp-cloudrepoproxy',
      title: 'cTitle',
      shortTitle: 'cShortTitle',
      subTitle: 'cSubTitle',
      description: 'cDescription',
      termsOfUse: 'cTermsOfUse',
      privacyPolicy: 'cPrivacyPolicy',
      offeringVersion: 1,
      links: [{
        rel: 'self',
        href: 'https://127.0.0.1/api/v2/offerings/3034204a-24da-4e93-9218-02f2e89e6b73'
      }, {
        rel: 'productCatalog',
        href: 'https://127.0.0.1/api/v2/productCatalogs/4ccd0f8f-32e0-438b-af85-cf8beea7b40a'
      }],
      urls: {
        browserLandingUrl: 'https://www-micro-staging.hpfoghorn.com/CloudRepoProxy',
        browserLandingPath: 'https://www-micro-staging.hpfoghorn.com',
        iconSmallUrl: 'assets/dev/hp-cloudrepoproxy/icon-sm.png',
        iconMediumUrl: 'assets/dev/hp-cloudrepoproxy/icon-med.png',
        iconLargeUrl: 'assets/dev/hp-cloudrepoproxy/icon-lrg.png',
        mfpLandingPath: '/oxp/home',
        rootServiceUrl: 'http://www-micro-staging.hpfoghorn.com/CloudRepoProxy',
        apiUrl: 'http://www-micro-staging.hpfoghorn.com/CloudRepoProxy/api',
        apiPath: '/api',
        healthUrl: null
      },
      metaTags: [
        'AMERICAS_DIRECT',
        'EMEA_DIRECT',
        'ASIA_OCEANIA_DIRECT',
        'OTHER_DIRECT'
      ]
    },
    products: [{
      product: {
        id: '4f5c5d06-d5a7-4df3-98fe-c40ac9184508',
        catalogId: '00672ba7-71ae-4dfd-b7f5-bae4198d2c6d',
        name: '5 users, 1 year',
        description: '',
        unitOfMeasure: 'years',
        units: 1,
        isFree: false,
        trialInDays: 14,
        productType: 'UNKNOWN',
        solutionId: '8b198c89-a68c-47c3-9ed0-0e5887dc208c',
        isProvisioningRequired: true,
        targetAudiences: [
          'ROLE_USER',
          'ROLE_COMPANY_ADMIN',
          'ROLE_USER_ADMIN'
        ],
        metaTags: [
          'AMERICAS_DIRECT',
          'EMEA_DIRECT',
          'ASIA_OCEANIA_DIRECT',
          'OTHER_DIRECT'
        ]
      },
      entitlement: {
        isEntitled: true,
        termsAccepted: true,
        privacyAccepted: true,
        tenantId: 'a1699c71-34d3-4b95-a5af-c19c41f30c43'
      }
    }, {
      product: {
        id: '1d6f35e3-eb7c-4475-a8a5-8d0e5e57dd92',
        catalogId: '00672ba7-71ae-4dfd-b7f5-bae4198d2c6d',
        name: '5 users, 2 year',
        description: '',
        unitOfMeasure: 'years',
        units: 2,
        isFree: false,
        trialInDays: 14,
        productType: 'UNKNOWN',
        solutionId: '8b198c89-a68c-47c3-9ed0-0e5887dc208c',
        isProvisioningRequired: true,
        targetAudiences: [
          'ROLE_USER',
          'ROLE_COMPANY_ADMIN',
          'ROLE_USER_ADMIN'
        ],
        metaTags: [
          'AMERICAS_DIRECT',
          'EMEA_DIRECT',
          'ASIA_OCEANIA_DIRECT',
          'OTHER_DIRECT'
        ]
      },
      entitlement: {
        isEntitled: false,
        termsAccepted: false,
        privacyAccepted: false,
        tenantId: 'a1699c71-34d3-4b95-a5af-c19c41f30c43'
      }
    }, {
      product: {
        id: '11888a42-db76-4ca9-8553-2e866d171037',
        catalogId: '00672ba7-71ae-4dfd-b7f5-bae4198d2c6d',
        name: '5 users, 3 year',
        description: '',
        unitOfMeasure: 'years',
        units: 3,
        isFree: false,
        trialInDays: 14,
        productType: 'UNKNOWN',
        solutionId: '8b198c89-a68c-47c3-9ed0-0e5887dc208c',
        isProvisioningRequired: true,
        targetAudiences: [
          'ROLE_USER',
          'ROLE_COMPANY_ADMIN',
          'ROLE_USER_ADMIN'
        ],
        metaTags: [
          'AMERICAS_DIRECT',
          'EMEA_DIRECT',
          'ASIA_OCEANIA_DIRECT',
          'OTHER_DIRECT'
        ]
      },
      entitlement: {
        isEntitled: false,
        termsAccepted: false,
        privacyAccepted: false,
        tenantId: 'a1699c71-34d3-4b95-a5af-c19c41f30c43'
      }
    }]
  };

  var mockUnentitledOffering = {
    offering: {
      fqName: 'hp-cloudrepoproxy',
      title: 'cTitle',
      shortTitle: 'cShortTitle',
      subTitle: 'cSubTitle',
      description: 'cDescription',
      termsOfUse: 'cTermsOfUse',
      privacyPolicy: 'cPrivacyPolicy',
      offeringVersion: 1,
      links: [{
        rel: 'self',
        href: 'https://127.0.0.1/api/v2/offerings/3034204a-24da-4e93-9218-02f2e89e6b73'
      }, {
        rel: 'productCatalog',
        href: 'https://127.0.0.1/api/v2/productCatalogs/4ccd0f8f-32e0-438b-af85-cf8beea7b40a'
      }],
      urls: {
        browserLandingUrl: 'https://www-micro-staging.hpfoghorn.com/CloudRepoProxy',
        browserLandingPath: 'https://www-micro-staging.hpfoghorn.com',
        iconSmallUrl: 'assets/dev/hp-cloudrepoproxy/icon-sm.png',
        iconMediumUrl: 'assets/dev/hp-cloudrepoproxy/icon-med.png',
        iconLargeUrl: 'assets/dev/hp-cloudrepoproxy/icon-lrg.png',
        mfpLandingPath: '/oxp/home',
        rootServiceUrl: 'http://www-micro-staging.hpfoghorn.com/CloudRepoProxy',
        apiUrl: 'http://www-micro-staging.hpfoghorn.com/CloudRepoProxy/api',
        apiPath: '/api',
        healthUrl: null
      },
      metaTags: [
        'AMERICAS_DIRECT',
        'EMEA_DIRECT',
        'ASIA_OCEANIA_DIRECT',
        'OTHER_DIRECT'
      ]
    },
    products: [{
      product: {
        id: '4f5c5d06-d5a7-4df3-98fe-c40ac9184508',
        catalogId: '00672ba7-71ae-4dfd-b7f5-bae4198d2c6d',
        name: '5 users, 1 year',
        description: '',
        unitOfMeasure: 'years',
        units: 1,
        isFree: false,
        trialInDays: 14,
        productType: 'UNKNOWN',
        solutionId: '8b198c89-a68c-47c3-9ed0-0e5887dc208c',
        isProvisioningRequired: true,
        targetAudiences: [
          'ROLE_USER',
          'ROLE_COMPANY_ADMIN',
          'ROLE_USER_ADMIN'
        ],
        metaTags: [
          'AMERICAS_DIRECT',
          'EMEA_DIRECT',
          'ASIA_OCEANIA_DIRECT',
          'OTHER_DIRECT'
        ]
      },
      entitlement: null
    }, {
      product: {
        id: '1d6f35e3-eb7c-4475-a8a5-8d0e5e57dd92',
        catalogId: '00672ba7-71ae-4dfd-b7f5-bae4198d2c6d',
        name: '5 users, 2 year',
        description: '',
        unitOfMeasure: 'years',
        units: 2,
        isFree: false,
        trialInDays: 14,
        productType: 'UNKNOWN',
        solutionId: '8b198c89-a68c-47c3-9ed0-0e5887dc208c',
        isProvisioningRequired: true,
        targetAudiences: [
          'ROLE_USER',
          'ROLE_COMPANY_ADMIN',
          'ROLE_USER_ADMIN'
        ],
        metaTags: [
          'AMERICAS_DIRECT',
          'EMEA_DIRECT',
          'ASIA_OCEANIA_DIRECT',
          'OTHER_DIRECT'
        ]
      },
      entitlement: null
    }, {
      product: {
        id: '11888a42-db76-4ca9-8553-2e866d171037',
        catalogId: '00672ba7-71ae-4dfd-b7f5-bae4198d2c6d',
        name: '5 users, 3 year',
        description: '',
        unitOfMeasure: 'years',
        units: 3,
        isFree: false,
        trialInDays: 14,
        productType: 'UNKNOWN',
        solutionId: '8b198c89-a68c-47c3-9ed0-0e5887dc208c',
        isProvisioningRequired: true,
        targetAudiences: [
          'ROLE_USER',
          'ROLE_COMPANY_ADMIN',
          'ROLE_USER_ADMIN'
        ],
        metaTags: [
          'AMERICAS_DIRECT',
          'EMEA_DIRECT',
          'ASIA_OCEANIA_DIRECT',
          'OTHER_DIRECT'
        ]
      },
      entitlement: null
    }]
  };

  var mockSelf = {
    links: [{
      rel: 'account',
      href: 'https://localhost:9000/api/v2/accounts/d8c0d964-43c8-4047-9f88-f8eec2c970e3'
    }, {
      rel: 'self',
      href: 'https://127.0.0.1/api/v2/users/1c4ede71-55b9-4cd1-a918-5e0329677f0f'
    }],
    account: {
      country: 'US'
    },
    getLocale: function () {
      return 'en-US';
    }
  };

  var mockShoppingCart = {
    accountId: '25020aab-ff98-4cb0-90dd-15361af903cb',
    expireDate: '2016-12-31T23:59:08.000+0000',
    state: 'SHOPPING',
    purchaseURL: 'http://fake-url.com/buy/this/product',
    items: [{
      productId: 'mock-product-id',
      quantity: 3
    }],
    links: [{
      rel: 'self',
      href: 'https://localhost:9000/api/v2/shoppingCarts/0e5ea1c2-badd-4509-b3f2-c0f4239bd97f'
    }, {
      rel: 'user',
      href: 'https://localhost:9000/api/v2/users/1c4ede71-55b9-4cd1-a918-5e0329677f0f'
    }]
  };

  var mockCatalogGenerator = function () {
    var mockCatalog = {
      offeringsAndProducts: []
    };

    for (var i = 0; i < 2; i++) {
      var entitled = angular.copy(mockEntitledOffering);
      entitled.products[0].entitlement.isEntitled = true;
      mockCatalog.offeringsAndProducts.push(entitled);
    }

    for (var j = 0; j < 3; j++) {
      var unentitled = angular.copy(mockUnentitledOffering);
      mockCatalog.offeringsAndProducts.push(unentitled);
    }
    return mockCatalog;
  };

  function MockModal(q) {
    var _result = 'success';
    var deferred = q.defer();

    deferred.resolve(_result);

    this.options = {};
    this.result = deferred.promise;
    this.setOptions = function (options) {
      this.options = options;
    };
    this.setResult = function (result) {
      _result = result;
    };
  }

  function MockModalService(mockModal) {
    this.open = function (options) {
      mockModal.setOptions(options);
      return mockModal;
    };
  }

  var modalResult = 'success';

  function MockModalInstance() {
    this.result = 'success';
    this.dismiss = function (result) {
      modalResult = result;
    };

    this.close = function (result) {
      modalResult = result;
    };
  }

  var shouldResolve = true;

  function MockOfferingsAndProductsByUser(q) {
    this.get = function (params) {
      params.shouldResolve = shouldResolve;
      var deferred = q.defer();
      if (shouldResolve) {
        deferred.resolve(mockCatalogGenerator());
      }
      else {
        deferred.reject({
          error: 'error'
        });
      }
      return deferred.promise;
    };
  }

  function MockShoppingCart(q) {
    this.callCount = 0;

    this.save = function (cart) {
      this.callCount++;
      var deferred = q.defer();
      if (cart) {
        angular.extend(this, cart);
      }
      else {
        angular.extend(this, mockShoppingCart);
      }
      deferred.resolve(this);
      return deferred.promise;
    };
  }

  function MockShoppingCartService(shoppingCart) {
    this.cart = shoppingCart;
    this.create = function (options) {
      angular.extend(this.cart, options);
      return this.cart;
    };
  }

  var mockShoppingCartCheckout = {
    startTask: function () {
      var deferred = $q.defer();
      deferred.resolve();
      return {
        $promise: deferred.promise
      };
    }
  };

  function MockPageAlerts() {
    this.type = '';
    this.add = function (options) {
      if (options.type) {
        this.type = options.type;
      }
    };
  }

  var $q;

  describe('Controller: AppCatalogCtrl', function () {
    var scope,
      AppCatalogCtrl,
      mockOfferingService,
      mockModal,
      mockModalService,
      mockPageAlerts;

    beforeEach(inject(function ($q, $rootScope, $controller, LinkParser) {
      scope = $rootScope.$new();
      mockOfferingService = new MockOfferingsAndProductsByUser($q);
      mockModal = new MockModal($q);
      mockModalService = new MockModalService(mockModal);
      mockPageAlerts = new MockPageAlerts();

      AppCatalogCtrl = $controller('AppCatalogCtrl', {
        $scope: scope,
        $uibModal: mockModalService,
        Queries: {
          OfferingsAndProductsByUser: mockOfferingService
        },
        Self: mockSelf,
        LinkParser: LinkParser,
        PageAlerts: mockPageAlerts
      });

      spyOn(mockModalService, 'open').and.callThrough();
      spyOn(mockPageAlerts, 'add').and.callThrough();
      spyOn(scope, 'signUp').and.callThrough();
    }));

    it('should have entitled products', function () {
      expect(AppCatalogCtrl).toBeDefined();
      scope.$digest();
      expect(scope.entitledApps).toBeDefined();
      expect(scope.entitledApps.length).toEqual(2);
    });

    it('should have unentitled products', function () {
      expect(AppCatalogCtrl).toBeDefined();
      scope.$digest();
      expect(scope.unentitledApps).toBeDefined();
      expect(scope.unentitledApps.length).toEqual(3);
    });
  });

  describe('Controller: ShoppingCartModalCtrl', function () {
    var scope,
      modalInstance,
      shoppingCartInstance,
      shoppingCartService,
      ShoppingCartCheckout,
      ShoppingCartModalCtrl,
      offering,
      product;

    beforeEach(inject(function ($rootScope, $controller, _$q_, LinkParser) {
      scope = $rootScope.$new();
      $q = _$q_;
      shoppingCartInstance = new MockShoppingCart($q);
      shoppingCartService = new MockShoppingCartService(shoppingCartInstance);
      ShoppingCartCheckout = mockShoppingCartCheckout;
      modalInstance = new MockModalInstance();
      offering = {
        fqName: 'test-fq-name'
      };
      product = {
        name: 'test product name',
        links: [{
          rel: 'self',
          href: 'https://localhost:9000/api/v2/products/86096f5a-ce62-40a4-83d0-d9bf010acd33'
        }]
      };

      ShoppingCartModalCtrl = $controller('ShoppingCartModalCtrl', {
        $scope: scope,
        $uibModalInstance: modalInstance,
        Self: mockSelf,
        LinkParser: LinkParser,
        ShoppingCartService: shoppingCartService,
        ShoppingCartCheckout: ShoppingCartCheckout,
        offering: offering,
        product: product
      });

      spyOn(shoppingCartInstance, 'save').and.callThrough();
      spyOn(modalInstance, 'close');
      spyOn(modalInstance, 'dismiss');
    }));

    it('should have an offering', function () {
      expect(ShoppingCartModalCtrl).toBeDefined();
      expect(scope.offering).toBeDefined();
      expect(scope.offering.fqName).toBeDefined();
    });

    it('should have a product', function () {
      expect(ShoppingCartModalCtrl).toBeDefined();
      expect(scope.product).toBeDefined();
      expect(scope.product.name).toBeDefined();
    });

    it('should call shoppingCartInstance.save', function () {
      expect(ShoppingCartModalCtrl).toBeDefined();
      expect(shoppingCartInstance).toBeDefined();
      scope.ok();
      scope.$digest();
      expect(shoppingCartInstance.save).toHaveBeenCalled();
      expect(shoppingCartInstance.callCount).toEqual(1);
    });

    it('should call $uibModalInstance.close', function () {
      expect(ShoppingCartModalCtrl).toBeDefined();
      expect(shoppingCartInstance).toBeDefined();
      scope.ok();
      scope.$digest();
      expect(modalInstance.close).toHaveBeenCalled();
    });

    it('should create a new shopping cart and add an item to it', function () {
      expect(ShoppingCartModalCtrl).toBeDefined();
      scope.ok();
      scope.$digest();
      expect(shoppingCartInstance.items).toBeDefined();
      expect(shoppingCartInstance.items.length).toEqual(1);
    });
  });
});
