'use strict';

describe('Module: hp.portal.home.webservices.tasks UpdateCredentails', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.webservices.tasks'));

  beforeEach(module(function ($provide) {
    $provide.constant('Self', {
      href: 'testSelfHref'
    });
    $provide.constant('LinkParser', mockLinkParser);
    $provide.constant('Endpoints', mockEndpoints);
  }));

  var httpBackend;
  beforeEach(inject(function ($httpBackend) {
    httpBackend = $httpBackend;
  }));

  var mockLinkParser = {
    getSelf: function (Self) {
      return Self;
    }
  };

  var mockEndpoints = {
    updateCredentials: '/updateCredentialsEndpoint'
  };

  var testAccount = {
    name: 'Test Company',
    customerId: 'cust_ID_1234',
    status: 'ACTIVE',
    state: 'ID',
    phoneNumber: '1234561234',
    links: [{
      rel: 'self',
      href: 'https://localhost:8443/HPStoreWeb/api/v2/accounts/ea28c75d-5de9-426d-a6d0-8e408f1e7c31'
    }]
  };

  describe('Factory: UpdateCredentials', function () {

    var updateCredentialsFactory, updateRan;

    beforeEach(inject(function (UpdateCredentials) {
      updateCredentialsFactory = UpdateCredentials;
      updateRan = false;
    }));

    afterEach(function () {
      httpBackend.verifyNoOutstandingRequest();
    });

    it('should change the password for a user', function () {
      httpBackend.expectPOST(mockEndpoints.updateCredentials).respond(200, testAccount);
      var updateRequest = updateCredentialsFactory.changePassword('testOldPassword', 'testNewPassword');

      updateRequest.$promise.then(function (task) {
        updateRan = true;
        expect(task.status).toEqual(testAccount.status);
      });
      httpBackend.flush();
      expect(updateRan).toBe(true);
    });

    it('should fail to change password for a user', function () {
      httpBackend.expectPOST(mockEndpoints.updateCredentials).respond(401);
      var updateRequest = updateCredentialsFactory.changePassword('testOldPassword', 'testNewPassword');

      updateRequest.$promise.then(function success() {}, function error(task) {
        updateRan = true;
        expect(task.httpStatus).toBe(401);
      });
      httpBackend.flush();
      expect(updateRan).toBe(true);
    });

    it('should delete the pins associated with an account', function () {
      httpBackend.expectPOST(mockEndpoints.updateCredentials).respond(200, testAccount);
      var deletePinRequest = updateCredentialsFactory.deletePins();

      deletePinRequest.$promise.then(function (task) {
        expect(task.status).toEqual(testAccount.status);
        updateRan = true;
      });
      httpBackend.flush();
      expect(updateRan).toBe(true);
    });

    it('should fail to delete the pins associated with an account', function () {
      httpBackend.expectPOST(mockEndpoints.updateCredentials).respond(404);
      var deletePinRequest = updateCredentialsFactory.deletePins();

      deletePinRequest.$promise.then(function success() {}, function error(task) {
        updateRan = true;
        expect(task.httpStatus).toBe(404);
      });
      httpBackend.flush();
      expect(updateRan).toBe(true);
    });
  });
});
