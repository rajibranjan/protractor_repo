'use strict';

describe('Factory: UserService', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.webservices-v2'));

  var userService, httpBackend;

  var selfUserData = {
    email: 'users-details-test@eggnogcookie.com',
    firstName: 'Rabbit',
    lastName: 'IsAwesome',
    status: 'ACTIVE',
    links: [{
      rel: 'self',
      href: 'https://localhost:8443/HPStoreWeb/api/v2/users/self'
    }]
  };

  var idUserData = angular.copy(selfUserData);
  idUserData.links[0].href = 'https://localhost:8443/HPStoreWeb/api/v2/users/251d9895-a07b-475d-96f9-1c3415dd0467';

  // Initialize the factory and a mock scope
  beforeEach(inject(function ($rootScope, $httpBackend, UserService) {
    httpBackend = $httpBackend;
    userService = UserService;
  }));


  it('Tests the get method for self.', function () {
    // Expect a GET with the URL regex, when received return 200 with mock data.
    httpBackend.expectGET('/api/v2/users/self').respond(200, selfUserData);
    var userPromise = userService.getById('self');

    // Assert that we got back the data we were expecting.
    userPromise.$promise.then(function (data) {
      expect(data.firstName).toBe('Rabbit');
      expect(data.lastName).toBe('IsAwesome');
      expect(data.status).toBe('ACTIVE');
    });
    httpBackend.flush();
  });

  it('Tests the get method for an id.', function () {
    // Expect a GET with the URL regex, when received return 200 with mock data.
    httpBackend.expectGET('/api/v2/users/251d9895-a07b-475d-96f9-1c3415dd0467').respond(200, idUserData);
    var userPromise = userService.getById('251d9895-a07b-475d-96f9-1c3415dd0467');

    // Assert that we got back the data we were expecting.
    userPromise.$promise.then(function (data) {
      expect(data.firstName).toBe('Rabbit');
      expect(data.lastName).toBe('IsAwesome');
      expect(data.status).toBe('ACTIVE');
    });
    httpBackend.flush();
  });

  it('Tests updating a user.', function () {
    httpBackend.expectPUT('/api/v2/users/251d9895-a07b-475d-96f9-1c3415dd0467').respond(200, selfUserData);

    userService.fromExisting(idUserData).save();
    httpBackend.flush();
  });

  it('Tests creating a user.', function () {
    httpBackend.expectPOST('/api/v2/users/251d9895-a07b-475d-96f9-1c3415dd0467').respond(201, selfUserData);

    userService.create(idUserData).save();
    httpBackend.flush();
  });

  it('Tests the delete() method with delete.', function () {
    httpBackend.expectDELETE('/api/v2/users/251d9895-a07b-475d-96f9-1c3415dd0467').respond(200);

    userService.fromExisting(idUserData).delete();
    httpBackend.flush();
  });

});
