'use strict';

describe('Factory: Reseller', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.webservices-v2'));

  var resellerService, httpBackend;

  var testReseller = {
    name: 'Test Reseller',
    customerId: 'cust_ID_1234',
    status: 'ACTIVE',
    state: 'ID',
    phoneNumber: '1234561234',
    paymentMethod: 'CHECK',
    links: [{
      rel: 'self',
      href: 'https://localhost:8443/HPStoreWeb/api/v2/resellers/87edaf0b-e4f0-4303-ac2b-f35fc40480e3'
    }]
  };

  // Initialize the factory and a mock scope
  beforeEach(inject(function ($rootScope, $httpBackend, ResellerService) {
    httpBackend = $httpBackend;
    resellerService = ResellerService;
  }));

  it('Tests the get method for an id.', function () {
    // Expect a GET with the URL regex, when received return 200 with mock data.
    httpBackend.expectGET('/api/v2/resellers/87edaf0b-e4f0-4303-ac2b-f35fc40480e3').respond(200, testReseller);
    var resellerPromise = resellerService.getById('87edaf0b-e4f0-4303-ac2b-f35fc40480e3');

    // Assert that we got back the data we were expecting.
    resellerPromise.$promise.then(function (data) {
      expect(data.name).toBe('Test Reseller');
      expect(data.state).toBe('ID');
      expect(data.status).toBe('ACTIVE');
      expect(data.paymentMethod).toBe('CHECK');
    });
    httpBackend.flush();
  });

  it('Tests the update() method.', function () {
    httpBackend.expectPUT('/api/v2/resellers/87edaf0b-e4f0-4303-ac2b-f35fc40480e3').respond(200, testReseller);

    resellerService.fromExisting(testReseller).save();
    httpBackend.flush();
  });

  it('Tests the save() method.', function () {
    httpBackend.expectPOST('/api/v2/resellers/87edaf0b-e4f0-4303-ac2b-f35fc40480e3').respond(201, testReseller);

    resellerService.create(testReseller).save();
    httpBackend.flush();
  });

  it('Tests the delete() method with delete.', function () {
    httpBackend.expectDELETE('/api/v2/resellers/87edaf0b-e4f0-4303-ac2b-f35fc40480e3').respond(200);

    resellerService.fromExisting(testReseller).delete();
    httpBackend.flush();
  });

});
