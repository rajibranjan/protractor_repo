'use strict';

describe('Home: Testing Modules', function () {

  var mockGlobals = {};
  beforeEach(module(function ($provide) {
    $provide.constant('Globals', mockGlobals);
  }));

  beforeEach(module('unitTestInit'));

  var mainModule, dependencies;
  var homeCtrl, scope, routes, controller;

  // load the controller's module
  beforeEach(module('hp.portal.home.homeApp'));

  beforeEach(inject(function ($controller, $rootScope, $route) {
    scope = $rootScope.$new();
    routes = $route.routes;

    controller = $controller;
    homeCtrl = $controller('HomeCtrl', {
      $scope: scope
    });
  }));

  beforeEach(function () {
    mainModule = angular.module('hp.portal.home.homeApp');
    dependencies = mainModule.requires;
  });

  it('should define permissions on globals', function () {
    expect(mockGlobals.permissions.isHPAdmin).toBe(false);
  });

  it('should be registered', function () {
    expect(mainModule).toBeDefined();
  });

  it('should define its variables', function () {

    expect(scope.self).toBeDefined();
  });

  //test routed, redirected or not found. template is loaded, controller is correct
  it('should have configured routes', function () {

    expect(routes).toBeDefined();

    //console.log('routes.length:', Object.keys(routes).length);
    expect(Object.keys(routes).length > 50);

    angular.forEach(routes, function (routeConfig, route) {

      expect(routeConfig).toBeDefined();
      //either (templateURL and Controller) OR (redirectTo) must be defined
      if (angular.isDefined(routeConfig.redirectTo)) {

        //verify redirect route exists
        expect(routes[routeConfig.redirectTo]).toBeDefined();

        //It should go to /welcome if route(without and ending '/') and redirect  dont match
        //i.e. route='/resellers/' redirects to '/resellers' but /null redirects to /welcome
        var trimmedRoute = route.substring(0, route.length - 1);
        if (trimmedRoute !== routeConfig.redirectTo) {
          expect(routeConfig.redirectTo).toEqual('/welcome');
        }

      }
      else {
        //it needs to have a template and controller
        expect(routeConfig.templateUrl).toBeDefined();
        expect(routeConfig.controller).toBeDefined();
        expect(routeConfig.resolve).toBeDefined();

        validateRouteNames(routeConfig);
      }
    });
  });

  var validateRouteNames = function (routeConfig) {
    var controllerRegex = /[A-Z][a-zA-Z]*Ctrl/;
    var templateUrlRegex = /app\/home\/[a-z][a-zA-Z\-\/]*.html/;

    expect(routeConfig.controller.match(controllerRegex));
    expect(routeConfig.templateUrl.match(templateUrlRegex));
  };

});
