'use strict';

describe('[Module: hp.portal.home.serviceAccounts] Controller: ServiceAccountsCtrl', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.serviceAccounts'));

  var alerts = [];
  var alertTimeouts = {
    short: 3000,
    medium: 5000,
    long: 10000
  };
  beforeEach(module(function ($provide) {
    $provide.constant('PageAlerts', {
      add: function (alert) {
        alerts.push(alert);
      },
      timeout: alertTimeouts
    });
    $provide.constant('Self', mockSelf);
  }));

  var mockAccountId = '042dec32-f7eb-4b27-b840-c82ab721e213';

  var mockSelf = {
    links: [{
      rel: 'account',
      href: 'https://localhost:9443/HPStoreWeb/api/v2/accounts/' + mockAccountId
    }]
  };

  var mockGlobals;

  function MockGlobals() {
    this.permissions = {
      isHPAdmin: false
    };
  }

  var mockPermittedFqNameService;

  function MockPermittedFqNameService() {
    this.get = function (params) {
      var deferred = $q.defer();
      this.params = params;

      deferred.resolve(this.permittedFqNames);

      return deferred.promise;
    };
    this.permittedFqNames = {
      data: [{
        offeringFqName: 'mock-offering'
      }]
    };
  }

  var mockOfferingService;

  function MockOfferingService() {
    this.getAllOfferings = function () {
      var dfd = $q.defer();
      dfd.resolve();
      Object.defineNonEnumerableProperty(this.offerings, '$promise', dfd.promise);
      return this.offerings;
    };

    this.offerings = {
      'mock-offering': {
        abc: '123'
      }
    };
  }

  var mockOfferingServiceAccountsService;

  function MockOfferingServiceAccountsService() {
    var that = this;
    var serviceUser = {
      clone: function () {
        return angular.copy(this);
      },
      $promise: $q.when(serviceUser),
      save: function () {
        that.savedUser(angular.copy(this));

        var dfd = $q.defer();
        if (that.saveShouldResolve) {
          dfd.resolve();
        }
        else {
          dfd.reject({});
        }
        return dfd.promise;
      }
    };

    this.savedUser = function (user) {
      this.savedServiceUser = user;
    };

    this.saveShouldResolve = true;
    this.serviceUser = serviceUser;
    this.getOne = function (params) {
      this.params = params;
      return serviceUser;
    };
  }

  var mockRabbitMQServiceUserService;

  function MockRabbitMQServiceUserService() {
    var that = this;
    var rabbitMQUser = {
      clone: function () {
        return angular.copy(this);
      },
      $promise: $q.when(rabbitMQUser),
      save: function () {
        that.savedUser(angular.copy(this));

        var dfd = $q.defer();
        if (that.saveShouldResolve) {
          dfd.resolve();
        }
        else {
          dfd.reject({});
        }
        return dfd.promise;
      }
    };

    this.savedUser = function (user) {
      this.savedRabbitMQUser = user;
    };

    this.saveShouldResolve = true;
    this.rabbitMQUser = rabbitMQUser;
    this.getOne = function (params) {
      this.params = params;
      return rabbitMQUser;
    };
  }

  var initController = function () {
    ServiceAccountsCtrl = controllerLoader('ServiceAccountsCtrl', {
      $scope: $scope,
      Globals: mockGlobals,
      OfferingServiceAccountsService: mockOfferingServiceAccountsService,
      RabbitMQServiceUserService: mockRabbitMQServiceUserService,
      PermittedFqNameService: mockPermittedFqNameService,
      OfferingService: mockOfferingService
    });
  };

  var ServiceAccountsCtrl, $scope, $q, $timeout, broadcastHandler, controllerLoader;
  beforeEach(inject(function ($rootScope, $controller, _$q_, _$timeout_) {
    $q = _$q_;
    $timeout = _$timeout_;
    controllerLoader = $controller;
    $scope = $rootScope.$new();
    $scope.$broadcast = function (eventName, method) {
      broadcastHandler(eventName, method);
    };

    $scope.$digest();
    alerts = [];

    mockOfferingServiceAccountsService = new MockOfferingServiceAccountsService();
    mockRabbitMQServiceUserService = new MockRabbitMQServiceUserService();
    mockPermittedFqNameService = new MockPermittedFqNameService();
    mockOfferingService = new MockOfferingService();
    mockGlobals = new MockGlobals();

    initController();
  }));

  describe('$scope.selectFqName', function () {

    it('should get the serviceUser and rabbitMQUser', function () {
      var selectedFqName = 'selected-fq-name';
      $scope.selectFqName(selectedFqName);
      $timeout.flush();

      expect(mockOfferingServiceAccountsService.params).toEqual({
        qs: 'offeringFqName==' + selectedFqName
      });

      expect(mockRabbitMQServiceUserService.params).toEqual({
        qs: 'offeringFqName==' + selectedFqName
      });

      // The $scope.serviceUser should be a copy/clone of the object returned from the OfferingServiceAccountsService.getOne call.
      expect($scope.serviceUser).not.toBe(mockOfferingServiceAccountsService.serviceUser);
      expect($scope.serviceUser).toEqual(mockOfferingServiceAccountsService.serviceUser);

      // The $scope.rabbitMQUser should be a copy/clone of the object returned from the RabbitMQServiceUserService.getOne call.
      expect($scope.rabbitMQUser).not.toBe(mockRabbitMQServiceUserService.rabbitMQUser);
      expect($scope.rabbitMQUser).toEqual(mockRabbitMQServiceUserService.rabbitMQUser);
    });
  });

  describe('scope initialization', function () {

    it('should populate the list of fqNames for Offering Admin', function () {

      expect($scope.fqNames).toBeUndefined();

      $timeout.flush();

      expect($scope.fqNames).toBeDefined();

      expect($scope.fqNames.length).toBe(mockPermittedFqNameService.permittedFqNames.data.length);
      mockPermittedFqNameService.permittedFqNames.data.forEach(function (permittedFqName, index) {
        expect($scope.fqNames[index]).toBe(permittedFqName.offeringFqName);
      });

    });

    it('should populate the list of fqNames for HPAdmin', function () {

      mockGlobals.permissions.isHPAdmin = true;
      initController();
      $timeout.flush();

      expect($scope.fqNames.length).toBe(Object.keys(mockOfferingService.offerings).length);
      Object.keys(mockOfferingService.offerings).forEach(function (fqName, index) {
        expect($scope.fqNames[index]).toBe(fqName);
      });
    });

  });

  describe('$scope.changeServiceAccountPassword', function () {

    it('should change the service account password', function () {
      broadcastHandler = function (eventName, method) {
        expect(eventName).toBe('hpChangeServiceAccountPassword');

        $scope.serviceUser.loginPassword = 'current-password';
        $scope.serviceUser.password = 'new-password';
        $scope.accordionStatus.showServiceAccountForm = true;

        method();
        $timeout.flush();

        expect(mockOfferingServiceAccountsService.savedServiceUser.loginPassword).toBe('current-password');
        expect(mockOfferingServiceAccountsService.savedServiceUser.password).toBe('new-password');

        expect($scope.serviceUser.loginPassword).toBeUndefined();
        expect($scope.serviceUser.password).toBeUndefined();
        expect($scope.accordionStatus.showServiceAccountForm).toBe(false);

        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cPasswordSuccessChange');
        expect(alerts[0].timeout).toBe(alertTimeouts.medium);
      };

      var selectedFqName = 'selected-fq-name';
      $scope.selectFqName(selectedFqName);
      $timeout.flush();

      $scope.changeServiceAccountPasswordSubmit();
    });

    it('should display error message when the user entered the wrong login password', function () {
      broadcastHandler = function (eventName, method) {
        expect(eventName).toBe('hpChangeServiceAccountPassword');

        mockOfferingServiceAccountsService.saveShouldResolve = false;
        $scope.accordionStatus.showServiceAccountForm = true;

        method();
        $timeout.flush();

        expect($scope.accordionStatus.showServiceAccountForm).toBe(true);
        expect($scope.serviceUser).toEqual(mockOfferingServiceAccountsService.serviceUser);

        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
        expect(alerts[0].msg).toBe('cPasswordError');
        expect(alerts[0].timeout).toBe(alertTimeouts.long);
      };

      var selectedFqName = 'selected-fq-name';
      $scope.selectFqName(selectedFqName);
      $timeout.flush();

      $scope.changeServiceAccountPasswordSubmit();
    });

  });

  describe('$scope.changeRabbitMQPassword', function () {

    it('should change the rabbitMQ user password', function () {
      broadcastHandler = function (eventName, method) {
        expect(eventName).toBe('hpChangeRabbitMQPassword');

        $scope.rabbitMQUser.loginPassword = 'current-password';
        $scope.rabbitMQUser.password = 'new-password';
        $scope.accordionStatus.showRabbitMQForm = true;

        method();
        $timeout.flush();

        expect(mockRabbitMQServiceUserService.savedRabbitMQUser.loginPassword).toBe('current-password');
        expect(mockRabbitMQServiceUserService.savedRabbitMQUser.password).toBe('new-password');

        expect($scope.rabbitMQUser.loginPassword).toBeUndefined();
        expect($scope.rabbitMQUser.password).toBeUndefined();
        expect($scope.accordionStatus.showRabbitMQForm).toBe(false);

        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cPasswordSuccessChange');
        expect(alerts[0].timeout).toBe(alertTimeouts.medium);
      };

      var selectedFqName = 'selected-fq-name';
      $scope.selectFqName(selectedFqName);
      $timeout.flush();

      $scope.changeRabbitMQPasswordSubmit();
    });

    it('should display error message when the user entered the wrong login password', function () {
      broadcastHandler = function (eventName, method) {
        expect(eventName).toBe('hpChangeRabbitMQPassword');

        mockRabbitMQServiceUserService.saveShouldResolve = false;
        $scope.accordionStatus.showRabbitMQForm = true;

        method();
        $timeout.flush();

        expect($scope.accordionStatus.showRabbitMQForm).toBe(true);
        expect($scope.rabbitMQUser).toEqual(mockRabbitMQServiceUserService.rabbitMQUser);

        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
        expect(alerts[0].msg).toBe('cPasswordError');
        expect(alerts[0].timeout).toBe(alertTimeouts.long);
      };

      var selectedFqName = 'selected-fq-name';
      $scope.selectFqName(selectedFqName);
      $timeout.flush();

      $scope.changeRabbitMQPasswordSubmit();
    });

  });

});
