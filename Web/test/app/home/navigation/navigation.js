'use strict';

describe('[Module: hp.portal.home.navigation]', function () {

  var mockSelf = {
    $promise: null,
    roles: ['ROLE_HPADMIN', 'ROLE_COMPANY_ADMIN']
  };

  var mockGlobals = {
    permissions: {
      isHPAdmin: true,
      isReseller: false,
      isCompanyAdmin: true,
      isUser: false
    }
  };

  beforeEach(module(function ($provide) {
    $provide.constant('Self', mockSelf);
    $provide.constant('Globals', mockGlobals);
  }));

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.navigation'));

  var httpBackend, q, timeout, linkParser;
  beforeEach(inject(function ($httpBackend, $q, $timeout, LinkParser) {
    httpBackend = $httpBackend;
    q = $q;
    timeout = $timeout;
    linkParser = LinkParser;
  }));

  var mockRoutes = {
    welcome: '/welcome',
    dashboard: '/dashboard',
    users: '/users',
    addUser: '/addUser'
  };

  var mockPath = '';

  var mockRoute = {
    current: {
      $$route: {
        get originalPath() {
          return mockPath;
        }
      }
    }
  };

  describe('Controller: NavCtrl', function () {

    var scope, NavCtrl, location;

    beforeEach(inject(function ($rootScope, $controller, $location, $timeout) {
      mockSelf.$promise = q.resolve(mockSelf);
      scope = $rootScope.$new();
      location = $location;
      NavCtrl = $controller('NavCtrl', {
        $scope: scope,
        $route: mockRoute,
        Routes: mockRoutes
      });

      expect(scope.showSidebar()).toBe(false);
      expect(scope.currentView()).toEqual({});

      $rootScope.$apply();
      $timeout.flush();
    }));

    it('should only contain views with defined route paths', function () {
      var viewsKeys = Object.keys(scope.views);
      expect(viewsKeys.length).toBe(4);

      angular.forEach(mockRoutes, function (path) {
        expect(viewsKeys).toContain(path);
        var view = scope.views[path];
        if (view.nav) {
          expect(view.nav.url).toBe('#' + path);
        }
        else if (path !== mockRoutes.welcome) {
          expect(view.parent).toBeDefined();
        }
      });
    });

    it('should populate the scope with unique nav items', function () {
      expect(scope.navs.length).toBe(2);

      expect(scope.navs).toContain(scope.views[mockRoutes.dashboard].nav);
      expect(scope.navs).toContain(scope.views[mockRoutes.users].nav);
    });

    it('should properly update the nav', function () {
      mockPath = mockRoutes.dashboard;
      expect(scope.currentView()).toBe(scope.views[mockRoutes.dashboard]);
      expect(scope.showSidebar()).toBe(true);
      expect(scope.navIsActive(scope.views[mockRoutes.dashboard].nav)).toBe(true);
      expect(scope.navIsActive(scope.views[mockRoutes.users].nav)).toBe(false);

      mockPath = mockRoutes.welcome;
      expect(scope.currentView()).toBe(scope.views[mockRoutes.welcome]);
      expect(scope.showSidebar()).toBe(false);
      expect(scope.navIsActive(scope.views[mockRoutes.dashboard].nav)).toBe(false);
      expect(scope.navIsActive(scope.views[mockRoutes.users].nav)).toBe(false);

      mockPath = mockRoutes.addUser;
      expect(scope.currentView()).toBe(scope.views[mockRoutes.addUser]);
      expect(scope.showSidebar()).toBe(true);
      expect(scope.navIsActive(scope.views[mockRoutes.dashboard].nav)).toBe(false);
      expect(scope.navIsActive(scope.views[mockRoutes.users].nav)).toBe(true);
    });

  });
});
