'use strict';

describe('[Module: hp.portal.home.users.userStatus]', function () {

  beforeEach(module('hp.portal.home.users.userStatus'));

  describe('UserStatus', function () {

    var TestUserStatus, scope;

    beforeEach(inject(function ($rootScope, UserStatus) {
      scope = $rootScope.$new();
      TestUserStatus = UserStatus;
    }));

    it('should initialize constants on module', function () {
      expect(TestUserStatus.CREATED).toBe('CREATED');
      expect(TestUserStatus.ACTIVE).toBe('ACTIVE');
      expect(TestUserStatus.SUSPENDED).toBe('SUSPENDED');
    });
  });
});
