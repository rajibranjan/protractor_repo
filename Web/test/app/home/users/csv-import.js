'use strict';

describe('[Module: hp.portal.home.users.csvImport]', function () {

  var alerts;
  var removedAlerts;
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.users.csvImport'));
  beforeEach(module('angularFileUpload'));

  var pageAlerts = {
    add: function (alert) {
      return alerts.push(alert);
    },
    remove: function (alert) {
      removedAlerts.push(alert);
    },
    timeout: {
      short: 3000,
      medium: 5000,
      long: 10000
    }
  };

  var mockUploader;

  var testItem = {
    upload: function () {
      this.uploadCall = true;
      this.testDocument = 'documentUploaded';
    },
    uploadCall: false,
    testDocument: 'documentNotUploaded'
  };

  var testResponseOk = 200;
  var testResponseFourHundredError = 400;
  var testResponseConstraintError = 409;
  var testResponseUnknownError = 403;
  var testResponseUnauthorizedError = 401;

  var testUrls = {
    manageUsers: function () {
      return '/manage-users';
    }
  };

  var csvUploadStatus;
  var q, timeout;
  beforeEach(inject(function ($q, $timeout) {
    q = $q;
    timeout = $timeout;
  }));

  var mockUserCSVUpload = {
    getExisting: function () {
      return {
        waitForComplete: function () {
          var dfd = q.defer();

          var response = {
            status: csvUploadStatus
          };
          if (csvUploadStatus === 'COMPLETED' || csvUploadStatus === 'FAILED') {
            dfd.resolve(response);
          }
          else {
            dfd.reject(response);
          }

          return dfd.promise;
        }
      };
    }
  };

  var mockWindow = {
    location: {
      href: 'Test value'
    }
  };

  var mockPortalConfigurations = {
    $promise: {
      then: function (success, failure) {
        var deferred = q.defer();
        deferred.resolve();
        return deferred.promise.then(success, failure);
      }
    }
  };

  describe('CsvImportCtrl', function () {

    var TestCtrl, scope, location;

    beforeEach(inject(function ($rootScope, $controller, $location) {
      scope = $rootScope.$new();
      location = $location;
      mockPortalConfigurations['com.portal.logoutUrl'] = 'logout';
      testItem.uploadCall = false;
      testItem.testDocument = 'documentNotUploaded';
      alerts = [];
      removedAlerts = [];

      TestCtrl = $controller('CsvImportCtrl', {
        $scope: scope,
        PageAlerts: pageAlerts,
        UserCSVUpload: mockUserCSVUpload,
        PortalConfigurations: mockPortalConfigurations,
        Urls: testUrls,
        $window: mockWindow
      });
      mockUploader = scope.uploader;
      csvUploadStatus = 'COMPLETED';

      expect(testItem.uploadCall).toBe(false);
      expect(testItem.testDocument).toBe('documentNotUploaded');

    }));

    it('should return a PageAlert with success', function () {
      mockUploader.onAfterAddingFile(testItem);
      scope.uploadFile();
      expect(testItem.uploadCall).toBe(true);
      expect(testItem.testDocument).toBe('documentUploaded');
      mockUploader.onCompleteItem(testItem, 'test response', testResponseOk);
      scope.$digest();
      expect(alerts.length).toBe(2);
      expect(alerts[0].type).toBe('info');
      expect(alerts[0].msg).toBe('cCSVUploadSuccess');
      expect(alerts[0].timeout).toBe(pageAlerts.timeout.long);
      expect(alerts[1].type).toBe('success');
      expect(alerts[1].msg).toBe('cAddUsersSuccess');
      expect(alerts[1].timeout).toBe(pageAlerts.timeout.medium);
      expect(location.url()).toBe(testUrls.manageUsers());

    });

    it('should return a PageAlert with warning and not added users', function () {
      csvUploadStatus = 'FAILED';
      mockUploader.onAfterAddingFile(testItem);
      scope.uploadFile();
      expect(testItem.uploadCall).toBe(true);
      expect(testItem.testDocument).toBe('documentUploaded');
      mockUploader.onCompleteItem(testItem, 'test response', testResponseOk);
      scope.$digest();
      expect(alerts.length).toBe(2);
      expect(alerts[1].type).toBe('warning');
      expect(alerts[1].templateUrl).toBe('csv-import-error.html');
      expect(alerts[1].timeout).toBe(pageAlerts.timeout.long);
    });

    it('should return a PageAlert with an error code', function () {
      csvUploadStatus = 'UNKNOWN';
      mockUploader.onAfterAddingFile(testItem);
      scope.uploadFile();
      expect(testItem.uploadCall).toBe(true);
      expect(testItem.testDocument).toBe('documentUploaded');
      mockUploader.onCompleteItem(testItem, 'test response', testResponseOk);
      scope.$digest();
      expect(alerts.length).toBe(2);
      expect(alerts[1].type).toBe('danger');
      expect(alerts[1].msg).toBe('cCSVImportError');
      expect(alerts[1].timeout).toBe(pageAlerts.timeout.long);
    });

    it('should return a PageAlert with a parse error', function () {
      mockUploader.onAfterAddingFile(testItem);
      scope.uploadFile();
      expect(testItem.uploadCall).toBe(true);
      expect(testItem.testDocument).toBe('documentUploaded');
      mockUploader.onCompleteItem(testItem, 'test response', testResponseFourHundredError);
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('danger');
      expect(alerts[0].msg).toBe('cCSVImportParseError');
      expect(alerts[0].timeout).toBe(pageAlerts.timeout.long);
    });

    it('should return a PageAlert with a constraint error', function () {
      mockUploader.onAfterAddingFile(testItem);
      scope.uploadFile();
      expect(testItem.uploadCall).toBe(true);
      expect(testItem.testDocument).toBe('documentUploaded');
      mockUploader.onCompleteItem(testItem, 'test response', testResponseConstraintError);
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('danger');
      expect(alerts[0].msg).toBe('cCSVImportConstraintError');
      expect(alerts[0].timeout).toBe(pageAlerts.timeout.long);
    });

    it('should return a PageAlert with an unknown error', function () {
      mockUploader.onAfterAddingFile(testItem);
      scope.uploadFile();
      expect(testItem.uploadCall).toBe(true);
      expect(testItem.testDocument).toBe('documentUploaded');
      mockUploader.onCompleteItem(testItem, 'test response', testResponseUnknownError);
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('danger');
      expect(alerts[0].msg).toBe('cCSVImportError');
      expect(alerts[0].timeout).toBe(pageAlerts.timeout.long);
    });

    it('should redirect to login when unauthorized', function () {
      mockUploader.onAfterAddingFile(testItem);
      scope.uploadFile();
      expect(testItem.uploadCall).toBe(true);
      expect(testItem.testDocument).toBe('documentUploaded');
      mockUploader.onCompleteItem(testItem, 'test response', testResponseUnauthorizedError);
      expect(mockWindow.location.href).toBe('logout');
      expect(scope.isImportingUsers).toBe(false);
    });

    it('should return a PageAlert with an unknown error for an undefined response', function () {
      var resp;
      mockUploader.onAfterAddingFile(testItem);
      scope.uploadFile();
      expect(testItem.uploadCall).toBe(true);
      expect(testItem.testDocument).toBe('documentUploaded');
      mockUploader.onCompleteItem(testItem, resp, testResponseOk);
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('danger');
      expect(alerts[0].msg).toBe('cCSVImportError');
      expect(alerts[0].timeout).toBe(pageAlerts.timeout.long);
    });
  });
});
