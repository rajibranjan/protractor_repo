'use strict';

describe('[Module: hp.portal.home.accounts.accountStatus]', function () {

  beforeEach(module('hp.portal.home.accounts.accountStatus'));

  describe('AccountStatus', function () {

    var TestAccountStatus, scope;

    beforeEach(inject(function ($rootScope, AccountStatus) {
      scope = $rootScope.$new();
      TestAccountStatus = AccountStatus;
    }));

    it('should initialize constants on module', function () {
      expect(TestAccountStatus.CREATED).toBe('CREATED');
      expect(TestAccountStatus.ACTIVE).toBe('ACTIVE');
      expect(TestAccountStatus.SUSPENDED).toBe('SUSPENDED');
      expect(TestAccountStatus.CANCELLED).toBe('CANCELLED');
    });
  });
});
