'use strict';

describe('[Module: hp.portal.home.accountsV2]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.accountsV2'));

  describe('controller:AccountsCtrl-v2', function () {

    var scope, $q, AccountsCtrlV2, controller, mockLinkParser, httpBackend, mockAccountService, mockLicenseService, mockAccountPager, mockRouteParams, accountsJson, licenseJson;

    accountsJson = {
      'limit': 1,
      'offset': 0,
      'total': 1,
      'data': [{
        'tenantId': '25020aab-ff98-4cb0-90dd-15361af9',
        'adminContact': {
          'email': 'admin1@eggnogcookie.com',
          'firstName': 'CompanyAdmin1First',
          'lastName': 'CompanyAdmin1Last',
          'status': 'ACTIVE',
          'accountId': '25020aab-ff98-4cb0-90dd-15361af903cb',
          'phoneNumber': '1-208-396-6000',
          'phoneExtension': null,
        },
        'name': 'Company #1',
        'activatedDate': '2014-03-25T16:29:51.000+0000',
        'status': 'ACTIVE',
        'addressLine': '4268, N Spruce Meadow Avenue',
        'addressLine2': 'North Five Mile Road',
        'city': 'Boise',
        'state': 'ID',
        'country': 'US',
        'zipcode': '83701',
        'phoneNumber': '2081234567',
        'phoneExtension': '123',
        'customerId': 'Abc123'
      }]
    };

    licenseJson = {
      'limit': 50,
      'offset': 0,
      'total': 1,
      'data': [{
        'startDate': '2015-10-28T17:04:53.000+0000',
        'endDate': '9999-01-01T00:00:00.000+0000',
        'quantity': 5,
        'status': 'ACTIVE',
        'isTrial': false,
        'offering': {
          'fqName': 'hp-scansend',
          'title': 'cTitle'
        }
      }]
    };

    mockAccountService = {
      getPager: function (opts) {
        mockAccountPager.opts = opts;
        mockAccountPager.list = accountsJson.data;
        return mockAccountPager;
      }
    };

    mockLicenseService = {
      get: function (params) {
        var dfd = $q.defer();
        mockLicenseService.params = params;
        if (params !== undefined && params.qs !== undefined && params.expand === 'offering' && params.qs.indexOf('account.id==') >= 0) {
          mockLicenseService.getShouldResolve = true;
        }
        else {
          mockLicenseService.getShouldResolve = false;
        }
        if (mockLicenseService.getShouldResolve) {
          dfd.resolve(licenseJson);
        }
        else {
          dfd.reject();
        }
        return dfd.promise;
      }
    };

    mockAccountPager = {
      getItemsPerPage: function () {
        return this.opts.itemsPerPage;
      },
      loadPage: function () {},
      searchBy: function () {
        return [];
      },
      list: {}
    };
    mockRouteParams = {
      resellerId: '25020aab-ff98-4cb0-90dd-15361af903cb',
    };

    var initController = function () {
      AccountsCtrlV2 = controller('AccountsCtrl-v2', {
        $scope: scope,
        LicenseService: mockLicenseService,
        AccountService: mockAccountService,
        $routeParams: mockRouteParams
      });
    };

    beforeEach(inject(function ($rootScope, $controller, LinkParser, $httpBackend, _$q_) {
      scope = $rootScope.$new();
      controller = $controller;
      mockLinkParser = LinkParser;
      httpBackend = $httpBackend;
      $q = _$q_;

      initController();
      scope.$apply();
    }));

    it('should set expected items on the scope', function () {
      expect(scope.accounts).toBeDefined();
      expect(scope.search).toBeDefined();
      expect(scope.accounts.list).toBeDefined();
      expect(scope.accounts.loadPage).toBeDefined();
      expect(scope.accounts.searchBy).toBeDefined();
      expect(scope.accounts.list[0][
        ['address']
      ]).toBeDefined();
    });

    it('should set accounts as expected', function () {
      expect(scope.accounts).toBe(mockAccountPager);
      expect(scope.accounts.list).toBe(mockAccountPager.list);
      expect(scope.accounts.list.length).toEqual(1);
    });

    it('should return list which contain searchString', function () {
      scope.search('mark');
      expect(scope.accounts.searchBy()).toEqual([]);
    });
  });
});
