'use strict';

describe('[Module: hp.portal.home.accounts.addProductToAccountModal]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.accounts.addProductToAccountModal'));

  describe('Factory: AddProductToAccountModal', function () {
    beforeEach(module(function ($provide) {
      $provide.constant('$uibModal', {
        open: function (params) {
          return {
            result: {
              params: params
            }
          };
        }
      });
    }));

    var AddProductToAccountModal;
    beforeEach(inject(function (_AddProductToAccountModal_) {
      AddProductToAccountModal = _AddProductToAccountModal_;
    }));

    describe('.open(data)', function () {
      it('should open the $uibModal with the correct params', function () {
        var products = 'products';
        var enrollmentInfo = 'selected-product';

        var result = AddProductToAccountModal.open({
          products: products,
          enrollmentInfo: enrollmentInfo
        });

        expect(result).toBeDefined();
        expect(result.params).toBeDefined();
        var params = result.params;
        var resolve = params.resolve;
        expect(resolve.products()).toBe(products);
        expect(resolve.enrollmentInfo).toBe(enrollmentInfo);

        delete params.resolve;
        expect(params).toEqual({
          templateUrl: 'app/home/accounts/add-product-to-account-modal.html',
          controller: 'AddProductToAccountModalCtrl',
          backdrop: 'static'
        });
      });
    });

  });


  describe('Controller: AddProductToAccountModalCtrl', function () {

    var $scope, $controller, rootScope, AddProductToAccountModalCtrl, mockEnrollmentInfo, randomString;

    var mockUibModalInstance = {
      dismiss: function (dismissResult) {
        this.dismissed = true;
        this.dismissResult = dismissResult;
      },
      close: function (result) {
        this.closed = true;
        this.result = result;
      }
    };

    var mockProducts = [{
      name: 'first-product',
      offering: {
        fqName: 'first-offering'
      }
    }, {
      name: 'second-product',
      offering: {
        fqName: 'second-offering'
      }
    }];

    var mockI18n = {
      getBySource: function (source) {
        return 'i18n-' + source;
      }
    };

    function loadController(mockEnrollmentInfo) {
      $scope = rootScope.$new();
      AddProductToAccountModalCtrl = $controller('AddProductToAccountModalCtrl', {
        $scope: $scope,
        $uibModalInstance: mockUibModalInstance,
        products: mockProducts,
        enrollmentInfo: mockEnrollmentInfo,
        i18n: mockI18n
      });
      return AddProductToAccountModalCtrl;
    }

    var additionalFields = [{
      name: 'Number of Devices',
      id: 'additional-field-1'
    }, {
      name: 'Contact Name',
      id: 'additional-field-2'
    }];
    var additionalFieldValues;

    function createMockEnrollmentInfo() {
      var enrollmentInfo = {
        product: {
          maxEntitlementAllowed: 5,
          additionalFields: [{
            name: additionalFields[0].name,
            links: [{
              rel: 'self',
              href: 'api/v2/licenseAdditionalFields/' + additionalFields[0].id
            }]
          }, {
            name: additionalFields[1].name,
            links: [{
              rel: 'self',
              href: 'api/v2/licenseAdditionalFields/' + additionalFields[1].id
            }]
          }],
          supportedMeteringInfo: [{
            i18n: 'cNumberOfDevice',
            links: [{
              rel: 'productAdditionalField',
              href: 'api/v2/productAdditionalFields/abc123'
            }]
          }, {
            i18n: 'cNumberOfUser',
            links: [{
              rel: 'productAdditionalField',
              href: 'api/v2/productAdditionalFields/abc456'
            }]
          }],
          links: [{
            rel: 'self',
            href: 'api/v2/products/productId'
          }]
        }
      };

      additionalFieldValues = [randomString(12), randomString(12)];
      enrollmentInfo.additionalFieldValues = {};
      enrollmentInfo.additionalFieldValues[additionalFields[0].name] = additionalFieldValues[0];
      enrollmentInfo.additionalFieldValues[additionalFields[1].name] = additionalFieldValues[1];
      enrollmentInfo.licenseEndDate = new Date();

      return enrollmentInfo;

    }

    beforeEach(inject(function ($rootScope, _$controller_, _randomString_) {
      $controller = _$controller_;
      rootScope = $rootScope;
      randomString = _randomString_;

      // Reset Mocks
      mockUibModalInstance.dismissed = false;
      mockUibModalInstance.closed = false;
      delete mockUibModalInstance.result;
      delete mockUibModalInstance.dismissResult;

    }));



    describe('initialization', function () {
      it('should set all of the necessary $scope fields with enrollmentInfo', function () {
        mockEnrollmentInfo = createMockEnrollmentInfo();

        // Load the controller
        AddProductToAccountModalCtrl = loadController(mockEnrollmentInfo);
        expect($scope.enrollmentInfo).toEqual(mockEnrollmentInfo);
        expect($scope.date).toBeDefined();
        expect($scope.date).toEqual(jasmine.any(Date));
        expect($scope.products).toBeUndefined();
        expect($scope.isEditing).toBe(true);
        expect($scope.save).toBeFunction();
        expect($scope.cancel).toBeFunction();
        expect($scope.delete).toBeFunction();
        expect($scope.bindings).toEqual({
          addProductForm: {}
        });
        expect($scope.meteringInfos).toBeDefined();
        expect($scope.meteringInfos.length).toEqual(2);
      });

      it('should set all of the necessary $scope fields without enrollmentInfo', function () {
        mockEnrollmentInfo = undefined;

        // Load the controller
        AddProductToAccountModalCtrl = loadController(mockEnrollmentInfo);
        expect($scope.date).toBeDefined();
        expect($scope.date).toEqual(jasmine.any(Date));
        expect($scope.products).toBeDefined();
        expect($scope.products.length).toBe(mockProducts.length);
        expect($scope.products[0].product).toBe(mockProducts[0]);
        expect($scope.products[0].i18n).toBe('i18n-' + mockProducts[0].offering.fqName);
        expect($scope.products[1].product).toBe(mockProducts[1]);
        expect($scope.products[1].i18n).toBe('i18n-' + mockProducts[1].offering.fqName);
        expect($scope.isEditing).toBe(false);
        expect($scope.save).toBeFunction();
        expect($scope.cancel).toBeFunction();
        expect($scope.delete).toBeFunction();
        expect($scope.bindings).toEqual({
          addProductForm: {}
        });
        expect($scope.meteringInfos).toBeUndefined();
      });
    });

    describe('$scope.save()', function () {
      it('should close the modal', function () {
        expect(mockUibModalInstance.closed).toBe(false);
        $scope.bindings.addProductForm.$dirty = true;
        $scope.save();
        expect(mockUibModalInstance.closed).toBe(true);
      });

      it('should dismiss the modal if nothing is dirty', function () {
        mockEnrollmentInfo = createMockEnrollmentInfo();

        // Load the controller
        AddProductToAccountModalCtrl = loadController(mockEnrollmentInfo);
        expect(mockUibModalInstance.dismissed).toBe(false);
        $scope.save();
        expect(mockUibModalInstance.dismissed).toBe(true);
      });

      it('should NOT close the modal if the form is $invalid', function () {
        expect(mockUibModalInstance.closed).toBe(false);
        $scope.bindings.addProductForm.$invalid = true;

        $scope.save();

        expect(mockUibModalInstance.closed).toBe(false);
      });

      it('should broadcast the hpValidateAddProductForm event even if the form is $invalid', function () {
        var eventWasBroadcast = false;
        $scope.$on('hpValidateAddProductForm', function () {
          eventWasBroadcast = true;
        });
        $scope.bindings.addProductForm.$invalid = true;

        $scope.save();

        expect(eventWasBroadcast).toBe(true);
      });

      it('should broadcast the hpValidateAddProductForm event', function () {
        var eventWasBroadcast = false;
        $scope.$on('hpValidateAddProductForm', function () {
          eventWasBroadcast = true;
        });

        $scope.save();

        expect(eventWasBroadcast).toBe(true);
      });

      it('should return the enrollmentInfo from the modal dialog', function () {
        mockEnrollmentInfo = createMockEnrollmentInfo();

        // Load the controller
        AddProductToAccountModalCtrl = loadController(mockEnrollmentInfo);
        expect(mockUibModalInstance.closed).toBe(false);
        expect(mockUibModalInstance.result).toBeUndefined();

        $scope.enrollmentInfo = createMockEnrollmentInfo();
        $scope.bindings.addProductForm.$dirty = true;

        $scope.save();

        expect(mockUibModalInstance.closed).toBe(true);
        expect(mockUibModalInstance.result).toBeDefined();
        expect(mockUibModalInstance.result).toBe(mockEnrollmentInfo);
        expect(mockUibModalInstance.result).toEqual($scope.enrollmentInfo);
      });

      it('should save additionalFields', function () {
        mockEnrollmentInfo = createMockEnrollmentInfo();

        // Load the controller
        AddProductToAccountModalCtrl = loadController(mockEnrollmentInfo);
        expect(mockUibModalInstance.closed).toBe(false);

        $scope.enrollmentInfo = createMockEnrollmentInfo();

        expect($scope.enrollmentInfo.licenseAdditionalFields).toBeUndefined();

        $scope.save();

        expect($scope.enrollmentInfo.licenseAdditionalFields).toBeDefined();
        expect($scope.enrollmentInfo.licenseAdditionalFields.length).toBe(2);

        $scope.enrollmentInfo.licenseAdditionalFields.forEach(function (additionalField, index) {
          expect(additionalField.value).toBe(additionalFieldValues[index]);
          expect(additionalField.links).toBeDefined();
          expect(additionalField.links.length).toBe(1);
          expect(additionalField.links[0].rel).toBe('productAdditionalField');
          expect(additionalField.links[0].href).toBe($scope.enrollmentInfo.product.additionalFields[index].links[0].href);
        });
      });
    });

    describe('$scope.cancel()', function () {
      it('should dismiss the uibModalInstance', function () {
        expect(mockUibModalInstance.dismissed).toBe(false);
        $scope.cancel();
        expect(mockUibModalInstance.dismissed).toBe(true);
      });

      it('should close the uibModalInstance with a result object', function () {
        expect(mockUibModalInstance.dismissed).toBe(false);
        expect(mockUibModalInstance.dismissResult).toBeUndefined();

        $scope.cancel();

        expect(mockUibModalInstance.dismissed).toBe(true);
        expect(mockUibModalInstance.dismissResult).toBeDefined();
        expect(mockUibModalInstance.dismissResult.operation).toBe('cancel');
      });
    });

    describe('$scope.delete()', function () {
      it('should dismiss the uibModalInstance', function () {
        expect(mockUibModalInstance.dismissed).toBe(false);
        $scope.delete();
        expect(mockUibModalInstance.dismissed).toBe(true);
      });

      it('should close the uibModalInstance with a result object', function () {
        expect(mockUibModalInstance.dismissed).toBe(false);
        expect(mockUibModalInstance.dismissResult).toBeUndefined();

        $scope.delete();

        expect(mockUibModalInstance.dismissed).toBe(true);
        expect(mockUibModalInstance.dismissResult).toBeDefined();
        var dismissResult = mockUibModalInstance.dismissResult;
        expect(dismissResult.operation).toBe('delete');
        expect(dismissResult.enrollmentInfo).toBe(mockEnrollmentInfo);
      });
    });
  });

});
