'use strict';

describe('[Module: deadMessages]', function () {

  var alerts = [];
  beforeEach(module(function ($provide) {
    $provide.constant('PageAlerts', {
      add: function (alert) {
        alerts.push(alert);
      },
      timeout: {
        short: 3000,
        medium: 5000,
        long: 10000
      }
    });
  }));

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.adminDashboard.deadMessages'));

  var httpBackend, q, timeout;
  beforeEach(inject(function ($httpBackend, $q, $timeout) {
    httpBackend = $httpBackend;
    q = $q;
    timeout = $timeout;
  }));

  var mockPromise = function () {
    var deferred = q.defer();
    timeout(function () {
      // This will accept all statusCode's of type 200 and no others.
      if (Math.floor(statusCode / 100) !== 2) {
        deferred.reject({
          responseCode: statusCode
        });
      }
      else {
        deferred.resolve(getMockPager());
      }
    }, 100);
    return deferred.promise;
  };

  var statusCode = 200;

  var getMockMessage = function () {
    return {
      links: [{
        rel: 'self',
        href: 'https://localhost:9000/api/v2/users/042dec32-f7eb-4b27-b840-c82ab721e213'
      }],
      createdBy: 'd265f528-f20c-4529-9e7f-133b75b45941',
      createdDate: '2014-03-25T16:29:47.000+0000',
      lastModifiedBy: '0666df1f-8b05-4b64-8408-9b840ec889f6',
      lastModifiedDate: '2014-03-25T16:29:52.000+0000',
      canonicalName: 'com.hp.ipg.services.queue.payload.AccountPayload',
      queue: 'fooQueue',
      requeue: false,
      payload: '{\"version\":1,\"onBehalfOf\":\"3076a1a7-619f-4442-a15f-7b1b9e6884b8\",\"causality\":\"9MBZCGbI\",\"command\":null,\"accountId\":null}',
      toeTag: 'Exception!',

      save: mockPromise,
      delete: mockPromise
    };
  };

  var numMessages = 3;
  var getMockPager = function () {
    var _isLoading = false,
      _list = [];
    var pager = {
      isLoading: function () {
        return _isLoading;
      },
      refresh: function () {
        pager.list = angular.copy(_list);
        _isLoading = false;
      },
      loadPage: function () {
        _isLoading = true;
        timeout(pager.refresh, 100);
      },
      list: []
    };

    //Produces some identical messages, except href
    for (var i = 0; i < numMessages; i++) {
      var message = getMockMessage();
      message.links.href = 'https://localhost:9000/api/v2/users/' + i;
      _list.push(message);
    }
    return pager;
  };
  var mockDeadMessageService = {
    getPager: getMockPager
  };

  var DeadMessagesCtrl, scope;

  beforeEach(inject(function ($rootScope, $controller, LinkParser) {
    scope = $rootScope.$new();
    alerts = [];
    statusCode = 200;
    DeadMessagesCtrl = $controller('DeadMessagesCtrl', {
      $scope: scope,
      DeadMessageService: mockDeadMessageService,
      LinkParser: LinkParser
    });
    scope.$digest();
    timeout.flush();
  }));

  describe('Controller: DeadMessagesCtrl', function () {
    describe('deleteMessages', function () {
      it('should successfully delete a dead message.', function () {
        scope.deadMessages.list[0].selected = true;
        scope.deleteSelected();
        timeout.flush();
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cSuccessfullyDeletedMessages');
        expect(scope.deadMessages.list[0].selected).toBe(undefined);
        expect(scope.deadMessages.list[0].requeue).toBe(false);
      });
      it('should not successfully delete a dead message.', function () {
        statusCode = 400;
        scope.deadMessages.list[0].selected = true;
        scope.deleteSelected();
        timeout.flush();
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
        expect(alerts[0].msg).toBe('cErrorDeletingMessages');
        expect(scope.deadMessages.list[0].selected).toBe(true);
        expect(scope.deadMessages.list[0].requeue).toBe(false);
      });
      it('should make sure the data loads correctly.', function () {
        scope.deadMessages = getMockPager();
        scope.deadMessages.loadPage();
        expect(scope.deadMessages.list.length).toBe(0);
        expect(scope.deadMessages.isLoading()).toBe(true);
        timeout.flush();
        expect(scope.deadMessages.list.length).toBe(numMessages);
        expect(scope.deadMessages.isLoading()).toBe(false);
      });
    });

    describe('updateMessages', function () {
      it('should successfully update a dead message.', function () {
        scope.deadMessages.list[0].selected = true;
        scope.requeueSelected();
        timeout.flush();
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cSuccessfullyRequeuedMessages');
        for (var i = 0; i < numMessages; i++) {
          expect(scope.deadMessages.list[i].selected).toBe(undefined);
        }
        expect(scope.deadMessages.list[0].requeue).toBe(false);
      });
      it('should not successfully update a dead message.', function () {
        statusCode = 400;
        scope.deadMessages.list[0].selected = true;
        scope.requeueSelected();
        timeout.flush();
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
        expect(alerts[0].msg).toBe('cErrorRequeuingMessages');
        expect(scope.deadMessages.list[0].selected).toBe(true);
        expect(scope.deadMessages.list[0].requeue).toBe(true);
      });
    });

    describe('utility methods', function () {
      it('should properly select/deselect all messages', function () {
        expect(scope.someAreSelected()).toBe(false);
        scope.setAllSelected(true);
        expect(scope.someAreSelected()).toBe(true);
        scope.setAllSelected(false);
        expect(scope.someAreSelected()).toBe(false);
        scope.deadMessages.list[0].selected = true;
        expect(scope.someAreSelected()).toBe(true);
        scope.deadMessages.list[0].selected = false;
        expect(scope.someAreSelected()).toBe(false);
      });
      it('should not do anything if nothing is selected', function () {
        scope.deleteSelected();
        expect(timeout.flush).toThrow();
      });
      it('should successfully change the click property of the object passed in.', function () {
        var deadMessage = {};
        scope.setClicked(deadMessage);
        expect(deadMessage.clicked).toBe(true);
        scope.setClicked(deadMessage);
        expect(deadMessage.clicked).toBe(false);
        scope.setClicked(deadMessage);
        expect(deadMessage.clicked).toBe(true);
      });
      it('should select or deselect all messages.', function () {
        scope.deadMessages = getMockPager();
        scope.setAllSelected(true);
        angular.forEach(scope.deadMessages.list, function (deadMessage) {
          expect(deadMessage.selected).toBe(true);
        });
        scope.setAllSelected(false);
        angular.forEach(scope.deadMessages.list, function (deadMessage) {
          expect(deadMessage.selected).toBe(false);
        });
      });
      it('should get the correct causality for the message if it exists.', function () {
        var message = getMockMessage();
        expect(scope.getCausality(message)).toBe('9MBZCGbI');
        message.payload = '{}';
        expect(scope.getCausality(message)).toBe('9MBZCGbI');
        message.causality = undefined;
        expect(scope.getCausality(message)).toBe(undefined);
        message.payload = '';
        expect(scope.getCausality(message)).toBe('');
      });
      it('should trim the ErrorPayload correctly.', function () {

        var longLine = 'org.springframework.security.access.AccessDeniedException: Access is denied' +
          'at org.springframework.security.access.vote.AffirmativeBased.decide(AffirmativeBased.java:83)' +
          'at org.springframework.security.access.intercept.aspectj.AspectJMethodSecurityInterceptor.invoke(AspectJMethodSecurityInterceptor.java:41)' +
          'at org.springframework.security.access.intercept.aspectj.aspect.AnnotationSecurityAspect.ajc$around$org_springframework_security_access_intercept_aspectj_aspect_AnnotationSecurityAspect$1$c4d57a2b(AnnotationSecurityAspect.aj:63)';

        var result = scope.formatErrorPayload(longLine);
        expect(result.indexOf('\n\t\t') > 0).toBe(true);

        var atString = 'at at at at at at at at at at at at at';
        result = scope.formatErrorPayload(atString, 2);
        expect(result.length).toBe(3);
        expect(result.indexOf('\n\t\t') > 0).toBe(false);
        result = scope.formatErrorPayload(atString);
        expect(result.length).toBe(atString.length);
        expect(result.indexOf('\n\t\t') > 0).toBe(false);

        expect(scope.formatErrorPayload(null)).toBeFalsy();
      });
    });
  });
});
