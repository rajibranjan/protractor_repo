'use strict';

describe('[Module: amqpGlobalConfigurations]', function () {

  var alerts = [];
  beforeEach(module(function ($provide) {
    $provide.constant('PageAlerts', {
      add: function (alert) {
        alerts.push(alert);
      },
      timeout: {
        short: 3000,
        medium: 5000,
        long: 10000
      }
    });
  }));

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.adminDashboard.amqpGlobalConfigurations'));

  var q, timeout;
  beforeEach(inject(function ($q, $timeout) {
    q = $q;
    timeout = $timeout;
  }));

  var mockPromise = function () {
    var deferred = q.defer();
    timeout(function () {
      // This will accept all statusCode's of type 200 and no others.
      if (Math.floor(statusCode / 100) !== 2) {
        deferred.reject({
          responseCode: statusCode
        });
      }
      else {
        deferred.resolve(getMockPager());
      }
    }, 100);
    return deferred.promise;
  };

  var returnOnlyCurrent = false;

  var statusCode = 200;

  var getGlobalConfig = function () {
    return {
      links: [{
        rel: 'self',
        href: 'https://localhost:9000/api/v2/amqpGlobalConfigurations/042dec32-f7eb-4b27-b840-c82ab721e213'
      }],
      createdBy: 'd265f528-f20c-4529-9e7f-133b75b45941',
      createdDate: '2014-03-25T16:29:47.000+0000',
      lastModifiedBy: '0666df1f-8b05-4b64-8408-9b840ec889f6',
      lastModifiedDate: '2014-03-25T16:29:52.000+0000',
      requeue: false,

      save: mockPromise
    };
  };

  var numStacks = 3;
  var currentGitHash = 'currentHash';
  var getMockPager = function (returnCurrent) {
    var _isLoading = false,
      _list = [];
    var pager = {
      isLoading: function () {
        return _isLoading;
      },
      refresh: function () {
        pager.list = angular.copy(_list);
        _isLoading = false;
      },
      loadPage: function () {
        _isLoading = true;
        timeout(pager.refresh, 100);
      },
      list: []
    };
    var config;

    if (returnCurrent === true) {
      config = getGlobalConfig();
      config.gitHash = currentGitHash;
      _list.push(config);
    }
    else {
      //Produces some identical configs, except gitHash
      for (var i = 0; i < numStacks; i++) {
        config = getGlobalConfig();
        config.gitHash = 'fooHash' + i;
        _list.push(config);
      }
    }

    return pager;
  };

  var AMQPGlobalConfigurationsCtrl, scope;

  beforeEach(inject(function ($rootScope, $controller) {
    scope = $rootScope.$new();
    alerts = [];
    statusCode = 200;
    AMQPGlobalConfigurationsCtrl = $controller('AMQPGlobalConfigurationsCtrl', {
      $scope: scope,
      AMQPGlobalConfigurationService: {
        getPager: getMockPager
      },
      currentGitHash: currentGitHash
    });
    timeout.flush();
  }));

  var updateMocks = function () {
    scope.globalConfigs = getMockPager();
    scope.globalConfigs.loadPage();
    timeout.flush();
  };

  describe('Controller: AMQPGlobalConfigurationsCtrl', function () {
    describe('load configData', function () {
      it('should make sure the data loads correctly.', function () {
        scope.globalConfigs = getMockPager();
        scope.globalConfigs.loadPage();
        expect(scope.globalConfigs).toBeDefined();
        expect(scope.globalConfigs.list.length).toBe(0);
        expect(scope.globalConfigs.isLoading()).toBe(true);
        timeout.flush();
        expect(scope.globalConfigs.list.length).toBe(numStacks);
        expect(scope.globalConfigs.isLoading()).toBe(false);
      });
    });

    describe('updateGlobalConfig: enable the selected items', function () {
      it('should not do anything if nothing is selected', function () {
        expect(scope.someHaveEnabledSetTo(false)).toBe(false);
        expect(scope.someHaveEnabledSetTo(true)).toBe(false);
        scope.setSelectedConsumersEnabled(false);
        expect(timeout.flush).toThrow();
      });
      it('should successfully enable the config data. Twice.', function () {
        returnOnlyCurrent = false;
        updateMocks();
        scope.globalConfigs.list[0].selected = true;
        scope.setSelectedConsumersEnabled(true);
        timeout.flush();
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cGlobalConfigUpdateSuccessful');
        for (var i = 0; i < numStacks; i++) {
          expect(scope.globalConfigs.list[i].selected).toBe(undefined);
        }
        expect(scope.globalConfigs.list[0].requeue).toBe(false);

        scope.globalConfigs.list[1].selected = true;
        scope.setSelectedConsumersEnabled(true);
        timeout.flush();
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cGlobalConfigUpdateSuccessful');
        for (i = 0; i < numStacks; i++) {
          expect(scope.globalConfigs.list[i].selected).toBe(undefined);
        }
        expect(scope.globalConfigs.list[0].requeue).toBe(false);
      });
      it('should not successfully enable the config data.', function () {
        statusCode = 400;
        updateMocks();
        scope.globalConfigs.list[0].selected = true;
        scope.setSelectedConsumersEnabled(true);
        timeout.flush();
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('error');
        expect(alerts[0].msg).toBe('cGlobalConfigUpdateError');
        expect(scope.globalConfigs.list[0].selected).toBe(true);
      });
    });

    describe('updateGlobalConfig: disable the selected items', function () {
      it('should successfully disable the config data.', function () {
        scope.globalConfigs.list[0].selected = true;
        scope.setSelectedConsumersEnabled(false);
        timeout.flush();
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cGlobalConfigUpdateSuccessful');
        for (var i = 0; i < numStacks; i++) {
          expect(scope.globalConfigs.list[i].selected).toBe(undefined);
        }
        expect(scope.globalConfigs.list[0].requeue).toBe(false);
      });
      it('should not successfully disable the config data.', function () {
        statusCode = 400;
        updateMocks();
        scope.globalConfigs.list[0].selected = true;
        scope.setSelectedConsumersEnabled(false);
        timeout.flush();
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('error');
        expect(alerts[0].msg).toBe('cGlobalConfigUpdateError');
        expect(scope.globalConfigs.list[0].selected).toBe(true);
      });
    });

    describe('utility methods', function () {
      it('should select or deselect all config objects.', function () {
        scope.setAllSelected(true);
        angular.forEach(scope.globalConfigs.list, function (globalConfig) {
          expect(globalConfig.selected).toBe(true);
        });
        scope.setAllSelected(false);
        angular.forEach(scope.globalConfigs.list, function (globalConfig) {
          expect(globalConfig.selected).toBe(false);
        });
      });
    });
  });
});
