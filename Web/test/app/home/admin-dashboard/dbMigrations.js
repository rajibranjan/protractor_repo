'use strict';

describe('[Module: dbMigrations]', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.adminDashboard.dbMigrations'));

  var q, timeout;
  beforeEach(inject(function ($q, $timeout) {
    timeout = $timeout;
    q = $q;
  }));

  var getMockSchemaVersion = function () {
    return {
      links: [{
        ref: 'self',
        href: 'https://localhost:9000/HPStoreWeb/api/v2/schemaVersions/07fb964c-edf3-402e-9a59-65a30fa0ad00'
      }],
      installedRank: 1,
      version: '1A',
      description: 'des',
      type: 'fooType',
      script: 'script',
      installedOn: 1390337957000,
      success: true,
      migrationType: 'expansion'
    };
  };

  var numExpansions = 3;
  var getMockPager = function () {
    var _isLoading = false,
      _list = [];
    var pager = {
      isLoading: function () {
        return _isLoading;
      },
      refresh: function () {
        pager.list = angular.copy(_list);
        _isLoading = false;
      },
      loadPage: function () {
        _isLoading = true;
        timeout(pager.refresh, 100);
      },
      list: []
    };

    var schemaVersion;
    //Produces some nearly identical schemaVersions.
    for (var i = 0; i < numExpansions; i++) {
      schemaVersion = getMockSchemaVersion();
      _list.push(schemaVersion);
    }
    schemaVersion = getMockSchemaVersion();
    schemaVersion.migrationType = 'contraction';
    _list.push(schemaVersion);
    return pager;
  };

  var DBMigrationsCtrl, scope;

  beforeEach(inject(function ($rootScope, $controller) {
    scope = $rootScope.$new();
    DBMigrationsCtrl = $controller('DBMigrationsCtrl', {
      $scope: scope,
      SchemaVersionService: {
        getPager: getMockPager
      },
    });
    scope.$digest();
  }));

  describe('Controller: DBMigrationsCtrl', function () {
    it('should make sure the data loads correctly.', function () {
      expect(scope.schemaVersions.list.length).toBe(0);
      expect(scope.schemaVersions.isLoading()).toBe(true);
      timeout.flush();
      expect(scope.schemaVersions.list.length).toBe(1 + numExpansions);
      expect(scope.schemaVersions.isLoading()).toBe(false);
    });
  });
});
