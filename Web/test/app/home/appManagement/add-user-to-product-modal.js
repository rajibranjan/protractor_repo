'use strict';

describe('[Module: hp.portal.home.appManagement.addUserToProductModal]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.appManagement.addUserToProductModal'));

  describe('Factory: AddUserToProductModal', function () {
    beforeEach(module(function ($provide) {
      $provide.constant('$uibModal', {
        open: function (params) {
          return {
            result: {
              params: params
            }
          };
        }
      });
    }));

    var AddUserToProductModal;
    beforeEach(inject(function (_AddUserToProductModal_) {
      AddUserToProductModal = _AddUserToProductModal_;
    }));

    describe('.open(data)', function () {
      it('should open the $uibModal with the correct params', function () {
        var selectedOffering = 'selected-offering';
        var productItem = 'product-item';

        var result = AddUserToProductModal.open({
          selectedOffering: selectedOffering,
          productItem: productItem
        });

        expect(result).toBeDefined();
        expect(result.params).toBeDefined();
        var params = result.params;
        var resolve = params.resolve;
        expect(resolve.offering).toBe(selectedOffering);
        expect(resolve.productItem).toBe(productItem);

        delete params.resolve;
        expect(params).toEqual({
          templateUrl: 'app/home/app-management/add-user-to-product-modal.html',
          controller: 'AddUserToProductModalCtrl',
          size: 'lg',
          backdrop: 'static'
        });
      });
    });

  });



  describe('Controller: AddUserToProductModalCtrl', function () {
    var mockSelf = {
      accountId: 'mock-account-id'
    };
    var mockOffering = {
      fqName: 'mock-offering-fq-name'
    };

    function generateMockUser(userId) {
      return {
        email: userId + '@eggnogcookie.com',
        links: [{
          rel: 'self',
          href: '/api/v2/users/' + userId
        }]
      };
    }
    var mockProductItem = {
      product: {
        maxEntitlementAllowed: 5
      },
      license: {},
      userAndEntitlements: [{
        user: generateMockUser('user1')
      }, {
        user: generateMockUser('user2')
      }, {
        user: generateMockUser('user3')
      }]
    };
    var mockUibModalInstance = {
      dismiss: function () {
        this.dismissed = true;
      },
      close: function (result) {
        this.closed = true;
        this.result = result;
      }
    };

    var mockUserService = {
      getPager: function (options) {
        return {
          options: options,
          loadPage: function () {
            mockUserService.userPageLoaded = true;
          }
        };
      }
    };

    var $scope, AddUserToProductModalCtrl;
    beforeEach(inject(function ($rootScope, $controller) {
      $scope = $rootScope.$new();

      // Reset Mocks
      mockUserService.userPageLoaded = false;
      mockUibModalInstance.dismissed = false;
      mockUibModalInstance.closed = false;
      delete mockUibModalInstance.result;

      // Load the controller
      AddUserToProductModalCtrl = $controller('AddUserToProductModalCtrl', {
        $scope: $scope,
        $uibModalInstance: mockUibModalInstance,
        offering: mockOffering,
        productItem: mockProductItem,
        Self: mockSelf,
        UserService: mockUserService,
      });
    }));

    describe('initialization', function () {
      it('should set all of the necessary $scope fields', function () {
        expect($scope.offering).toEqual(mockOffering);
        expect($scope.product).toBe(mockProductItem.product);

        var expectedEntitledUsers = mockProductItem.userAndEntitlements.map(function (userItem) {
          return userItem.user;
        });
        expect($scope.entitledUsers).toEqual(expectedEntitledUsers);

        expect($scope.users).toBeDefined();
        expect($scope.bindings).toEqual({
          confirmSave: false
        });
        expect($scope.save).toBeFunction();
        expect($scope.cancel).toBeFunction();
        expect($scope.confirm).toBeFunction();
        expect($scope.entitledUsersUpdated).toBeFunction();
        expect($scope.toggleUser).toBeFunction();
        expect($scope.maxEntitlementsReached).toBeFunction();

        var expectedIsEntitled = mockProductItem.userAndEntitlements.reduce(function (result, userItem) {
          result[userItem.user.email] = true;
          return result;
        }, {});
        expect($scope.isEntitled).toEqual(expectedIsEntitled);
      });

      it('should get a UserService pager with the expected options', function () {
        expect($scope.users.options).toEqual({
          queryString: 'accountId==' + mockSelf.accountId
        });
        expect(mockUserService.userPageLoaded).toBe(true);
      });
    });

    describe('$scope.save()', function () {
      it('should set items on the $scope', function () {
        expect($scope.addedUsers).toBeUndefined();
        expect($scope.removedUsers).toBeUndefined();
        $scope.save();
        expect($scope.addedUsers).toBeDefined();
        expect($scope.removedUsers).toBeDefined();
      });

      it('should dismiss the modal if no users are added or removed', function () {
        expect(mockUibModalInstance.dismissed).toBe(false);
        expect($scope.bindings.confirmSave).toBe(false);
        $scope.save();
        expect(mockUibModalInstance.dismissed).toBe(true);
        expect($scope.bindings.confirmSave).toBe(false);
      });

      it('should set bindings.confirmSave if some users are added', function () {
        // Add a user to be entitled
        expect($scope.bindings.confirmSave).toBe(false);
        $scope.entitledUsers.push(generateMockUser('user4'));
        $scope.save();
        expect(mockUibModalInstance.dismissed).toBe(false);
        expect($scope.bindings.confirmSave).toBe(true);
      });

      it('should set bindings.confirmSave if some users are removed', function () {
        // Remove an entitled user
        expect($scope.bindings.confirmSave).toBe(false);
        $scope.entitledUsers.pop();
        $scope.save();
        expect(mockUibModalInstance.dismissed).toBe(false);
        expect($scope.bindings.confirmSave).toBe(true);
      });

      it('should set $scope.addedUsers if some users are added', function () {
        expect($scope.addedUsers).toBeUndefined();
        // Add a user to be entitled
        var newUser = generateMockUser('user4');
        $scope.entitledUsers.push(newUser);
        $scope.save();
        expect($scope.addedUsers).toBeDefined();
        expect($scope.addedUsers.length).toBe(1);
        expect($scope.addedUsers[0]).toEqual(newUser);
      });

      it('should set $scope.removedUsers if some users are removed', function () {
        expect($scope.removedUsers).toBeUndefined();
        // Remove an entitled user
        var removedUser = $scope.entitledUsers.pop();
        $scope.save();
        expect($scope.removedUsers).toBeDefined();
        expect($scope.removedUsers.length).toBe(1);
        expect($scope.removedUsers[0].user).toEqual(removedUser);
      });
    });

    describe('$scope.cancel()', function () {
      it('should dismiss the uibModalInstance', function () {
        expect(mockUibModalInstance.dismissed).toBe(false);
        $scope.cancel();
        expect(mockUibModalInstance.dismissed).toBe(true);
      });
    });

    describe('$scope.confirm()', function () {
      it('should close the uibModalInstance with a result object', function () {
        expect(mockUibModalInstance.closed).toBe(false);
        expect(mockUibModalInstance.result).toBeUndefined();

        // Setup scope data that is expected to be passed along to the result.
        $scope.addedUsers = 'added-users';
        $scope.removedUsers = 'removed-users';

        $scope.confirm();

        expect(mockUibModalInstance.closed).toBe(true);
        expect(mockUibModalInstance.result).toBeDefined();
        var result = mockUibModalInstance.result;
        expect(result.addedUsers).toBe($scope.addedUsers);
        expect(result.removedUsers).toBe($scope.removedUsers);
      });
    });

    describe('$scope.entitledUsersUpdated()', function () {
      it('should populate the $scope.isEntitled object with each entitledUser', function () {
        delete $scope.isEntitled;

        $scope.entitledUsersUpdated();

        var expectedIsEntitled = $scope.entitledUsers.reduce(function (result, user) {
          result[user.email] = true;
          return result;
        }, {});
        expect($scope.isEntitled).toEqual(expectedIsEntitled);
      });

      it('should populate the $scope.isEntitled object with each entitledUser after adding users', function () {
        delete $scope.isEntitled;

        // Add a user to be entitled
        var newUser = generateMockUser('user4');
        $scope.entitledUsers.push(newUser);

        $scope.entitledUsersUpdated();

        var expectedIsEntitled = $scope.entitledUsers.reduce(function (result, user) {
          result[user.email] = true;
          return result;
        }, {});
        expect($scope.isEntitled).toEqual(expectedIsEntitled);
      });
    });

    describe('$scope.toggleUser()', function () {
      var event = {
        target: {}
      };

      beforeEach(function () {
        event.target.type = 'tr';
      });

      it('should do nothing if the event.target is a checkbox', function () {
        var previousEntitledUsers = angular.copy($scope.entitledUsers);
        event.target.type = 'checkbox';

        $scope.toggleUser(mockProductItem.userAndEntitlements[0].user, event);

        expect($scope.entitledUsers).toEqual(previousEntitledUsers);
      });

      it('should add a user that is not already in the entitledUsers list', function () {
        var previousEntitledUsers = angular.copy($scope.entitledUsers);

        var newUser = generateMockUser('user4');
        $scope.toggleUser(newUser, event);

        previousEntitledUsers.push(newUser);
        expect($scope.entitledUsers).toEqual(previousEntitledUsers);
      });

      it('should not add a user if the maxEntitlementAllowed limit has already been reached', function () {
        // Add enough users to reach the maxEntitlementAllowed.
        $scope.entitledUsers.push(generateMockUser('user4'));
        $scope.entitledUsers.push(generateMockUser('user5'));

        var previousEntitledUsers = angular.copy($scope.entitledUsers);

        expect($scope.entitledUsers.length).toBe(5);
        expect($scope.entitledUsers).toEqual(previousEntitledUsers);

        var newUser = generateMockUser('user6');
        $scope.toggleUser(newUser, event);

        // There should be no new user added.
        expect($scope.entitledUsers.length).toBe(5);
        expect($scope.entitledUsers).toEqual(previousEntitledUsers);
      });

      it('should remove a user that is already in the entitledUsers list', function () {
        var previousEntitledUsers = angular.copy($scope.entitledUsers);

        $scope.toggleUser(mockProductItem.userAndEntitlements[0].user, event);

        previousEntitledUsers.splice(0, 1);
        expect($scope.entitledUsers).toEqual(previousEntitledUsers);
      });

      it('should remove a user if the maxEntitlementAllowed limit has already been reached', function () {
        // Add enough users to reach the maxEntitlementAllowed.
        $scope.entitledUsers.push(generateMockUser('user4'));
        $scope.entitledUsers.push(generateMockUser('user5'));

        var previousEntitledUsers = angular.copy($scope.entitledUsers);

        expect($scope.entitledUsers.length).toBe(5);
        expect($scope.entitledUsers).toEqual(previousEntitledUsers);

        $scope.toggleUser(mockProductItem.userAndEntitlements[0].user, event);

        expect($scope.entitledUsers.length).toBe(4);
        previousEntitledUsers.splice(0, 1);
        expect($scope.entitledUsers).toEqual(previousEntitledUsers);
      });
    });

    describe('$scope.maxEntitlementsReached()', function () {
      it('should return true if entitledUsers.length >= maxEntitlementAllowed', function () {
        var maxEntitlementAllowed = 10;
        mockProductItem.product.maxEntitlementAllowed = maxEntitlementAllowed;

        $scope.entitledUsers.length = 0;
        expect($scope.maxEntitlementsReached()).toBe(false);

        $scope.entitledUsers.length = maxEntitlementAllowed - 1;
        expect($scope.maxEntitlementsReached()).toBe(false);

        $scope.entitledUsers.length = maxEntitlementAllowed;
        expect($scope.maxEntitlementsReached()).toBe(true);

        $scope.entitledUsers.length = maxEntitlementAllowed + 1;
        expect($scope.maxEntitlementsReached()).toBe(true);

        $scope.entitledUsers.length = maxEntitlementAllowed + 100;
        expect($scope.maxEntitlementsReached()).toBe(true);
      });

      it('should always return false if maxEntitlementAllowed is 0', function () {
        mockProductItem.product.maxEntitlementAllowed = 0;

        $scope.entitledUsers.length = 0;
        expect($scope.maxEntitlementsReached()).toBe(false);

        $scope.entitledUsers.length = 1;
        expect($scope.maxEntitlementsReached()).toBe(false);

        $scope.entitledUsers.length = 10;
        expect($scope.maxEntitlementsReached()).toBe(false);

        $scope.entitledUsers.length = 50;
        expect($scope.maxEntitlementsReached()).toBe(false);

        $scope.entitledUsers.length = 100;
        expect($scope.maxEntitlementsReached()).toBe(false);
      });
    });

  });

});
