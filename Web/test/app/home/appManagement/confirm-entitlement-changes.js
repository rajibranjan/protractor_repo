'use strict';

describe('[Module: hp.portal.home.appManagement.confirmEntitlementChanges]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.appManagement.confirmEntitlementChanges'));

  describe('Factory: ConfirmEntitlementChangesModal', function () {
    beforeEach(module(function ($provide) {
      $provide.constant('$uibModal', {
        open: function (params) {
          return {
            result: {
              params: params
            }
          };
        }
      });
    }));

    var ConfirmEntitlementChangesModal;
    beforeEach(inject(function (_ConfirmEntitlementChangesModal_) {
      ConfirmEntitlementChangesModal = _ConfirmEntitlementChangesModal_;
    }));

    describe('.open(data)', function () {
      it('should open the $uibModal with the correct params', function () {
        var addedUsers = ['user1', 'user2'];
        var removedUsers = ['user3', 'user4'];

        var result = ConfirmEntitlementChangesModal.open({
          addedUsers: addedUsers,
          removedUsers: removedUsers
        });

        expect(result).toBeDefined();
        expect(result.params).toBeDefined();
        var params = result.params;
        var resolve = params.resolve;
        expect(resolve.addedUsers()).toBe(addedUsers);
        expect(resolve.removedUsers()).toBe(removedUsers);

        delete params.resolve;
        expect(params).toEqual({
          template: '<confirm-entitlement-changes added-users="addedUsers" removed-users="removedUsers" on-cancel="cancel()" on-confirm="confirm()"></confirm-entitlement-changes>',
          controller: 'ConfirmEntitlementChangesModalCtrl',
          backdrop: 'static',
          size: 'lg'
        });
      });
    });

  });


  describe('Controller: ConfirmEntitlementChangesModalCtrl', function () {
    var mockUibModalInstance = {
      dismiss: function () {
        this.dismissed = true;
      },
      close: function (result) {
        this.closed = true;
        this.result = result;
      }
    };
    var mockAddedUsers = ['user1', 'user3'];
    var mockRemovedUsers = ['user2', 'user4'];

    var $scope, ConfirmEntitlementChangesModalCtrl;
    beforeEach(inject(function ($rootScope, $controller) {
      $scope = $rootScope.$new();

      // Load the controller
      ConfirmEntitlementChangesModalCtrl = $controller('ConfirmEntitlementChangesModalCtrl', {
        $scope: $scope,
        $uibModalInstance: mockUibModalInstance,
        addedUsers: mockAddedUsers,
        removedUsers: mockRemovedUsers
      });
    }));

    it('should set all of the necessary $scope fields', function () {
      expect($scope.addedUsers).toBe(mockAddedUsers);
      expect($scope.removedUsers).toBe(mockRemovedUsers);
      expect($scope.cancel).toBe(mockUibModalInstance.dismiss);
      expect($scope.confirm).toBe(mockUibModalInstance.close);
    });
  });

});
