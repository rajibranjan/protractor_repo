'use strict';

describe('[Module: hp.portal.home.resellers.resellerDetails]', function () {

  var alerts = [];
  beforeEach(module(function ($provide) {
    $provide.constant('PageAlerts', {
      add: function (alert) {
        alerts.push(alert);
      },
      timeout: {
        short: 3000,
        medium: 5000,
        long: 10000
      }
    });
  }));

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.resellers.resellerDetails'));

  var httpBackend, $q, timeout;
  beforeEach(inject(function ($httpBackend, _$q_, $timeout) {
    httpBackend = $httpBackend;
    $q = _$q_;
    timeout = $timeout;
  }));

  var shouldFail = false;
  var deleteFail = false;

  var mockResendActivationEmail = {
    startTask: function (payload) {
      var deferred = $q.defer();
      if (payload.userId) {
        deferred.resolve();
      }
      else {
        deferred.reject();
      }
      return {
        $promise: deferred.promise
      };
    }
  };

  var testReseller = {
    createdBy: 'd265f528-f20c-4529-9e7f-133b75b45941',
    createdDate: '2014-03-25T16:29:47.000+0000',
    lastModifiedBy: '0666df1f-8b05-4b64-8408-9b840ec889f6',
    lastModifiedDate: '2014-03-25T16:29:52.000+0000',
    tenantId: null,
    name: 'Reseller 4',
    poNumber: null,
    crsId: null,
    registrationId: null,
    activatedDate: '2014-03-25T16:29:51.000+0000',
    status: 'SUSPENDED',
    crmId: null,
    addressLine: null,
    addressLine2: null,
    city: null,
    state: null,
    country: 'US',
    zipcode: null,
    phoneNumber: '2081234567',
    phoneExtension: '123',
    productIds: [],
    paymentMethod: 'CHECK',
    links: [{
      rel: 'self',
      href: 'https://localhost:9000/api/v2/resellers/042dec32-f7eb-4b27-b840-c82ab721e213'
    }, {
      rel: 'adminContact',
      href: 'https://localhost:9000/api/v2/users/13189ae1-b63b-49db-8539-21fd3361c099'
    }],
    save: function () {
      var deferred = $q.defer();
      timeout(function () {
        if (shouldFail) {
          deferred.reject(new Error('I just don\'t like you exception'));
        }
        else {
          deferred.resolve(updatedReseller);
        }
      }, 100);
      return deferred.promise;
    },
    delete: function () {
      var deferred = $q.defer();
      timeout(function () {
        if (deleteFail) {
          deferred.reject(new Error('I just don\'t like you exception'));
        }
        else {
          deferred.resolve();
        }
      }, 100);
      return deferred.promise;
    }
  };

  var updatedReseller = angular.copy(testReseller);
  updatedReseller.name = 'Changed Man';

  var testUser = {
    email: 'user2@eggnogcookie.com',
    firstName: 'User2First',
    lastName: 'en-US',
    phoneNumber: '1-208-396-6000',
    phoneExtension: null,
    links: [{
      rel: 'self',
      href: 'https://localhost:9000/HPStoreWeb/api/v2/users/testUser'
    }]
  };

  var updatedUser = angular.copy(testUser);
  updatedUser.links[0] = {
    rel: 'self',
    href: 'https://localhost:9000/HPStoreWeb/api/v2/users/updatedUser'
  };


  var mockCountrySettings = {
    get: function () {
      if (isSupportedCountry) {
        return countrySettingsSupported;
      }
      else {
        return countrySettingsUnsupported;
      }
    }
  };

  var mockUserService = {
    getById: function () {
      var deferred = $q.defer();
      timeout(function () {
        if (adminGetFail) {
          deferred.reject(new Error('I just don\'t like you exception'));
        }
        else {
          deferred.resolve(testUser);
        }
      }, 100);
      return {
        $promise: deferred.promise
      };
    }
  };

  var isSupportedCountry;

  var countrySettingsSupported = {
    isSupported: true
  };

  var countrySettingsUnsupported = {
    isSupported: false
  };

  var ResellerDetailsCtrl, scope, location, emitFunctionRan, adminGetFail, linkParser;
  beforeEach(inject(function ($rootScope, $controller, $location, LinkParser) {
    scope = $rootScope.$new();
    location = $location;
    linkParser = LinkParser;
    isSupportedCountry = true;
    ResellerDetailsCtrl = $controller('ResellerDetailsCtrl', {
      $scope: scope,
      reseller: angular.copy(testReseller),
      Routes: {
        resellers: '/blah'
      },
      ResendActivationEmail: mockResendActivationEmail,
      LinkParser: linkParser,
      UserService: mockUserService,
      CountrySettings: mockCountrySettings
    });
    alerts = [];
    emitFunctionRan = false;
    adminGetFail = false;
  }));

  describe('Controller: ResellerDetailsCtrl', function () {

    it('should put the reseller on the scope', function () {
      expect(scope.reseller).toEqual(testReseller);
    });

    it('should successfully update the reseller', function () {
      scope.$on('hpDisableResellerDetailsForm', function (event, updateReseller, checkIsInvalid) {
        emitFunctionRan = true;
        var promise = updateReseller();
        expect(promise).toBeDefined();
        expect(typeof promise).toBe('object');
        expect(typeof promise.then).toBe('function');
        expect(typeof promise.finally).toBe('function');
        expect(typeof promise.catch).toBe('function');
        timeout.flush();
        expect(scope.user).toEqual(testUser);
        expect(checkIsInvalid).toEqual(true);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
      });
      scope.updateResellerSubmit();
      expect(emitFunctionRan).toEqual(true);
    });

    it('should successfully update the reseller with 5 digit zipcode', function () {
      scope.$on('hpDisableResellerDetailsForm', function (event, updateReseller, checkIsInvalid) {
        emitFunctionRan = true;
        testReseller.zipcode = 83646;
        var promise = updateReseller();
        expect(promise).toBeDefined();
        expect(typeof promise).toBe('object');
        expect(typeof promise.then).toBe('function');
        expect(typeof promise.finally).toBe('function');
        expect(typeof promise.catch).toBe('function');
        timeout.flush();
        expect(scope.user).toEqual(testUser);
        expect(checkIsInvalid).toEqual(true);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
      });
      scope.updateResellerSubmit();
      expect(emitFunctionRan).toEqual(true);
    });

    it('should successfully update the reseller with 9 digit zipcode', function () {
      scope.$on('hpDisableResellerDetailsForm', function (event, updateReseller, checkIsInvalid) {
        emitFunctionRan = true;
        testReseller.zipcode = 836461868;
        var promise = updateReseller();
        expect(promise).toBeDefined();
        expect(typeof promise).toBe('object');
        expect(typeof promise.then).toBe('function');
        expect(typeof promise.finally).toBe('function');
        expect(typeof promise.catch).toBe('function');
        timeout.flush();
        expect(scope.user).toEqual(testUser);
        expect(checkIsInvalid).toEqual(true);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
      });
      scope.updateResellerSubmit();
      expect(emitFunctionRan).toEqual(true);
    });

    it('should successfully update the reseller with Canadian zipcode', function () {
      scope.$on('hpDisableResellerDetailsForm', function (event, updateReseller, checkIsInvalid) {
        emitFunctionRan = true;
        testReseller.country = 'CA';
        testReseller.zipcode = 'A4A 5C5';
        var promise = updateReseller();
        expect(promise).toBeDefined();
        expect(typeof promise).toBe('object');
        expect(typeof promise.then).toBe('function');
        expect(typeof promise.finally).toBe('function');
        expect(typeof promise.catch).toBe('function');
        timeout.flush();
        expect(scope.user).toEqual(testUser);
        expect(checkIsInvalid).toEqual(true);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
      });
      scope.updateResellerSubmit();
      expect(emitFunctionRan).toEqual(true);
    });

    it('should deal with an unsuccessful update', function () {
      shouldFail = true;
      scope.$on('hpDisableResellerDetailsForm', function (event, updateReseller, checkIsInvalid) {
        emitFunctionRan = true;
        var promise = updateReseller();
        expect(promise).toBeDefined();
        expect(typeof promise).toBe('object');
        expect(typeof promise.then).toBe('function');
        expect(typeof promise.finally).toBe('function');
        expect(typeof promise.catch).toBe('function');
        timeout.flush();
        expect(scope.user).toEqual(testUser);
        expect(checkIsInvalid).toEqual(true);
        expect(scope.reseller).toEqual(testReseller);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
      });
      scope.updateResellerSubmit();
      expect(emitFunctionRan).toEqual(true);
    });

    it('should deal with an unsuccessful update with 7 digit zipcode', function () {
      shouldFail = true;
      scope.$on('hpDisableResellerDetailsForm', function (event, updateReseller, checkIsInvalid) {
        emitFunctionRan = true;

        testReseller.zipcode = 8364618;
        var promise = updateReseller();
        expect(promise).toBeDefined();
        expect(typeof promise).toBe('object');
        expect(typeof promise.then).toBe('function');
        expect(typeof promise.finally).toBe('function');
        expect(typeof promise.catch).toBe('function');
        timeout.flush();
        expect(scope.user).toEqual(testUser);
        expect(checkIsInvalid).toEqual(true);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
      });
      scope.updateResellerSubmit();
      expect(emitFunctionRan).toEqual(true);
    });

    it('should deal with an unsuccessful update with Canadian zipcode', function () {
      shouldFail = true;
      scope.$on('hpDisableResellerDetailsForm', function (event, updateReseller, checkIsInvalid) {
        emitFunctionRan = true;
        testReseller.country = 'CA';
        testReseller.zipcode = 'A4A';
        var promise = updateReseller();
        expect(promise).toBeDefined();
        expect(typeof promise).toBe('object');
        expect(typeof promise.then).toBe('function');
        expect(typeof promise.finally).toBe('function');
        expect(typeof promise.catch).toBe('function');
        timeout.flush();
        expect(scope.user).toEqual(testUser);
        expect(checkIsInvalid).toEqual(true);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
      });
      scope.updateResellerSubmit();
      expect(emitFunctionRan).toEqual(true);
    });

    it('should deal with an unsuccessful update with 4 digit zipcode', function () {
      shouldFail = true;
      scope.$on('hpDisableResellerDetailsForm', function (event, updateReseller, checkIsInvalid) {
        emitFunctionRan = true;

        testReseller.zipcode = 8364;
        var promise = updateReseller();
        expect(promise).toBeDefined();
        expect(typeof promise).toBe('object');
        expect(typeof promise.then).toBe('function');
        expect(typeof promise.finally).toBe('function');
        expect(typeof promise.catch).toBe('function');
        timeout.flush();
        expect(scope.user).toEqual(testUser);
        expect(checkIsInvalid).toEqual(true);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
      });
      scope.updateResellerSubmit();
      expect(emitFunctionRan).toEqual(true);
    });

    it('should successfully delete the reseller and redirect to resellers list', function () {
      scope.$on('hpDisableResellerDetailsForm', function (event, deleteReseller, checkIsInvalid) {
        emitFunctionRan = true;
        var promise = deleteReseller();
        expect(promise).toBeDefined();
        expect(typeof promise).toBe('object');
        expect(typeof promise.then).toBe('function');
        expect(typeof promise.finally).toBe('function');
        expect(typeof promise.catch).toBe('function');
        timeout.flush();
        expect(scope.user).toEqual(testUser);
        expect(checkIsInvalid).toEqual(false);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
        expect(location.url()).toBe('/blah');
      });
      scope.deleteResellerSubmit();
      expect(emitFunctionRan).toEqual(true);
    });

    it('should fail when deleting reseller', function () {
      deleteFail = true;
      scope.$on('hpDisableResellerDetailsForm', function (event, deleteReseller, checkIsInvalid) {
        emitFunctionRan = true;
        var promise = deleteReseller();
        expect(promise).toBeDefined();
        expect(typeof promise).toBe('object');
        expect(typeof promise.then).toBe('function');
        expect(typeof promise.finally).toBe('function');
        expect(typeof promise.catch).toBe('function');
        timeout.flush();
        expect(scope.user).toEqual(testUser);
        expect(checkIsInvalid).toEqual(false);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
      });
      scope.deleteResellerSubmit();
      expect(emitFunctionRan).toEqual(true);
    });

    it('should ensure the right link is returned', function () {
      scope.resendActivationEmail();
      timeout.flush();
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('success');

      alerts.splice(0, 1);
      scope.reseller.links = [{
        rel: 'self',
        href: 'https://localhost:9000/api/v2/resellers/042dec32-f7eb-4b27-b840-c82ab721e213'
      }];

      scope.resendActivationEmail();
      timeout.flush();
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('danger');
    });

    it('should successfully change the company contact associated with an account and update the links', function () {
      scope.changeCompanyContact(updatedUser);
      expect(scope.user).toBe(updatedUser);
      expect(linkParser.getLinkId(scope.reseller, 'adminContact')).toEqual('updatedUser');
    });

    it('should not show the state and zip code fields with unsupported country and null fields on country switch', function () {
      isSupportedCountry = true;
      scope.reseller.country = 'US';
      scope.reseller.state = 'testState';
      scope.reseller.zipcode = 123456789;
      scope.reseller.phoneNumber = 123456789;
      scope.$digest();
      expect(scope.countrySettings.isSupported).toBe(true);
      expect(scope.reseller.state).toBe('testState');
      expect(scope.reseller.zipcode).toBe(123456789);
      expect(scope.reseller.phoneNumber).toBe(123456789);

      isSupportedCountry = false;
      scope.reseller.phoneNumber = 987654321;
      scope.reseller.zipcode = 88888;
      scope.reseller.state = 'coolState';
      scope.reseller.country = 'Antarctica';
      scope.$digest();
      expect(scope.countrySettings.isSupported).toBe(false);
      expect(scope.reseller.state).toBe(null);
      expect(scope.reseller.zipcode).toBe(null);
      expect(scope.reseller.phoneNumber).toBe(987654321);
    });
  });
});
