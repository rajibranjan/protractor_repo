'use strict';

describe('hp.portal.home.welcome', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.welcome'));

  describe('WelcomeCtrl', function () {

    var mockProfiles = {
      LOCAL_TEST: {
        label: 'local_test',
        isActive: false
      }
    };

    var mockPermissions = {
      isHPAdmin: false,
      isReseller: false,
      isCompanyAdmin: false,
      isUser: false
    };

    var mockRoutes = {
      dashboard: 'DashboardRoute',
      welcome: 'WelcomeRoute'
    };

    function LocationMock() {
      var _url = mockRoutes.welcome;
      this.url = function (value) {
        if (value === undefined) {
          return _url;
        }
        else {
          _url = value;
        }
      };
    }

    var getEntitlementsShouldReturnSuccess = true;
    var mockEntitlementService = {
      getWithOfferingAndProductAndLicense: function () {
        var dfd = $qq.defer();

        if (getEntitlementsShouldReturnSuccess) {
          dfd.resolve(entitlementsResult);
        }
        else {
          dfd.reject();
        }

        return dfd.promise;
      }
    };

    var scope, WelcomeCtrl, controllerLoader, rootScope, $qq, timeout;
    var sdkConfig = {};
    var entitlements;
    var entitlementsResult = {};
    var mockLocation = new LocationMock();

    var testRouteChange = function () {
      rootScope.$broadcast('$routeChangeStart');
    };

    var initController = function () {
      WelcomeCtrl = controllerLoader('WelcomeCtrl', {
        SdkConfig: sdkConfig,
        $scope: scope,
        EntitlementService: mockEntitlementService,
        Globals: {
          permissions: mockPermissions
        },
        Routes: mockRoutes,
        $location: mockLocation,
        Profiles: mockProfiles
      });
      timeout.flush();
    };

    beforeEach(inject(function ($rootScope, $controller, $q, $timeout) {
      rootScope = $rootScope;
      scope = $rootScope.$new();
      controllerLoader = $controller;
      $qq = $q;
      timeout = $timeout;

      // Reset the roles on every test run.
      mockPermissions.isHPAdmin = false;
      mockPermissions.isReseller = false;
      mockPermissions.isCompanyAdmin = false;
      mockPermissions.isUser = false;

      // Reset the location.
      mockLocation.url(mockRoutes.welcome);

      // Reset entitlements.
      entitlements = [{
        isEntitled: true,
        product: {
          name: 'mockProduct'
        },
        offering: {
          fqName: 'a'
        }
      }, {
        isEntitled: true,
        product: {
          name: 'mockProduct2'
        },
        offering: {
          fqName: 'b'
        }
      }, {
        isEntitled: true,
        product: {
          name: 'mockProduct3'
        },
        offering: {
          fqName: 'c'
        }
      }];
      entitlementsResult.data = entitlements;
    }));

    it('HPAdmin should be redirected to the dashboard', function () {
      mockPermissions.isHPAdmin = true;
      initController();
      expect(mockLocation.url()).toBe(mockRoutes.dashboard);
    });

    it('Reseller should be redirected to the dashboard', function () {
      mockPermissions.isReseller = true;
      initController();
      expect(mockLocation.url()).toBe(mockRoutes.dashboard);
    });

    it('CompanyAdmin should NOT be redirected to the dashboard', function () {
      mockPermissions.isCompanyAdmin = true;
      initController();
      expect(mockLocation.url()).toBe(mockRoutes.welcome);
    });

    it('End User should NOT be redirected to the dashboard', function () {
      mockPermissions.isUser = true;
      initController();
      expect(mockLocation.url()).toBe(mockRoutes.welcome);
    });

    it('should set offerings on the scope for all entitlements', function () {
      initController();
      expect(scope.products).toBeDefined();
      expect(scope.products.length).toBe(entitlements.length);
    });

    it('should set offerings on the scope for all entitlements even if isEntitled is false', function () {
      entitlements[0].isEntitled = false;
      initController();
      expect(scope.products).toBeDefined();
      expect(scope.products.length).toBe(entitlements.length);
    });

    it('should set an entitlements array on each offering that gets set on the scope', function () {
      initController();
      expect(scope.products).toBeDefined();
      expect(scope.products.length).toBe(entitlements.length);
      angular.forEach(scope.products, function (product) {
        expect(product.offering).toBeDefined();
        expect(product.entitlements).toBeDefined();
        expect(product.entitlements.length).toBe(1);
      });
    });

    it('should set an entitlements array on each offering with the correct number of entitlements', function () {
      // Add an additional entitlement for one of the offerings.
      var additionalEntitlement = angular.copy(entitlements[1]);
      additionalEntitlement.isEntitled = false;
      entitlements.push(additionalEntitlement);
      initController();
      expect(scope.products).toBeDefined();
      expect(scope.products.length).toBe(entitlements.length);
      angular.forEach(scope.products, function (product) {
        expect(product.entitlements).toBeDefined();
        if (product.offering.fqName === additionalEntitlement.offering.fqName) {
          expect(product.entitlements.length).toBe(1);
        }
        else {
          expect(product.entitlements.length).toBe(1);
        }
      });
    });

    it('should set the SdkConfig.isWelcomePage value on load', function () {
      initController();
      expect(sdkConfig.isWelcomePage).toBe(true);
    });

    it('should set the SdkConfig.isWelcomePage value on route change start', function () {
      initController();
      expect(sdkConfig.isWelcomePage).toBe(true);
      testRouteChange();
      expect(sdkConfig.isWelcomePage).toBe(false);
    });
  });

});
