'use strict';

describe('[Module: hp.portal.common.modals.licenseCountdownModal]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.modals.licenseCountdownModal'));

  describe('Factory: LicenseCountdownModal', function () {
    beforeEach(module(function ($provide) {
      $provide.constant('$uibModal', {
        open: function (data) {
          return {
            result: {
              data: data
            }
          };
        }
      });
    }));

    var LicenseCountdownModal;
    beforeEach(inject(function (_LicenseCountdownModal_) {
      LicenseCountdownModal = _LicenseCountdownModal_;
    }));

    describe('.open(data)', function () {
      it('should open the $uibModal with the correct param', function () {
        var mockdata = {
          daysTillExpiration: 5,
          offering: 'mock-offering',
          source: 'portal'
        };

        var result = LicenseCountdownModal.open(mockdata);

        expect(result).toBeDefined();
        expect(result.data).toBeDefined();
        var data = result.data;
        expect(data.resolve).toBeDefined();
        var resolve = data.resolve;
        expect(resolve.offering()).toBe(mockdata.offering);
        expect(resolve.daysTillExpiration()).toBe(mockdata.daysTillExpiration);

        delete data.resolve;
        expect(data).toEqual({
          templateUrl: 'app/common/modals/license-countdown-modal.html',
          controller: 'LicenseCountdownModalCtrl'
        });
      });
    });
  });

  describe('Controller: LicenseCountdownModalCtrl', function () {
    var mockUibModalInstance = {
      close: function () {
        this.closed = true;
      },
      result: {
        finally: function (cancelCallback) {
          this.cancelCallback = cancelCallback;
          return this;
        }
      }
    };
    var mockEulaPageUrl = 'http://localhost/eula';
    var mockOfferingFqname = 'mock-offering';

    var mockOffering = {
      fqName: mockOfferingFqname,
      urls: {
        browserLandingUrl: 'mock-landing-url'
      }
    };

    var mockDaysTillExpiration = 10;

    var mockPortalConfigurations = {
      'com.portal.eulaPageUrl': mockEulaPageUrl,
    };

    var mockEntitlement = {
      offering: {
        fqName: mockOfferingFqname
      },
      license: {
        isTrial: true,
        endDate: '2016-04-30T06:00:00.000+0000'
      },
      product: {
        name: 'mock-product',
        launchBehavior: 'SAME_TAB'
      },
      restrictions: []
    };

    var mockRestrictionCodes = {
      eulaAcceptance: 1340150173
    };

    var mockEntitlementWithRestrictions = angular.copy(mockEntitlement);
    angular.extend(mockEntitlementWithRestrictions, {
      restrictions: [{
        code: mockRestrictionCodes.eulaAcceptance,
        description: 'cMustAcceptEula'
      }]
    });

    var mockWindow = {
      open: function (location) {
        this.location.href = location;
      },
      location: {
        href: undefined
      }
    };

    var mockLocation = {
      url: function (href) {
        mockWindow.location.href = href;
      }
    };

    var $scope, LicenseCountdownModalCtrl, rootScope, $controller, $timeout;

    function loadController(entitlement) {
      $scope = rootScope.$new();
      LicenseCountdownModalCtrl = $controller('LicenseCountdownModalCtrl', {
        $scope: $scope,
        $window: mockWindow,
        $location: mockLocation,
        $timeout: $timeout,
        $uibModalInstance: mockUibModalInstance,
        offering: mockOffering,
        daysTillExpiration: mockDaysTillExpiration,
        entitlement: entitlement,
        LaunchBehaviors: {},
        PortalConfigurations: mockPortalConfigurations,
        RestrictionCodes: mockRestrictionCodes
      });
    }

    beforeEach(inject(function ($rootScope, _$controller_, _$timeout_) {
      $controller = _$controller_;
      rootScope = $rootScope;
      $timeout = _$timeout_;

      // Reset Mocks
      mockUibModalInstance.closed = false;

      mockWindow.location.href = undefined;
    }));

    it('should set all of the necessary $scope fields', function () {
      loadController(mockEntitlement);
      expect($scope.offering).toEqual(mockOffering);
      expect($scope.daysTillExpiration).toEqual(mockDaysTillExpiration);
      expect($scope.redirect).toBeFunction();
    });

    it('should redirect to eula page', function () {
      loadController(mockEntitlementWithRestrictions);
      $timeout.flush();
      $scope.redirect();
      expect(mockWindow.location.href).toBe(mockPortalConfigurations['com.portal.eulaPageUrl'] + '#/' + $scope.offering.fqName);
    });
  });
});
