'use strict';

describe('[Module: hp.portal.common.widgets.widgetPullPrint]', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.widgets.widgetPullPrint'));

  var getEntitlementSuccess = true;

  var mockEntitlementResponse = {
    data: [{
      isEntitled: false,
      restrictions: [{
        code: -1592735507
      }]
    }]
  };

  var mockOfferingService = {
    getByFqName: function () {
      return $qq.resolve('hp-pullprint');
    }
  };

  var mockEntitlementService = {
    getByUserAndOffering: function () {
      var dfd = $qq.defer();
      if (getEntitlementSuccess) {
        dfd.resolve(mockEntitlementResponse);
      }
      else {
        dfd.reject();
      }
      return dfd.promise;
    }
  };

  var initCtrl = function () {
    controller('PullPrintWidgetCtrl', {
      $scope: scope,
      OfferingService: mockOfferingService,
      EntitlementService: mockEntitlementService,
      Self: function () {}
    });
    timeout.flush();
  };

  var scope, timeout, controller, $qq;
  beforeEach(inject(function ($rootScope, $controller, $timeout, $q) {
    scope = $rootScope.$new();
    timeout = $timeout;
    controller = $controller;
    $qq = $q;
  }));

  describe('Widget: PullPrintWidget', function () {

    it('should initialize the scope', function () {
      initCtrl();
      expect(scope.offering).toBe('hp-pullprint');
    });

    it('should enabled the widget', function () {
      mockEntitlementResponse.data[0].isEntitled = true;
      initCtrl();
      expect(scope.isDisabled).toBe(false);
      expect(scope.isProvisioning).toBe(false);
    });

    it('should disable the widget but not set the isProvisioning', function () {
      mockEntitlementResponse.data[0].isEntitled = false;
      mockEntitlementResponse.data[0].restrictions[0].code = 123;
      initCtrl();
      expect(scope.isDisabled).toBe(true);
      expect(scope.isProvisioning).toBe(false);
    });

    it('should disable the widget set the isProvisioning', function () {
      mockEntitlementResponse.data[0].isEntitled = false;
      mockEntitlementResponse.data[0].restrictions[0].code = -1592735507;
      initCtrl();
      expect(scope.isDisabled).toBe(true);
      expect(scope.isProvisioning).toBe(true);
    });

    it('should disable the widget set the isProvisioning based on second restriction', function () {
      mockEntitlementResponse.data[0].isEntitled = false;
      var mockRestrictions = [{
        code: -123
      }, {
        code: -1592735507
      }];
      mockEntitlementResponse.data[0].restrictions = mockRestrictions;
      initCtrl();
      expect(scope.isDisabled).toBe(true);
      expect(scope.isProvisioning).toBe(true);
    });

    it('should disable widget on failure', function () {
      getEntitlementSuccess = false;
      initCtrl();
      expect(scope.isDisabled).toBe(true);
      expect(scope.isProvisioning).toBe(false);
    });
  });
});
