'use strict';

describe('[hp.portal.common.webservices] Factory: ShoppingCartService', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.webservices'));

  var httpBackend,
    ShoppingCartService;

  var mockShoppingCart = {
    id: 'test-shopping-cart-id',
    accountId: 'test-account-id'
  };

  beforeEach(inject(function ($httpBackend, _ShoppingCartService_) {
    httpBackend = $httpBackend;
    ShoppingCartService = _ShoppingCartService_;
  }));

  it('should POST to endpoint', function () {
    httpBackend.expectPOST('/api/v2/shoppingCarts').respond(200, mockShoppingCart);
    ShoppingCartService.create({
      accountId: 'test-account-id'
    }).save().then(function (data) {
      expect(data.id).toBeDefined();
      expect(data.id).toEqual(mockShoppingCart.id);
      expect(data.accountId).toBeDefined();
      expect(data.accountId).toEqual(mockShoppingCart.accountId);
    });
    httpBackend.flush();
  });
});
