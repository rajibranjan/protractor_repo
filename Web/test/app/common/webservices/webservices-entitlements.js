'use strict';

describe('[hp.portal.common.webservices] Factory: EntitlementService', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.webservices.entitlements'));

  var generateEntitlements = function (count, termsAccepted, privacyAccepted) {
    var entitlements = [];

    for (var i = 0; i < count; i++) {
      entitlements.push(generateEntitlement(termsAccepted, privacyAccepted));
    }

    return entitlements;
  };

  var generateEntitlementsPage = function (count, termsAccepted, privacyAccepted) {
    return {
      total: count,
      data: generateEntitlements(count, termsAccepted, privacyAccepted)
    };
  };

  var generateEntitlement = function (termsAccepted, privacyAccepted) {
    return {
      termsAccepted: termsAccepted,
      privacyAccepted: privacyAccepted,
      save: function () {
        var dfd = $qq.defer();
        if (this.failOnSave) {
          dfd.reject();
        }
        else {
          dfd.resolve();
        }
        return dfd.promise;
      }
    };
  };

  var generateEntitlementWithExpands = function (fqname) {
    return {
      data: [{
        license: {
          createdBy: '03f0a3f7-df32-419e-9659-96efe8b1a9f5',
          createdDate: '2015-08-20T22:16:18.000+0000',
          lastModifiedBy: '03f0a3f7-df32-419e-9659-96efe8b1a9f5',
          lastModifiedDate: '2015-08-20T22:16:18.000+0000',
          tenantId: 'cd3c9e6b-b714-4885-aee0-fe9efe1909a0',
          startDate: '2015-08-20T22:16:18.000+0000',
          endDate: '2016-04-20T00:00:00.000+0000',
          quantity: 1,
          status: 'ACTIVE',
          isTrial: false,
          contractId: null,
          additionalFields: {}
        },
        product: {
          solutionId: '4939cb54-b58f-4ecc-bb6b-9536ab2881be',
          name: 'cTitle',
          description: 'cDescription',
          category: 'meat',
          unitOfMeasure: 'nugget',
          units: 8,
          maxEntitlementAllowed: 0,
          isFree: true,
          provisioningRequired: false,
          trialTermDays: 0,
          productType: 'UNKNOWN',
          launchBehavior: 'NEW_TAB',
          targetAudiences: [],
          eulaPolicy: 'PARENT_EULA',
          metaTags: ['EMEA_DIRECT', 'AMERICAS_DIRECT', 'ASIA_OCEANIA_DIRECT', 'OTHER_DIRECT'],
          additionalFields: [],
          supportedCommerceProviders: ['NO_OP']
        },
        offering: {
          fqName: fqname,
          title: 'cTitle',
          shortTitle: 'cShortTitle',
          subTitle: 'cSubTitle',
          description: 'cDescription',
          termsOfUse: 'cTermsOfUse',
          privacyPolicy: 'cPrivacyPolicy',
          reverseProxyName: null,
          offeringVersion: 1,
          isIframe: false,
          productTypes: ['UNKNOWN'],
          metaTags: ['EMEA_DIRECT', 'AMERICAS_DIRECT', 'ASIA_OCEANIA_DIRECT', 'OTHER_DIRECT'],
          urls: {
            iconLargeUrl: 'https://www-micro-staging.hpfoghorn.com/static/images/hp-chicken/chicken.jpg',
            browserLandingUrl: 'https://www-micro-staging.hpfoghorn.com/null',
            apiPath: '/api',
            apiUrl: 'https://www-micro-staging.hpfoghorn.com/ChickenOffer/api',
            browserLandingPath: '',
            rootServiceUrl: 'https://www-micro-staging.hpfoghorn.com/ChickenOffer',
            iconSmallUrl: '/assets/dev/hp-chicken/chicken-sm.png',
            iconMediumUrl: 'https://www-micro-staging.hpfoghorn.com/static/images/hp-chicken/chicken.jpg'
          }
        }
      }],

    };
  };

  var EntitlementService, scope, httpBackend, $qq, timeout;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($rootScope, $httpBackend, $q, $timeout, _EntitlementService_) {
    httpBackend = $httpBackend;
    scope = $rootScope.$new();
    $qq = $q;
    timeout = $timeout;
    EntitlementService = _EntitlementService_;
  }));

  it('Tests the response generator', function () {
    var ents = generateEntitlements(21, true, true);
    expect(ents.length).toBe(21);
  });

  it('Tests the getById method', function () {
    httpBackend.expectGET(/\/api\/v2\/entitlements\/entId/).respond(200, generateEntitlement(true, true));
    EntitlementService.getById('entId');
    httpBackend.flush();
  });

  it('should get an entitlement by user and offering expanding offering, product and license', function () {
    httpBackend.expectGET(/\/api\/v2\/entitlements\?expand=offering,product,license&qs=userId%3D%3DbennyMouserishId%26%26offeringFqName%3D%3DawesomeOfferingId/)
      .respond(200, generateEntitlementWithExpands('awesomeOfferingId'));

    var promise = EntitlementService.getByUser('bennyMouserishId', 'awesomeOfferingId', 'offering,product,license');
    promise.then(function (res) {
      expect(res.data.length).toBe(1);
      var entitlement = res.data[0];
      expect(entitlement.license).toBeDefined();
      expect(entitlement.license.quantity).toBe(1);
      expect(entitlement.product).toBeDefined();
      expect(entitlement.product.launchBehavior).toBe('NEW_TAB');
    });
  });

  it('Tests the getByUserAndOffering method', function () {
    httpBackend.expectGET(/\/api\/v2\/entitlements\?qs=userId%3D%3DbennyMouserishId%26%26offeringFqName%3D%3DawesomeOfferingId/).respond(200, generateEntitlementsPage(2, true, true));
    var entsPromise = EntitlementService.getByUserAndOffering('bennyMouserishId', 'awesomeOfferingId');

    entsPromise.then(function (entsPage) {
      expect(entsPage.data.length).toBe(2);
    });
  });

  it('Tests the saveAll method happy case YAY', function () {
    var entitlements = generateEntitlements(3, true, true);

    var saveSuccess = false;
    EntitlementService.saveAll(entitlements).then(function (ents) {
      expect(ents.length).toBe(3);
      saveSuccess = true;
    });
    timeout.flush();
    expect(saveSuccess).toBe(true);
  });

  it('Tests the saveAll method unhappy case', function () {
    var entitlements = generateEntitlements(3, true, true);
    // One of the entitlements should fail upon save.
    entitlements[1].failOnSave = true;

    var saveFailed = false;
    EntitlementService.saveAll(entitlements).catch(function () {
      saveFailed = true;
    });
    timeout.flush();
    expect(saveFailed).toBe(true);
  });
});
