'use strict';

describe('[hp.portal.common.webservices.tasks] Factory: ShoppingCartCheckout', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.webservices.tasks'));

  var httpBackend,
    ShoppingCartCheckout;

  var mockTask = {
    name: 'checkout-task',
    status: 'PROCESSING',
    referenceId: 'mock-reference-id'
  };

  beforeEach(inject(function ($httpBackend, _ShoppingCartCheckout_) {
    httpBackend = $httpBackend;
    ShoppingCartCheckout = _ShoppingCartCheckout_;
  }));

  it('should POST to endpoint', function () {
    httpBackend.expectPOST('/api/v2/tasks/shoppingCartCheckout').respond(200, mockTask);
    ShoppingCartCheckout.startTask(mockTask).$promise.then(function (data) {
      expect(data.name).toBe(mockTask.name);
      expect(data.status).toBe(mockTask.status);
      expect(data.referenceId).toBe(mockTask.referenceId);
    });
    httpBackend.flush();
  });
});
