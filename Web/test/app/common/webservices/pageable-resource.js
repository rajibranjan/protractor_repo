'use strict';

describe('[Module: hp.portal.common.webservices.pageable-resource]', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.webservices.pageable-resource'));

  // Factories to be tested
  var CacheFactory, PageableResourceServiceFactory;

  // Injected services
  var httpBackend, timeout;

  beforeEach(inject(function ($httpBackend, $timeout, Cache, PageableResourceService) {
    // Inject factories to be tested
    CacheFactory = Cache;
    PageableResourceServiceFactory = PageableResourceService;

    // Inject services needed for testing
    httpBackend = $httpBackend;
    timeout = $timeout;
  }));

  describe('Factory: Cache', function () {

    var cache;

    beforeEach(function () {
      cache = new CacheFactory();
    });

    it('should keep track of things', function () {
      var magic = {
        foo: 'bar',
        baz: 'biz'
      };
      expect(cache.get('magic')).toBeFalsy();
      expect(cache.exists('magic')).toBe(false);

      cache.add('magic', magic);
      expect(cache.exists('magic')).toBe(true);
      expect(cache.get('magic')).toBeTruthy();
      expect(cache.get('magic')).toBe(magic);

      cache.remove('magic');
      expect(cache.get('magic')).toBeFalsy();
      expect(cache.exists('magic')).toBe(false);
    });

  });

  describe('Factory: PageableResourceService', function () {

    var ChickenService;

    var chickens = [{
      name: 'Alice',
      eggs: 8
    }, {
      name: 'Bob',
      eggs: 0
    }, {
      name: 'Charlie',
      eggs: 4
    }, {
      name: 'David',
      eggs: 2
    }, {
      name: 'Eve',
      eggs: 10
    }];

    var chickensPage = function (pageNumber, pageLength) {
      var offset = (pageNumber - 1) * pageLength;
      return {
        offset: offset,
        limit: pageLength,
        total: chickens.length,
        data: chickens.slice(offset, offset + pageLength)
      };
    };

    beforeEach(function () {
      ChickenService = new PageableResourceServiceFactory('/api/v2/chickens');
      expect(ChickenService).toBeDefined();

      httpBackend.when('GET', '/api/v2/chickens').respond(200, chickensPage(1, 2));
      httpBackend.when('GET', '/api/v2/chickens?limit=2&offset=0').respond(200, chickensPage(1, 2));
      httpBackend.when('GET', '/api/v2/chickens?limit=2&offset=2').respond(200, chickensPage(2, 2));
      httpBackend.when('GET', '/api/v2/chickens?limit=2&offset=4').respond(200, chickensPage(3, 2));
    });

    it('should allow retrieval of a pager', function () {
      expect(ChickenService.getPager).toBeDefined();
      expect(typeof ChickenService.getPager).toBe('function');
      expect(typeof (ChickenService.getPager())).toBe('object');
    });

    describe('Pager', function () {

      var pager;

      beforeEach(function () {
        pager = ChickenService.getPager();
      });

      it('should protect the options object', function () {
        var options = {
          itemsPerPage: 50
        };
        pager = ChickenService.getPager(options);
        expect(pager.getItemsPerPage()).toBe(50);

        options.itemsPerPage = 1;
        expect(pager.getItemsPerPage()).toBe(50);

        pager.setItemsPerPage(1);
        timeout.flush();
        expect(pager.getItemsPerPage()).toBe(1);
      });

      it('should have reasonable defaults', function () {
        expect(pager.getTotal()).toBe(0);
        expect(pager.getItemsPerPage()).toBe();
        expect(pager.getPageNumber()).toBe(1);
      });

      it('should load the first page properly', function () {
        expect(pager.isLoading()).toBe(false);
        pager.loadPage();
        timeout.flush();
        expect(pager.isLoading()).toBe(true);
        httpBackend.flush();
        expect(pager.isLoading()).toBe(false);
        expect(pager.getTotal()).toBe(5);
        expect(pager.getItemsPerPage()).toBe(2);
        expect(pager.getPageNumber()).toBe(1);
        expect(pager.list[0]).toEqual(jasmine.objectContaining(chickens[0]));
        expect(pager.list[1]).toEqual(jasmine.objectContaining(chickens[1]));
      });

      it('should load other pages properly', function () {
        pager.loadPage();
        timeout.flush();
        httpBackend.flush();

        pager.loadNextPage();
        timeout.flush();
        httpBackend.flush();
        expect(pager.getTotal()).toBe(5);
        expect(pager.getItemsPerPage()).toBe(2);
        expect(pager.getPageNumber()).toBe(2);
        expect(pager.list[0]).toEqual(jasmine.objectContaining(chickens[2]));
        expect(pager.list[1]).toEqual(jasmine.objectContaining(chickens[3]));

        pager.loadNextPage();
        timeout.flush();
        httpBackend.flush();
        expect(pager.getTotal()).toBe(5);
        expect(pager.getItemsPerPage()).toBe(2);
        expect(pager.getPageNumber()).toBe(3);
        expect(pager.list.length).toEqual(1);
        expect(pager.list[0]).toEqual(jasmine.objectContaining(chickens[4]));
        expect(pager.list[1]).toBeUndefined();

        expect(pager.isLoading()).toBe(false);
        pager.loadPreviousPage();
        expect(pager.isLoading()).toBe(false);
        timeout.flush();
        expect(pager.isLoading()).toBe(false);
        expect(httpBackend.flush).toThrow();
        expect(pager.getTotal()).toBe(5);
        expect(pager.getItemsPerPage()).toBe(2);
        expect(pager.getPageNumber()).toBe(2);
        expect(pager.list[0]).toEqual(jasmine.objectContaining(chickens[2]));
        expect(pager.list[1]).toEqual(jasmine.objectContaining(chickens[3]));
      });

    });

  });

});
