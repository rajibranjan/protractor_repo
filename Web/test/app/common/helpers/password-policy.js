'use strict';

describe('[Module: hp.portal.common.helpers] Factory: PasswordPolicy', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.helpers'));

  var PasswordPolicy, PasswordPolicies, Roles;

  beforeEach(inject(function (_PasswordPolicy_, _PasswordPolicies_, _Roles_) {
    PasswordPolicy = _PasswordPolicy_;
    PasswordPolicies = _PasswordPolicies_;
    Roles = _Roles_;
  }));

  describe('.getForUser(user)', function () {
    it('should return DEFAULT for anything other than HP_ADMIN or OFFERING_ADMIN', function () {
      var user = {
        roles: []
      };

      expect(PasswordPolicy.getForUser(user)).toBe(PasswordPolicies.DEFAULT);

      user.roles = [Roles.USER];
      expect(PasswordPolicy.getForUser(user)).toBe(PasswordPolicies.DEFAULT);

      user.roles = [Roles.COMPANY_ADMIN];
      expect(PasswordPolicy.getForUser(user)).toBe(PasswordPolicies.DEFAULT);

      user.roles = ['unknown'];
      expect(PasswordPolicy.getForUser(user)).toBe(PasswordPolicies.DEFAULT);

      user.roles = [Roles.USER, 'unknown'];
      expect(PasswordPolicy.getForUser(user)).toBe(PasswordPolicies.DEFAULT);
    });

    it('should return STRICT for HP_ADMIN or OFFERING_ADMIN', function () {
      var user = {
        roles: []
      };

      user.roles = [Roles.HP_ADMIN];
      expect(PasswordPolicy.getForUser(user)).toBe(PasswordPolicies.STRICT);

      user.roles = [Roles.OFFERING_ADMIN];
      expect(PasswordPolicy.getForUser(user)).toBe(PasswordPolicies.STRICT);

      user.roles = [Roles.HP_ADMIN, Roles.OFFERING_ADMIN];
      expect(PasswordPolicy.getForUser(user)).toBe(PasswordPolicies.STRICT);

      user.roles = [Roles.HP_ADMIN, Roles.OFFERING_ADMIN, Roles.USER, 'unknown'];
      expect(PasswordPolicy.getForUser(user)).toBe(PasswordPolicies.STRICT);
    });
  });

});
