'use strict';

describe('[Module: hp.portal.common.helpers] Factory: ErrorCodeMapper', function () {
  beforeEach(module('unitTestInit'));

  var alerts = [];
  beforeEach(module(function ($provide) {
    $provide.constant('PageAlerts', {
      add: function (alert) {
        alerts.push(alert);
      },
      timeout: {
        short: 3000,
        medium: 5000,
        long: 10000
      }
    });
  }));

  beforeEach(module('hp.portal.common.helpers'));

  var ErrorCodeMapper;

  beforeEach(inject(function (_ErrorCodeMapper_) {
    ErrorCodeMapper = _ErrorCodeMapper_;
    alerts = [];
  }));

  describe('.getCString', function () {
    it('should map service and error codes to a valid cString', function () {
      expect(ErrorCodeMapper.getCString(1, 1)).toBe('cLicenseKeyNotFound');
      expect(ErrorCodeMapper.getCString(1, 3)).toBe('cLicenseKeyUsed');
      expect(ErrorCodeMapper.getCString(1, 5)).toBe('cOrderStateInvalid');
    });

    it('should map unknown service and error codes to a default string', function () {
      expect(ErrorCodeMapper.getCString(500, 1)).toBe('cUnknownError');
      expect(ErrorCodeMapper.getCString(1, 555)).toBe('cUnknownError');
      expect(ErrorCodeMapper.getCString(2, 999)).toBe('cUnknownError');
      expect(ErrorCodeMapper.getCString(5, 1111)).toBe('cUnknownError');
    });
  });

  describe('.hasErrorCodes', function () {
    it('should add a page alert for a single error code', function () {
      var payload = {
        errors: [{
          serviceCode: 1,
          errorCode: 1
        }]
      };

      expect(ErrorCodeMapper.hasErrorCodes(payload)).toBe(true);
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('danger');
      expect(alerts[0].msg).toBe('cLicenseKeyNotFound');
    });

    it('should add page alerts for 2 error codes', function () {
      var payload = {
        errors: [{
          serviceCode: 1,
          errorCode: 3
        }, {
          serviceCode: 1,
          errorCode: 5
        }]
      };

      expect(ErrorCodeMapper.hasErrorCodes(payload)).toBe(true);
      expect(alerts.length).toBe(2);
      expect(alerts[0].type).toBe('danger');
      expect(alerts[0].msg).toBe('cLicenseKeyUsed');
      expect(alerts[1].type).toBe('danger');
      expect(alerts[1].msg).toBe('cOrderStateInvalid');
    });

    it('should not add a page alert if there are no errors', function () {
      expect(ErrorCodeMapper.hasErrorCodes()).toBe(false);
      expect(alerts.length).toBe(0);

      expect(ErrorCodeMapper.hasErrorCodes({})).toBe(false);
      expect(alerts.length).toBe(0);

      expect(ErrorCodeMapper.hasErrorCodes({
        errors: []
      })).toBe(false);
      expect(alerts.length).toBe(0);
    });
  });
});
