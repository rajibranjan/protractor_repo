'use strict';

describe('Factory: CDN', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.init.cdn'));

  var cdnInterceptorFactory, cdnURLConst, serverURLConst;

  beforeEach(inject(function (cdnInterceptor, cdnURL, serverURL) {
    cdnInterceptorFactory = cdnInterceptor;
    cdnURLConst = cdnURL;
    serverURLConst = serverURL;
  }));

  it('Tests CDN URL.', function () {
    var config = {},
      urlRequest = 'app/common/fake/library.js';
    config.url = urlRequest;
    cdnInterceptorFactory.request(config);

    expect(config.url).toBe(cdnURLConst + urlRequest);
  });

  it('Tests CDN URL with beginning slash.', function () {
    var config = {},
      urlRequest = '/app/common/fake/library.js';
    config.url = urlRequest;
    cdnInterceptorFactory.request(config);

    expect(config.url).toBe(cdnURLConst + 'app/common/fake/library.js');
  });

  it('Tests HTTP requests are not modified.', function () {
    var config = {},
      urlRequest = 'hTtP://www.eggnogcookie.com';
    config.url = urlRequest;
    cdnInterceptorFactory.request(config);

    expect(config.url).toBe(urlRequest);
  });

  it('Tests HTTPS requests are not modified.', function () {
    var config = {},
      urlRequest = 'HtTpS://www.eggnogcookie.com';
    config.url = urlRequest;
    cdnInterceptorFactory.request(config);

    expect(config.url).toBe(urlRequest);
  });

  it('Tests Server URL with beginning slash.', function () {
    var config = {},
      urlRequest = '/aPi/v2/users';
    config.url = urlRequest;
    cdnInterceptorFactory.request(config);

    expect(config.url).toBe(serverURLConst + 'aPi/v2/users');
  });

  it('Tests Server API V2 URL.', function () {
    var config = {},
      urlRequest = 'aPi/v2/users';
    config.url = urlRequest;
    cdnInterceptorFactory.request(config);

    expect(config.url).toBe(serverURLConst + urlRequest);
  });

  it('Tests Server API V1 URL.', function () {
    var config = {},
      urlRequest = 'reSt/accounts';
    config.url = urlRequest;
    cdnInterceptorFactory.request(config);

    expect(config.url).toBe(serverURLConst + urlRequest);
  });

  it('Tests j_spring URL.', function () {
    var config = {},
      urlRequest = 'j_spriNg_security_check';
    config.url = urlRequest;
    cdnInterceptorFactory.request(config);

    expect(config.url).toBe(serverURLConst + urlRequest);
  });

  it('Tests forgot password URL.', function () {
    var config = {},
      urlRequest = 'foRgot/193478';
    config.url = urlRequest;
    cdnInterceptorFactory.request(config);

    expect(config.url).toBe(serverURLConst + urlRequest);
  });

});
