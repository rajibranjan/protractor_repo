'use strict';

describe('[hp.portal.common.init.utils]', function () {

  var $window = {
    location: {
      search: ''
    }
  };
  // beforeEach(module('unitTestInit'));
  beforeEach(module(function ($provide) {
    $provide.constant('$window', $window);
  }));
  beforeEach(module('hp.portal.common.init.utils'));

  var $location, queryParams;

  beforeEach(inject(function (_$location_, _queryParams_) {
    $location = _$location_;
    queryParams = _queryParams_;
  }));

  describe('Factory: queryParams()', function () {
    it('should parse a normal query string', function () {
      $window.location.search = '?productId=ef0d9666-c6c6-11e5-a359-e709292494e2&licenseKey=&noredirect=true';

      expect(queryParams()).toEqual({
        productId: 'ef0d9666-c6c6-11e5-a359-e709292494e2',
        licenseKey: '',
        noredirect: 'true'
      });
    });

    it('should aggregate query params with $location.search()', function () {
      $location.search('foo', 'bar');
      $location.search('fiz', 'buzz');
      $window.location.search = '?productId=ef0d9666-c6c6-11e5-a359-e709292494e2&licenseKey=&noredirect=true';

      expect(queryParams()).toEqual({
        productId: 'ef0d9666-c6c6-11e5-a359-e709292494e2',
        licenseKey: '',
        noredirect: 'true',
        foo: 'bar',
        fiz: 'buzz'
      });
    });
  });

});
