'use strict';

describe('Module: hp.portal.common.forms.pattern', function () {

  var $scope, $compile;

  function changeVal(element, val) {
    element.val(val);
    element.triggerHandler('change');

    $scope.$digest();
  }

  beforeEach(module('hp.portal.common.forms.pattern'));

  beforeEach(inject(function (_$rootScope_, _$compile_) {
    $scope = _$rootScope_.$new();
    $compile = _$compile_;
  }));

  describe('Directive: hpPattern', function () {

    var inputZipcode;

    beforeEach(inject(function ($compile) {

      $scope.model = {};
      $scope.pattern = /^[0-9]{5}(?:-____)?(?:[0-9]{4})?(?:-[0-9]{4})?$/;
      inputZipcode = angular.element('<input id="zipcode" name="zipcode" type="text" hp-pattern="pattern" ng-model="model.zipcode">');

      $compile(inputZipcode)($scope);
      $scope.$digest();

    }));

    it('should allow a zipcode that matches the regex', function () {

      expect(inputZipcode.val()).toBe('');

      changeVal(inputZipcode, '12345');
      expect(inputZipcode.val()).toBe('12345');
      expect($scope.model.zipcode).toBe('12345');
      expect(inputZipcode.hasClass('ng-valid')).toBe(true);

      changeVal(inputZipcode, '12345-____');
      expect(inputZipcode.val()).toBe('12345-____');
      expect($scope.model.zipcode).toBe('12345-____');
      expect(inputZipcode.hasClass('ng-valid')).toBe(true);


      changeVal(inputZipcode, '12345-1___');
      expect(inputZipcode.val()).toBe('12345-1___');
      expect($scope.model.zipcode).toBe(undefined);
      expect(inputZipcode.hasClass('ng-invalid')).toBe(true);

      changeVal(inputZipcode, '12345-12__');
      expect(inputZipcode.val()).toBe('12345-12__');
      expect($scope.model.zipcode).toBe(undefined);
      expect(inputZipcode.hasClass('ng-invalid')).toBe(true);

      changeVal(inputZipcode, '12345-123_');
      expect(inputZipcode.val()).toBe('12345-123_');
      expect($scope.model.zipcode).toBe(undefined);
      expect(inputZipcode.hasClass('ng-invalid')).toBe(true);

      changeVal(inputZipcode, 'asdf12345');
      expect(inputZipcode.val()).toBe('asdf12345');
      expect($scope.model.zipcode).toBe(undefined);
      expect(inputZipcode.hasClass('ng-invalid')).toBe(true);

      changeVal(inputZipcode, '123450000');
      expect(inputZipcode.val()).toBe('123450000');
      expect($scope.model.zipcode).toBe('123450000');
      expect(inputZipcode.hasClass('ng-valid')).toBe(true);

      changeVal(inputZipcode, '12345-0000');
      expect(inputZipcode.val()).toBe('12345-0000');
      expect($scope.model.zipcode).toBe('12345-0000');
      expect(inputZipcode.hasClass('ng-valid')).toBe(true);

      changeVal(inputZipcode, '`~!@#$%^&*()_+');
      expect(inputZipcode.val()).toBe('`~!@#$%^&*()_+');
      expect($scope.model.zipcode).toBe(undefined);
      expect(inputZipcode.hasClass('ng-invalid')).toBe(true);

    });

    it('should allow null or empty', function () {
      expect(inputZipcode.val()).toBe('');

      changeVal(inputZipcode, null);
      expect($scope.model.zipcode).toBe('');
      expect(inputZipcode.val()).toBe('');
      expect(inputZipcode.hasClass('ng-valid')).toBe(true);

      changeVal(inputZipcode, '');
      expect($scope.model.zipcode).toBe('');
      expect(inputZipcode.val()).toBe('');
      expect(inputZipcode.hasClass('ng-valid')).toBe(true);
    });

  });

});
