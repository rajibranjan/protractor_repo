'use strict';

describe('[Module: hp.portal.common.webservices.unauthServices]', function () {

  // Sample data
  var sampleData = {
    foo: 'bar',
    baz: 'biz'
  };

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.login.webservices.tasks'));

  // Factories to be tested
  var ActivateUser, ForgotPassword, ResetPassword, UnsubscribeValidate, Unsubscribe, signIn;

  // Injected services
  var Endpoints, httpBackend;

  beforeEach(function () {
    inject(function ($injector, $httpBackend, _Endpoints_, _ActivateUser_, _ForgotPassword_, _ResetPassword_, _UnsubscribeValidate_, _Unsubscribe_, _signIn_) {
      // Inject factories to be tested
      ActivateUser = _ActivateUser_;
      ForgotPassword = _ForgotPassword_;
      ResetPassword = _ResetPassword_;
      UnsubscribeValidate = _UnsubscribeValidate_;
      Unsubscribe = _Unsubscribe_;
      signIn = _signIn_;

      // Inject services needed for testing
      Endpoints = _Endpoints_;
      httpBackend = $httpBackend;
    });
  });

  afterEach(function () {
    httpBackend.flush();
    httpBackend.verifyNoOutstandingRequest();
  });

  describe('Factory: Activate.startTask', function () {

    it('should post to the right endpoint', function () {
      httpBackend.expect('POST', Endpoints.activateUser, function (data) {
        expect(JSON.parse(data)).toEqual(sampleData);
        return true;
      }).respond(200, {});

      ActivateUser.startTask(sampleData);
    });

    it('should return a task object', function () {
      httpBackend.expectPOST(Endpoints.activateUser).respond(200, {});

      var task = ActivateUser.startTask(sampleData);
      expect(task.$promise).toBePromise();
      expect(task.waitForComplete).toBeFunction();
      expect(task.getStatus).toBeFunction();
    });

  });

  describe('Factory: ForgotPassword.startTask', function () {

    it('should post to the right endpoint', function () {
      httpBackend.expect('POST', Endpoints.forgotPassword, function (data) {
        expect(JSON.parse(data)).toEqual(sampleData);
        return true;
      }).respond(200, {});

      ForgotPassword.startTask(sampleData);
    });

    it('should return a task object', function () {
      httpBackend.expect('POST', Endpoints.forgotPassword).respond(200, {});

      var task = ForgotPassword.startTask(sampleData);
      expect(task.$promise).toBePromise();
      expect(task.waitForComplete).toBeFunction();
      expect(task.getStatus).toBeFunction();
    });

  });

  describe('Factory: ResetPassword.startTask', function () {

    it('should post to the right endpoint', function () {
      httpBackend.expect('POST', Endpoints.resetPassword, function (data) {
        expect(JSON.parse(data)).toEqual(sampleData);
        return true;
      }).respond(200, {});

      ResetPassword.startTask(sampleData);
    });

    it('should return a task object', function () {
      httpBackend.expect('POST', Endpoints.resetPassword).respond(200, {});

      var task = ResetPassword.startTask(sampleData);
      expect(task.$promise).toBePromise();
      expect(task.waitForComplete).toBeFunction();
      expect(task.getStatus).toBeFunction();
    });

  });

  describe('Factory: UnsubscribeValidate.startTask', function () {

    it('should post to the right endpoint', function () {
      httpBackend.expect('POST', Endpoints.unsubscribeValidate, function (data) {
        expect(JSON.parse(data)).toEqual(sampleData);
        return true;
      }).respond(200, {});

      UnsubscribeValidate.startTask(sampleData);
    });

    it('should return a task object', function () {
      httpBackend.expect('POST', Endpoints.unsubscribeValidate).respond(200, {});

      var task = UnsubscribeValidate.startTask(sampleData);
      expect(task.$promise).toBePromise();
      expect(task.waitForComplete).toBeFunction();
      expect(task.getStatus).toBeFunction();
    });

  });

  describe('Factory: Unsubscribe.startTask', function () {

    it('should post to the right endpoint', function () {
      httpBackend.expect('POST', Endpoints.unsubscribe, function (data) {
        expect(JSON.parse(data)).toEqual(sampleData);
        return true;
      }).respond(200, {});

      Unsubscribe.startTask(sampleData);
    });

    it('should return a task object', function () {
      httpBackend.expect('POST', Endpoints.unsubscribe).respond(200, {});

      var task = Unsubscribe.startTask(sampleData);
      expect(task.$promise).toBePromise();
      expect(task.waitForComplete).toBeFunction();
      expect(task.getStatus).toBeFunction();
    });

  });

  describe('Factory: signIn', function () {

    it('should post to the right endpoint', function () {
      httpBackend.expect('POST', Endpoints.signIn, function (data) {
        /* global $ */
        expect(decodeURIComponent(data)).toEqual($.param(sampleData));
        return true;
      }, function (headers) {
        expect(Object.keys(headers)).toContain('Content-Type');
        expect(headers['Content-Type']).toBe('application/x-www-form-urlencoded;charset=utf-8');
        return true;
      }).respond(200, {});

      signIn(sampleData);
    });

    it('should provide a promise', function () {
      httpBackend.expect('POST', Endpoints.signIn).respond(200, {});

      var promise = signIn(sampleData);
      expect(promise).toBePromise();
    });

  });

});
