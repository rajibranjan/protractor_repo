'use strict';

describe('[Module: hp.portal.login.signIn]', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.login.signIn'));

  var mockPageAlerts;

  function MockPageAlerts() {
    this.alerts = [];
    this.add = function (alert) {
      return this.alerts.push(alert);
    };
  }
  MockPageAlerts.prototype.timeout = {
    short: 3000,
    medium: 5000,
    long: 10000
  };

  var mockSignIn = function (formData) {
    mockSignIn.formData = angular.copy(formData);
    var dfd = $q.defer();

    mockSignIn.resolve = function (location, expiresIn) {
      var headers = {
        location: location,
        'X-Expires-In': expiresIn
      };
      dfd.resolve({
        headers: function (key) {
          return headers[key];
        }
      });
      $timeout.flush();
    };

    mockSignIn.reject = function () {
      dfd.reject();
      $timeout.flush();
    };

    return dfd.promise;
  };

  var mockWindow;

  function MockWindow() {
    this.location = {};
  }

  var mockHomeUrl = '/home/url';
  var mockPortalConfigurations = {
    $promise: {
      then: function (successHandler) {
        successHandler();
      }
    },
    'com.portal.homeUrl': mockHomeUrl,
  };

  var queryParams;

  function initQueryParams() {
    queryParams = {};
    return function () {
      return queryParams;
    };
  }

  var mockSelf;

  function MockSelf() {
    this.refresh = function () {
      return $q.when();
    };

    this.id = 'mock-self-id';
    this.links = [{
      rel: 'self',
      href: this.id
    }];
  }

  var mockEntitlementService;

  function MockEntitlementService() {
    this.mockEntitlement = {
      offering: 'my-offering'
    };
    this.mockEntitlements = [this.mockEntitlement];

    var dfd;
    this.getByUser = function (userId, fqName, expands) {
      this.params = {
        userId: userId,
        fqName: fqName,
        expands: expands
      };

      dfd = $q.defer();
      return dfd.promise;
    };
    this.resolve = function () {
      dfd.resolve({
        data: this.mockEntitlements
      });
      $timeout.flush();
    };
  }

  var mockLicenseCountdownModal;

  function MockLicenseCountdownModal() {
    var dfd;
    this.open = function (modalData) {
      this.modalData = modalData;

      dfd = $q.defer();
      return dfd.promise;
    };

    this.resolve = function () {
      dfd.resolve();
      $timeout.flush();
    };
  }

  var $q, $timeout;
  beforeEach(inject(function (_$q_, _$timeout_) {
    $q = _$q_;
    $timeout = _$timeout_;
  }));

  describe('SignInCtrl', function () {

    var $scope, $controller, broadcastHandler;
    beforeEach(inject(function ($rootScope, _$controller_) {
      $scope = $rootScope.$new();
      $scope.$broadcast = function (eventName, method) {
        broadcastHandler(eventName, method);
      };

      $controller = _$controller_;

      mockSelf = new MockSelf();
      mockWindow = new MockWindow();
      mockPageAlerts = new MockPageAlerts();
      mockEntitlementService = new MockEntitlementService();
      mockLicenseCountdownModal = new MockLicenseCountdownModal();
      spyOn(mockLicenseCountdownModal, 'open').and.callThrough();

      $controller('SignInCtrl', {
        $scope: $scope,
        $window: mockWindow,
        Self: mockSelf,
        signIn: mockSignIn,
        queryParams: initQueryParams(),
        PageAlerts: mockPageAlerts,
        PortalConfigurations: mockPortalConfigurations,
        EntitlementService: mockEntitlementService,
        LicenseCountdownModal: mockLicenseCountdownModal,
      });
    }));

    describe('initialization', function () {
      it('should set all of the necessary $scope fields', function () {
        expect($scope.formData).toEqual({});
        expect($scope.submit).toBeFunction();
      });
    });

    describe('$scope.submit', function () {
      it('should broadcast the hpDisableSignInForm event', function () {
        var eventHandled = false;
        broadcastHandler = function (eventName, signInHandler) {
          expect(eventName).toBe('hpDisableSignInForm');
          expect(signInHandler).toBeFunction();
          eventHandled = true;
        };

        $scope.submit();
        expect(eventHandled).toBe(true);
      });
    });

    describe('signInHandler', function () {
      var signInHandler;
      beforeEach(function () {
        broadcastHandler = function (eventName, method) {
          signInHandler = method;
        };
        $scope.submit();
      });

      it('should return a promise', function () {
        var promise = signInHandler();
        expect(promise).toBePromise();
      });

      it('should submit the formData the signIn factory', function () {
        /*jshint camelcase: false */
        $scope.formData = {
          j_username: 'user',
          j_password: 'pass'
        };
        /*jshint camelcase: true */

        signInHandler();
        expect(mockSignIn.formData).toEqual($scope.formData);
      });

      describe('->loginSuccess', function () {

        it('should successfully sign a new user in and redirect to home', function () {
          signInHandler();

          expect(mockWindow.location.href).toBeUndefined();
          mockSignIn.resolve();
          expect(mockWindow.location.href).toBe(mockHomeUrl);
        });

        it('should successfully sign a new user in and redirect to the location header', function () {
          signInHandler();

          expect(mockWindow.location.href).toBeUndefined();
          var redirectLocation = 'redirect-here!';
          mockSignIn.resolve(redirectLocation);
          expect(mockWindow.location.href).toBe(redirectLocation);
        });

        describe('->openLicenseCountdownModal', function () {

          it('should open the license-countdown-modal', function () {
            signInHandler();

            var redirectLocation = 'redirect-here!';
            var expiresIn = 7;
            queryParams.redirect = 'fq-name';
            mockSignIn.resolve(redirectLocation, expiresIn);

            mockEntitlementService.resolve();
            expect(mockEntitlementService.params).toEqual({
              userId: mockSelf.id,
              fqName: queryParams.redirect,
              expands: 'offering,product,license'
            });

            expect(mockLicenseCountdownModal.open).toHaveBeenCalled();
            expect(mockLicenseCountdownModal.modalData).toEqual({
              redirectUrl: redirectLocation,
              offering: mockEntitlementService.mockEntitlement.offering,
              daysTillExpiration: expiresIn,
              backdrop: 'static',
              entitlement: mockEntitlementService.mockEntitlement,
            });
          });

          it('should open the license-countdown-modal and redirect to home when it closes', function () {
            signInHandler();

            var redirectLocation = 'redirect-here!';
            var expiresIn = 7;
            queryParams.redirect = 'fq-name';
            mockSignIn.resolve(redirectLocation, expiresIn);

            mockEntitlementService.resolve();
            expect(mockLicenseCountdownModal.open).toHaveBeenCalled();

            expect(mockWindow.location.href).toBeUndefined();
            mockLicenseCountdownModal.resolve();
            expect(mockWindow.location.href).toBe(mockHomeUrl);
          });

          it('should redirect to home immediately if there are no entitlements', function () {
            signInHandler();

            var redirectLocation = 'redirect-here!';
            var expiresIn = 7;
            queryParams.redirect = 'fq-name';
            mockSignIn.resolve(redirectLocation, expiresIn);

            mockEntitlementService.mockEntitlements.length = 0; // Clear out the entitlements.
            expect(mockWindow.location.href).toBeUndefined();
            mockEntitlementService.resolve();
            expect(mockWindow.location.href).toBe(mockHomeUrl);
            expect(mockLicenseCountdownModal.open).not.toHaveBeenCalled();
          });

        });
      });

      describe('->loginError', function () {

        it('should have an error upon sign-in and show a page alert', function () {
          spyOn(angular, 'element').and.callThrough();

          /*jshint camelcase: false */
          $scope.formData.j_password = 'best-password-evar!';
          /*jshint camelcase: true */

          signInHandler();

          mockSignIn.reject();

          expect(mockPageAlerts.alerts.length).toBe(1);
          expect(mockPageAlerts.alerts[0].type).toBe('danger');
          expect(mockPageAlerts.alerts[0].msg).toBe('cErrorUserNamePasswordIncorrect');
          expect(mockPageAlerts.alerts[0].timeout).toBe(mockPageAlerts.timeout.long);

          /*jshint camelcase: false */
          expect($scope.formData.j_password).toBe('');
          /*jshint camelcase: true */

          expect(angular.element).not.toHaveBeenCalled();
          $timeout.flush();
          expect(angular.element).toHaveBeenCalled();
          expect(angular.element).toHaveBeenCalledWith('#passwordValue');
        });

      });

    });

  });
});
