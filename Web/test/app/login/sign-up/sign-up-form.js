'use strict';

describe('[Module: hp.portal.login.signUp.signUpForm]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.login.signUp.signUpForm'));
  beforeEach(module('hp.templates'));

  var recaptchaPublicKey = 'recaptcha-public-key';
  var mockSelfLang = 'foo';
  var mockSelf = {
    getLang: function () {
      return mockSelfLang;
    }
  };

  beforeEach(module(function ($provide) {
    $provide.provider('PortalConfigurations', function () {
      // Mock the PortalConfigurations factory to return the recaptchaPublicKey.
      this.$get = function () {
        return {
          'com.portal.recaptchaPublicKey': recaptchaPublicKey,
          $promise: {
            then: function (callback) {
              callback();
            }
          }
        };
      };
    });
    $provide.constant('Self', mockSelf);
  }));

  describe('Directive: sign-up-form', function () {
    var mockElement, elementScope, $compile, scope, defaultParams;

    beforeEach(inject(function ($rootScope, _$compile_) {
      scope = $rootScope.$new();
      $compile = _$compile_;
    }));

    var compileDirective = function (params) {
      defaultParams = {
        account: {},
        adminContact: {},
        bindings: {
          recaptcha: {}
        },
      };
      params = angular.extend(defaultParams, params);
      angular.extend(scope, params);

      var elemString = '<sign-up-form account="account" admin-contact="adminContact" recaptcha="bindings.recaptcha"></sign-up-form>';
      mockElement = $compile(elemString)(scope);
      scope.$digest();
      elementScope = mockElement.isolateScope();
    };

    describe('scope initialization', function () {
      it('should set the passed in parameters on the scope', function () {
        compileDirective();

        expect(elementScope.account).toBe(defaultParams.account);
        expect(elementScope.adminContact).toBe(defaultParams.adminContact);
        expect(elementScope.recaptcha).toBe(defaultParams.bindings.recaptcha);
      });

      it('should setup default scope variables and functions', function () {
        compileDirective();

        expect(elementScope.unsupportedAcceptance).toBe(false);
        expect(elementScope.initRecaptcha).toBeFunction();
        expect(elementScope.setRecaptchaResponse).toBeFunction();
        expect(elementScope.captchaPublicKey).toBe(recaptchaPublicKey);
      });
    });

    describe('scope.setRecaptchaLocale()', function () {
      var spy, iframe, oldLocale;

      function ngElementFake() {
        return {
          length: 1,
          attr: function (key, value) {
            if (value === undefined) {
              return ngElementFake[key];
            }
            else {
              ngElementFake[key] = value;
            }
          }
        };
      }

      beforeEach(function () {
        spy = spyOn(angular, 'element').and.callFake(ngElementFake);

        oldLocale = 'changeMe';
        iframe = angular.element('iframe');
        iframe.attr('src', 'hl=' + oldLocale + '&foo=bar');
        expect(iframe.attr('src')).toBe('hl=' + oldLocale + '&foo=bar');
      });

      afterEach(function () {
        spy.and.callThrough();
      });

      it('should update the src attr with an updated locale', function () {
        compileDirective();

        expect(iframe.attr('src')).toBe('hl=' + oldLocale + '&foo=bar');
        elementScope.initRecaptcha();
        expect(iframe.attr('src')).toBe('hl=' + mockSelfLang + '&foo=bar');
      });

      it('should get called when scope.$on(changeLanguage) is fired', function () {
        compileDirective();

        expect(iframe.attr('src')).toBe('hl=' + oldLocale + '&foo=bar');
        scope.$broadcast('changeLanguage');
        expect(iframe.attr('src')).toBe('hl=' + mockSelfLang + '&foo=bar');
      });

      it('should set recaptcha value', function () {
        compileDirective();

        elementScope.setRecaptchaResponse('TEST');
        expect(elementScope.recaptcha).toBe('TEST');
      });
    });

  });
});
