'use strict';

describe('[Module: hp.portal.login.signUpWithProduct] Controller: SignUpWithProductCtrl', function () {

  beforeEach(module('vcRecaptcha'));
  beforeEach(module('unitTestInit'));

  var $q, $timeout;
  var alerts;
  var removedAlerts;
  var mockSelfAccountId = 'mock-self-account-id';
  var mockSelfEmail = 'mock-self-email';
  var mockSelf;

  function Self() {
    this.email = mockSelfEmail;
    this.refresh = function () {
      var dfd = $q.defer();
      if (mockSelf.refreshShouldResolve) {
        dfd.resolve(mockSelf);
      }
      else {
        dfd.reject();
      }
      return dfd.promise;
    };

    // The COMPANY_ADMIN role gets set on mockSelf below.

    this.links = [{
      rel: 'account',
      href: mockSelfAccountId
    }];
  }

  beforeEach(module(function ($provide) {
    $provide.constant('PageAlerts', {
      add: function (alert) {
        alerts.push(alert);
      },
      remove: function (alert) {
        removedAlerts.push(alert);
      },
      timeout: {
        short: 3000,
        medium: 5000,
        long: 10000
      }
    });
    mockSelf = new Self();
    $provide.constant('Self', mockSelf);
  }));
  beforeEach(module('hp.portal.login.signUpWithProduct'));

  beforeEach(inject(function (_$q_, _$timeout_, Roles) {
    $q = _$q_;
    $timeout = _$timeout_;
    mockSelf.roles = [Roles.COMPANY_ADMIN];
  }));

  var mockVcRecaptcha = {
    $create: function () {
      return;
    },
    reload: function () {
      mockVcRecaptcha.reloaded = true;
      return {};
    }
  };

  var mockProductName = 'mock-product-name';
  var localizedProductName = 'localized-product-name';
  var mockProductId = 'mock-product-id';
  var mockOfferingFqName = 'mock-offering-fq-name';
  var mockProduct = {
    name: mockProductName,
    additionalFields: [{
      name: 'Field 1',
      applicableCommerceProviders: [
        'GLIS'
      ]
    }, {
      name: 'Field 2',
      applicableCommerceProviders: [
        'HP_INTERNAL'
      ]
    }, {
      name: 'Field 3',
      applicableCommerceProviders: [
        'HP_INTERNAL'
      ]
    }],
    offering: {
      fqName: mockOfferingFqName
    },
    links: [{
      rel: 'self',
      href: mockProductId
    }]
  };

  var testLicenseAdditionalFields = 'test-license-additional-fields';
  var mockProductService = {
    getOne: function (params) {
      var dfd = $q.defer();
      mockProduct.$promise = dfd.promise;

      mockProductService.params = params;
      if (mockProductService.getOneShouldResolve) {
        dfd.resolve(mockProduct);
      }
      else {
        dfd.reject();
      }

      return mockProduct;
    },
    generateLicenseAdditionalFields: function () {
      return testLicenseAdditionalFields;
    }
  };

  var mockI18n = {
    getBySource: function () {
      var dfd = $q.defer();

      dfd.resolve({
        mockProductName: localizedProductName
      });

      return {
        $promise: dfd.promise
      };
    }
  };

  var mockSignIn = function (loginForm) {
    var dfd = $q.defer();

    mockSignIn.loginForm = loginForm;

    if (mockSignIn.shouldResolve) {
      dfd.resolve();
    }
    else {
      dfd.reject();
    }

    return dfd.promise;
  };

  var mockShoppingCartHref = 'mock-shopping-cart-href';
  var mockShoppingCart = {
    save: function () {
      var dfd = $q.defer();

      if (mockShoppingCart.saveShouldResolve) {
        dfd.resolve();
      }
      else {
        dfd.reject();
      }

      return dfd.promise;
    },
    links: [{
      rel: 'self',
      href: mockShoppingCartHref
    }]
  };

  var mockShoppingCartService = {
    create: function (shoppingCart) {
      mockShoppingCartService.shoppingCart = shoppingCart;
      return mockShoppingCart;
    }
  };

  var mockShoppingCartCheckout = {
    startTask: function (payload) {
      var dfd = $q.defer();

      mockShoppingCartCheckout.payload = payload;

      var task = generateMockTask(mockShoppingCartCheckout.waitForCompleteShouldResolve);
      task.errors = mockShoppingCartCheckout.taskErrors;
      task.status = mockShoppingCartCheckout.status;

      if (mockShoppingCartCheckout.startTaskShouldResolve) {
        dfd.resolve(task);
      }
      else {
        dfd.reject(task);
      }

      return {
        $promise: dfd.promise
      };
    }
  };

  function generateMockTask(shouldResolve) {
    return {
      waitForComplete: function () {
        var dfd = $q.defer();

        if (shouldResolve) {
          dfd.resolve(this);
        }
        else {
          dfd.reject(this);
        }

        return dfd.promise;
      }
    };
  }

  var mockForgotPassword = {
    startTask: function (payload) {
      var dfd = $q.defer();

      mockForgotPassword.payload = payload;

      if (mockForgotPassword.startTaskShouldResolve) {
        dfd.resolve();
      }
      else {
        dfd.reject();
      }

      return {
        $promise: dfd.promise
      };
    }
  };

  var mockCreateAccountAndCheckout = {
    startTask: function (payload) {
      var dfd = $q.defer();

      mockCreateAccountAndCheckout.payload = payload;

      var task = generateMockTask(mockCreateAccountAndCheckout.waitForCompleteShouldResolve);
      task.errors = mockCreateAccountAndCheckout.taskErrors;

      if (mockCreateAccountAndCheckout.startTaskShouldResolve) {
        dfd.resolve(task);
      }
      else {
        dfd.reject(task);
      }

      return {
        $promise: dfd.promise
      };
    }
  };

  var mockQueryParams = function () {
    return {
      productId: mockQueryParams.productId,
      licenseKey: mockQueryParams.licenseKey
    };
  };

  var scope;
  var mockDefaultAccount = {
    name: 'mock-default-account'
  };
  var mockLicenseKey = 'mock-license-key';
  var loadController, taskStatus;
  beforeEach(inject(function ($rootScope, $controller, TaskStatus) {
    taskStatus = TaskStatus;
    // Reset mocks
    alerts = [];
    removedAlerts = [];
    mockVcRecaptcha.reloaded = false;
    mockForgotPassword.startTaskShouldResolve = true;
    mockSelf.refreshShouldResolve = true;
    delete mockProductService.params;
    mockProductService.getOneShouldResolve = true;
    delete mockSignIn.loginForm;
    mockSignIn.shouldResolve = true;
    mockShoppingCart.saveShouldResolve = true;
    delete mockShoppingCartCheckout.payload;
    delete mockShoppingCartCheckout.taskErrors;
    mockShoppingCartCheckout.startTaskShouldResolve = true;
    mockShoppingCartCheckout.waitForCompleteShouldResolve = true;
    mockShoppingCartCheckout.status = taskStatus.PROCESSING;
    delete mockCreateAccountAndCheckout.payload;
    delete mockCreateAccountAndCheckout.taskErrors;
    mockCreateAccountAndCheckout.startTaskShouldResolve = true;
    mockCreateAccountAndCheckout.waitForCompleteShouldResolve = true;
    mockQueryParams.productId = mockProductId;
    mockQueryParams.licenseKey = mockLicenseKey;

    loadController = function () {
      scope = $rootScope.$new();
      $controller('SignUpWithProductCtrl', {
        $scope: scope,
        vcRecaptchaService: mockVcRecaptcha,
        defaultAccount: mockDefaultAccount,
        queryParams: mockQueryParams,
        ProductService: mockProductService,
        ShoppingCartService: mockShoppingCartService,
        ShoppingCartCheckout: mockShoppingCartCheckout,
        CreateAccountAndCheckout: mockCreateAccountAndCheckout,
        i18n: mockI18n,
        signIn: mockSignIn,
        ForgotPassword: mockForgotPassword
      });
    };
  }));

  describe('$scope properties', function () {
    beforeEach(function () {
      loadController();
    });

    it('should include state helpers', function () {
      expect(scope.states).toBeDefined();
      expect(Object.keys(scope.states).length).toBe(9);
      expect(scope.changeState).toBeFunction();
      expect(scope.state).toBe(scope.states.loading);
    });

    it('should include the product', function () {
      expect(scope.product).toBe(mockProduct);
      expect(mockProductService.params).toEqual({
        id: mockProductId,
        expand: 'offering'
      });
    });

    it('should only have GLIS additionalFields', function () {
      expect(scope.product).toBe(mockProduct);
      $timeout.flush();
      expect(scope.product.additionalFields.length).toBe(1);
      expect(scope.product.additionalFields[0]).toEqual({
        name: 'Field 1',
        applicableCommerceProviders: [
          'GLIS'
        ]
      });
    });

    it('should include basic bindings', function () {
      expect(scope.account).toEqual(mockDefaultAccount); // Tests for value equality, not reference equality
      expect(scope.account).not.toBe(mockDefaultAccount); // Tests for reference equality.

      expect(scope.adminContact).toBe(mockSelf);
      expect(scope.bindings).toEqual({});
      expect(scope.licenseData).toEqual({
        licenseKey: mockLicenseKey
      });
      expect(scope.loginForm).toEqual({});
    });

    it('should include form submit functions', function () {
      expect(scope.mainFormSubmit).toBeFunction();
      expect(scope.forgotPasswordFormSubmit).toBeFunction();
    });
  });

  describe('$scope.changeState', function () {
    it('should set the submitButtonText based on a few states', function () {
      loadController();

      expect(scope.state).toBe(scope.states.loading);
      expect(scope.submitButtonText).toBe(undefined);

      scope.changeState(scope.states.noIDontHaveAnAccount);
      expect(scope.submitButtonText).toBe('cSignUp');

      scope.changeState(scope.states.yesIHaveAnAccount);
      expect(scope.submitButtonText).toBe('cSubmit');

      scope.changeState(scope.states.isLoggedIn);
      expect(scope.submitButtonText).toBe('cSubmit');
    });

    it('should change state to doYouHaveAnAccount if the user is not logged in', function () {
      mockSelf.refreshShouldResolve = false;
      loadController();
      $timeout.flush();

      expect(scope.state).toBe(scope.states.doYouHaveAnAccount);
    });
  });

  describe('setFatalError(error)', function () {
    it('should set the invalidParam error for missing productId', function () {
      delete mockQueryParams.productId;

      expect(scope.fatalError).toBeUndefined();

      loadController();

      expect(scope.fatalError).toEqual({
        msg: 'cInvalidParam',
        params: {
          paramName: 'productId'
        }
      });
    });

    it('should set the invalidParam error for invalid productId', function () {
      mockProductService.getOneShouldResolve = false;
      loadController();

      expect(scope.fatalError).toBeUndefined();

      $timeout.flush();

      expect(scope.fatalError).toEqual({
        msg: 'cInvalidParam',
        params: {
          paramName: 'productId'
        }
      });
    });

    it('should set the cNotCompanyAdmin error', function () {
      // Remove the company_admin role from Self.
      mockSelf.roles.length = 0;
      loadController();
      expect(scope.fatalError).toBeUndefined();

      $timeout.flush();

      expect(scope.fatalError).toEqual({
        msg: 'cNotCompanyAdmin'
      });
    });
  });

  describe('$scope.forgotPasswordFormSubmit', function () {
    beforeEach(function () {
      loadController();
    });

    it('should successfully submit the forgotPassword form', function () {
      $timeout.flush();
      var eventCallbackHandled = false;
      var testEmail = 'test-email';

      scope.$on('hpSubmitForgotPasswordForm', function (event, forgotPasswordCallback) {
        var forgotPasswordPromise = forgotPasswordCallback();
        expect(forgotPasswordPromise).toBePromise();

        $timeout.flush();

        expect(mockForgotPassword.payload).toEqual({
          email: testEmail
        });
        expect(scope.state).toBe(scope.states.didYouReceiveAnEmail);

        eventCallbackHandled = true;
      });

      scope.forgotPasswordFormSubmit(testEmail);

      expect(eventCallbackHandled).toBe(true);
    });

    it('should handle an error when submitting the forgotPassword form', function () {
      $timeout.flush();
      var eventCallbackHandled = false;
      var testEmail = 'test-email';
      mockForgotPassword.startTaskShouldResolve = false;
      scope.state = scope.states.doYouHaveAnAccount;

      scope.$on('hpSubmitForgotPasswordForm', function (event, forgotPasswordCallback) {
        var forgotPasswordPromise = forgotPasswordCallback();
        expect(forgotPasswordPromise).toBePromise();

        $timeout.flush();

        expect(mockForgotPassword.payload).toEqual({
          email: testEmail
        });
        expect(scope.state).toBe(scope.states.doYouHaveAnAccount);
        expect(alerts.length).toBe(1);
        expect(alerts[0].msg).toBe('cErrorUnknown');

        eventCallbackHandled = true;
      });

      scope.forgotPasswordFormSubmit(testEmail);

      expect(eventCallbackHandled).toBe(true);
    });
  });

  describe('$scope.mainFormSubmit', function () {
    beforeEach(function () {
      loadController();
    });

    it('should thrown an error for an unexpected state', function () {
      var errorWasThrown = false;

      scope.$on('hpSubmitMainForm', function (event, mainFormDoWork) {
        $timeout.flush();

        scope.changeState(scope.states.fatalError);

        try {
          mainFormDoWork();
        }
        catch (error) {
          errorWasThrown = true;
        }
      });

      scope.mainFormSubmit();
      expect(errorWasThrown).toBe(true);
    });

    describe('state yesIHaveAnAccount', function () {
      it('should successfully login and checkout', function () {
        var eventCallbackHandled = false;
        scope.loginForm = 'test-login-form-data';
        scope.licenseData.licenseKey = mockLicenseKey;

        scope.$on('hpSubmitMainForm', function (event, mainFormDoWork) {
          expect(scope.adminContact.emailVerify).toBe(scope.adminContact.email);

          $timeout.flush();

          scope.changeState(scope.states.yesIHaveAnAccount);
          var mainFormPromise = mainFormDoWork();
          expect(mainFormPromise).toBePromise();

          $timeout.flush();

          expect(scope.state).toBe(scope.states.submitSuccess);
          expect(alerts.length).toBe(1);
          expect(alerts[0].msg).toBe('cCreatingLicenseToProduct');
          expect(removedAlerts.length).toBe(1);
          expect(removedAlerts[0].msg).toBe('cCreatingLicenseToProduct');
          expect(scope.successMessages).toEqual(['cSuccessAddingLicense']);

          expect(mockSignIn.loginForm).toBe(scope.loginForm);
          expect(mockShoppingCartService.shoppingCart).toBeDefined();
          var shoppingCart = mockShoppingCartService.shoppingCart;
          expect(shoppingCart.links).toEqual([{
            rel: 'account',
            href: mockSelfAccountId
          }]);
          expect(shoppingCart.items.length).toBe(1);
          expect(shoppingCart.items[0]).toEqual({
            commerceProvider: 'GLIS',
            licenseKey: mockLicenseKey,
            licenseAdditionalFields: testLicenseAdditionalFields,
            links: [{
              rel: 'product',
              href: mockProductId
            }]
          });

          expect(mockShoppingCartCheckout.payload).toEqual({
            links: [{
              rel: 'shoppingCart',
              href: mockShoppingCartHref
            }]
          });

          eventCallbackHandled = true;
        });

        scope.mainFormSubmit();
        expect(eventCallbackHandled).toBe(true);
        expect(mockVcRecaptcha.reloaded).toBe(false);
      });

      it('should handle errors when logged in user is not a company admin', function () {
        var eventCallbackHandled = false;

        scope.$on('hpSubmitMainForm', function (event, mainFormDoWork) {
          $timeout.flush();

          mockSelf.roles.length = 0;
          scope.changeState(scope.states.yesIHaveAnAccount);

          mainFormDoWork();

          $timeout.flush();

          expect(scope.state).toBe(scope.states.fatalError);
          expect(scope.fatalError).toEqual({
            msg: 'cNotCompanyAdmin'
          });

          eventCallbackHandled = true;
        });

        scope.mainFormSubmit();
        expect(eventCallbackHandled).toBe(true);
      });

      it('should handle errors when ShoppingCartCheckout.startTask fails', function () {
        var eventCallbackHandled = false;

        scope.$on('hpSubmitMainForm', function (event, mainFormDoWork) {
          $timeout.flush();

          scope.changeState(scope.states.yesIHaveAnAccount);
          mockShoppingCartCheckout.startTaskShouldResolve = false;
          mockShoppingCartCheckout.taskErrors = [{
            serviceCode: 1,
            errorCode: 3
          }];

          mainFormDoWork();

          $timeout.flush();

          expect(scope.state).toBe(scope.states.yesIHaveAnAccount);
          expect(alerts.length).toBe(1);
          expect(alerts[0].msg).toBe('cLicenseKeyUsed');
          expect(removedAlerts.length).toBe(0);

          eventCallbackHandled = true;
        });

        scope.mainFormSubmit();
        expect(eventCallbackHandled).toBe(true);
      });

      it('should handle error codes returned from ShoppingCartCheckoutTask', function () {
        var eventCallbackHandled = false;

        scope.$on('hpSubmitMainForm', function (event, mainFormDoWork) {
          $timeout.flush();

          scope.changeState(scope.states.yesIHaveAnAccount);
          mockShoppingCartCheckout.waitForCompleteShouldResolve = false;
          mockShoppingCartCheckout.taskErrors = [{
            serviceCode: 1,
            errorCode: 7
          }];

          mainFormDoWork();

          $timeout.flush();

          expect(scope.state).toBe(scope.states.yesIHaveAnAccount);
          expect(alerts.length).toBe(2);
          expect(alerts[0].msg).toBe('cCreatingLicenseToProduct');
          expect(alerts[1].msg).toBe('cCommerceProviderUnavailable');
          expect(removedAlerts.length).toBe(1);
          expect(removedAlerts[0].msg).toBe('cCreatingLicenseToProduct');

          eventCallbackHandled = true;
        });

        scope.mainFormSubmit();
        expect(eventCallbackHandled).toBe(true);
      });

      it('should handle fallback error message when there are no error codes returned from ShoppingCartCheckoutTask', function () {
        var eventCallbackHandled = false;

        scope.$on('hpSubmitMainForm', function (event, mainFormDoWork) {
          $timeout.flush();

          scope.changeState(scope.states.yesIHaveAnAccount);
          mockShoppingCartCheckout.waitForCompleteShouldResolve = false;

          mainFormDoWork();

          $timeout.flush();

          expect(scope.state).toBe(scope.states.yesIHaveAnAccount);
          expect(alerts.length).toBe(2);
          expect(alerts[0].msg).toBe('cCreatingLicenseToProduct');
          expect(alerts[1].msg).toBe('cErrorCreatingLicense');
          expect(removedAlerts.length).toBe(1);
          expect(removedAlerts[0].msg).toBe('cCreatingLicenseToProduct');

          eventCallbackHandled = true;
        });

        scope.mainFormSubmit();
        expect(eventCallbackHandled).toBe(true);
      });

      it('should gracefully handle timeouts from ShoppingCartCheckoutTask', function () {
        var eventCallbackHandled = false;

        scope.$on('hpSubmitMainForm', function (event, mainFormDoWork) {
          $timeout.flush();

          scope.changeState(scope.states.yesIHaveAnAccount);
          mockShoppingCartCheckout.waitForCompleteShouldResolve = false;
          mockShoppingCartCheckout.status = taskStatus.TIMEOUT;

          mainFormDoWork();

          $timeout.flush();

          expect(scope.state).toBe(scope.states.yesIHaveAnAccount);
          expect(alerts.length).toBe(2);
          expect(alerts[0].msg).toBe('cCreatingLicenseToProduct');
          expect(alerts[1].msg).toBe('cCreateLicenseTimeoutFailed');
          expect(removedAlerts.length).toBe(1);
          expect(removedAlerts[0].msg).toBe('cCreatingLicenseToProduct');

          eventCallbackHandled = true;
        });

        scope.mainFormSubmit();
        expect(eventCallbackHandled).toBe(true);
      });

      it('should fail to log the user in', function () {
        mockSignIn.shouldResolve = false;
        var eventCallbackHandled = false;

        scope.$on('hpSubmitMainForm', function (event, mainFormDoWork) {
          $timeout.flush();

          scope.changeState(scope.states.yesIHaveAnAccount);
          mainFormDoWork();

          $timeout.flush();

          expect(scope.state).toBe(scope.states.yesIHaveAnAccount);
          expect(alerts.length).toBe(1);
          expect(alerts[0].msg).toBe('cErrorUserNamePasswordIncorrect');
          expect(removedAlerts.length).toBe(0);

          eventCallbackHandled = true;
        });

        scope.mainFormSubmit();
        expect(eventCallbackHandled).toBe(true);
      });
    });

    describe('state isLoggedIn', function () {
      it('should successfully checkout', function () {
        var eventCallbackHandled = false;
        scope.licenseData.licenseKey = mockLicenseKey;

        scope.$on('hpSubmitMainForm', function (event, mainFormDoWork) {
          $timeout.flush();

          scope.changeState(scope.states.isLoggedIn);
          var mainFormPromise = mainFormDoWork();
          expect(mainFormPromise).toBePromise();

          $timeout.flush();

          expect(scope.state).toBe(scope.states.submitSuccess);
          expect(alerts.length).toBe(1);
          expect(alerts[0].msg).toBe('cCreatingLicenseToProduct');
          expect(removedAlerts.length).toBe(1);
          expect(removedAlerts[0].msg).toBe('cCreatingLicenseToProduct');
          expect(scope.successMessages).toEqual(['cSuccessAddingLicense']);

          expect(mockShoppingCartService.shoppingCart).toBeDefined();
          var shoppingCart = mockShoppingCartService.shoppingCart;
          expect(shoppingCart.links).toEqual([{
            rel: 'account',
            href: mockSelfAccountId
          }]);
          expect(shoppingCart.items.length).toBe(1);
          expect(shoppingCart.items[0]).toEqual({
            commerceProvider: 'GLIS',
            licenseKey: mockLicenseKey,
            licenseAdditionalFields: testLicenseAdditionalFields,
            links: [{
              rel: 'product',
              href: mockProductId
            }]
          });

          expect(mockShoppingCartCheckout.payload).toEqual({
            links: [{
              rel: 'shoppingCart',
              href: mockShoppingCartHref
            }]
          });

          eventCallbackHandled = true;
        });

        scope.mainFormSubmit();
        expect(eventCallbackHandled).toBe(true);
        expect(mockVcRecaptcha.reloaded).toBe(false);
      });
    });

    describe('state noIDontHaveAnAccount', function () {
      it('should successfully create account and checkout', function () {
        var eventCallbackHandled = false;
        scope.licenseData.licenseKey = mockLicenseKey;

        scope.$on('hpSubmitMainForm', function (event, mainFormDoWork) {
          expect(scope.adminContact.emailVerify).toBe(scope.adminContact.email);

          $timeout.flush();

          scope.bindings.recaptcha = 'test-recaptcha';
          scope.changeState(scope.states.noIDontHaveAnAccount);
          var mainFormPromise = mainFormDoWork();
          expect(mainFormPromise).toBePromise();

          $timeout.flush();

          expect(scope.state).toBe(scope.states.submitSuccess);
          expect(alerts.length).toBe(1);
          expect(alerts[0].msg).toBe('cAddAccountProcessing');
          expect(removedAlerts.length).toBe(1);
          expect(removedAlerts[0].msg).toBe('cAddAccountProcessing');
          expect(scope.successMessages).toEqual(['cSignupSuccessful', 'cSuccessAddingLicense']);

          expect(mockCreateAccountAndCheckout.payload).toBeDefined();
          var payload = mockCreateAccountAndCheckout.payload;
          expect(payload.account).toEqual(mockDefaultAccount);
          expect(payload.adminContact).toBe(scope.adminContact);
          expect(payload.captcha).toBe(scope.bindings.recaptcha);
          expect(payload.shoppingCartItems).toBeDefined();
          expect(payload.shoppingCartItems.length).toBe(1);
          expect(payload.shoppingCartItems[0]).toEqual({
            commerceProvider: 'GLIS',
            licenseKey: mockLicenseKey,
            licenseAdditionalFields: testLicenseAdditionalFields,
            links: [{
              rel: 'product',
              href: mockProductId
            }]
          });

          eventCallbackHandled = true;
        });

        scope.mainFormSubmit();
        expect(mockVcRecaptcha.reloaded).toBe(false);
        expect(eventCallbackHandled).toBe(true);
      });

      it('should reload the vcRecaptcha service upon failure', function () {
        var eventCallbackHandled = false;

        scope.$on('hpSubmitMainForm', function (event, mainFormDoWork) {
          mockCreateAccountAndCheckout.waitForCompleteShouldResolve = false;

          $timeout.flush();

          scope.bindings.recaptcha = 'test-recaptcha';
          scope.changeState(scope.states.noIDontHaveAnAccount);
          mainFormDoWork();

          $timeout.flush();

          expect(scope.state).toBe(scope.states.noIDontHaveAnAccount);
          expect(alerts.length).toBe(2);
          expect(alerts[0].msg).toBe('cAddAccountProcessing');
          expect(alerts[1].msg).toBe('cAddAccountError');
          expect(removedAlerts.length).toBe(1);
          expect(removedAlerts[0].msg).toBe('cAddAccountProcessing');

          eventCallbackHandled = true;
        });

        scope.mainFormSubmit();
        expect(mockVcRecaptcha.reloaded).toBe(true);
        expect(eventCallbackHandled).toBe(true);
      });

      it('should handle errors when CreateAccountAndCheckoutTask.startTask fails', function () {
        var eventCallbackHandled = false;

        scope.$on('hpSubmitMainForm', function (event, mainFormDoWork) {
          $timeout.flush();

          scope.changeState(scope.states.noIDontHaveAnAccount);
          mockCreateAccountAndCheckout.startTaskShouldResolve = false;
          mockCreateAccountAndCheckout.taskErrors = [{
            serviceCode: 1,
            errorCode: 8
          }];

          mainFormDoWork();

          $timeout.flush();

          expect(scope.state).toBe(scope.states.noIDontHaveAnAccount);
          expect(alerts.length).toBe(1);
          expect(alerts[0].msg).toBe('cUnexpectedClientResponse');
          expect(removedAlerts.length).toBe(0);

          eventCallbackHandled = true;
        });

        scope.mainFormSubmit();
        expect(eventCallbackHandled).toBe(true);
      });

      it('should handle error codes returned from CreateAccountAndCheckoutTask', function () {
        var eventCallbackHandled = false;

        scope.$on('hpSubmitMainForm', function (event, mainFormDoWork) {
          $timeout.flush();

          scope.changeState(scope.states.noIDontHaveAnAccount);
          mockCreateAccountAndCheckout.waitForCompleteShouldResolve = false;
          mockCreateAccountAndCheckout.taskErrors = [{
            serviceCode: 1,
            errorCode: 5
          }];

          mainFormDoWork();

          $timeout.flush();

          expect(scope.state).toBe(scope.states.noIDontHaveAnAccount);
          expect(alerts.length).toBe(2);
          expect(alerts[0].msg).toBe('cAddAccountProcessing');
          expect(alerts[1].msg).toBe('cOrderStateInvalid');
          expect(removedAlerts.length).toBe(1);
          expect(removedAlerts[0].msg).toBe('cAddAccountProcessing');

          eventCallbackHandled = true;
        });

        scope.mainFormSubmit();
        expect(eventCallbackHandled).toBe(true);
      });
    });

  });

});
