'use strict';

describe('[Module: hp.portal.login.signUp]', function () {

  beforeEach(module('vcRecaptcha'));
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.login.signUp'));

  var alerts = [];
  var statusCode;
  var reloadRan;

  var mockVcRecaptcha = {
    $create: function () {
      return;
    },
    reload: function () {
      reloadRan = true;
      return {};
    }
  };

  var mockSelf = {
    locale: 'en-US'
  };

  var httpBackend, $q, timeout, linkParser;
  beforeEach(inject(function ($httpBackend, _$q_, $timeout, LinkParser) {
    httpBackend = $httpBackend;
    $q = _$q_;
    timeout = $timeout;
    linkParser = LinkParser;
  }));

  var pageAlerts = {
    add: function (alert) {
      return alerts.push(alert);
    },
    timeout: {
      short: 3000,
      medium: 5000,
      long: 10000
    }
  };

  var mockCreateAccountFactory = {
    startTask: function () {
      var deferred = $q.defer();
      if (statusCode / 100 !== 2) {
        deferred.reject({
          status: statusCode
        });
      }
      else {
        deferred.resolve();
      }
      return {
        $promise: deferred.promise
      };
    }
  };

  var testUser = {
    firstName: 'Nate',
    lastName: 'Blair',
    email: 'jaredormeiscool@hp.com'
  };
  var testAccount = {
    name: 'Matt Falk Imports',
    phoneNumber: '1234567890'
  };
  var testCaptcha = {
    response: '66kjg8723',
    challenge: 'k7saj34328732'
  };

  var formEntry = function (object) {
    object.adminContact.firstName = testUser.firstName;
    object.adminContact.lastName = testUser.lastName;
    object.adminContact.email = testUser.email;
    object.account = testAccount;
    object.captcha = testCaptcha;
  };

  describe('SignUpCtrl', function () {

    var scope, SignUpCtrl, emitFunctionRan;
    var mockDefaultAccount = {
      foo: 'bar'
    };

    beforeEach(inject(function ($rootScope, $controller) {
      scope = $rootScope.$new();
      scope.adminContact = testUser;
      scope.account = testAccount;
      alerts = [];
      emitFunctionRan = false;
      reloadRan = false;
      statusCode = 200;
      SignUpCtrl = $controller('SignUpCtrl', {
        $scope: scope,
        PageAlerts: pageAlerts,
        CreateAccount: mockCreateAccountFactory,
        vcRecaptchaService: mockVcRecaptcha,
        Self: mockSelf,
        defaultAccount: mockDefaultAccount
      });

    }));

    it('should add a single account for Direct Sign-Up', function () {
      formEntry(scope);
      scope.$on('hpSubmitSignUpForm', function (even, addAccount, checkIsInvalid) {
        addAccount();
        timeout.flush();
        expect(checkIsInvalid).toBe(true);
        expect(scope.adminContact.email).toEqual(testUser.email);
        expect(scope.adminContact.emailVerify).toEqual(testUser.email);
        expect(scope.adminContact.firstName).toEqual(testUser.firstName);
        expect(scope.adminContact.lastName).toEqual(testUser.lastName);
        expect(scope.adminContact.locale).toEqual(mockSelf.locale);
        expect(scope.account.name).toEqual(testAccount.name);
        expect(scope.account.phoneNumber).toEqual(testAccount.phoneNumber);
        expect(scope.captcha.response).toEqual('66kjg8723');
        expect(scope.captcha.challenge).toEqual('k7saj34328732');
        expect(scope.submitted).toBe(true);
        expect(alerts.length).toBe(0);
        emitFunctionRan = true;
      });
      scope.signUpSubmit();
      expect(emitFunctionRan).toBe(true);
      expect(reloadRan).toBe(false);
    });

    it('should throw an error and not add a Direct Sign-Up account', function () {
      statusCode = 400;
      formEntry(scope);
      scope.$on('hpSubmitSignUpForm', function (even, addAccount, checkIsInvalid) {
        addAccount();
        timeout.flush();
        expect(checkIsInvalid).toBe(true);
        expect(scope.adminContact.email).toEqual(testUser.email);
        expect(scope.adminContact.emailVerify).toEqual(testUser.email);
        expect(scope.adminContact.firstName).toEqual(testUser.firstName);
        expect(scope.adminContact.lastName).toEqual(testUser.lastName);
        expect(scope.adminContact.locale).toEqual(mockSelf.locale);
        expect(scope.account.name).toEqual(testAccount.name);
        expect(scope.account.phoneNumber).toEqual(testAccount.phoneNumber);
        expect(scope.captcha.response).toEqual('66kjg8723');
        expect(scope.captcha.challenge).toEqual('k7saj34328732');
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
        expect(alerts[0].msg).toBe('cErrorRequestAccount');
        emitFunctionRan = true;
      });
      scope.signUpSubmit();
      expect(emitFunctionRan).toBe(true);
      expect(reloadRan).toBe(true);
    });

    it('should make a copy of the defaultAccount and set it on the scope', function () {
      expect(scope.account).toEqual(mockDefaultAccount); // Tests for value equality, not reference equality
      expect(scope.account).not.toBe(mockDefaultAccount); // Tests for reference equality.
    });

  });

});
