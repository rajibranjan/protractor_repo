'use strict';

describe('[Module: hp.portal.login.reset]', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.login.reset'));

  var alerts = [];
  beforeEach(module(function ($provide) {
    $provide.constant('PageAlerts', {
      add: function (alert) {
        alerts.push(alert);
      },
      timeout: {
        short: 3000,
        medium: 5000,
        long: 10000
      }
    });
  }));

  var $q, timeout;
  beforeEach(inject(function (_$q_, $timeout) {
    $q = _$q_;
    timeout = $timeout;
  }));

  var resetPasswordShouldReturnSuccess;
  var addPasswordError;

  var mockResetPassword = {
    startTask: function () {
      var deferred = $q.defer();
      if (resetPasswordShouldReturnSuccess) {
        deferred.resolve();
      }
      else {
        var payload = {
          errors: []
        };

        if (addPasswordError) {
          payload.errors.push({
            serviceCode: 5,
            errorCode: 3
          });
        }

        deferred.reject(payload);
      }
      return {
        $promise: deferred.promise
      };
    }
  };

  var mockRoute = {
    current: {
      params: {
        id: 'route-id'
      }
    }
  };

  describe('Controller: ResetCtrl', function () {

    var scope, ResetCtrl;

    beforeEach(inject(function ($rootScope, $controller) {
      scope = $rootScope.$new();
      resetPasswordShouldReturnSuccess = true;
      alerts = [];
      addPasswordError = false;
      ResetCtrl = $controller('ResetCtrl', {
        $scope: scope,
        $route: mockRoute,
        ResetPassword: mockResetPassword,
        passwordPolicy: 'STRICT'
      });
    }));

    it('should set items on the scope', function () {
      expect(scope.formData).toBeDefined();
      expect(scope.submit).toBeDefined();
    });

    it('should successfully reset the password for a user', function () {
      var resetPasswordRan = false;
      scope.$on('hpDisableResetForm', function (event, submitFn, checkIsInvalid) {
        submitFn();
        timeout.flush();
        expect(checkIsInvalid).toBe(true);
        expect(scope.hasSuccess).toBe(true);
        resetPasswordRan = true;
      });
      scope.submit();
      expect(resetPasswordRan).toBe(true);
    });

    it('should throw an error reseting the password for a user', function () {
      var resetPasswordRan = false;
      resetPasswordShouldReturnSuccess = false;
      scope.$on('hpDisableResetForm', function (event, submitFn, checkIsInvalid) {
        submitFn();
        timeout.flush();
        expect(checkIsInvalid).toBe(true);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
        expect(alerts[0].msg).toBe('cErrorResetPassword');
        expect(alerts[0].timeout).toBe(10000);
        resetPasswordRan = true;
      });
      scope.submit();
      expect(resetPasswordRan).toBe(true);
    });

    it('should throw an invalid password error when an invalid password error comes back from the server', function () {
      var resetPasswordRan = false;
      resetPasswordShouldReturnSuccess = false;
      addPasswordError = true;
      scope.$on('hpDisableResetForm', function (event, submitFn, checkIsInvalid) {
        submitFn();
        timeout.flush();
        expect(checkIsInvalid).toBe(true);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
        expect(alerts[0].msg).toBe('cPasswordValidationFailed');
        resetPasswordRan = true;
      });
      scope.submit();
      expect(resetPasswordRan).toBe(true);
    });

  });

});
