(function () {
  'use strict';

  /**
   * This module should be included at the beginning of each test suite. It will
   * load any "global" modules that are not part of other modules' dependency trees.
   */
  angular.module('unitTestInit', [
    'appConfig',
    'iOxpwSdkApp',
    'hp.portal.common.init.utils',
    'hp.portal.common.helpers',
    'hp.common.sdk'
  ])

  // A helper function to generate a random alpha-numeric string of the given length.
  .constant('randomString', function (length) {
    return new Array(length + 1).join((Math.random().toString(36) + '00000000000000000').slice(2, 18)).slice(0, length);
  });

  angular.module('appConfig', ['init'])
    .constant('i18nPath', 'app/home/i18n/');

  angular.module('ui-bootstrap-datepicker-tpls', []);

  angular.module('init', [])
    .constant('cdnURL', '/CDN/URL/')
    .constant('serverURL', '/SERVER/URL/')
    .constant('profile', 'local_test')
    .constant('allowedLocales', '["en-US", "fr-CA", "it-IT", "de-DE", "es-ES", "nl-NL", "pt-BR", "pt-PT", "zh-CN", "zh-TW"]')
    .constant('localeMap', {
      'en-US': 'app/common/i18n/locale-en-US.json',
      'fr-CA': 'app/common/i18n/locale-fr-CA.json',
      'de-DE': 'app/common/i18n/locale-de-DE.json',
      'es-ES': 'app/common/i18n/locale-es-ES.json',
      'it-IT': 'app/common/i18n/locale-it-IT.json',
      'nl-NL': 'app/common/i18n/locale-nl-NL.json',
      'pt-BR': 'app/common/i18n/locale-pt-BR.json',
      'pt-PT': 'app/common/i18n/locale-pt-PT.json',
      'zh-CN': 'app/common/i18n/locale-zh-CN.json',
      'zh-TW': 'app/common/i18n/locale-zh-TW.json'
    })
    .constant('redirectUrl', undefined)
    .constant('licenseExpiresIn', undefined);


  angular.module('vcRecaptcha', [])

  .service('vcRecaptchaService', function () {
    return {};
  });

  angular.module('angularFileUpload', [])

  .factory('FileUploader', function() {
    function FileUploader() {}

    FileUploader.prototype.isHTML5 = true;

    return FileUploader;
  });

  beforeEach(function () {
    jasmine.addMatchers({
      toBeFunction: function () {
        return {
          compare: function (actual) {
            var passed = Object.prototype.toString.call(actual) === '[object Function]';

            return {
              pass: passed,
              message: 'Expected ' + Object.prototype.toString.call(actual) + ' to be a function.'
            };
          }
        };
      }
    });

    jasmine.addMatchers({
      toBePromise: function () {
        return {
          compare: function (actual) {
            var passed = true;
            if (actual === undefined) {
              passed = false;
            } else if (typeof actual !== 'object') {
              passed = false;
            } else if (typeof actual.then !== 'function') {
              passed = false;
            } else if (typeof actual.finally !== 'function') {
              passed = false;
            } else if (typeof actual.catch !== 'function') {
              passed = false;
            }

            return {
              pass: passed,
              message: 'Expected ' + Object.prototype.toString.call(actual) + ' to be a promise.'
            };
          }
        };

      }
    });
  });

})();
