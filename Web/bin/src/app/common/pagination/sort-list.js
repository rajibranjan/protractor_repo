(function () {
  'use strict';

  angular.module('hp.portal.common.pagination.sortList', [])

  .directive('sortList', function () {
    var sortGroups = {};
    return {
      restrict: 'E',
      templateUrl: 'app/common/pagination/sort-list.html',
      scope: {
        sortGroup: '@',
        fieldName: '@',
        fieldTitle: '@',
        onSortChange: '=',
        fieldId: '@',
        btnId: '@'

      },
      link: function (scope) {
        if (!(scope.sortGroup in sortGroups)) {
          sortGroups[scope.sortGroup] = {};
        }
        var group = sortGroups[scope.sortGroup];

        scope.reloadAndSort = function (ascending) {
          group.currentField = scope.fieldName;
          if (ascending) {
            scope.currentSelection = 'cAToZ';
          }
          else {
            scope.currentSelection = 'cZToA';
          }
          scope.onSortChange(scope.fieldName, !ascending);
        };
        scope.activeSort = function () {
          return (group.currentField === scope.fieldName);
        };

      }
    };
  });
})();
