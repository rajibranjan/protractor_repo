(function () {
  'use strict';

  angular.module('hp.portal.common.forms.addItem', [])

  .directive('hpAddItemForm', function () {
    return {
      restrict: 'E',
      scope: {
        addItem: '&',
        editItem: '&',
        items: '=',
        displayItemTemplate: '@',
        showAddButtonWhen: '=',
        addItemLabel: '@'
      },
      templateUrl: 'app/common/forms/add-item.html'
    };
  });

})();
