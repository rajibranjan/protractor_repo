(function () {
  'use strict';

  // Load all constants for dependencies
  angular.module('hp.portal.common.forms.pattern', [])

  .directive('hpPattern', function () {
    return {
      require: '^ngModel',
      link: function (scope, element, attrs, ngModelCtrl) {
        var pattern = scope.$eval(attrs.hpPattern);
        ngModelCtrl.$parsers.push(function (value) {
          // And set validity on the model to true if the element 
          // is empty  or passes the regex test
          if (ngModelCtrl.$isEmpty(value) || pattern.test(value)) {
            ngModelCtrl.$setValidity('pattern', true);
            return value;
          }
          else {
            ngModelCtrl.$setValidity('pattern', false);
            return undefined;
          }
        });
      }
    };
  });
})();
