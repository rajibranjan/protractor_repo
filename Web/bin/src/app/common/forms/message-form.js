(function () {
  'use strict';

  angular.module('hp.portal.common.forms.hpMessageForm', [])

  .directive('hpMessageForm', function () {
    return {
      restrict: 'A',
      scope: {
        disableOnEvent: '@',
        heading: '@',
        messageBodyTemplateUrl: '@',
        messageBodyParams: '=',
        submitBtnClick: '&',
        submitBtnLabel: '@'
      },
      templateUrl: 'app/common/forms/message-form.html'
    };
  });
})();
