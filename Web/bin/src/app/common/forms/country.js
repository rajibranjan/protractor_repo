(function () {
  'use strict';

  angular.module('hp.portal.common.forms.country', [])

  .directive('hpCountryField', function ($parse, Countries) {
    return {
      restrict: 'A',
      scope: true,
      templateUrl: 'app/common/forms/country.html',
      compile: function (tElem, tAttrs) {
        var selectElem = tElem.children('select');
        selectElem.attr('ng-model', tAttrs.hpCountryField);
        if (tAttrs.required !== undefined) {
          selectElem.attr('required', tAttrs.required);
        }

        return function link(scope, elem, attrs) {
          scope.countries = Countries;
          scope.selectedValue = $parse(attrs.hpCountryField)(scope);
        };
      }
    };
  });
})();
