(function () {
  'use strict';

  angular.module('hp.common.filters.hpStartsWith', [])

  .filter('hpStartsWith', function () {
    return function (allValues, viewValue, key) {
      var filteredValues = [];

      if (typeof viewValue === 'string') {
        angular.forEach(allValues, function (value) {
          if (!value) {
            return;
          }
          var filterValue;
          if (typeof key === 'undefined') {
            filterValue = value;
          }
          else {
            filterValue = value[key];
          }
          if (filterValue && typeof filterValue === 'string' &&
            filterValue.substr(0, viewValue.length).toLowerCase() === viewValue.toLowerCase()) {
            filteredValues.push(value);
          }
        });
      }
      return filteredValues;
    };
  });
})();
