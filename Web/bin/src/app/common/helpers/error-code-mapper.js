(function () {
  'use strict';

  angular.module('hp.portal.common.helpers', [])

  .factory('ErrorCodeMapper', function ($translate, PageAlerts) {

    function hasErrorCodes(payload) {
      if (payload && payload.errors && payload.errors.length > 0) {
        payload.errors.forEach(function (error) {
          PageAlerts.add({
            type: 'danger',
            msg: getCString(error.serviceCode, error.errorCode),
            translateValues: error,
            timeout: PageAlerts.timeout.long
          });
        });

        return true;
      }
      return false;
    }

    function getCString(serviceCode, errorCode) {
      switch (serviceCode) {
        case 1: // Commerce Service
          switch (errorCode) {
            case 1:
              return 'cLicenseKeyNotFound';
            case 2:
              return 'cLicenseStateInvalid';
            case 3:
              return 'cLicenseKeyUsed';
            case 4:
              return 'cOrderNotFound';
            case 5:
              return 'cOrderStateInvalid';
            case 6:
              return 'cProductMismatch';
            case 7:
              return 'cCommerceProviderUnavailable';
            case 8:
              return 'cUnexpectedClientResponse';
            case 10:
              return 'cLicenseHasPreviousTrialRecord';
          }
          break;

        case 2: // CreateAccountTaskService
          switch (errorCode) {
            case 1:
              return 'cMismatchedRecaptcha';
            case 2:
              return 'cAccessDeniedToNonAffiliateResellers';
          }
          break;

        case 3: // CreateAccountAndCheckoutTaskService
          switch (errorCode) {
            case 1:
              return 'cTaskPayloadMissing';
          }
          break;

        case 4: // CommonErrorCodes
          switch (errorCode) {
            case 1:
              return 'cInternalErrorMissingField';
            case 2:
              return 'cInternalErrorExtraneousField';
            case 3:
              return 'cInvalidValue';
            case 4:
              return 'cIllegalAccess';
            case 7:
              return 'cImmutableField';
            case 9:
              return 'cPasswordErrorBadPassword';
            case 10:
              return 'cUnsupportedOperation';
            case 11:
              return 'cClientError';
          }
          break;

        case 5: // UserService
          switch (errorCode) {
            case 1:
              return 'cEmailAlreadyInUse';
            case 3:
              return 'cPasswordValidationFailed';
          }
          break;

        case 6: // RabbitMQUser Service
          switch (errorCode) {
            case 1:
              return 'cRabbitUserAlreadyExists';
            case 2:
              return 'cRabbitUserNotFound';
          }
          break;
      }

      // There was no error mapping found.
      return 'cUnknownError';
    }

    return {
      getCString: getCString,
      hasErrorCodes: hasErrorCodes,
    };
  });

})();
