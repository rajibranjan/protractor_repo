(function () {
  'use strict';

  angular.module('hp.portal.common.helpers')

  .factory('PasswordPolicy', function (RolesHelper, Roles, PasswordPolicies) {
    var PasswordPolicy = {};

    PasswordPolicy.getForUser = function (user) {
      if (RolesHelper.hasAnyRole(user, [Roles.HP_ADMIN, Roles.OFFERING_ADMIN])) {
        return PasswordPolicies.STRICT;
      }
      else {
        return PasswordPolicies.DEFAULT;
      }
    };

    return PasswordPolicy;
  });

})();
