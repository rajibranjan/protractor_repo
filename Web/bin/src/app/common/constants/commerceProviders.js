(function () {
  'use strict';

  // Load all constants for dependencies
  angular.module('hp.portal.common.constants.commerceProviders', [])
    .constant('CommerceProviders', {
      GLIS: 'GLIS',
      HP_INTERNAL: 'HP_INTERNAL',
      NO_OP: 'NO_OP',
      BETA: 'BETA'
    });

})();
