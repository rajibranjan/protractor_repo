(function () {
  'use strict';

  // Load all constants for dependencies
  angular.module('hp.portal.common.constants.billingCategories', [])
    .constant('BillingCategories', {
      US: 'US',
      INDIA: 'INDIA',
      CONTRACT: 'CONTRACT'
    });

})();
