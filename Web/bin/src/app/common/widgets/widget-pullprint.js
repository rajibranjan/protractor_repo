(function () {
  'use strict';

  angular.module('hp.portal.common.widgets.widgetPullPrint', [
      'hp.portal.home.webservices-v2',
      'hp.portal.common.webservices.entitlements'
    ])
    .controller('PullPrintWidgetCtrl', function ($scope, $timeout, OfferingService, EntitlementService, RestrictionCodes, LinkParser, Self) {
      var offeringFqName = 'hp-pullprint';
      // TODO: the fqName should be pulled from somewhere...ideally passed into the widget.
      OfferingService.getByFqName(offeringFqName).then(function (offering) {
        $scope.offering = offering;
      });

      // refresh every 5 seconds
      var refreshDisabled = false;
      var privatePrintWidgetRefreshTimeout;
      var privatePrintWidgetRefreshTime = 5000;

      function refreshPrivatePrintWidget() {
        if (refreshDisabled) {
          $timeout.cancel(privatePrintWidgetRefreshTimeout);
        }
        else {
          checkOfferingEntitlement().then(function () {
            privatePrintWidgetRefreshTimeout = $timeout(refreshPrivatePrintWidget, privatePrintWidgetRefreshTime);
          });
        }
      }
      // First call to looping function to start the process.
      refreshPrivatePrintWidget();

      function disableRefresh() {
        refreshDisabled = true;
        $timeout.cancel(privatePrintWidgetRefreshTimeout);
      }

      function checkOfferingEntitlement() {
        return EntitlementService.getByUserAndOffering(LinkParser.getSelfId(Self), offeringFqName).then(function success(result) {
          var isEntitled = false;
          var isProvisioning = false;
          angular.forEach(result.data, function (entitlement) {
            if (entitlement.isEntitled) {
              isEntitled = true;
              disableRefresh();
            }
            else {
              // Check to see if provisioning
              angular.forEach(entitlement.restrictions, function (restriction) {
                if (restriction.code === RestrictionCodes.provisioning) {
                  isProvisioning = true;
                }
              });
            }
          });
          $scope.isProvisioning = isProvisioning;
          $scope.isDisabled = !isEntitled;
        }, function error() {
          $scope.isProvisioning = false;
          $scope.isDisabled = true;
        });
      }

      $scope.$on('$destroy', disableRefresh);
    });

})();
