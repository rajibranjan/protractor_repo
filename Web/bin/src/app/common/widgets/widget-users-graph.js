(function () {
  'use strict';

  angular.module('hp.portal.common.widgets.widgetUsersGraph', [
    'angular-flot',
    'pascalprecht.translate'
  ])

  .controller('GraphCtrl', function ($scope, $http, $translate, $q, Self, Routes, LinkParser, Urls, Endpoints) {
    $scope.userStatus = Self.status;
    $scope.manageUsersUrl = '#' + Urls.manageUsers();
    $scope.totalUserCount = 0;
    $scope.graphData = [];

    $scope.graphOptions = {
      series: {
        pie: {
          show: true,
          innerRadius: 0.4,
          offset: {
            top: 0,
            left: 0
          },
          stroke: {
            width: 2
          }
        }
      },
      legend: {
        show: true,
        position: 'sw',
        margin: [10, -90],
        labelFormatter: function (label, series) {
          var thisSeriesCount = series.data[0][1];
          var percentage = $scope.totalUserCount > 0 ? Math.round(thisSeriesCount / $scope.totalUserCount * 100) : 0;
          return thisSeriesCount + ' - ' + label + ' (' + percentage + '%)';
        }
      }
    };


    $scope.displayLoading = true;
    var colors = {
      CREATED: 'rgb(240, 173, 78)',
      ACTIVE: 'rgb(0, 132, 36)',
      SUSPENDED: 'rgb(215, 65, 11)'
    };

    var userStatusHelper = function (getUrl, userStatus) {
      var translatePromise = $translate('c' + userStatus);
      var statusPromise = $http.get(getUrl);
      return $q.all([translatePromise, statusPromise]).then(function (resolves) {
        //idString operations are to produce element IDs as test expects, with title case, and without localization
        var idString = angular.lowercase(userStatus);
        idString = idString.replace(idString.charAt(0), angular.uppercase(idString.charAt(0)));
        var translateString = resolves[0];
        var statusResponse = resolves[1].data;

        $scope.totalUserCount += statusResponse.total;
        $scope.graphData.push({
          id: idString,
          label: translateString,
          status: userStatus,
          data: statusResponse.total,
          color: colors[userStatus]
        });
      });
    };

    var getUserGraphData = function () {
      $scope.graphData.length = 0;
      $scope.totalUserCount = 0;
      var userStatusPromises = [];
      userStatusPromises.push(userStatusHelper(Endpoints.accountUsers.Created, 'CREATED'));
      userStatusPromises.push(userStatusHelper(Endpoints.accountUsers.Active, 'ACTIVE'));
      userStatusPromises.push(userStatusHelper(Endpoints.accountUsers.Suspended, 'SUSPENDED'));
      $q.all(userStatusPromises).then(function success() {
        $scope.legendColSpan = Math.round(12 / $scope.graphData.length);
        $scope.displayLoading = false;
      });
    };

    getUserGraphData();

    $scope.$on('changeLanguage', function () {
      getUserGraphData();
    });

  });

})();
