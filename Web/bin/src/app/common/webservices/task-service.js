(function () {
  'use strict';

  angular.module('hp.portal.common.webservices.task-service', [
    'ngResource',
  ])

  .constant('TaskStatus', {
    PROCESSING: 'PROCESSING',
    COMPLETED: 'COMPLETED',
    FAILED: 'FAILED',
    TIMEOUT: 'TIMEOUT'
  })

  /**
   * The TaskService is a base class to handle common task operations for a single task endpoint.
   * The individual objects returned from TaskService methods are called TaskEntities.
   * For example, TaskService.startTask will return a TaskEntity, which has useful
   * operations like waitForComplete() and getStatus(). See below for more info.
   *
   * The TaskService constructor has the following signature:
   *  - TaskService(taskUrl)
   * The taskUrl is the endpoint for the task that this service represents. For example: '/api/v2/myTask'.
   *
   * The following methods are available on the instantiated TaskService:
   *  - startTask([payload]): fires off an http POST to the task endpoint, with the given payload.
   *      - A TaskEntity is returned. See TaskEntity documentation for more info.
   *  - getExisting(taskId): fires off an http GET to the task endpoint with the given task ID.
   *      - Used to get the status of an existing task by ID. A TaskEntity is returned.
   *
   *
   * The TaskEntity is a special object returned from TaskService operations which contains
   * the data returned from the backend task, as well as other helpful methods. A TaskEntity object
   * will immediately be returned from a TaskService operation, although the data will not be
   * filled out on the object until the actual operation is complete.  The TaskEntity will have a
   * $promise property available immediately, which can be used to wait for the initial operation to complete.
   *
   * The following methods are available on the TaskEntity:
   * - getStatus(): gets the current status of the task. A promise is returned that will resolve when the request is complete.
   *  - waitForComplete([options]): polls the task as long as the task status is PROCESSING.
   *      - returns a promise which will be resolved once the task goes to COMPLETED or FAILED. The promise is
   *        rejected if the task doesn't complete before the max number of polls, max time, or an unknown status is returned.
   *        If the task has already completed when this is called, the promise will be immediately resolved.
   *      - The options parameter can contain the following properties:
   *        - maxPollCount: the maximum number of polls to make before rejecting the promise. Any negative number indicates infinite polling. Defaults to -1.
   *        - maxPollTimeMs: the maximum amount of time in milliseconds to poll before rejecting the promise. Defaults to 3000.
   *        - pollIntervalMs: the interval in milliseconds to wait between each polling attempt. Defaults to 300.
   *
   */
  .factory('TaskService', function ($resource, $q, $timeout, TaskStatus, LinkParser, PageAlerts, ErrorCodeMapper) {

    function updateFromData(data) {
      delete data.$promise;
      delete data.$resolved;
      angular.extend(this, data);
      return this;
    }

    function getTaskStatus(resource) {
      return resource.get({
        id: LinkParser.getSelfId(this)
      }).$promise.then(updateFromData.bind(this), handleHttpError.bindArgs(this));
    }

    function checkStatusAndPoll(pollOptions, task) {
      switch (task.status) {
        // Terminal states.
        case TaskStatus.COMPLETED:
          return task;

        case TaskStatus.FAILED:
        case TaskStatus.TIMEOUT:
          return $q.reject(task);

          // Non-terminal - keep polling until complete.
        case TaskStatus.PROCESSING:
          return pollUntilComplete(pollOptions, task);

        default:
          throw new Error('Unknown task status: ' + task.status);
      }
    }

    function handleHttpError(task, response) {
      task.httpStatus = response.status;
      angular.extend(task, response.data);
      return $q.reject(task);
    }

    function pollUntilComplete(pollOptions, task) {
      // Remove any old message.
      delete task.message;

      if (pollOptions.maxCount >= 0 && pollOptions.count >= pollOptions.maxCount) {
        task.status = TaskStatus.TIMEOUT;
        task.message = 'Max poll count exceeded';
        return $q.reject(task);
      }

      var elapsedTimeMs = Date.now() - pollOptions.beginTime;
      if (elapsedTimeMs > pollOptions.maxTimeMs) {
        task.status = TaskStatus.TIMEOUT;
        task.message = 'Max poll time exceeded';
        return $q.reject(task);
      }

      return $timeout(function () {
        pollOptions.count++;
        return task.getStatus().then(checkStatusAndPoll.bindArgs(pollOptions));
      }, pollOptions.intervalMs);
    }

    var defaultPollOptions = {
      count: 0,
      maxCount: -1,
      maxTimeMs: 3000,
      intervalMs: 300
    };

    function TaskEntity(resource, promise) {
      var _promise = promise.then(updateFromData.bind(this), handleHttpError.bindArgs(this));
      Object.defineReadonlyGetter(this, '$promise', function () {
        return _promise;
      });

      this.waitForComplete = function (options) {
        var pollOptions = angular.copy(defaultPollOptions);
        pollOptions.beginTime = Date.now();

        if (options) {
          if (options.maxPollCount) {
            pollOptions.maxCount = options.maxPollCount;
          }
          if (options.maxPollTimeMs) {
            pollOptions.maxTimeMs = options.maxPollTimeMs;
          }
          if (options.pollIntervalMs) {
            pollOptions.intervalMs = options.pollIntervalMs;
          }
        }

        return _promise.then(checkStatusAndPoll.bindArgs(pollOptions));
      };

      this.getStatus = function () {
        return _promise.then(getTaskStatus.bind(this, resource));
      };

      var defaultPollWithMessagesOptions = {
        pollIntervalMs: 1000,
        maxMessagePollCount: 5,
        shortPollTimeMs: 10000,
        longPollTimeMs: 60000
      };

      this.pollWithMessages = function (messages, pollOptions, count) {
        pollOptions = angular.extend({}, defaultPollWithMessagesOptions, pollOptions);

        count = count || 0;

        if (count > pollOptions.maxMessagePollCount) {
          PageAlerts.add({
            type: 'danger',
            msg: messages.timeoutMsg,
            timeout: PageAlerts.timeout.long
          });
          return;
        }

        var successAlert = {
          type: 'success',
          msg: messages.successMsg,
          timeout: PageAlerts.timeout.medium
        };

        var timeoutAlert;
        if (count === 0) {
          timeoutAlert = {
            type: 'info',
            msg: messages.pleaseWaitMsg,
            timeout: PageAlerts.timeout.long
          };
          pollOptions.maxPollTimeMs = pollOptions.shortPollTimeMs;
        }
        else {
          timeoutAlert = {
            type: 'warning',
            msg: messages.pleaseWaitLongerMsg,
            timeout: PageAlerts.timeout.long
          };
          pollOptions.maxPollTimeMs = pollOptions.longPollTimeMs;
        }
        PageAlerts.add(timeoutAlert);

        // Continue polling, less frequently - display another message after each timeout period (1 minute).
        // We will continue to do this until pollOptions.maxMessagePollCount is reached, at which point we will stop polling.
        return this.waitForComplete(pollOptions)
          .finally(PageAlerts.remove.bindArgs(timeoutAlert))
          .then(PageAlerts.add.bindArgs(successAlert))
          .catch(function (task) {
            if (task.status === TaskStatus.FAILED) {
              if (!ErrorCodeMapper.hasErrorCodes(task)) {
                PageAlerts.add({
                  type: 'danger',
                  msg: messages.fallbackErrorMsg,
                  translateValues: {
                    errorCode: (task.httpStatus || 'unknown')
                  },
                  timeout: PageAlerts.timeout.long
                });
              }
              return $q.reject(task);
            }

            if (task.status === TaskStatus.TIMEOUT) {
              task.status = TaskStatus.PROCESSING;
            }

            task.pollWithMessages(messages, pollOptions, count + 1);
          });
      };
    }

    function TaskService(taskUrl) {
      var resource = $resource(taskUrl + '/:id', {
        id: '@id'
      });

      this.startTask = function (payload) {
        var promise = resource.save(payload).$promise;
        return new TaskEntity(resource, promise);
      };

      this.getExisting = function (taskId) {
        var promise = resource.get({
          id: taskId
        }).$promise;
        return new TaskEntity(resource, promise);
      };
    }

    return TaskService;
  });

})();
