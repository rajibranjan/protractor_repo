'use strict';

angular.module('hp.portal.common.webservices.entitlements', [
  'hp.portal.common.webservices.resource-service'
])

.constant('RestrictionCodes', {
  provisioning: -1592735507,
  eulaAcceptance: 1340150173,
  licenseExpired: 219523870,
  licenseTerminated: 1071278268
})

.factory('EntitlementService', function ($q, ResourceService) {
  var EntitlementService = new ResourceService('/api/v2/entitlements');

  EntitlementService.getWithOffering = function (userId) {
    return EntitlementService.get({
      qs: 'userId==' + userId,
      expand: 'offering'
    });
  };

  EntitlementService.getByUser = function (userId, offeringFqName, expand) {
    var config = {
      qs: 'userId==' + userId,
      latest: true
    };
    if (offeringFqName) {
      config.qs += '&&offeringFqName==' + offeringFqName;
    }
    if (expand) {
      config.expand = expand;
    }
    return EntitlementService.get(config);
  };

  /**
   * @deprecated use getByUser instead
   * @param userId
   * @returns {*}
   */
  EntitlementService.getWithOfferingAndProductAndLicense = function (userId) {
    return EntitlementService.get({
      qs: 'userId==' + userId,
      expand: 'offering,product,license',
      latest: true
    });
  };

  EntitlementService.getByUserAndOffering = function (userId, offeringFqName) {
    return EntitlementService.get({
      qs: 'userId==' + userId + '&&offeringFqName==' + offeringFqName,
      latest: true
    });
  };

  EntitlementService.saveAll = function (entitlements) {
    var promises = entitlements.map(function (entitlement) {
      return entitlement.save();
    });
    return $q.all(promises);
  };

  return EntitlementService;
});
