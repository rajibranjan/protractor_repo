(function () {
  'use strict';

  angular.module('hp.portal.common.modals.licenseCountdownModal', [
    'hp.portal.common.webservices'
  ])

  .factory('LicenseCountdownModal', function ($uibModal) {
    function open(data) {
      var opts = {
        templateUrl: 'app/common/modals/license-countdown-modal.html',
        controller: 'LicenseCountdownModalCtrl',
        resolve: {
          redirectUrl: function () {
            return data.redirectUrl || data.offering.urls.browserLandingUrl;
          },
          daysTillExpiration: function () {
            return data.daysTillExpiration;
          },
          offering: function () {
            return data.offering;
          },
          entitlement: function () {
            return data.entitlement;
          },
        }
      };
      if (data.backdrop && data.backdrop === 'static') {
        opts.backdrop = data.backdrop;
      }
      var modalInstance = $uibModal.open(opts);
      return modalInstance.result;
    }
    return {
      open: open
    };
  })

  .controller('LicenseCountdownModalCtrl', function ($scope, $window, $location, $timeout, $uibModalInstance, redirectUrl, offering, daysTillExpiration, entitlement, LaunchBehaviors, i18n, PortalConfigurations, RestrictionCodes) {
    $scope.offering = offering;
    $scope.translate = {};
    $scope.daysTillExpiration = daysTillExpiration;

    $scope.redirect = function () {
      $uibModalInstance.close();
      if ($scope.product.launchBehavior === LaunchBehaviors.NEW_TAB) {
        $window.open(redirectUrl);
        $window.location.href = PortalConfigurations['com.portal.homeUrl'];
        return;
      }
      $window.location.href = redirectUrl;
    };

    if (entitlement) {
      $scope.isTrial = entitlement.license.isTrial;
      $scope.product = entitlement.product;

      var restrictions = entitlement.restrictions.map(function (restriction) {
        return restriction.code;
      });
      if (restrictions.length && restrictions.indexOf(RestrictionCodes.eulaAcceptance) > -1) {
        redirectUrl = PortalConfigurations['com.portal.eulaPageUrl'] + '#/' + offering.fqName;
      }
    }

    i18n.getBySource(offering.fqName).$promise.then(function success(localeStrings) {
      $scope.translate.productName = localeStrings[$scope.product.name];
    });
  });
})();
