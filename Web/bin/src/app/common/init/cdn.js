'use strict';

angular.module('hp.portal.common.init.cdn', [
  'iOxpwSdkApp'
])

.factory('cdnInterceptor', function ($q, $templateCache, cdnURL, PortalApiRootUrl) {
  return {
    request: function (config) {
      if (!config.url.match(/^https?:\/\//i)) { // Do not modify FQ url requests
        if ($templateCache.get(config.url) === undefined) { // Compiled templates should remain untouched. Not sure why angular still makes an http request for cached templates...)
          var standardURL = config.url.indexOf('/') === 0 ? config.url.substring(1, config.url.length) : config.url;
          if (standardURL.match(/^(api|j_spring|rest|forgot)/i)) {
            config.url = PortalApiRootUrl.url + standardURL;
          }
          else if (standardURL.match(/.json|.html|.js$/)) {
            config.url = cdnURL + standardURL;
          }
        }
      }
      return config;
    }
  };
})

.config(function ($httpProvider) {
  $httpProvider.interceptors.push('cdnInterceptor');
});
