(function () {
  'use strict';

  angular.module('hp.portal.login.unsubscribe', [
    'hp.portal.login.webservices.tasks',
    'hp.portal.common.forms.hpMessageForm'
  ])

  .controller('UnsubscribeCtrl', function ($scope, $location, $window, $routeParams, Unsubscribe, UnsubscribeValidate, PortalConfigurations) {

    $scope.heading = 'cUnsubscribe';
    $scope.messageBodyParams = {
      content: 'cUnsubscribeContent',
      unsubscribeData: null,
      showDisclaimer: true
    };
    $scope.submitBtnLabel = 'cConfirm';

    var formHasBeenSubmitted = false;
    var submitFn = function () {
      return Unsubscribe.startTask($scope.messageBodyParams.unsubscribeData)
        .$promise.then(function success() {
          $scope.heading = 'cUnsubscribeSuccessful';
          $scope.messageBodyParams.content = 'cUnsubscribeContentSuccess';
        }, function error() {
          $scope.heading = 'cError';
          $scope.messageBodyParams.content = 'cUnsubscribeContentError';
        }).finally(function () {
          formHasBeenSubmitted = true;
          $scope.submitBtnLabel = 'cHome';
          $scope.messageBodyParams.showDisclaimer = false;
        });
    };

    $scope.messageBodyParams.unsubscribeData = UnsubscribeValidate.getExisting($routeParams.id);

    $scope.submit = function () {
      if (formHasBeenSubmitted) {
        $window.location.href = PortalConfigurations['com.portal.homeUrl'];
      }
      else {
        $scope.$broadcast('hpDisableUnsubscribeForm', submitFn, true);
      }
    };

  });

})();
