(function () {
  'use strict';

  angular.module('hp.portal.login.signIn', [
    'hp.portal.login.webservices.tasks',
    'hp.portal.common.modals.licenseCountdownModal',
    'hp.portal.common.webservices',
    'hp.portal.common.webservices.entitlements',
    'angularMoment'
  ])

  .constant('LaunchBehaviors', {
    NEW_TAB: 'NEW_TAB',
    SAME_TAB: 'SAME_TAB'
  })

  .controller('SignInCtrl', function ($scope, $window, $timeout, signIn, Self, LinkParser, EntitlementService, LicenseCountdownModal, PageAlerts, PortalConfigurations, queryParams) {

    // Create a blank object to hold our form data
    $scope.formData = {};

    function signInHandler() {
      // This function must return a promise for the hpDisableOn directive.
      return signIn($scope.formData).then(loginSuccess, function loginError() {
        PageAlerts.add({
          type: 'danger',
          msg: 'cErrorUserNamePasswordIncorrect',
          timeout: PageAlerts.timeout.long
        });

        /*jshint camelcase: false */
        // Clear inputs
        $scope.formData.j_password = '';
        /*jshint camelcase: true */

        // The password input does not receive focus without this timeout
        $timeout(function () {
          angular.element('#passwordValue').focus();
        }, 100);
      });
    }

    $scope.submit = function () {
      $scope.$broadcast('hpDisableSignInForm', signInHandler);
    };

    function loginSuccess(signInResponse) {
      var redirectUrl = signInResponse.headers('location');
      var expiresIn = signInResponse.headers('X-Expires-In');

      if (!redirectUrl) {
        return redirectToHome();
      }

      // If there is an X-Expires-In header then we need to launch the License-Countdown-Modal.
      if (expiresIn) {
        return Self.refresh().then(openLicenseCountdownModal.bindArgs(redirectUrl, expiresIn));
      }
      else {
        $window.location.href = redirectUrl;
      }
    }

    function openLicenseCountdownModal(redirectUrl, expiresIn) {
      return EntitlementService.getByUser(LinkParser.getSelfId(Self), queryParams().redirect, 'offering,product,license').then(function (response) {
        var entitlement = response.data && response.data.length ? response.data[0] : undefined;

        if (!entitlement) {
          return redirectToHome();
        }

        var modalData = {
          redirectUrl: redirectUrl,
          offering: entitlement.offering,
          daysTillExpiration: expiresIn,
          backdrop: 'static',
          entitlement: entitlement,
        };

        return LicenseCountdownModal.open(modalData).then(redirectToHome);
      });
    }

    function redirectToHome() {
      return PortalConfigurations.$promise.then(function () {
        $window.location.href = PortalConfigurations['com.portal.homeUrl'];
      });
    }
  });
})();
