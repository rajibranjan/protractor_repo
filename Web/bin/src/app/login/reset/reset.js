(function () {
  'use strict';

  angular.module('hp.portal.login.reset', [
    'hp.portal.login.webservices.tasks'
  ])

  .controller('ResetCtrl', function ($scope, $route, ResetPassword, PageAlerts, ErrorCodeMapper, UserRole, Globals, RolesHelper, Roles, passwordPolicy) {

    $scope.passwordPolicy = passwordPolicy;

    // Create a blank object to hold our form data
    $scope.formData = {};

    var submitFn = function () {

      // Make sure you return a promise still with your new line, hpDisableOn directive now needs it. ~Nate
      return ResetPassword.startTask({
          password: $scope.formData.password,
          verificationHash: $route.current.params.id
        })
        .$promise.then(function () {
          $scope.hasSuccess = true;
        }, function error(task) {
          if (!ErrorCodeMapper.hasErrorCodes(task)) {
            PageAlerts.add({
              type: 'danger',
              msg: 'cErrorResetPassword',
              timeout: 10000
            });
          }
        });
    };

    // Submit the form data
    $scope.submit = function () {
      $scope.$broadcast('hpDisableResetForm', submitFn, true);
    };
  });

})();
