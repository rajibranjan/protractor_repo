(function () {
  'use strict';

  angular.module('hp.portal.login.webservices.tasks', [
    'ngRoute',
    'hp.portal.common.webservices.task-service',
    'hp.portal.common.webservices.endpoints',
  ])

  .factory('ForgotPassword', function (Endpoints, TaskService) {
    return new TaskService(Endpoints.forgotPassword);
  })

  .factory('ResetPassword', function (Endpoints, TaskService) {
    return new TaskService(Endpoints.resetPassword);
  })

  .factory('ActivateUser', function (Endpoints, TaskService) {
    return new TaskService(Endpoints.activateUser);
  })

  .factory('UserRole', function (Endpoints, TaskService) {
    return new TaskService(Endpoints.userRole);
  })

  .factory('UnsubscribeValidate', function (Endpoints, TaskService) {
    return new TaskService(Endpoints.unsubscribeValidate);
  })

  .factory('Unsubscribe', function (Endpoints, TaskService) {
    return new TaskService(Endpoints.unsubscribe);
  })

  .factory('signIn', function ($http, Endpoints) {
    return function (formData) {
      return $http({
        method: 'POST',
        url: Endpoints.signIn,
        data: angular.element.param(formData), // pass stringified data
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        } //set the headers so angular passes info as form data (not request payload)
      });
    };
  });

})();
