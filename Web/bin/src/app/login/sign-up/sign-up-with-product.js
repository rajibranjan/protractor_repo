(function () {
  'use strict';

  angular.module('hp.portal.login.signUpWithProduct', [
    'vcRecaptcha',
    'hp.portal.common.webservices',
    'hp.portal.common.webservices.tasks',
    'hp.portal.common.constants.commerceProviders',
    'hp.portal.login.webservices.tasks',
    'hp.portal.login.signUp.signUpForm',
  ])

  .controller('SignUpWithProductCtrl', function ($scope, $q, queryParams, defaultAccount, PageAlerts, TaskStatus, CreateAccountAndCheckout, vcRecaptchaService, Self, ProductService, ShoppingCartService, ShoppingCartCheckout, RolesHelper, Roles, ForgotPassword, CommerceProviders, LinkParser, signIn, i18n, ErrorCodeMapper) {
    var states = $scope.states = {
      loading: 'loading',
      fatalError: 'fatalError',
      isLoggedIn: 'isLoggedIn',
      doYouHaveAnAccount: 'doYouHaveAnAccount',
      yesIHaveAnAccount: 'yesIHaveAnAccount',
      noIDontHaveAnAccount: 'noIDontHaveAnAccount',
      imNotSureIfIHaveAnAccount: 'imNotSureIfIHaveAnAccount',
      didYouReceiveAnEmail: 'didYouReceiveAnEmail',
      submitSuccess: 'submitSuccess',
    };

    function changeState(newState) {
      $scope.state = newState;

      if (newState === states.noIDontHaveAnAccount) {
        $scope.submitButtonText = 'cSignUp';
      }
      else if (newState === states.yesIHaveAnAccount || newState === states.isLoggedIn) {
        $scope.submitButtonText = 'cSubmit';
      }
    }
    $scope.changeState = changeState;

    // Start off in the loading state.
    changeState(states.loading);

    function setFatalError(error) {
      $scope.fatalError = error;
      changeState(states.fatalError);
    }

    if (!queryParams().productId) {
      setFatalError({
        msg: 'cInvalidParam',
        params: {
          paramName: 'productId'
        }
      });
      return;
    }

    // Fetch the product and expand offering.
    $scope.product = ProductService.getOne({
      id: queryParams().productId,
      expand: 'offering'
    });
    var productPromise = $scope.product.$promise.then(function (product) {
      i18n.getBySource(product.offering.fqName).$promise.then(function (i18nData) {
        product.productName = i18nData[product.name];
        // Filter out any additionalFields that are not applicable to this page - this is the GLIS path
        for (var i = product.additionalFields.length - 1; i >= 0; i--) {
          var index = product.additionalFields[i].applicableCommerceProviders.indexOf(CommerceProviders.GLIS);
          if (index === -1) {
            product.additionalFields.splice(i, 1);
          }
        }
      });
    }, function () {
      setFatalError({
        msg: 'cInvalidParam',
        params: {
          paramName: 'productId'
        }
      });
      return $q.reject();
    });

    function refreshSelf() {
      return Self.refresh().then(function isLoggedIn() {
        if (!RolesHelper.selfHasRole(Roles.COMPANY_ADMIN)) {
          setFatalError({
            msg: 'cNotCompanyAdmin'
          });
          return $q.reject();
        }
        return true;
      }, function notLoggedIn() {
        return false;
      });
    }

    // Check if the user is already logged in.
    var isLoggedInPromise = refreshSelf();

    // Wait for all necessary promises to finish, and then change the state as appropriate.
    $q.all([productPromise, isLoggedInPromise]).then(function (resolutions) {
      var isLoggedIn = resolutions[1];
      if (isLoggedIn) {
        changeState(states.isLoggedIn);
      }
      else {
        changeState(states.doYouHaveAnAccount);
      }
    });

    $scope.account = angular.copy(defaultAccount);
    $scope.adminContact = Self;
    $scope.bindings = {};
    $scope.licenseData = {};
    $scope.loginForm = {};

    if (queryParams().licenseKey) {
      $scope.licenseData.licenseKey = queryParams().licenseKey;
    }

    function mainFormDoWork() {
      var shoppingCartItem = {
        commerceProvider: CommerceProviders.GLIS,
        licenseKey: $scope.licenseData.licenseKey,
        licenseAdditionalFields: ProductService.generateLicenseAdditionalFields($scope.product, $scope.bindings.additionalFieldValues),
        links: [{
          rel: 'product',
          href: LinkParser.getSelf($scope.product).href
        }]
      };

      // This function must return a promise, so that it works with the hp-disable-on directive.
      switch ($scope.state) {
        case states.isLoggedIn:
          return submitShoppingCart(shoppingCartItem);
        case states.yesIHaveAnAccount:
          return login().then(submitShoppingCart.bindArgs(shoppingCartItem));
        case states.noIDontHaveAnAccount:
          return createAccountAndCheckout(shoppingCartItem);
        default:
          throw new Error('Unexpected state upon submit: ' + $scope.state);
      }
    }

    function login() {
      return signIn($scope.loginForm).then(refreshSelf, function () {
        PageAlerts.add({
          type: 'danger',
          msg: 'cErrorUserNamePasswordIncorrect',
          timeout: PageAlerts.timeout.long
        });
        return $q.reject();
      });
    }

    function createAccountAndCheckout(shoppingCartItem) {
      var taskPayload = {
        account: $scope.account,
        adminContact: $scope.adminContact,
        captcha: $scope.bindings.recaptcha,
        shoppingCartItems: [shoppingCartItem]
      };

      var task = new CreateAccountAndCheckout.startTask(taskPayload);

      return waitForTask(task.$promise, 'cAddAccountProcessing', ['cSignupSuccessful', 'cSuccessAddingLicense'], 'cAddAccountError', 'cAddAccountTimeoutFailed')
        .catch(vcRecaptchaService.reload);
    }

    function submitShoppingCart(shoppingCartItem) {
      var shoppingCart = ShoppingCartService.create({
        items: [shoppingCartItem],
        links: [{
          rel: 'account',
          href: LinkParser.getLink(Self, 'account').href
        }]
      });

      var taskPromise = shoppingCart.save().then(function () {
        return ShoppingCartCheckout.startTask({
          links: [{
            rel: 'shoppingCart',
            href: LinkParser.getSelf(shoppingCart).href
          }]
        }).$promise;
      });

      return waitForTask(taskPromise, 'cCreatingLicenseToProduct', ['cSuccessAddingLicense'], 'cErrorCreatingLicense', 'cCreateLicenseTimeoutFailed');
    }

    function waitForTask(taskPromise, pleaseWaitAlertMsg, successMessages, fallbackErrorMsg, timeoutMessage) {
      return taskPromise.then(function (task) {
          var pleaseWaitAlert = {
            type: 'info',
            msg: pleaseWaitAlertMsg
          };
          PageAlerts.add(pleaseWaitAlert);

          // After the task has been started, now we need to wait for it to complete.
          return task.waitForComplete({
              maxPollTimeMs: 60000
            })
            .finally(PageAlerts.remove.bindArgs(pleaseWaitAlert));
        })
        .then(function success() {
          changeState(states.submitSuccess);
          $scope.successMessages = successMessages;
        }, function error(task) {
          if (task.status === TaskStatus.TIMEOUT) {
            PageAlerts.add({
              type: 'warning',
              msg: timeoutMessage,
              timeout: PageAlerts.timeout.long
            });
          }
          else if (!ErrorCodeMapper.hasErrorCodes(task)) {
            // Alert a generic error message because there was not a specific errorCode.
            PageAlerts.add({
              type: 'danger',
              msg: fallbackErrorMsg,
              translateValues: {
                errorCode: (task.httpStatus || 'unknown')
              },
              timeout: PageAlerts.timeout.long
            });
          }

          return $q.reject();
        });
    }

    function forgotPassword(email) {
      return ForgotPassword.startTask({
          email: email
        })
        .$promise.then(function success() {
            changeState(states.didYouReceiveAnEmail);
          },
          function error() {
            PageAlerts.add({
              type: 'danger',
              msg: 'cErrorUnknown',
              timeout: PageAlerts.timeout.long
            });
          });
    }


    $scope.mainFormSubmit = function () {
      // On the Add-User and Add-Accounts pages the user has to enter their email twice to verify.
      // This is here to eliminate that requirement for the sign up.
      $scope.adminContact.emailVerify = $scope.adminContact.email;

      $scope.$broadcast('hpSubmitMainForm', mainFormDoWork);
    };

    $scope.forgotPasswordFormSubmit = function (forgotPasswordEmail) {
      $scope.$broadcast('hpSubmitForgotPasswordForm', forgotPassword.bindArgs(forgotPasswordEmail));
    };

  });

})();
