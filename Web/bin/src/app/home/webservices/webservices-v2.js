(function () {
  'use strict';

  angular.module('hp.portal.home.webservices-v2', [
    'hp.portal.common.webservices.resource-service',
    'hp.portal.common.webservices.pageable-resource'
  ])

  .factory('AccountGroupService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/accountGroups');
  })

  .factory('AccountService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/accounts');
  })

  .factory('AMQPGlobalConfigurationService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/amqpGlobalConfigurations');
  })

  .factory('DeadMessageService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/deadMessages');
  })

  .factory('DownloadService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/downloads');
  })

  .factory('ResellerService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/resellers');
  })

  .factory('PermittedFqNameService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/permittedFqNames');
  })

  .factory('OfferingIconService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/offeringIcons');
  })

  .factory('I18nService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/i18n');
  })

  .factory('OfferingServiceAccountsService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/serviceUsers');
  })

  .factory('RabbitMQServiceUserService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/rabbitMQUsers');
  })

  .factory('SchemaVersionService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/schemaVersions');
  })

  .factory('UserService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/users');
  })

  .factory('LicenseService', function (ResourceService) {
    return new ResourceService('/api/v2/licenses');
  })

  .factory('MeteringInfoService', function (PageableResourceService) {
    return new PageableResourceService('/api/v2/meteringInfo');
  })

  .factory('AccountServiceRes', function (ResourceService) {
    return new ResourceService('/api/v2/accounts');
  })

  .factory('DeviceService', function (PageableResourceService, PortalConfigurations) {
    return new PageableResourceService(PortalConfigurations['com.portal.titanDevicesUrl']);
  })

  .factory('Queries', function ($http) {
    function Query(url) {
      this.url = url;
    }
    Query.prototype.get = function (params) {
      return $http.get(this.url, {
        params: params
      }).then(function success(response) {
        return response.data;
      });
    };

    return {
      OfferingsAndProductsByUser: new Query('/api/v2/queries/offeringsAndProductsByUser'),
      ProductsAndUsersByOfferingAndAccount: new Query('/api/v2/queries/productsAndUsersByOfferingAndAccount')
    };
  });

})();
