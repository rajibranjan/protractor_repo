(function () {
  'use strict';

  angular.module('hp.portal.home.navigation.routesAndUrls', [])

  .constant('Routes', {
    dashboard: '/dashboard',
    dashboardV2: '/dashboard-v2',
    resellerDashboardV2: '/resellers/:resellerId/dashboard-v2',
    appCatalog: '/catalog',
    appManagement: '/app-management',
    utilities: '/utilities',
    myCompany: '/my-company',
    help: '/help',
    profile: '/profile',
    addUser: '/users/add-user',
    csvImport: '/users/csv-import',
    users: '/users',
    userDetails: '/users/:id',
    accounts: '/accounts',
    accountUsers: '/accounts/:accountId/users',
    addAccount: '/accounts/add-account',
    accountDetails: '/accounts/:id',
    resellers: '/resellers',
    resellerDetails: '/resellers/:id',
    addReseller: '/resellers/add-reseller',
    resellerAccounts: '/resellers/:resellerId/accounts',
    devices: '/devices',
    adminDashboard: '/admin-dashboard',
    deadMessages: '/admin-dashboard/dead-messages',
    dbMigrations: '/admin-dashboard/db-migrations',
    entityRepair: '/admin-dashboard/entity-repair',
    amqpGlobalConfigurations: '/admin-dashboard/amqpGlobalConfigurations',
    accountGroups: '/admin-dashboard/account-groups',
    accountGroupsDetails: '/accountGroups/:id',
    stackCleanup: '/admin-dashboard/stack-cleanup',
    welcome: '/welcome',
    serviceAccounts: '/service-accounts',
    permissionDenied: '/permission-denied',
    internalError: '/internal-error',
    accountsV2: '/accounts-v2',
    resellerAccountsV2: '/resellers/:resellerId/accounts-v2',
    devicesV2: '/devices-v2'
  })

  .run(function (Globals, Routes) {
    Globals.routes = Routes;
  })

  .factory('Urls', function (LinkParser, Self, Globals, Routes) {
    return {
      manageUsers: function () {
        if (Globals.permissions.isHPAdmin) {
          return Routes.users;
        }
        else {
          return Routes.accountUsers.replace(':accountId', LinkParser.getLinkId(Self, 'account'));
        }
      },
      manageAccounts: function () {
        if (Globals.permissions.isHPAdmin) {
          return Routes.accounts;
        }
        else {
          return Routes.resellerAccounts.replace(':resellerId', LinkParser.getLinkId(Self, 'account'));
        }
      },
      manageResellers: function () {
        return Routes.resellers;
      }
    };
  })

  .constant('Actions', {
    ViewProfile: 'profile',
    ViewAppCatalog: 'appCatalog',
    ViewAppManagement: 'appManagement',
    AddUser: 'addUser',
    ImportUsers: 'csvImporting',
    ViewAllUsers: 'users',
    ViewUser: 'user',
    ViewAllResellers: 'resellers',
    ViewReseller: 'reseller',
    AddReseller: 'addReseller',
    ViewResellerAccounts: 'viewResellerAccounts',
    ViewAllAccounts: 'accounts',
    ViewAccountUsers: 'accountUsers',
    AddAccount: 'addAccount',
    AccountGroupsDetails: 'accountGroupsDetails',
    ViewAccount: 'account',
    ViewAdminDashboard: 'adminDashboard',
    ViewMyCompany: 'MyCompany',
    ManageServiceAccounts: 'serviceAccounts',
    ViewCompanyDevices: 'devicesV2',
    ViewResellerAccountsV2: 'resellerAccountsV2',
    ViewAllAccountsV2: 'accountsV2',
    ViewDashboardV2: 'dashboardV2',
    ViewResellerDashboardV2: 'resellerDashboardV2'
  })

  .factory('AvailableActions', function (Actions, Self, Globals) {
    return Self.$promise.then(function () {
      if (Globals.permissions.isHPAdmin) {
        // HP Admin gets all actions.
        return Object.keys(Actions).map(function (key) {
          return Actions[key];
        });
      }

      var actions = [];

      if (Globals.permissions.isUser) {
        actions = actions.concat(Actions.ViewProfile);
      }

      if (Globals.permissions.isCompanyAdmin) {
        // These are actions that both Company Admins and Resellers should get.
        actions = actions.concat(Actions.ViewMyCompany, Actions.AddUser, Actions.ImportUsers, Actions.ViewUser, Actions.ViewAccountUsers);

        if (!Globals.permissions.isReseller) {
          // These are actions that only Company Admins should get, but not Resellers.
          actions = actions.concat(Actions.ViewAppCatalog, Actions.ViewAppManagement, Actions.ViewCompanyDevices);
        }
      }

      if (Globals.permissions.isReseller) {
        actions = actions.concat(Actions.ViewReseller, Actions.ViewResellerAccounts, Actions.ViewAccount, Actions.AddAccount, Actions.ViewResellerDashboardV2, Actions.ViewResellerAccountsV2);

      }

      if (Globals.permissions.isOfferingAdmin) {
        actions = actions.concat(Actions.ManageServiceAccounts);
      }

      return actions;
    });
  })

  .factory('RoutePermissions', function (Routes, Actions, AvailableActions) {
    return AvailableActions.then(function (AvailableActions) {
      var availableRoutes = [Routes.help, Routes.dashboard, Routes.devices, Routes.utilities, Routes.welcome, Routes.permissionDenied, Routes.internalError];
      angular.forEach(AvailableActions, function (action) {
        switch (action) {
          case Actions.ViewProfile:
            availableRoutes.push(Routes.profile);
            break;
          case Actions.AddUser:
            availableRoutes.push(Routes.addUser);
            break;
          case Actions.ImportUsers:
            availableRoutes.push(Routes.csvImport);
            break;
          case Actions.ViewAllUsers:
            availableRoutes.push(Routes.users);
            break;
          case Actions.ViewUser:
            availableRoutes.push(Routes.userDetails);
            break;
          case Actions.ViewAllResellers:
            availableRoutes.push(Routes.resellers);
            break;
          case Actions.ViewReseller:
            availableRoutes.push(Routes.resellerDetails);
            break;
          case Actions.AddReseller:
            availableRoutes.push(Routes.addReseller);
            break;
          case Actions.ViewResellerAccounts:
            availableRoutes.push(Routes.resellerAccounts);
            break;
          case Actions.ViewAllAccounts:
            availableRoutes.push(Routes.accounts);
            break;
          case Actions.ViewAccountUsers:
            availableRoutes.push(Routes.accountUsers);
            break;
          case Actions.AddAccount:
            availableRoutes.push(Routes.addAccount);
            break;
          case Actions.ManageServiceAccounts:
            availableRoutes.push(Routes.serviceAccounts);
            break;
          case Actions.AccountGroupsDetails:
            availableRoutes.push(Routes.accountGroupsDetails);
            break;
          case Actions.ViewAccount:
            availableRoutes.push(Routes.accountDetails);
            break;
          case Actions.ViewMyCompany:
            availableRoutes.push(Routes.myCompany);
            break;
          case Actions.ViewAppCatalog:
            availableRoutes.push(Routes.appCatalog);
            break;
          case Actions.ViewAppManagement:
            availableRoutes.push(Routes.appManagement);
            break;
          case Actions.ViewResellerAccountsV2:
            availableRoutes.push(Routes.resellerAccountsV2);
            break;
          case Actions.ViewAllAccountsV2:
            availableRoutes.push(Routes.accountsV2);
            break;
          case Actions.ViewCompanyDevices:
            availableRoutes.push(Routes.devicesV2);
            break;
          case Actions.ViewDashboardV2:
            availableRoutes.push(Routes.dashboardV2);
            break;
          case Actions.ViewResellerDashboardV2:
            availableRoutes.push(Routes.resellerDashboardV2);
            break;
        }
      });
      return availableRoutes;
    });
  });

})();
