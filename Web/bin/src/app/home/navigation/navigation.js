(function () {
  'use strict';

  angular.module('hp.portal.home.navigation', [
      'ngRoute',
      'hp.portal.home.navigation.routesAndUrls'
    ])
    .controller('NavCtrl', function ($scope, $route, Globals, Routes, Actions, AvailableActions, Urls) {

      var randomFeedbackNum = Math.floor(Math.random() * 5) + 1; // integer between 1-5
      $scope.feedbackTitle = 'cFeedbackTitle' + randomFeedbackNum;
      $scope.feedbackPlaceholder = 'cFeedbackPlaceholder' + randomFeedbackNum;


      $scope.showSidebar = function () {
        if (!$scope.views) {
          return false;
        }
        return $scope.currentView().showSidebar;
      };

      var currentRoutePath = function () {
        return $route.current.$$route && $route.current.$$route.originalPath;
      };

      $scope.currentView = function () {
        if (!$scope.views) {
          return {};
        }
        return $scope.views[currentRoutePath()] || {};
      };

      $scope.navs = [];

      $scope.navIsActive = function (nav) {
        var activeView = $scope.currentView();
        if (activeView.parent) {
          return nav === $scope.views[activeView.parent].nav;
        }
        return nav === activeView.nav;
      };

      AvailableActions.then(function (AvailableActions) {
        var hasAction = function (action) {
          return AvailableActions.indexOf(action) !== -1;
        };
        $scope.views = {
          dashboard: {
            showSidebar: true,
            nav: {
              title: 'cDashboard',
              url: '#' + Routes.dashboard,
              id: 'home-dashboard',
              icon: 'dashboard'
            }
          },
          dashboardV2: {
            showSidebar: true,
            parent: 'dashboard'
          },
          resellerDashboardV2: {
            showSidebar: true,
            parent: 'dashboard'
          },
          appCatalog: {
            showSidebar: true,
            nav: {
              title: 'cAppCatalog',
              url: '#' + Routes.appCatalog,
              id: 'home-catalog',
              icon: 'apps',
              profiled: true,
              exclude: (true || !hasAction(Actions.ViewAppCatalog))
            }
          },
          appManagement: {
            showSidebar: true,
            nav: {
              title: 'cAppManagement',
              url: '#' + Routes.appManagement,
              id: 'home-app-management',
              icon: 'app-management',
              exclude: !hasAction(Actions.ViewAppManagement)
            }
          },
          utilities: {
            showSidebar: true,
            nav: {
              title: 'cUtilities',
              url: '#' + Routes.utilities,
              id: 'home-utilities',
              icon: 'utilities'
            }
          },
          myCompany: {
            showSidebar: true,
            nav: {
              title: 'cMyCompany',
              exclude: !hasAction(Actions.ViewMyCompany),
              url: '#' + Routes.myCompany,
              id: 'home-my-company',
              icon: 'user'
            }
          },
          serviceAccounts: {
            showSidebar: true,
            nav: {
              profiled: true,
              title: 'cServiceAccounts',
              exclude: !hasAction(Actions.ManageServiceAccounts),
              url: '#' + Routes.serviceAccounts,
              id: 'home-service-accounts',
              icon: 'app-management'
            }
          },
          help: {
            showSidebar: true
          },
          profile: {
            breadcrumb: [{
              title: 'cMyProfile'
            }],
            showSidebar: true
          },
          addUser: {
            breadcrumb: [{
              title: 'cAddUser'
            }],
            showSidebar: true,
            parent: 'users'
          },
          csvImport: {
            breadcrumb: [{
              title: 'cImportFromCsv'
            }],
            showSidebar: true,
            parent: 'users'
          },
          users: {
            showSidebar: true,
            nav: {
              title: 'cManageUsers',
              // Exclude if you can't view the users of an account OR if you are a resller
              exclude: !hasAction(Actions.ViewAccountUsers),
              url: '#' + Urls.manageUsers(),
              id: 'home-users',
              icon: 'manage-users'
            }
          },
          userDetails: {
            breadcrumb: [{
              title: 'cEditUser'
            }],
            showSidebar: true,
            parent: 'users'
          },
          accounts: {
            showSidebar: true,
            nav: {
              title: 'cManageAccounts',
              exclude: !hasAction(Actions.ViewResellerAccounts),
              url: '#' + Urls.manageAccounts(),
              id: 'home-accounts',
              icon: 'manage-users'
            }
          },
          accountsV2: {
            showSidebar: true,
            parent: 'accounts'
          },
          accountUsers: {
            showSidebar: true,
            parent: 'users'
          },
          addAccount: {
            breadcrumb: [{
              title: 'cAddAccount'
            }],
            showSidebar: true,
            parent: 'accounts'
          },
          accountDetails: {
            breadcrumb: [{
              title: 'cEditAccount'
            }],
            showSidebar: true,
            parent: 'accounts'
          },
          resellers: {
            showSidebar: true,
            nav: {
              title: 'cManageResellers',
              exclude: !hasAction(Actions.ViewAllResellers),
              url: '#' + Routes.resellers,
              id: 'home-resellers',
              icon: 'manage-users'
            }
          },
          resellerDetails: {
            breadcrumb: [{
              title: 'cEditReseller'
            }],
            showSidebar: true,
            parent: 'resellers'
          },
          addReseller: {
            breadcrumb: [{
              title: 'cAddReseller'
            }],
            showSidebar: true,
            parent: 'resellers'
          },
          resellerAccounts: {
            showSidebar: true,
            parent: 'accounts'
          },
          resellerAccountsV2: {
            showSidebar: true,
            parent: 'accounts'
          },
          devices: {
            showSidebar: true,
            nav: {
              title: 'cDevices',
              url: '#' + Routes.devices,
              id: 'home-devices',
              icon: 'devices'
            }
          },
          devicesV2: {
            showSidebar: true,
            nav: {
              title: 'cManageDevices',
              exclude: true,
              url: '#' + Routes.devicesV2,
              id: 'home-devices-v2',
              icon: 'devices',
              profiled: true
            }
          },
          adminDashboard: {
            showSidebar: true,
            nav: {
              title: 'cAdminDashboard',
              exclude: !hasAction(Actions.ViewAdminDashboard),
              url: '#' + Routes.adminDashboard,
              id: 'home-admin-dashboard'
            }
          },
          deadMessages: {
            breadcrumb: [{
              title: 'cDeadMessages'
            }],
            showSidebar: true,
            parent: 'adminDashboard'
          },
          amqpGlobalConfigurations: {
            breadcrumb: [{
              title: 'cAmqpGlobalConfigurations'
            }],
            showSidebar: true,
            parent: 'adminDashboard'
          },
          dbMigrations: {
            breadcrumb: [{
              title: 'cDatabaseMigrations'
            }],
            showSidebar: true,
            parent: 'adminDashboard'
          },
          entityRepair: {
            breadcrumb: [{
              title: 'cEntityRepair'
            }],
            showSidebar: true,
            parent: 'adminDashboard'
          },
          accountGroups: {
            breadcrumb: [{
              title: 'cAccountGroups'
            }],
            showSidebar: true,
            parent: 'adminDashboard'
          },
          accountGroupsDetails: {
            breadcrumb: [{
              title: 'cAccountGroups',
              url: '#' + Routes.accountGroups
            }],
            showSidebar: true,
            parent: 'adminDashboard'
          },
          stackCleanup: {
            breadcrumb: [{
              title: 'cStackCleanup'
            }],
            showSidebar: true,
            parent: 'adminDashboard'
          },
          welcome: {
            showSidebar: false
          },
          permissionDenied: {
            showSidebar: true
          },
          internalError: {
            showSidebar: true
          }
        };

        // Convert all route keys to route paths
        for (var key in $scope.views) {
          if ($scope.views[key].parent) {
            $scope.views[key].parent = Routes[$scope.views[key].parent];
          }
          if (!$scope.views[key].breadcrumb) {
            $scope.views[key].breadcrumb = [];
          }
          if (Routes[key] !== undefined) {
            $scope.views[Routes[key]] = $scope.views[key];
          }
          delete $scope.views[key];
        }

        updateNav();
      });

      var updateNav = function () {
        $scope.navs = [];

        for (var viewName in $scope.views) {
          var view = $scope.views[viewName];

          if (view.nav) {
            // Pull out all primary navs
            if (view.nav.id) {
              $scope.navs.push(view.nav);
            }

            // Add this view's nav as a breadcrumb
            view.breadcrumb.unshift({
              title: view.nav.title,
              url: view.breadcrumb.length ? view.nav.url : ''
            });
          }

          else if (view.parent) {
            var referencedNav = $scope.views[view.parent].nav;
            // Add the breadcrumb
            view.breadcrumb.unshift({
              title: referencedNav.title,
              url: referencedNav.url
            });
          }
        }
      };

    });

})();
