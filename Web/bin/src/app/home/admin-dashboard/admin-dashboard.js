(function () {
  'use strict';

  angular.module('hp.portal.home.adminDashboard', [
    'hp.portal.home.adminDashboard.stackCleanup'
  ])

  .controller('AdminDashboardCtrl', function ($scope, Routes) {
    $scope.adminDashboardLinksList = [{
      listName: 'cSystemRepair',
      links: [{
        linkName: 'cDeadMessages',
        id: 'deadMessagesLink',
        href: '#' + Routes.deadMessages
      }, {
        linkName: 'cEntityRepair',
        id: 'repairButtonLink',
        href: '#' + Routes.entityRepair
      }]
    }, {
      listName: 'cPromotion',
      links: [{
        linkName: 'cAmqpGlobalConfigurations',
        id: 'amqpGlobalConfigurationsLink',
        href: '#' + Routes.amqpGlobalConfigurations
      }]
    }, {
      listName: 'cDatabase',
      links: [{
        linkName: 'cDatabaseMigrations',
        id: 'dbMigrationsLink',
        href: '#' + Routes.dbMigrations
      }]
    }, {
      listName: 'cAccountGroups',
      links: [{
        linkName: 'cAccountGroupsManage',
        id: 'accountGroupsLink',
        href: '#' + Routes.accountGroups
      }]
    }, {
      listName: 'cStackCleanup',
      links: [{
        linkName: 'cCleanupOfferingsAndAccounts',
        id: 'cleanupOfferingsAndAccountsLink',
        href: '#' + Routes.stackCleanup
      }]
    }];
  });
})();
