(function () {
  'use strict';

  angular.module('hp.portal.home.adminDashboard.entityRepair', [
    'hp.portal.home.webservices.tasks',
  ])

  .controller('EntityRepairCtrl', function ($scope, PageAlerts, EntityRepair) {
    $scope.entityRepair = {};

    $scope.submit = function () {
      EntityRepair.startTask({
        repairEntityId: $scope.entityRepair.entityId,
        repairScope: $scope.entityRepair.entityScope
      }).$promise.then(
        function success() {
          PageAlerts.add({
            type: 'success',
            msg: 'cSuccessfullyFixedMessage',
            timeout: PageAlerts.timeout.medium
          });
        },
        function error() {
          PageAlerts.add({
            type: 'danger',
            msg: 'cFailToFixMessage',
            timeout: PageAlerts.timeout.long
          });
        });
    };
  });
})();
