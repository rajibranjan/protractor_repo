(function () {
  'use strict';

  angular.module('hp.portal.home.adminDashboard.stackCleanup.entityCleanup.fetchEntitiesModal', [])

  .factory('FetchEntitiesModal', function ($uibModal) {
    function open(data) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/home/admin-dashboard/stack-cleanup/fetch-entities-modal.html',
        controller: 'FetchEntitiesModalCtrl',
        backdrop: 'static',
        keyboard: false,
        windowClass: 'entity-cleanup-modal',
        resolve: {
          entityName: function () {
            return data.entityName;
          },
          pager: data.pager,
          logger: data.logger
        }
      });

      return modalInstance.result;
    }

    return {
      open: open
    };
  })

  .controller('FetchEntitiesModalCtrl', function ($scope, $uibModalInstance, LinkParser, entityName, pager, logger) {
    $scope.entityName = entityName;
    $scope.pager = pager;
    $scope.count = 0;

    $scope.cancel = function () {
      $scope.shouldCancel = true;
    };

    logger.info('[START] Fetching all ' + entityName + '.');

    getAllEntities().then(function (entities) {
      logger.info('[END] Fetching all ' + entityName + '. Found ' + entities.length + '.');
      $uibModalInstance.close({
        entities: entities,
        cancelled: $scope.shouldCancel
      });
    });

    /**
     * Recursively get all entities until there are no more pages.
     */
    function getAllEntities(entities) {
      if ($scope.shouldCancel) {
        return entities;
      }

      var loadFn;
      if (!entities) {
        // For the first page load we use refresh, which clears any cache and loads the first page.
        entities = [];
        loadFn = pager.refresh.bindArgs(1);
        logger.info('Fetching the first page...');
      }
      else {
        // Subsequent pages use loadNextPage.
        loadFn = pager.loadNextPage;
        logger.info('Fetching page ' + (pager.getPageNumber() + 1) + ' of ' + pager.getTotalPages() + ' (' + pager.getTotal() + ' total entities)');
      }

      return loadFn().then(function (page) {
        page = page.map(function (entity) {
          // Create EntityInfo objects for each entity.
          return {
            entity: entity,
            entityId: LinkParser.getSelfId(entity),
            entityName: entityName
          };
        });
        entities.push.apply(entities, page);
        $scope.total = pager.getTotal();
        $scope.count = entities.length;

        if (pager.getPageNumber() < pager.getTotalPages()) {
          return getAllEntities(entities);
        }
        else {
          return entities;
        }
      }, function (response) {
        logger.error({
          msg: 'Error fetching ' + entityName + '.',
          httpStatus: response.status,
          details: response.data,
        });
      });
    }
  });

})();
