(function () {
  'use strict';

  angular.module('hp.portal.home.adminDashboard.stackCleanup', [
    'hp.portal.home.adminDashboard.stackCleanup.entityCleanup',
    'hp.portal.common.webservices',
    'hp.portal.home.webservices-v2',
  ])

  .controller('StackCleanupCtrl', function ($scope, testHooks, copyToClipboard, UserService, AccountService, ResellerService, PermittedFqNameService, OfferingService, DownloadService, I18nService, OfferingIconService, MeteringInfoService, PageAlerts) {
    $scope.bindings = {
      allLogsOpen: false
    };
    $scope.aggregateLogs = [];
    $scope.logger = {
      add: function (logEntry) {
        $scope.aggregateLogs.push(logEntry);
      }
    };

    testHooks.getStackCleanupLogs = function () {
      var output = '';
      $scope.aggregateLogs.forEach(function (logEntry) {
        output += '[' + logEntry.entityName + ']';
        output += logEntry.entityId ? ' [' + logEntry.entityId + ']' : '';
        output += logEntry.httpStatus ? ' [' + logEntry.httpStatus + ']' : '';
        output += logEntry.msg ? ' ' + logEntry.msg : '';
        output += '\n';
        if (logEntry.details) {
          output += ' >>> ' + JSON.stringify(logEntry.details) + '\n';
        }
      });
      return output;
    };

    function refreshTextOnlyLogs() {
      if ($scope.bindings.textOnlyLogsOpen) {
        var logs = testHooks.getStackCleanupLogs();
        logs = logs.replace(/\n/g, '<br>');
        angular.element('#text-only-logs').html(logs);
      }
    }

    $scope.$watch('bindings.textOnlyLogsOpen', refreshTextOnlyLogs);


    var rsApiTestId = '*0711a45f86f26*';
    var entityTestId = '*e0e649700856*';
    var offeringTestFqName = 'test-Offering-*';
    var downloadTestFqName = 'test-Download-*';
    var resellerTestId = '*755aa136938e*';
    var userTestEmail = 'titan-test*';
    var pagers = {
      accounts: AccountService.getPager({
        itemsPerPage: 100,
        queryString: 'name*=' + rsApiTestId + ' || name*=' + entityTestId + ' || name*=APITestAccount* || name=="The ₣€üÜŒôÔïÏëËêÊéÉèÈçÇæÆâÂàÀ Company" || name=="Foo Bar" || name=="New test company" || name=="newCompanyName"',
        orderBy: 'metaDataBase.createdDate',
      }),
      permittedFqNames: PermittedFqNameService.getPager({
        itemsPerPage: 100,
        queryString: 'offeringFqName*=' + rsApiTestId + ' || offeringFqName*=' + offeringTestFqName + ' || offeringFqName*=offering*fqName*',
        orderBy: 'metaDataBase.createdDate',
      }),
      resellers: ResellerService.getPager({
        itemsPerPage: 100,
        queryString: 'name*=' + rsApiTestId + ' || name*=' + resellerTestId + ' || name*=APITestReseller* || name*=' + entityTestId,
        orderBy: 'metaDataBase.createdDate',
      }),
      users: UserService.getPager({
        itemsPerPage: 100,
        queryString: 'email*=' + rsApiTestId + ' || email*=' + userTestEmail,
        orderBy: 'metaDataBase.createdDate',
      }),
      offerings: OfferingService.getPager({
        itemsPerPage: 25,
        queryString: 'fqName*=' + rsApiTestId + ' || fqName*=' + offeringTestFqName,
        orderBy: 'metaDataBase.createdDate',
      }),
      downloads: DownloadService.getPager({
        itemsPerPage: 100,
        queryString: 'fqName*=' + rsApiTestId + ' || fqName*=' + downloadTestFqName,
        orderBy: 'metaDataBase.createdDate',
      }),
      i18n: I18nService.getPager({
        itemsPerPage: 100,
        queryString: 'source*=' + rsApiTestId + ' || source*=' + offeringTestFqName,
        orderBy: 'metaDataBase.createdDate',
      }),
      offeringIcons: OfferingIconService.getPager({
        itemsPerPage: 100,
        queryString: 'offeringFqName*=' + rsApiTestId + ' || offeringFqName*=' + offeringTestFqName,
        orderBy: 'metaDataBase.createdDate',
      }),
      meteringInfo: MeteringInfoService.getPager({
        itemsPerPage: 100,
        queryString: 'i18n*=' + rsApiTestId + ' || i18n*=' + entityTestId,
        orderBy: 'metaDataBase.createdDate',
      }),
    };

    function EntityType(name, pager) {
      this.name = name;
      this.pager = pager;
    }

    $scope.entityTypes = [
      new EntityType('Accounts', pagers.accounts),
      new EntityType('Permitted FQ Names', pagers.permittedFqNames),
      new EntityType('Resellers', pagers.resellers),
      new EntityType('Users', pagers.users),
      new EntityType('Offerings', pagers.offerings),
      new EntityType('Downloads', pagers.downloads),
      new EntityType('i18n', pagers.i18n),
      new EntityType('Offering Icons', pagers.offeringIcons),
      new EntityType('MeteringInfo', pagers.meteringInfo),
    ];

    var cleanupAllHasErrors;
    $scope.cleanupAll = function () {
      // Close the logs to speed up performance.
      $scope.bindings.allLogsOpen = false;
      $scope.bindings.textOnlyLogsOpen = false;
      $scope.bindings.cleanupInProgress = true;
      cleanupAllHasErrors = false;

      var now = new Date();
      var startTime = new Date(now.getTime() - (1000 * 60 * 60)).toISOString();
      cleanupNext(startTime, 0);
    };

    function cleanupNext(startTime, index, result) {
      if (result && result.errors.length) {
        cleanupAllHasErrors = true;
      }

      if (index >= $scope.entityTypes.length) {
        $scope.bindings.cleanupInProgress = false;

        PageAlerts.add({
          type: cleanupAllHasErrors ? 'danger' : 'success',
          msg: cleanupAllHasErrors ? 'cStackCleanupFailed' : 'cStackCleanupSuccess',
          timeout: PageAlerts.timeout.long
        });

        return;
      }

      if (result && result.cancelled) {
        $scope.bindings.cleanupInProgress = false;
        return;
      }

      var entityType = $scope.entityTypes[index];
      $scope.$broadcast(entityType.name, cleanupNext.bindArgs(startTime, index + 1), startTime);
    }

    testHooks.cleanupAll = $scope.cleanupAll;
    $scope.$on('$destroy', function () {
      delete testHooks.cleanupAll;
    });
    $scope.copyLogs = copyToClipboard;
  })

  .directive('entityLogs', function () {
    return {
      result: 'E',
      templateUrl: 'entity-logs.html',
      scope: {
        logEntries: '=',
        show: '='
      }
    };
  })

  .constant('copyToClipboard', function (event, selectorId) {
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    var element = angular.element('#' + selectorId)[0];
    if (element) {
      var windowSelection = window.getSelection();
      windowSelection.removeAllRanges();
      var range = document.createRange();
      range.selectNode(element);
      windowSelection.addRange(range);
      document.execCommand('copy');
      windowSelection.removeAllRanges();
    }
  });

})();
