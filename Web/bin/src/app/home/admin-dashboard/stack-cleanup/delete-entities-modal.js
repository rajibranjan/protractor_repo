(function () {
  'use strict';

  angular.module('hp.portal.home.adminDashboard.stackCleanup.entityCleanup.deleteEntitiesModal', [])

  .factory('DeleteEntitiesModal', function ($uibModal) {
    function open(data) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/home/admin-dashboard/stack-cleanup/delete-entities-modal.html',
        controller: 'DeleteEntitiesModalCtrl',
        backdrop: 'static',
        keyboard: false,
        windowClass: 'entity-cleanup-modal',
        resolve: {
          entityName: function () {
            return data.entityName;
          },
          entities: function () {
            return data.entities;
          },
          logger: data.logger
        }
      });

      return modalInstance.result;
    }

    return {
      open: open
    };
  })

  .controller('DeleteEntitiesModalCtrl', function ($scope, $uibModalInstance, $q, entityName, entities, logger) {
    $scope.entityName = entityName;
    $scope.entities = entities;
    $scope.total = entities.length;
    $scope.deleted = [];
    $scope.completed = 0;
    $scope.errors = [];
    $scope.retriables = [];
    var maxAttempts = 2;

    $scope.cancel = function () {
      $scope.shouldCancel = true;
    };

    logger.info('[START] Deleting ' + $scope.total + ' ' + entityName + '...');

    // Fire off 10 deletion "threads". When each promise resolves,
    // another one will be kicked off, until they are all done.
    // This will keep 10 "threads" continually processing.
    var promises = [];
    for (var index = 0; index < 10 && entities.length; index++) {
      promises.push(deleteNextEntity());
    }

    // Wait on all promises to know when we're finished.
    $q.all(promises).then(function () {
      logger.info('[END] Deleted ' + $scope.deleted.length + ' out of ' + $scope.total + ' ' + entityName + ', with ' + $scope.retriables.length + ' to retry and ' + $scope.errors.length + ' errored-out.');
      $uibModalInstance.close({
        cancelled: $scope.shouldCancel,
        deleted: $scope.deleted,
        errors: $scope.errors
      });
    });

    function deleteNextEntity() {
      if (entities.length && !$scope.shouldCancel) {
        return deleteEntity(entities.shift()).then(deleteNextEntity);
      }
    }

    function deleteEntity(entityInfo) {
      entityInfo.msg = 'Deleting...';
      delete entityInfo.details;
      delete entityInfo.httpStatus;
      logger.info(entityInfo);

      entityInfo.numAttempts = entityInfo.numAttempts ? entityInfo.numAttempts + 1 : 1;

      return entityInfo.entity.delete().then(function () {
        entityInfo.msg = 'Successfully deleted!';

        if (entityInfo.numAttempts > 1) {
          entityInfo.msg += ' (After ' + (entityInfo.numAttempts - 1) + ' retr' + (entityInfo.numAttempts > 2 ? 'ies' : 'y') + ')';
          $scope.retriables.splice($scope.retriables.indexOf(entityInfo), 1);
        }

        logger.success(entityInfo);
        $scope.deleted.push(entityInfo);
        $scope.completed++;
      }, function (response) {
        entityInfo.details = response.data;
        entityInfo.httpStatus = response.status;

        if (entityInfo.numAttempts < maxAttempts && response.status !== 404) {
          entityInfo.msg = 'Error deleting entity. Attempt ' + entityInfo.numAttempts + ' of ' + maxAttempts + '. Will retry again.';

          // Push it back on the queue to be retried.
          entities.push(entityInfo);
          logger.warn(entityInfo);

          if (entityInfo.numAttempts === 1) {
            $scope.retriables.push(entityInfo);
          }
        }
        else {
          entityInfo.msg = 'Error deleting entity. Attempted ' + entityInfo.numAttempts + ' times.';
          $scope.retriables.splice($scope.retriables.indexOf(entityInfo), 1);

          logger.error(entityInfo);
          $scope.errors.push(entityInfo);
          $scope.completed++;
        }
      });
    }
  });

})();
