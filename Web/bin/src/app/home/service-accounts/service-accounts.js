(function () {
  'use strict';

  angular.module('hp.portal.home.serviceAccounts', [
    'hp.portal.common.webservices',
    'hp.portal.home.webservices-v2'
  ])

  .controller('ServiceAccountsCtrl', function ($scope, OfferingServiceAccountsService, OfferingService, RabbitMQServiceUserService, PermittedFqNameService, PageAlerts, ErrorCodeMapper, Self, LinkParser, Globals, PasswordPolicies) {

    $scope.accordionStatus = {};

    $scope.passwordPolicy = PasswordPolicies.STRICT;

    // Initially get the current user's PermittedFQNames
    if (Globals.permissions.isHPAdmin) {
      var offerings = OfferingService.getAllOfferings();
      offerings.$promise.then(function () {
        $scope.fqNames = Object.keys(offerings);
      });
    }
    else {
      PermittedFqNameService.get({
        qs: 'resellerId==' + LinkParser.getLinkId(Self, 'account')
      }).then(function (response) {
        $scope.fqNames = response.data.map(function (permittedFqName) {
          return permittedFqName.offeringFqName;
        });
      });
    }

    var serviceUser, rabbitMQUser;
    $scope.selectFqName = function (selectedFqName) {
      delete $scope.serviceUser;
      delete $scope.rabbitMQUser;
      delete $scope.showErrorMessage;
      serviceUser = OfferingServiceAccountsService.getOne({
        qs: 'offeringFqName==' + selectedFqName
      });
      serviceUser.$promise.then(function () {
        $scope.serviceUser = serviceUser.clone();
      }, function () {
        $scope.showErrorMessage = true;
      });

      rabbitMQUser = RabbitMQServiceUserService.getOne({
        qs: 'offeringFqName==' + selectedFqName
      });
      rabbitMQUser.$promise.then(function () {
        $scope.rabbitMQUser = rabbitMQUser.clone();
      });
    };

    function resetChangePasswordForm() {
      $scope.accordionStatus.showServiceAccountForm = false;
      $scope.accordionStatus.showRabbitMQForm = false;

      $scope.serviceUser = serviceUser.clone();
      $scope.rabbitMQUser = rabbitMQUser.clone();
    }

    function changePasswordSuccess() {
      PageAlerts.add({
        type: 'success',
        msg: 'cPasswordSuccessChange',
        timeout: PageAlerts.timeout.medium
      });
      resetChangePasswordForm();
    }

    function changePasswordError(response) {
      if (!ErrorCodeMapper.hasErrorCodes(response.data)) {
        PageAlerts.add({
          type: 'danger',
          msg: 'cPasswordError',
          timeout: PageAlerts.timeout.long
        });
      }
    }

    function changeServiceAccountPassword() {
      return $scope.serviceUser.save().then(changePasswordSuccess, changePasswordError);
    }

    function changeRabbitMQPassword() {
      return $scope.rabbitMQUser.save().then(changePasswordSuccess, changePasswordError);
    }

    $scope.changeServiceAccountPasswordSubmit = $scope.$broadcast.bind($scope, 'hpChangeServiceAccountPassword', changeServiceAccountPassword);
    $scope.changeRabbitMQPasswordSubmit = $scope.$broadcast.bind($scope, 'hpChangeRabbitMQPassword', changeRabbitMQPassword);

  });

})();
