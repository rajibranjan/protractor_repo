(function () {
  'use strict';

  angular.module('hp.portal.home.forms.state', [
    'hp.common.filters.hpStartsWith',
  ])


  .directive('hpStateField', function ($parse, States) {
    return {
      restrict: 'A',
      scope: true,
      templateUrl: 'app/home/forms/state.html',
      compile: function (tElem, tAttrs) {
        var inputElem = tElem.children('input');
        inputElem.attr('ng-model', tAttrs.hpStateField);
        if (tElem.required !== undefined) {
          inputElem.attr('required', tElem.required);
        }

        return function link(scope, elem, attrs) {
          scope.disableState = false;
          var initialRequired = elem.required;
          var countryStateMap = {};
          var updateCountryStates = function (newCountry) {
            var country = newCountry;
            if (!country || country === 'None') {
              elem.required = initialRequired;
              scope.disableState = true;
              scope.countryStates = [];
            }
            else if (scope.countrySettings && !scope.countrySettings.isSupported) {
              if (elem.required !== undefined) {
                elem.required = false;
              }
              scope.disableState = true;
              scope.countryStates = [];
            }
            else {
              if (elem.required !== undefined) {
                elem.required = true;
              }
              scope.disableState = false;
              if (!countryStateMap[country]) {
                scope.countryStates = [];
                for (var i = 0; i < States.length; i++) {
                  if (States[i].countryCode === country) {
                    scope.countryStates.push(States[i]);
                  }
                }
                countryStateMap[country] = angular.copy(scope.countryStates);
              }
              else {
                scope.countryStates = countryStateMap[country];
              }
            }
          };
          scope.countryStates = States;

          var inputModel = inputElem.data('$ngModelController');

          scope.formatStateModel = function () {
            if (inputModel.$modelValue) {
              for (var i = 0; i < scope.countryStates.length; i++) {
                if (scope.countryStates[i].abbreviation.toLowerCase() === inputModel.$modelValue.toLowerCase()) {
                  return scope.countryStates[i].name;
                }
              }
            }
          };

          var convertToAbbreviation = function (inputValue) {
            if (!inputValue) {
              inputModel.$setValidity('editable', true);
              return inputValue;
            }
            for (var i = 0; i < scope.countryStates.length; i++) {
              if (scope.countryStates[i].name.toLowerCase() === inputValue.toLowerCase()) {
                inputModel.$setValidity('editable', true);
                return scope.countryStates[i].abbreviation;
              }
            }
            inputModel.$setValidity('editable', false);
            return inputValue;
          };

          inputModel.$parsers.push(convertToAbbreviation);

          var countryChangeHandler = function (newValue) {
            if (newValue) {
              updateCountryStates(newValue.country);
            }
            else {
              updateCountryStates();
            }
            inputModel.$modelValue = convertToAbbreviation(inputModel.$viewValue);
          };
          scope.$watch(function () {
            if (scope[attrs.countrySettings] && scope[attrs.countrySettings].$resolved) {
              return scope[attrs.countrySettings];
            }
            else {
              return undefined;
            }
          }, countryChangeHandler);
        };
      }
    };
  });
})();
