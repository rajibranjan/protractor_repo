(function () {
  'use strict';

  angular.module('hp.portal.home.accounts.addAccount', [
    'hp.portal.common.webservices',
    'hp.portal.common.webservices.tasks',
    'hp.portal.home.webservices-v2',
    'hp.portal.home.webservices.tasks',
    'hp.portal.home.navigation.routesAndUrls',
    'hp.portal.home.accounts.addProductToAccountModal',
    'hp.portal.common.forms.country',
    'hp.portal.common.forms.metaTags',
    'hp.portal.common.forms.addItem',
    'hp.portal.common.forms.userForm',
    'hp.portal.common.forms.pattern',
    'hp.portal.common.constants.commerceProviders',
    'hp.portal.common.productHelpers.addAndEditProduct'
  ])

  .controller('AddAccountCtrl', function ($scope, $location, $filter, Self, LinkParser, PageAlerts, CreateAccount, CreateAccountAndCheckout, Locales, Urls, CountrySettings, Globals, AddProductToAccountModal, ProductService, ErrorCodeMapper, ValidateLicenseKey, TaskStatus, CommerceProviders, RolesHelper, Roles, AddAndEditProduct) {

    var isHPResellerOrHPAdmin = RolesHelper.hasAnyRole(Self, [Roles.HP_ADMIN, Roles.HP_RESELLER]);
    var isOfferingAdminOrHPAdmin = RolesHelper.hasAnyRole(Self, [Roles.HP_ADMIN, Roles.OFFERING_ADMIN]);
    $scope.isPermittedToCheckout = isHPResellerOrHPAdmin || isOfferingAdminOrHPAdmin;

    $scope.bindings = {};

    var productsPromise = ProductService.get({
      // Using a crazy huge limit so we get all the products back from the back-end.  They page everthing...silly back end kids!
      limit: 1000000,
      expand: 'offering'
    });

    var setupAccount = function () {
      $scope.account = {
        metaTags: [],
        links: [{
          rel: 'reseller',
          href: LinkParser.getLink(Self, 'account').href
        }]
      };
      $scope.user = {
        locale: Self.getLocale(),
        email: $location.search().email || ''
      };

      // Initialize enrollmentInfos and availableProducts.
      $scope.bindings.enrollmentInfos = [];
      productsPromise.then(function (products) {
        $scope.bindings.availableProducts = products.data.filter(function (product) {
          //Pick the appropriate commerce provider(s) based on permissions and supported providers.
          if (isHPResellerOrHPAdmin && product.supportedCommerceProviders.indexOf(CommerceProviders.HP_INTERNAL) > -1) {
            product.checkoutCommerceProvider = CommerceProviders.HP_INTERNAL;
          }
          else if (isOfferingAdminOrHPAdmin && product.supportedCommerceProviders.indexOf(CommerceProviders.BETA) > -1) {
            product.checkoutCommerceProvider = CommerceProviders.BETA;
          }
          else {
            //If the product doesn't have an appropriate commerce provider then don't show it.
            return false;
          }

          // Filter out any additionalFields that are not applicable to this page - this is the HP_INTERNAL/BETA path
          product.additionalFields = product.additionalFields.filter(function (additionalField) {
            return additionalField.applicableCommerceProviders.indexOf(product.checkoutCommerceProvider) > -1;
          });

          return product.isFree === false;
        });
        if ($scope.bindings.availableProducts.length > 0) {
          $scope.showProductButton = true;
        }
      });
    };
    setupAccount(); // Initialize the models on the scope to begin with.

    $scope.$watch('account.country', function (newValue) {
      $scope.account.state = null;
      $scope.account.zipcode = null;

      if (newValue !== undefined) {
        $scope.countrySettings = CountrySettings.get(newValue);
      }
    });

    $scope.isAdding = true;
    $scope.locales = Locales.getAll();

    var filterMetaTags = function (metaTags) {
      var suffix = '_DIRECT';
      var result = [];

      angular.forEach(metaTags, function (metaTag) {
        //Need to use indexOf because phantomJS doesn't seem to like endsWith.
        if (metaTag.indexOf(suffix, metaTag.length - suffix.length) === -1) {
          result.push(metaTag);
        }
      });
      return result;
    };

    $scope.metaTags = filterMetaTags(Self.account.metaTags);
    $scope.showMetaTagForm = (Globals.permissions.isHPAdmin || (Globals.permissions.isReseller && $scope.metaTags.length > 0));

    $scope.addProduct = function () {
      AddAndEditProduct.addProduct($scope.bindings.availableProducts, $scope.bindings.enrollmentInfos);
    };

    $scope.editProduct = function (enrollmentInfo) {
      AddAndEditProduct.editProduct(enrollmentInfo, $scope.bindings.availableProducts, $scope.bindings.enrollmentInfos);
    };

    $scope.updateLocales = function () {
      var localesByCountry = Locales.getByCountry($scope.account.country);
      if ($scope.locales.length > 0) {
        $scope.user.locale = localesByCountry[0].lang;
      }
    };

    function addAccount(saveAndNew) {
      var taskPayload = {
        account: $scope.account,
        adminContact: $scope.user
      };

      if ($scope.bindings.enrollmentInfos.length > 0) {
        // Kick off the asynchronous CreateAccountAndCheckout task, and wait for it to complete.
        return createAccountAndCheckout(taskPayload, saveAndNew);
      }
      else {
        // Kick off the synchronous CreateAccount task.
        return CreateAccount.startTask(taskPayload).$promise
          .then(redirectOrResetForm.bindArgs(saveAndNew))
          .then(addAccountSuccess, addAccountError);
      }
    }

    var pollingMessages = {
      pleaseWaitMsg: 'cAddAccountProcessing',
      pleaseWaitLongerMsg: 'cAddAccountTimeout',
      successMsg: 'cAddAccountSuccess',
      fallbackErrorMsg: 'cAddAccountError',
      timeoutMsg: 'cAddAccountTimeoutFailed'
    };

    function createAccountAndCheckout(taskPayload, saveAndNew) {
      taskPayload.shoppingCartItems = $scope.bindings.enrollmentInfos.map(function (enrollmentInfo) {
        return createShoppingCartItem(enrollmentInfo);
      });

      var task = CreateAccountAndCheckout.startTask(taskPayload);

      return task.$promise // The promise just for starting the async task.
        .then(function () {
          return task.pollWithMessages(pollingMessages)
            .then(redirectOrResetForm.bindArgs(saveAndNew));
        });
    }

    function createShoppingCartItem(enrollmentInfo) {

      var shoppingCartItem = {
        commerceProvider: enrollmentInfo.product.checkoutCommerceProvider,
        endDate: $filter('date')(enrollmentInfo.licenseEndDate, 'yyyy-MM-ddTHH:mm:ss.sssZ'),
        licenseKey: enrollmentInfo.licenseKey,
        contractId: enrollmentInfo.contractId,
        billingCategory: enrollmentInfo.billingCategory,
        isTrial: enrollmentInfo.isTrial,
        licenseAdditionalFields: enrollmentInfo.licenseAdditionalFields,
        links: [{
          rel: 'product',
          href: LinkParser.getSelf(enrollmentInfo.product).href
        }]
      };

      if (enrollmentInfo.meteringInfo) {
        var meteringInfoLink = LinkParser.getSelf(enrollmentInfo.meteringInfo);
        LinkParser.setLink(shoppingCartItem, 'meteringInfo', meteringInfoLink.href);
      }

      return shoppingCartItem;
    }

    function redirectOrResetForm(saveAndNew) {
      if (saveAndNew) {
        // Re-initialize the models to be able to create a new account.
        setupAccount();
      }
      else {
        // Redirect to the manage account apge.
        $location.url(Urls.manageAccounts());
      }
    }

    function addAccountSuccess() {
      var alert = {
        type: 'success',
        msg: 'cAddAccountSuccess',
        timeout: PageAlerts.timeout.medium
      };

      PageAlerts.add(alert);
    }

    function addAccountError(task) {
      if (!ErrorCodeMapper.hasErrorCodes(task)) {
        PageAlerts.add({
          type: 'danger',
          msg: 'cAddAccountError',
          translateValues: {
            errorCode: (task.httpStatus || 'unknown')
          },
          timeout: PageAlerts.timeout.long
        });
      }
    }

    $scope.addAccountSubmit = function () {
      $scope.$broadcast('hpDisableAddAccountForm', addAccount.bindArgs(false));
    };

    $scope.addAccountAndNewSubmit = function () {
      $scope.$broadcast('hpDisableAddAccountForm', addAccount.bindArgs(true));
    };

    $scope.cancelAddAccount = function () {
      $location.url(Urls.manageAccounts());
    };
  });
})();
