(function () {
  'use strict';

  angular.module('hp.portal.home.accounts.addProductToAccountModal', [
    'hp.portal.common.webservices',
    'hp.portal.common.constants.commerceProviders',
    'hp.portal.common.constants.billingCategories'
  ])

  .factory('AddProductToAccountModal', function ($uibModal) {
    function open(data) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/home/accounts/add-product-to-account-modal.html',
        controller: 'AddProductToAccountModalCtrl',
        backdrop: 'static',
        resolve: {
          products: function () {
            return data.products;
          },
          enrollmentInfo: data.enrollmentInfo,
        }
      });

      return modalInstance.result;
    }

    return {
      open: open
    };
  })

  .controller('AddProductToAccountModalCtrl', function ($scope, $uibModalInstance, LinkParser, ProductService, products, enrollmentInfo, i18n, CommerceProviders, BillingCategories) {
    $scope.isEditing = enrollmentInfo !== undefined;
    $scope.date = new Date();

    if ($scope.isEditing) {
      var date = new Date(enrollmentInfo.licenseEndDate);
      if (typeof enrollmentInfo.licenseEndDate === 'string') {
        date = new Date(enrollmentInfo.licenseEndDate.replace(/\..*/g, 'Z'));
      }
      enrollmentInfo.licenseEndDate = date;

      if (enrollmentInfo.existing) {
        $scope.existingProduct = true;
      }
    }

    $scope.billingCategories = Object.keys(BillingCategories).map(function (key) {
      return BillingCategories[key];
    });

    $scope.enrollmentInfo = angular.copy(enrollmentInfo, {}); // Copy the passed in enrollmentInfo object, so it can be modified on the scope.

    if (!$scope.isEditing) {
      $scope.products = products.map(function (product) {
        return {
          product: product,
          i18n: i18n.getBySource(product.offering.fqName)
        };
      });
    }

    $scope.bindings = {
      addProductForm: {}
    };

    function setupMeteringInfos() {
      if ($scope.enrollmentInfo.product && $scope.enrollmentInfo.product.supportedMeteringInfo && $scope.enrollmentInfo.product.supportedMeteringInfo.length > 0) {
        $scope.meteringInfos = $scope.enrollmentInfo.product.supportedMeteringInfo;
        $scope.meteringInfoi18n = i18n.getBySource('meteringInfo');
      }
    }
    $scope.productSelected = setupMeteringInfos;
    setupMeteringInfos();

    $scope.save = function () {
      var isDirty = $scope.bindings.addProductForm.$dirty;
      $scope.$broadcast('hpValidateAddProductForm');
      if ($scope.bindings.addProductForm.$invalid) {
        return;
      }

      $scope.enrollmentInfo.licenseAdditionalFields = ProductService.generateLicenseAdditionalFields($scope.enrollmentInfo.product, $scope.enrollmentInfo.additionalFieldValues);

      if (!isDirty) {
        $scope.cancel();
      }
      else {
        $scope.enrollmentInfo.dirty = true;

        if (enrollmentInfo) {
          // Copy the modified data back into the enrollmentInfo object that was passed into the modal.
          angular.extend(enrollmentInfo, $scope.enrollmentInfo);
          // Remove any properties from enrollmentInfo that were removed.
          angular.forEach(enrollmentInfo, function (value, key) {
            if (enrollmentInfo.hasOwnProperty(key) && !$scope.enrollmentInfo.hasOwnProperty(key)) {
              delete enrollmentInfo[key];
            }
          });
          $uibModalInstance.close(enrollmentInfo);
        }
        else {
          $uibModalInstance.close($scope.enrollmentInfo);
        }
      }

    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss({
        operation: 'cancel'
      });
    };

    $scope.delete = function () {
      $uibModalInstance.dismiss({
        operation: 'delete',
        enrollmentInfo: enrollmentInfo
      });
    };

    $scope.toggleTrial = function () {
      if ($scope.enrollmentInfo.isTrial && !$scope.isEditing) {
        var trialEndDate = new Date();
        trialEndDate.setDate(trialEndDate.getDate() + 30);
        $scope.enrollmentInfo.licenseEndDate = trialEndDate;
      }

      delete $scope.enrollmentInfo.meteringInfo;
    };

    $scope.showContractId = function () {
      return $scope.enrollmentInfo.product && $scope.enrollmentInfo.product.supportedCommerceProviders.indexOf(CommerceProviders.HP_INTERNAL) > -1;
    };

  });

})();
