(function () {
  'use strict';

  angular.module('hp.portal.home.accounts.accountDetails', [
    'ngRoute',
    'hp.portal.home.accounts.accountStatus',
    'hp.portal.common.webservices',
    'hp.portal.common.webservices.tasks',
    'hp.portal.home.webservices-v2',
    'hp.portal.home.webservices.tasks',
    'hp.portal.home.navigation.routesAndUrls',
    'hp.portal.home.forms.state',
    'hp.portal.common.forms.country',
    'hp.portal.common.forms.companyContact',
    'hp.portal.common.forms.metaTags',
    'hp.portal.common.forms.addItem',
    'hp.portal.common.forms.userForm',
    'hp.portal.common.forms.pattern',
    'hp.portal.common.constants.commerceProviders',
    'hp.portal.common.productHelpers.addAndEditProduct'
  ])

  .controller('AccountDetailsCtrl', function ($q, $scope, $uibModal, $filter, $window, $location, LinkParser, PageAlerts, AccountStatus, account, Self, ResendActivationEmail, Urls, Roles, RolesHelper, PortalConfigurations, UserService, CountrySettings, Globals, ProductService, LicenseService, ShoppingCartService, CommerceProviders, ShoppingCartCheckout, AddAndEditProduct) {

    $scope.account = account;

    var isHPResellerOrHPAdmin = RolesHelper.hasAnyRole(Self, [Roles.HP_ADMIN, Roles.HP_RESELLER]);
    var isOfferingAdminOrHPAdmin = RolesHelper.hasAnyRole(Self, [Roles.HP_ADMIN, Roles.OFFERING_ADMIN]);
    $scope.isPermittedToCheckout = isHPResellerOrHPAdmin || isOfferingAdminOrHPAdmin;

    $scope.bindings = {
      availableProducts: [],
      enrollmentInfos: []
    };

    var licensePromise = LicenseService.get({
      qs: 'account.id==' + LinkParser.getSelfId(account),
      expand: 'product,offering',
      latest: true
    }).then(addExistingLicenses);

    var productPromise = ProductService.get({
      limit: 1000000,
      expand: 'offering'
    });

    $q.all([licensePromise, productPromise]).then(function (params) {
      var products = params[1];
      addAvailableProducts(products);
    });

    function addExistingLicenses(licenses) {
      licenses.data.filter(isPaidProduct).forEach(function (license) {
        var product = license.product;
        if (applyCheckoutCommerceProvider(product) === false) {
          // If there is no appropriate commerce provider, then don't show this product.
          return;
        }

        product.offering = license.offering;

        var enrollmentInfo = {
          product: product,
          existing: true,
          status: license.status,
          licenseEndDate: license.endDate,
          isTrial: license.isTrial,
          contractId: license.contractId,
          billingCategory: license.billingCategory,
          additionalFieldValues: {},
        };

        if (license.meteringInfo) {
          enrollmentInfo.meteringInfo = license.meteringInfo;
        }

        filterAdditionalFields(product);

        var licenseAdditionalFieldValues = license.additionalFields;
        product.additionalFields.forEach(function (productAdditionalField) {
          if (licenseAdditionalFieldValues[productAdditionalField.name] !== undefined) {
            enrollmentInfo.additionalFieldValues[productAdditionalField.name] = licenseAdditionalFieldValues[productAdditionalField.name][0];
          }
        });

        $scope.bindings.enrollmentInfos.push(enrollmentInfo);
        $scope.showProductButton = true;
      });
    }

    function isPaidProduct(license) {
      return !license.product.isFree;
    }

    function addAvailableProducts(products) {
      products.data.filter(notFreeAndNotEnrolled).forEach(function (product) {

        if (applyCheckoutCommerceProvider(product) === false) {
          // If there is no appropriate commerce provider, then don't show this product.
          return;
        }

        filterAdditionalFields(product);

        $scope.bindings.availableProducts.push(product);
        $scope.showProductButton = true;
      });
    }

    function notFreeAndNotEnrolled(product) {
      if (product.isFree) {
        return false;
      }

      return !$scope.bindings.enrollmentInfos.some(function (enrollmentInfo) {
        return (LinkParser.getSelfId(enrollmentInfo.product) === LinkParser.getSelfId(product));
      });
    }

    function applyCheckoutCommerceProvider(product) {
      //Pick the appropriate commerce provider(s) based on permissions and supported providers.
      if (isHPResellerOrHPAdmin && product.supportedCommerceProviders.indexOf(CommerceProviders.HP_INTERNAL) > -1) {
        product.checkoutCommerceProvider = CommerceProviders.HP_INTERNAL;
      }
      else if (isOfferingAdminOrHPAdmin && product.supportedCommerceProviders.indexOf(CommerceProviders.BETA) > -1) {
        product.checkoutCommerceProvider = CommerceProviders.BETA;
      }
      else {
        // The product doesn't have an appropriate commerce provider.
        return false;
      }
      return true;
    }

    function filterAdditionalFields(product) {
      // Filter out any additionalFields that are not applicable to this page - this is the HP_INTERNAL/BETA path
      product.additionalFields = product.additionalFields.filter(function (additionalField) {
        return additionalField.applicableCommerceProviders.indexOf(product.checkoutCommerceProvider) > -1;
      });
    }

    function showChangesNotSavedMessage() {
      PageAlerts.add({
        type: 'info',
        msg: 'cChangesNotYetSaved',
        timeout: PageAlerts.timeout.medium
      });
    }

    $scope.addProduct = function () {
      AddAndEditProduct.addProduct($scope.bindings.availableProducts, $scope.bindings.enrollmentInfos).then(showChangesNotSavedMessage);
    };

    $scope.editProduct = function (enrollmentInfo) {
      AddAndEditProduct.editProduct(enrollmentInfo, $scope.bindings.availableProducts, $scope.bindings.enrollmentInfos).then(showChangesNotSavedMessage);
    };

    var adminId = LinkParser.getLinkId(account, 'adminContact');
    $scope.user = UserService.getById(adminId);
    $scope.hasStatusCreated = (account.status === AccountStatus.CREATED);
    $scope.isMyCompany = (LinkParser.getLinkId(Self, 'account') === LinkParser.getSelfId(account));

    var isHPAdmin = Globals.permissions.isHPAdmin;
    var isReseller = Globals.permissions.isReseller;
    var isCompanyAdmin = Globals.permissions.isCompanyAdmin;

    $scope.$watch('account.country', function (newValue, oldValue) {
      if (newValue !== oldValue) {
        account.state = null;
        account.zipcode = null;
      }

      $scope.countrySettings = CountrySettings.get(newValue);
    });

    $scope.canDelete = !$scope.isMyCompany && (isHPAdmin || (isReseller && $scope.hasStatusCreated));
    $scope.canTerminate = $scope.isMyCompany && isCompanyAdmin && !isHPAdmin && !isReseller;

    var filterMetaTags = function (metaTags) {
      var suffix = '_DIRECT';
      var result = [];

      angular.forEach(metaTags, function (metaTag) {
        // Need to use indexOf because phantomJS doesn't seem to like endsWith.
        // Also need to be careful not to add duplicates.
        if (metaTag.indexOf(suffix, metaTag.length - suffix.length) === -1 && result.indexOf(metaTag) === -1) {
          result.push(metaTag);
        }
      });
      return result;
    };

    $scope.metaTags = filterMetaTags(account.metaTags !== undefined ? Self.account.metaTags.concat(account.metaTags) : Self.account.metaTags);
    $scope.showMetaTagForm = !$scope.isMyCompany && (isHPAdmin || (isReseller && $scope.metaTags.length > 0));

    $scope.resendActivationEmail = function () {
      var userId = LinkParser.getLinkId(account, 'adminContact');
      ResendActivationEmail.startTask({
        userId: userId
      }).$promise.then(
        function success() {
          PageAlerts.add({
            type: 'success',
            msg: 'cActivationEmailSent',
            timeout: PageAlerts.timeout.medium
          });
        },
        function error() {
          PageAlerts.add({
            type: 'danger',
            msg: 'cFailureToSendActivationEmail',
            timeout: PageAlerts.timeout.medium
          });
        });
    };

    var deleteCompany = function () {
      return account.delete().then(function success() {
        PageAlerts.add({
          path: Urls.manageAccounts(),
          type: 'success',
          msg: 'cSuccessfullyDeletedCompany',
          translateValues: {
            name: account.name
          },
          timeout: PageAlerts.timeout.medium
        });
        $location.url(Urls.manageAccounts());
      }, function error() {
        PageAlerts.add({
          type: 'danger',
          msg: 'cAccountDeletionFailed',
          timeout: PageAlerts.timeout.medium
        });
      });
    };

    var pollingMessages = {
      pleaseWaitMsg: 'cCreatingLicenseToProduct',
      pleaseWaitLongerMsg: 'cCreatingLicenseTimeout',
      successMsg: 'cSuccessAddingLicense',
      fallbackErrorMsg: 'cErrorCreatingLicense',
      timeoutMsg: 'cCreateLicenseTimeoutFailed'
    };

    $scope.deleteCompanySubmit = function () {
      $scope.$broadcast('hpDisableAccountDetailsForm', deleteCompany, false);
    };

    function doCheckout(taskPayload) {
      var task = ShoppingCartCheckout.startTask(taskPayload);

      return task.$promise // The promise just for starting the async task.
        .then(function () {
          return task.pollWithMessages(pollingMessages).then(function () {
            angular.forEach($scope.bindings.enrollmentInfos, function (enrollmentInfo) {
              enrollmentInfo.dirty = false;
            });
          });
        });
    }

    var updateAccount = function () {
      // This method MUST return a promise in order to work with the
      // hpDisableOn directive.
      var shoppingCart = ShoppingCartService.create({
        items: [],
        links: [{
          rel: 'account',
          href: LinkParser.getSelf(account).href
        }]
      });

      angular.forEach($scope.bindings.enrollmentInfos, function (enrollmentInfo) {
        if (enrollmentInfo.dirty) {
          enrollmentInfo.existing = true;

          var shoppingCartItem = {
            commerceProvider: enrollmentInfo.product.checkoutCommerceProvider,
            endDate: $filter('date')(enrollmentInfo.licenseEndDate, 'yyyy-MM-ddTHH:mm:ss.sssZ'),
            licenseKey: enrollmentInfo.licenseKey,
            contractId: enrollmentInfo.contractId,
            billingCategory: enrollmentInfo.billingCategory,
            isTrial: enrollmentInfo.isTrial,
            licenseAdditionalFields: enrollmentInfo.licenseAdditionalFields,
            links: [{
              rel: 'product',
              href: LinkParser.getSelf(enrollmentInfo.product).href
            }]
          };

          if (enrollmentInfo.meteringInfo) {
            var meteringInfoLink = LinkParser.getSelf(enrollmentInfo.meteringInfo);
            LinkParser.setLink(shoppingCartItem, 'meteringInfo', meteringInfoLink.href);
          }

          shoppingCart.items.push(shoppingCartItem);
        }
      });

      var promises = [];
      if (shoppingCart.items.length > 0) {
        promises.push(shoppingCart.save().then(function success() {
          return doCheckout({
            links: [{
              rel: 'shoppingCart',
              href: LinkParser.getSelf(shoppingCart).href
            }]
          });
        }, function error() {
          PageAlerts.add({
            type: 'danger',
            msg: 'cErrorCreatingLicense',
            timeout: PageAlerts.timeout.medium
          });
        }));
      }

      promises.push(account.save().then(function success(updatedAccount) {
        if ($scope.isMyCompany) {
          Self.account = updatedAccount;
          $scope.$emit('changeMyCompany');
        }

        PageAlerts.add({
          type: 'success',
          msg: 'cAccountSaveSuccess',
          timeout: PageAlerts.timeout.medium
        });
      }, function error() {
        PageAlerts.add({
          type: 'danger',
          msg: 'cAccountSaveFailed',
          timeout: PageAlerts.timeout.medium
        });
      }));
      return $q.all(promises);
    };

    $scope.updateAccountSubmit = function () {
      $scope.$broadcast('hpDisableAccountDetailsForm', updateAccount, true);
    };

    $scope.companyContactChange = function (newUser) {
      $scope.user = newUser;
      LinkParser.updateLink(account, 'adminContact', LinkParser.getLink(newUser, 'self').href);
    };

    $scope.openTerminateModal = function () {
      $scope.modalInstance = $uibModal.open({
        templateUrl: 'app/home/accounts/terminate-modal.html',
        controller: 'TerminateAccountModalCtrl'
      });

      $scope.modalInstance.result.then(function (result) {
        switch (result) {
          case 'success':
            // Termination successful, redirect user //
            $window.location.href = PortalConfigurations['com.portal.logoutUrl'];
            break;
          case 409:
            // Incorrect Password //
            PageAlerts.add({
              type: 'danger',
              msg: 'cIncorrectPassword',
              timeout: PageAlerts.timeout.medium
            });
            break;
          default:
            // Some other error occurred... //
            PageAlerts.add({
              type: 'danger',
              msg: 'cTerminateAccountError',
              timeout: PageAlerts.timeout.medium
            });
            break;
        }
      });
    };

  })

  .controller('TerminateAccountModalCtrl', function ($scope, $uibModalInstance, PageAlerts, Self, TerminateAccount) {
    $scope.currentPassword = {
      value: ''
    };
    $scope.reason = {
      choice: 'TERM_GENERIC',
      elaboration: ''
    };

    $scope.terminate = function () {
      var deleteAccountData = {
        password: $scope.currentPassword.value,
        reason: $scope.reason.choice,
        elaboration: $scope.reason.elaboration
      };
      TerminateAccount.startTask(deleteAccountData)
        .$promise.then(function success() {
          $uibModalInstance.close('success');
        }, function error(response) {
          $uibModalInstance.close(response.httpStatus);
        });
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss();
    };
  });

})();
