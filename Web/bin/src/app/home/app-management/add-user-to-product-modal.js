(function () {
  'use strict';

  angular.module('hp.portal.home.appManagement.addUserToProductModal', [
    'hp.portal.common.webservices.entitlements',
    'checklist-model'
  ])

  .factory('AddUserToProductModal', function ($uibModal) {
    function open(data) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/home/app-management/add-user-to-product-modal.html',
        controller: 'AddUserToProductModalCtrl',
        size: 'lg',
        backdrop: 'static',
        resolve: {
          offering: data.selectedOffering,
          productItem: data.productItem
        }
      });

      return modalInstance.result;
    }

    return {
      open: open
    };
  })

  .controller('AddUserToProductModalCtrl', function ($scope, $uibModalInstance, offering, productItem, Self, UserService, LinkParser) {
    $scope.offering = offering;
    $scope.product = productItem.product;
    $scope.entitledUsers = productItem.userAndEntitlements.map(function (userItem) {
      return userItem.user;
    });

    $scope.users = UserService.getPager({
      queryString: 'accountId==' + Self.accountId
    });
    $scope.users.loadPage();

    var searchFields = ['email', 'firstName', 'lastName'];
    $scope.search = function (searchString) {
      var searchExpressions = searchFields.map(function (param) {
        return param + '*="*' + searchString + '*"';
      });

      var fullSearchString = searchExpressions.join(' || ');

      // This will trigger the current page to refresh with the new search.
      $scope.users.searchBy(fullSearchString);
    };

    $scope.bindings = {
      confirmSave: false
    };

    $scope.entitledUsersUpdated = function () {
      $scope.isEntitled = {};
      angular.forEach($scope.entitledUsers, function (user) {
        $scope.isEntitled[user.email] = true;
      });
    };
    $scope.entitledUsersUpdated(); // Run it once to prime the list.

    $scope.maxEntitlementsReached = function () {
      return (productItem.product.maxEntitlementAllowed !== 0 && $scope.entitledUsers.length >= productItem.product.maxEntitlementAllowed);
    };

    $scope.toggleUser = function (user, $event) {
      if ($event.target.type === 'checkbox') {
        // This will happen if the user clicked on the actual checkbox, instead of somewhere else in the table row.
        // In this case the checklist-model will take care of updating the entitledUsers list.
        return;
      }

      if ($scope.isEntitled[user.email]) {
        // If the user is already entitled, then remove them from the list.
        for (var index = 0; index < $scope.entitledUsers.length; index++) {
          if ($scope.entitledUsers[index].email === user.email) {
            $scope.entitledUsers.splice(index, 1);
            return;
          }
        }
      }
      else if (!$scope.maxEntitlementsReached()) {
        // The user was not already in the list, and we haven't reached the max yet, so add the user to the list.
        $scope.entitledUsers.push(user);
      }
    };

    $scope.save = function () {
      $scope.addedUsers = getAddedUsers();
      $scope.removedUsers = getRemovedUsers();

      if ($scope.addedUsers.length || $scope.removedUsers.length) {
        $scope.bindings.confirmSave = true;
      }
      else {
        $uibModalInstance.dismiss();
      }
    };
    $scope.cancel = function () {
      $uibModalInstance.dismiss();
    };
    $scope.confirm = function () {
      $uibModalInstance.close({
        addedUsers: $scope.addedUsers,
        removedUsers: $scope.removedUsers
      });
    };

    function getAddedUsers() {
      var previouslyEntitledUsers = {};
      productItem.userAndEntitlements.forEach(function (userItem) {
        previouslyEntitledUsers[LinkParser.getSelfId(userItem.user)] = true;
      });

      var addedUsers = [];
      $scope.entitledUsers.forEach(function (user) {
        var userId = LinkParser.getSelfId(user);
        if (!previouslyEntitledUsers[userId]) {
          addedUsers.push(user);
        }
      });

      return addedUsers;
    }

    function getRemovedUsers() {
      var entitledUsers = {};
      $scope.entitledUsers.forEach(function (user) {
        entitledUsers[LinkParser.getSelfId(user)] = user;
      });

      var removedUsers = [];
      productItem.userAndEntitlements.forEach(function (userItem) {
        var userId = LinkParser.getSelfId(userItem.user);
        if (!entitledUsers[userId]) {
          removedUsers.push(userItem);
        }
      });

      return removedUsers;
    }
  });

})();
