(function () {
  'use strict';

  angular.module('hp.portal.home.appManagement.updateEntitlementsModal', [
    'hp.portal.home.webservices-v2'
  ])

  .factory('UpdateEntitlementsModal', function ($uibModal) {
    function open(data) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/home/app-management/update-entitlements-modal.html',
        controller: 'UpdateEntitlementsModalCtrl',
        backdrop: 'static',
        size: 'lg',
        keyboard: false,
        resolve: {
          data: data
        }
      });

      return modalInstance.result;
    }

    return {
      open: open
    };
  })

  .controller('UpdateEntitlementsModalCtrl', function ($scope, $uibModalInstance, $q, data, EntitlementService, LinkParser) {
    var removedUsersCount = 0;
    if (data.removedUsers) {
      data.removedUsers.forEach(function (userToRemove) {
        removedUsersCount += (userToRemove.entitlements && userToRemove.entitlements.length) || 0;
      });
    }

    var totalOperations = removedUsersCount + ((data.addedUsers && data.addedUsers.length) || 0);
    if (totalOperations === 0) {
      $uibModalInstance.close();
      return;
    }

    $scope.close = function () {
      $uibModalInstance.close();
    };

    $scope.productItem = data.productItem;
    $scope.addUserErrors = {};
    $scope.completedOperations = {
      removedUsers: [],
      addedUsers: {
        success: [],
        failed: []
      }
    };

    $scope.percentComplete = 0;
    var operationsComplete = 0;

    function updatePercentComplete() {
      $scope.percentComplete = (++operationsComplete / totalOperations) * 100;
    }

    function removeUsers() {
      var promises = [];
      if (data.removedUsers && data.removedUsers.length > 0) {
        data.removedUsers.forEach(function (userToRemove) {
          var userPromises = [];
          userToRemove.entitlements.forEach(function (entitlement) {
            var entitlementToDelete = EntitlementService.fromExisting(entitlement);
            var promise = entitlementToDelete.delete().finally(updatePercentComplete);
            userPromises.push(promise);
          });
          promises.push($q.all(userPromises).then(function () {
            $scope.completedOperations.removedUsers.push(userToRemove.user);
          }));
        });
      }
      return $q.all(promises);
    }

    function addUsers() {
      if (data.addedUsers && data.addedUsers.length > 0) {
        data.addedUsers.forEach(function (userToAdd) {
          var newEntitlement = EntitlementService.create();
          LinkParser.setLink(newEntitlement, 'user', LinkParser.getSelf(userToAdd).href);
          LinkParser.setLink(newEntitlement, 'license', LinkParser.getSelf(data.productItem.license).href);
          newEntitlement.save()
            .then(function () {
              $scope.completedOperations.addedUsers.success.push(userToAdd);
            }, function (response) {
              $scope.completedOperations.addedUsers.failed.push(userToAdd);
              if (response.status === 409) {
                $scope.addUserErrors.cMaxEntitlementsReached = true;
              }
              else {
                $scope.addUserErrors.cSaveFailed = true;
              }
              return $q.reject(response);
            })
            .finally(updatePercentComplete);
        });
      }
    }

    // Remove users first, then only add users if all users were successfully removed.
    removeUsers().then(addUsers);
  });

})();
