(function () {
  'use strict';

  angular.module('hp.portal.home.appManagement.confirmEntitlementChanges', [])

  .directive('hpUsersTable', function () {
    return {
      restrict: 'A',
      templateUrl: 'app/home/app-management/users-table.html',
      scope: {
        users: '=hpUsersTable',
        heading: '@'
      }
    };
  })

  .directive('confirmEntitlementChanges', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/home/app-management/confirm-entitlement-changes.html',
      scope: {
        addedUsers: '=',
        removedUsers: '=',
        onCancel: '&',
        onConfirm: '&'
      },
      link: function ($scope) {
        $scope.removedUsersMap = $scope.removedUsers && $scope.removedUsers.map(function (removedUser) {
          return removedUser.user;
        });
      }
    };
  })

  .factory('ConfirmEntitlementChangesModal', function ($uibModal) {
    function open(data) {
      var modalInstance = $uibModal.open({
        template: '<confirm-entitlement-changes added-users="addedUsers" removed-users="removedUsers" on-cancel="cancel()" on-confirm="confirm()"></confirm-entitlement-changes>',
        controller: 'ConfirmEntitlementChangesModalCtrl',
        backdrop: 'static',
        size: 'lg',
        resolve: {
          addedUsers: function () {
            return data.addedUsers;
          },
          removedUsers: function () {
            return data.removedUsers;
          }
        }
      });

      return modalInstance.result;
    }

    return {
      open: open
    };
  })

  .controller('ConfirmEntitlementChangesModalCtrl', function ($scope, $uibModalInstance, addedUsers, removedUsers) {
    $scope.addedUsers = addedUsers;
    $scope.removedUsers = removedUsers;
    $scope.cancel = $uibModalInstance.dismiss;
    $scope.confirm = $uibModalInstance.close;
  });

})();
