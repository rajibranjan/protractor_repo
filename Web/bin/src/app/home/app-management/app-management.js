(function () {
  'use strict';

  angular.module('hp.portal.home.appManagement', [
    'hp.portal.home.webservices-v2',
    'hp.portal.home.navigation.routesAndUrls',
    'hp.portal.home.appManagement.addUserToProductModal',
    'hp.portal.home.appManagement.confirmEntitlementChanges',
    'hp.portal.home.appManagement.updateEntitlementsModal'
  ])

  .controller('AppManagementCtrl', function ($scope, Self, LinkParser, Queries, AddUserToProductModal, UpdateEntitlementsModal, ConfirmEntitlementChangesModal) {
    $scope.fetchProductsByOffering = function (offering) {
      Queries.ProductsAndUsersByOfferingAndAccount.get({
        accountId: Self.accountId,
        offeringFqName: offering.fqName
      }).then(function success(data) {
        $scope.productAndUsers = data.productAndUsers;
        $scope.productAndUsers.forEach(function (productItem) {
          productItem.productId = LinkParser.getSelfId(productItem.product);
        });
      }, function error() {
        delete $scope.productAndUsers;
      });
    };

    $scope.manageUsers = function (productItem) {
      AddUserToProductModal.open({
        selectedOffering: $scope.selectedOffering,
        productItem: productItem
      }).then(function (result) {
        return UpdateEntitlementsModal.open({
          addedUsers: result.addedUsers,
          removedUsers: result.removedUsers,
          productItem: productItem
        });
      }).then(function () {
        // Refresh the product listing.
        $scope.fetchProductsByOffering($scope.selectedOffering);
      });
    };

    $scope.removeAccess = function (productItem, userItem) {
      var removedUsers = [userItem];
      ConfirmEntitlementChangesModal.open({
        removedUsers: removedUsers
      }).then(function () {
        return UpdateEntitlementsModal.open({
          removedUsers: removedUsers,
          productItem: productItem
        });
      }).then(function () {
        // Refresh the product listing.
        $scope.fetchProductsByOffering($scope.selectedOffering);
      });
    };
  })

  .directive('licensedAppSelector', function (Self, LicenseService, i18n) {
    return {
      restrict: 'E',
      template: '<select class="form-control" ng-model="selectedModel" ng-change="onSelect(selectedModel)" ng-options="i18n[offering.fqName][offering.title] for offering in offerings" ><option translate="cSelectAppToManage" ng-show="!selectedModel"></option></select>',
      scope: {
        selectedModel: '=',
        onSelect: '='
      },
      link: function (scope) {
        scope.i18n = {};

        var fqNameList = {};

        var unique = function (license) {
          if (fqNameList[license.offering.fqName]) {
            return false;
          }
          fqNameList[license.offering.fqName] = true;
          return true;
        };

        // Get all licensed offerings for this account.
        LicenseService.get({
          qs: 'account.id==' + Self.accountId + '&&status==ACTIVE',
          expand: 'offering'
        }).then(function (response) {
          scope.offerings = response.data.filter(unique).map(function (license) {
            var offering = license.offering;
            scope.i18n[offering.fqName] = i18n.getBySource(offering.fqName);
            return offering;
          });
        });
      }
    };
  });

})();
