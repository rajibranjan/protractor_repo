(function () {
  'use strict';

  angular.module('hp.portal.home.help', [])

  .constant('helpLinksAdmin', [{
    listName: 'cHPPullPrintOnDemand',
    helpLinks: [{
      linkName: 'cHelpMacPullPrintDriver',
      id: 'helpMacPullPrintDriverLink',
      href: 'http://h20564.www2.hp.com/portal/site/hpsc/public/kb/docDisplay/?docId=emr_na-c04432786'
    }, {
      linkName: 'cHelpAccessPullPrintFromPrinter',
      id: 'helpAccessPullPrintFromPrinterLink',
      href: 'http://h20564.www2.hp.com/hpsc/doc/public/display?docId=emr_na-c04432800'
    }]
  }, {
    listName: 'cManageUsers',
    helpLinks: [{
      linkName: 'cAddUsers',
      id: 'helpAddUserLink',
      href: 'http://h20564.www2.hp.com/hpsc/doc/public/display?docId=emr_na-c04432798'
    }, {
      linkName: 'cAddMultiUsers',
      id: 'helpAddMultiUsersLink',
      href: 'http://h20564.www2.hp.com/hpsc/doc/public/display?docId=emr_na-c04432782'
    }]
  }, {
    listName: 'cManageDevices',
    helpLinks: [{
      linkName: 'cHelpEnableOther',
      id: 'helpEnableOtherPrintersLink',
      href: 'http://h20564.www2.hp.com/hpsc/doc/public/display?docId=emr_na-c04432790'
    }, {
      linkName: 'cHelpSupportedDevice',
      id: 'helpSupportedDevicesLink',
      href: 'http://h20564.www2.hp.com/hpsc/doc/public/display?docId=emr_na-c04432778'
    }, {
      linkName: 'cHelpCardReaderConfiguration',
      id: 'helpCardReaderConfigurationLink',
      href: 'http://h20564.www2.hp.com/portal/site/hpsc/public/kb/docDisplay/?docId=emr_na-c04635064'
    }, {
      linkName: 'cHelpUsbProximityCardReaderInstall',
      id: 'helpUsbProximityCardReader',
      href: 'http://h20564.www2.hp.com/portal/site/hpsc/public/kb/docDisplay/?docId=emr_na-c04658200'
    }]
  }])

  .constant('helpLinksUser', [{
    listName: 'cHPPullPrintOnDemand',
    helpLinks: [{
      linkName: 'cHelpMacPullPrintDriver',
      id: 'helpMacPullPrintDriverLink',
      href: 'http://h20564.www2.hp.com/portal/site/hpsc/public/kb/docDisplay/?docId=emr_na-c04432786'
    }, {
      linkName: 'cHelpAccessPullPrintFromPrinter',
      id: 'helpAccessPullPrintFromPrinterLink',
      href: 'http://h20564.www2.hp.com/hpsc/doc/public/display?docId=emr_na-c04432800'
    }, {
      linkName: 'cHelpProximityCardActivationGuide',
      id: 'helpProximityCardActivationGuide',
      href: 'http://h20564.www2.hp.com/portal/site/hpsc/public/kb/docDisplay/?docId=emr_na-c04635215'
    }]
  }, {
    listName: 'cProfilePassword',
    helpLinks: [{
      linkName: 'cHelpHowChangeMyProfile',
      id: 'helpHowChangeProfileLink',
      href: 'http://h20566.www2.hp.com/portal/site/hpsc/template.PAGE/public/kb/docDisplay/?sp4ts.oid=7416310&spf_p.tpst=kbDocDisplay&spf_p.prp_kbDocDisplay=wsrp-navigationalState%3DdocId%253Demr_na-c04432771-3%257CdocLocale%253Den_US%257CcalledBy%253DSearch_Result&javax.portlet.begCacheTok=com.vignette.cachetoken&javax.portlet.endCacheTok=com.vignette.cachetoken'
    }, {
      linkName: 'cHelpHowChangeMyPassword',
      id: 'helpHowChangePasswordLink',
      href: 'http://h20566.www2.hp.com/portal/site/hpsc/template.PAGE/public/kb/docDisplay/?sp4ts.oid=7416310&spf_p.tpst=kbDocDisplay&spf_p.prp_kbDocDisplay=wsrp-navigationalState%3DdocId%253Demr_na-c04432771-3%257CdocLocale%253Den_US%257CcalledBy%253DSearch_Result&javax.portlet.begCacheTok=com.vignette.cachetoken&javax.portlet.endCacheTok=com.vignette.cachetoken'
    }]
  }])

  .controller('HelpCtrl', function ($scope, helpLinksAdmin, helpLinksUser, Globals, Self) {
    $scope.locale = Self.getLocale();

    //We don't want reseller to see admin Help links because they are based on adding users, which resellers don't currently do.
    var showAdminLink = (Globals.permissions.isHPAdmin || (Globals.permissions.isCompanyAdmin && !Globals.permissions.isReseller));

    if (showAdminLink) {
      $scope.helpLinkList = helpLinksAdmin;
    }
    else {
      $scope.helpLinkList = helpLinksUser;
    }
  });

})();
