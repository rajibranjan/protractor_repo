(function () {
  'use strict';

  angular.module('hp.portal.home.resellers.addReseller', [
    'hp.portal.common.webservices',
    'hp.portal.common.forms.country',
    'hp.portal.common.forms.userForm',
    'hp.portal.common.forms.pattern',
    'hp.portal.home.navigation.routesAndUrls',
    'hp.portal.home.webservices.tasks',
  ])

  .controller('AddResellerCtrl', function ($scope, Self, $location, PageAlerts, CreateReseller, Urls, Countries, Locales, CountrySettings, ErrorCodeMapper) {

    var setupReseller = function () {
      $scope.reseller = {
        metaTags: []
      };
      $scope.companyAdmin = {};
      $scope.adminContact = {
        locale: Self.getLocale()
      };
      $scope.user = $scope.adminContact;
      $scope.locales = Locales.getAll();
      $scope.countries = Countries;
    };

    $scope.$watch('reseller.country', function (newValue) {
      $scope.reseller.state = null;
      $scope.reseller.zipcode = null;

      if (newValue !== undefined) {
        $scope.countrySettings = CountrySettings.get(newValue);
      }
    });

    setupReseller();
    $scope.isAdding = true;

    var addReseller = function (saveAndNew) {
      var taskPayload = {
        reseller: $scope.reseller,
        adminContact: $scope.user
      };
      // Kick off the synchronous CreateReseller task.
      return CreateReseller.startTask(taskPayload).$promise
        .then(redirectOrResetForm.bindArgs(saveAndNew))
        .then(addResellerSuccess.bindArgs(taskPayload), addResellerError);
    };

    var addResellerSuccess = function (taskPayload) {
      var alert = {
        type: 'success',
        msg: 'cAddResellerSuccess',
        translateValues: {
          name: taskPayload.reseller.name
        },
        timeout: PageAlerts.timeout.medium
      };

      PageAlerts.add(alert);
    };

    var addResellerError = function (task) {
      if (!ErrorCodeMapper.hasErrorCodes(task)) {
        PageAlerts.add({
          type: 'danger',
          msg: 'cAddResellerError',
          translateValues: {
            errorCode: (task.httpStatus || 'unknown')
          },
          timeout: PageAlerts.timeout.long
        });
      }
    };

    function redirectOrResetForm(saveAndNew) {
      if (saveAndNew) {
        // Re-initialize the models to be able to create a new account.
        setupReseller();
      }
      else {
        // Redirect to the manage account apge.
        $location.url(Urls.manageResellers());
      }
    }


    $scope.isAdding = true;

    $scope.addResellerSubmit = function () {
      $scope.$broadcast('hpDisableAddResellerForm', addReseller.bindArgs(false));
    };

    $scope.addResellerAndNewSubmit = function () {
      $scope.$broadcast('hpDisableAddResellerForm', addReseller.bindArgs(true));
    };

    $scope.cancelAdd = function () {
      $location.url(Urls.manageResellers());
    };

    //FIXME: We should refactor any code that might use somthing that isn't there.
    $scope.updateLocales = function () {
      var localesByCountry = Locales.getByCountry($scope.reseller.country);
      if ($scope.locales.length > 0) {
        $scope.user.locale = localesByCountry[0].lang;
      }
    };
  });
})();
