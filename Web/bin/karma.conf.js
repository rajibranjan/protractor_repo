// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function (config) {
  'use strict';

  config.set({

    // base path, that will be used to resolve files and exclude
    basePath: '',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [

      // bind-polyfill is required because PhantomJS doesn't support Function.prototype.bind!
      'src/bower_components/bind-polyfill/index.js',

      'src/bower_components/titan-web-sdk/dist/scripts/titan-testhooks-globals.js',

      'src/bower_components/jquery/dist/jquery.js',
      'src/bower_components/angular/angular.js',
      'src/bower_components/angular-animate/angular-animate.js',
      'src/bower_components/angular-flot/angular-flot.js',
      'src/bower_components/angular-recaptcha/release/angular-recaptcha.js',
      'src/bower_components/angular-mocks/angular-mocks.js',
      'src/bower_components/angular-translate/angular-translate.js',
      'src/bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
      'src/bower_components/angular-resource/angular-resource.js',
      'src/bower_components/angular-cookies/angular-cookies.js',
      'src/lib/angular-locker.js',
      'src/bower_components/angular-sanitize/angular-sanitize.js',
      'src/bower_components/angular-route/angular-route.js',
      'src/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      'src/bower_components/angular-ui-mask/dist/mask.js',
      'src/bower_components/angular-ui-select/dist/select.js',
      'src/bower_components/angular-file-upload/angular-file-upload.js',
      'src/bower_components/checklist-model/checklist-model.js',
      'src/bower_components/angulartics/src/angulartics.js',
      'src/bower_components/angulartics/src/angulartics-ga.js',
      'src/bower_components/ngToast/dist/ngToast.js',
      'src/bower_components/moment/min/moment-with-locales.js',
      'src/bower_components/angular-moment/angular-moment.js',
      'src/bower_components/titan-web-sdk/dist/scripts/titan-sdk.js',

      'mocks/mock-angular-translate.js',
      'mocks/mock_data/api/v2/*.json',

      'src/app/**/*.html',
      'src/app/**/*.js',

      'test/unitTestInit.js',
      'test/app/**/*.js'
    ],

    preprocessors: {
      'src/app/**/*.js': 'coverage',
      'src/app/**/*.html': ['ng-html2js']
    },

    ngHtml2JsPreprocessor: {
      // strip this from the file path
      stripPrefix: 'src/',
      // prepend this to the template path
      prependPrefix: '',
      moduleName: 'hp.templates'
    },

    reporters: ['progress', 'coverage'],

    coverageReporter: {
      type: 'html',
      dir: 'test-output/coverage/'
    },

    // list of files / patterns to exclude
    exclude: [],

    // web server port
    port: 8080,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    // browsers: ['Chrome', 'PhantomJS', 'Firefox'],
    browsers: ['Chrome'],

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  });
};
