angular.module('pascalprecht.translate', [])
  .provider('$translate', [
    function() {
      this.$get = [
        function() {
          var $translate = function () {};
          return $translate;
        }
      ];

      this.youAreUsingAMockWARNING = function() {};

      //Stub any methods we use here
      this.useLoader = function() {};
      this.preferredLanguage = function() {};
      this.useCookieStorage = function() {};
      this.useLocalStorage = function() {};
      this.useMissingTranslationHandler = function() {};
      this.translationNotFoundIndicatorLeft = function() {};
      this.translationNotFoundIndicatorRight = function() {};
    }
  ]).filter('translate', function () {
    return function () {};
  });
