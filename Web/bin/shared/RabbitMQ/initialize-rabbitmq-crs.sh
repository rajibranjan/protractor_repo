#!/bin/bash

# This script initializes rabbitmq for crs.  Requires for things like aged document delete to function.

# Invoke the setup script that initializes vhost, rabbitUrl, user and password
source ./initialize-rabbitmq-setup.sh

echo "extra socks commands => $socks_options"

# Delete existing crs vhost
echo "Delete vhost if already exists, vhost is $vhost"
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XDELETE $rabbitUrl/api/vhosts/$vhost 

# Create new crs vhost
echo "Creating the vhost, vhost is $vhost"
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPUT $rabbitUrl/api/vhosts/$vhost 

# Set permissions on that vhost
echo "Setting permissions on the crs vhost, vhost is $vhost, user is $user"
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPUT -d'{"configure":".*","write":".*","read":".*"}' $rabbitUrl/api/permissions/$vhost/$vhost_user 
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPUT -d'{"configure":".*","write":".*","read":".*"}' $rabbitUrl/api/permissions/$vhost/$user 

# Curl commands to create queue, exchange and bindings
# Create exchanges.  We're stuck with curl here because rabbitmqctl doesn't expose functionality to create exchanges and bind them to queues.
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPUT -d'{"type":"direct","auto_delete":false,"durable":true,"arguments":[]}' $rabbitUrl/api/exchanges/$vhost/document.event.x 
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPUT -d'{"type":"direct","auto_delete":false,"durable":true,"arguments":[]}' $rabbitUrl/api/exchanges/$vhost/task.event.x 

# Create queues

# Task document cleanup queue
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPUT -d'{"durable" : true, "exclusive" : false,"auto_delete" : false,"arguments" : []}' $rabbitUrl/api/queues/$vhost/task.event.document.cleanup 

# Document event queues
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPUT -d'{"durable" : true, "exclusive" : false,"auto_delete" : false,"arguments" : []}' $rabbitUrl/api/queues/$vhost/document.event.add 
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPUT -d'{"durable" : true, "exclusive" : false,"auto_delete" : false,"arguments" : []}' $rabbitUrl/api/queues/$vhost/document.event.update 
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPUT -d'{"durable" : true, "exclusive" : false,"auto_delete" : false,"arguments" : []}' $rabbitUrl/api/queues/$vhost/document.event.delete 
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPUT -d'{"durable" : true, "exclusive" : false,"auto_delete" : false,"arguments" : []}' $rabbitUrl/api/queues/$vhost/document.event.get 
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPUT -d'{"durable" : true, "exclusive" : false,"auto_delete" : false,"arguments" : []}' $rabbitUrl/api/queues/$vhost/document.event.download 


# Create bindings

# Task document cleanup binding
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPOST -d'{"routing_key":"Document_cleanup","arguments":[]}' $rabbitUrl/api/bindings/$vhost/e/task.event.x/q/task.event.document.cleanup 

# Document event bindings
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPOST -d'{"routing_key":"Add","arguments":[]}' $rabbitUrl/api/bindings/$vhost/e/document.event.x/q/document.event.add 
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPOST -d'{"routing_key":"Update","arguments":[]}' $rabbitUrl/api/bindings/$vhost/e/document.event.x/q/document.event.update 
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPOST -d'{"routing_key":"Delete","arguments":[]}' $rabbitUrl/api/bindings/$vhost/e/document.event.x/q/document.event.delete 
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPOST -d'{"routing_key":"Get","arguments":[]}' $rabbitUrl/api/bindings/$vhost/e/document.event.x/q/document.event.get 
curl  $socks_options -u $user:$password -H "content-type:application/json"  -XPOST -d'{"routing_key":"Download","arguments":[]}' $rabbitUrl/api/bindings/$vhost/e/document.event.x/q/document.event.download 

echo "Finished creating all the crs exchanges and queues in the crs vhost"
