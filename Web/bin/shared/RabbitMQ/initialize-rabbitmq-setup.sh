# create needed rabbitmq vhost / queues / exchanges
#
# if called with an argument of 'local' then nothing else needs to be passed in
# otherwise, possible variables passed in via environment:
#   gitVersion (required)
#   stackName (required)
#   BUILD_ID (required)
#   vhost (optional)


if [ ! -z "$1" -a "$1" = "local" ] ; then
  gitVersion=NA
  stackName=local
  BUILD_ID=NA
  if [ ! -z "$2" ] ; then
    vhost=$2
  else
    vhost=local
  fi
fi


if [ -z "$gitVersion" ] ; then
  echo $0 ERROR: gitVersion must be set
  exit 1
fi

if [ -z "$BUILD_ID" ] ; then
  echo $0 ERROR: BUILD_ID must be set
  exit 1
fi
buildid=`echo $BUILD_ID | sed 's/_/t/g'`
echo $0 Built buildid=$buildid from BUILD_ID=$BUILD_ID

if [ -z "$stackName" ] ; then
  echo $0 ERROR: stackName must be set
  exit 1
fi


## vhost ##
if [ -z "$vhost" ] ; then
  if [ $stackName = prod -o $stackName = preprod -o "$stackName" = "crs-prod" -o "$stackName" = "crs-preprod" ] ; then
    vhost=vhost-$stackName
  else
    vhost=$gitVersion-$buildid-$stackName
  fi
fi

# Note for preprod and prod.  username/password is in secure deploy.  And, the url is passed in via jenkins jobs.
if [ $stackName = local ] ; then
  echo "Create rabbitmq artifacts for local"
  rabbitUrl=http://127.0.0.1:15672
  user=testhost
  password=Riptide_1234
  vhost_user=${user}
  socks_options="--noproxy localhost,127.0.0.1"
else
  if [ ! -z "$rabbitUrl" ] ; then
    echo $0 INFO: use rabbitUrl=$rabbitUrl set in environment
  else
    rabbitUrl=https://rmq-titan-dev.hpccp.net:15671
  fi
  user=hpadmin4jenkins
  password=riptide@123
  vhost_user=r1pt1de_rabbitmq_tst
  socks_options="--socks4 socks-server.hp.com:1080 --noproxy localhost,127.0.0.1"
fi

