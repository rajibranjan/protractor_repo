to upload files to a stack, run the following:
  ./deploy.sh -u [user email] -p [user password] -f [file of upload commands] -s [url of HPStore] -h [ScanSend rootServiceUrl] -a [PullPrint rootServiceUrl] -b [Puffin rootServiceUrl] -c [Incatern rootServiceUrl]

e.g., to upload locally with all .json resources, run:
  ./deploy.sh -u hpadmin@eggnogcookie.com -p Hpadmin12345 -f local_files.txt -s https://127.0.0.1:9443/HPStoreWeb -h https://127.0.0.1:9443/ScanSend -a https://127.0.0.1:9443/PullPrint -b http://127.0.0.1:8080 -c http://127.0.0.1:8080

e.g., to upload dev with all .json resources, run:
  ./deploy.sh -u hpadmin@eggnogcookie.com -p Hpadmin12345 -f local_files.txt -s https://www-micro-dev.hpfoghorn.com -h https://ss-micro-dev.hpfoghorn.com -a https://pp-micro-dev.hpfoghorn.com -b https://puffin-micro-dev.hpbizapps.com -c https://incatern-micro-dev.hpbizapps.com

e.g., to upload staging with all .json resources, run:
  ./deploy.sh -u hpadmin@eggnogcookie.com -p Hpadmin12345 -f local_files.txt -s https://www-micro-staging.hpfoghorn.com -h https://ss-micro-staging.hpfoghorn.com -a https://pp-micro-staging.hpfoghorn.com -b https://puffin-micro-staging.hpbizapps.com -c https://incatern-micro-staging.hpbizapps.com

e.g., to upload production with all .json resources, run:
  ./deploy.sh -u hpadmin@example.com -p y0urHpAdm1nPa5sw0rd -f production_files.txt -s https://www.hpondemand.com -h https://ss.hpondemand.com -a https://pp.hpondemand.com -b https://puffin.hpbizapps.com -c https://incatern.hpbizapps.com
