#~/bin/bash

# temp directory
temp_dir_name="temp_localization"
portal_grunt_temp_dir="../../../../../titan-portal/Clients/Web/$temp_dir_name"
pullPrint_grunt_temp_dir="../../../../../titan-PullPrint/PullPrintUI/$temp_dir_name"
cloudRepo_grunt_temp_dir="../../../../../titan-scansend/CloudRepoProxy/CloudRepoProxyUI/$temp_dir_name"
proxyroot_grunt_temp_dir="../../../../../titan-proxyroot/ProxyRootService/$temp_dir_name"

# initial zipped delivery file
delivery_folder="localization_delivery"
delivery="delivery"
delivery_dir=""

# vm template directory
vm_dir="../../../../../titan-portal/HPServices/src/main/resources/vm"

# framework json file
frameworkJSON="strings.json"

# default Locale
default_Locale="en-US"

# create file structure
function createFileStructure {
  if [ -z "$1" ] ; then
    echo "ERROR: No directory argument specified"
    return 0
  fi

  currDir=${PWD}
  cd $1
  baseDir=${PWD}

  for filename in $baseDir/*
    do
      fileArray=(`echo "${filename##*/}" | tr "__" "\n"`)
      arraySize=$((${#fileArray[@]}-1))
      for ((x=0;x<=arraySize;x++))
        do
          if [ $x -eq $arraySize ] ; then
            echo "Moving and renaming file $filename to ${fileArray[$x]}"
            mv $filename ${fileArray[x]}
            cd $baseDir
          else
            if [ ! -d ${fileArray[x]} ] ; then
              mkdir ${fileArray[x]}
            fi
            cd ${fileArray[x]}
          fi
      done
  done
  cd $currDir
}

# clean up and deploy the vm resources
function processVM {
  if [ -z "$1" ] ; then
    echo "ERROR: No directory argument specified"
    return 0
  fi
  if [ -z "$2" ] ; then
    echo "ERROR: No locale argument specified"
    return 0
  fi
  vmTempDir=$1/vm
  if [ ! -d $vmTempDir ] ; then
    echo "ERROR: $vmTempDir is not a directory"
    return 0
  fi
  # perform file cleanup on all .vm files in the directory
  echo "CLEANUP: $vmTempDir"
  # create the VM drop directory if necessary
  mkdir -p $vm_dir/$2

  # rename all .html file to .vm files
  files=$(find $vmTempDir/*html -type f)
  for file in $files
    do
      mv $file "${file/%html/vm}"
  done

  # cleanup the vm files
  files=$(find $vmTempDir/*vm -type f)
  for file in $files
  do
    vmTemplateCleanup $file $2
    # deploy the file
    cp -v -r $file $vm_dir/$2/${file##*/}
  done
}

# clean up the framework resources
function processFramework {
  if [ -z "$1" ] ; then
    echo "ERROR: No directory argument specified"
    return 0
  fi
  if [ -z "$2" ] ; then
    echo "ERROR: No framework directory name argument specified"
    return 0
  fi
  if [ -z "$3" ] ; then
    echo "ERROR: No localization argument specified"
    return 0
  fi
  frameworkTempDir=$1/$2
  if [ ! -d $frameworkTempDir ] ; then
    echo "ERROR: $frameworkTempDir is not a directory"
    return 0
  fi
  # perform file cleanup
  echo "CLEANUP: $frameworkTempDir"
  # rename the LOCALE directory to use the actual locale
  if [ -d $frameworkTempDir/LOCALE ] ; then
    mv $frameworkTempDir/LOCALE $frameworkTempDir/$3
  fi
  jsonCleanup $frameworkTempDir/$3/$frameworkJSON
}

# clean up the Portal resources
function processI18N {
  if [ -z "$1" ] ; then
    echo "ERROR: No directory argument specified"
    return 0
  fi
  if [ -z "$2" ] ; then
    echo "ERROR: No localization argument specified"
    return 0
  fi
  portalTempDir=$1
  if [ ! -d $portalTempDir ] ; then
    echo "ERROR: $portalTempDir is not a directory"
    return 0
  fi
  # get the app subdirectories
  appDirs=$(find $portalTempDir/* -maxdepth 0 -type d)
  # if project is bootstrap or scanSendUI then default appDirs to just be the base directory
  if [[ "$portalTempDir" =~ .*platformBootstrap.* ]] ; then
    appDirs=$portalTempDir
  fi
  if [[ "$portalTempDir" =~ .*scanSendUI.* ]] ; then
    appDirs=$portalTempDir
  fi
  if [[ "$portalTempDir" =~ .*scanSendBootstrap.* ]] ; then
    appDirs=$portalTempDir
  fi
  if [[ "$portalTempDir" =~ .*pullPrintBootstrap.* ]] ; then
    appDirs=$portalTempDir
  fi
  for appDir in $appDirs
  do
    appTempDir=$appDir/i18n
    if [ ! -d $appTempDir ] ; then
      echo "ERROR: $appTempDir is not a directory"
      return 0
    fi
    # perform file cleanup
    echo "CLEANUP: $appTempDir"
    # process json files
    files=$(find $appTempDir/*json -type f)
    for file in $files
    do
      echo "$file"
      cleanupI18nJSON $file $2
    done
  done
}

# clean up a json file
function jsonCleanup {
  if [ ! -e $1 ] ; then
    echo "ERROR: $1 is not a file"
    return 0
  fi
  # this is to cleanup any formatting mistakes from the localization vendors
  # these issues may not exist, but they have happened in the past and we want to catch them

  # this removes any characters prior to the opening {
  sed -r -i '1s/(.*\{)/\{/g' $1
  # this is for non-breaking spaces copied in a windows char-set
  sed -r -i 's/(\xc2\xa0)/\&nbsp;/g' $1
  # remove quote escapes so grunt won't add extras
  sed -r -i 's/(\\")/"/g' $1
  sed -r -i 's/(\«)/&laquo;/g' $1
  sed -r -i 's/(\»)/&raquo;/g' $1
  sed -r -i 's/(\“)/\&ldquo;/g' $1
  sed -r -i 's/(\”)/\&rdquo;/g' $1
  sed -r -i 's/(\„)/\&bdquo;/g' $1
  # html encode non-standard quotations
  sed -r -i 's/(«)/&laquo;/g' $1
  sed -r -i 's/(»)/&raquo;/g' $1
  sed -r -i 's/(“)/\&ldquo;/g' $1
  sed -r -i 's/(”)/\&rdquo;/g' $1
  sed -r -i 's/(„)/\&bdquo;/g' $1
  # make sure product names are current
  sed -r -i 's/(Pull Print)/Private Print/g' $1

  replaceSpecialCharacters $1
}

# clean up a .vm file
function vmTemplateCleanup {
  if [ ! -e $1 ] ; then
    echo "ERROR: $1 is not a file"
    return 0
  fi
  if [ -z "$2" ] ; then
    echo "ERROR: No language code argument specified"
    return 0
  fi
  echo "** Processing file: $1"
  # replace 'en-US' with the correct language code
  sed -r -i "s/(_en-US)/_$2/g" $1
  # this is to cleanup any formatting mistakes from the localization vendors
  # these issues may not exist, but they have happened in the past and we want to catch them
  sed -r -i 's/\\&quot;/\&quot;/g' $1
  sed -r -i 's/(&quot;)/\"/g' $1
  sed -r -i 's/\r$//g' $1
  sed -r -i 's/(\xc2\xa0)/\&nbsp;/g' $1
  # dutch grammar uses hyphens a lot and the translations can hyphenate variables
  # which will then not work correctly in .vm templates.  This will add a zero-width space to a hyphen
  if [ $2 = 'nl-NL' ]
  then
    sed -i 's/\(\$[a-zA-Z]*\)-/\1\&#8203;-/g' $1
  fi

  replaceSpecialCharacters $1
}

function replaceSpecialCharacters {
  if [ ! -e $1 ] ; then
    echo "ERROR: $1 is not a file"
    return 0
  fi

  # Replace special characters (umlauts, accents, tildes, circumflex, cedilla, ligature, punctuation)
  echo "replacing special characters in file: $1"
  sed -i 's/\Ä/\&Auml\;/g;s/\ä/\&auml\;/g;s/\Ë/\&Euml\;/g;s/\ë/\&euml\;/g;s/\Ï/\&Iuml\;/g;s/\ï/\iuml\;/g;s/\Ö/\&Ouml\;/g;s/\ö/\&ouml\;/g;s/\Ü/\&Uuml\;/g;s/\ü/\&uuml\;/g;' $1
  sed -i 's/\Á/\&Aacute\;/g;s/\á/\&aacute\;/g;s/\É/\&Eacute\;/g;s/\é/\&eacute\;/g;s/\Í/\&Iacute\;/g;s/\í/\&iacute\;/g;s/\Ó/\&Oacute\;/g;s/\ó/\&oacute\;/g;s/\Ú/\&Uacute\;/g;s/\ú/\&uacute\;/g;' $1
  sed -i 's/\À/\&Agrave\;/g;s/\à/\&agrave\;/g;s/\È/\&Egrave\;/g;s/\è/\&egrave\;/g;s/\Ì/\&Igrave\;/g;s/\ì/\&igrave\;/g;s/\Ò/\&Ograve\;/g;s/\ò/\&ograve\;/g;s/\Ù/\&Ugrave\;/g;s/\ù/\&ugrave\;/g;' $1
  sed -i 's/\Ñ/\&Ntilde\;/g;s/\ñ/\&ntilde\;/g;s/\Ã/\&Atilde\;/g;s/\ã/\&atilde\;/g;s/\Õ/\&Otilde\;/g;s/\õ/\&otilde\;/g;' $1
  sed -i 's/\Â/\&Acirc\;/g;s/\â/\&acirc\;/g;s/\Ê/\&Ecirc\;/g;s/\ê/\&ecirc\;/g;s/\Î/\&Icirc\;/g;s/\î/\&icirc\;/g;s/\Ô/\&Ocirc\;/g;s/\ô/\&ocirc\;/g;s/\Û/\&Ucirc\;/g;s/\û/\&ucirc\;/g;' $1
  sed -i 's/\Ç/\&Ccedil\;/g;s/\ç/\&ccedil\;/g;' $1
  sed -i 's/\Æ/\&AElig\;/g;s/\æ/\&aelig\;/g;s/\Œ/\&OElig\;/g;s/\œ/\&oelig\;/g;s/\ß/\&szlig\;/g;' $1
}

# clean up a UI i18n json file
function cleanupI18nJSON {
  if [ ! -e $1 ] ; then
    echo "ERROR: $1 is not a file"
    return 0
  fi
  if [ -z "$2" ] ; then
    echo "ERROR: No localization argument specified"
    return 0
  fi
  echo "** Processing file: $1"
  # this is to cleanup any formatting mistakes from the localization vendors
  # these issues may not exist, but they have happened in the past and we want to catch them
  jsonCleanup $1
}

# rename a localized file
function renameLocFile {
  if [ -z "$1" ] ; then
    echo "ERROR: No file specified"
    return 0
  fi
  if [ -z "$2" ] ; then
    echo "ERROR: No search string specified"
    return 0
  fi
  if [ -z "$3" ] ; then
    echo "ERROR: No replacement string specified"
    return 0
  fi
  # try to replace the default locale in the file name
  newFile=${1/$default_Locale/$3}
  newFile=${newFile/$2/$3}
  newFileName=${newFile##*/}
  if [[ -e $1 ]] && [[ "$1" != "$newFile" ]]
  then
    echo "** Renaming file: ${1##*/} to $newFileName"
    mv $1 $newFile
  fi
}

# check directories for incorrectly named files and correct if found
function renameBadLocaleFiles {
  if [ ! -d $1 ] ; then
    echo "ERROR: $1 is not a directory"
    return 0
  fi
  if [ -z "$2" ] ; then
    echo "ERROR: No bad locale value argument specified"
    return 0
  fi
  if [ -z "$3" ] ; then
    echo "ERROR: No replacement locale argument specified"
    return 0
  fi
  for file in $1/*
  do
    # search subdirectories
    if [ -d "$file" ]
    then
      renameBadLocaleFiles $file $2 $3
    fi
    # fix any invalid locale formats
    if [[ -e $file ]]
    then
      if [[ "$file" =~ $2 ]] || [[ "$file" =~ $defaultLocale ]]
      then
        renameLocFile $file $2 $3
      fi
    fi
  done
}

# firmware only allows for fr-FR and pt-PT so oxpmfp files have to be those locales (even though we technically don't support them)
function renameOxpmfpFiles {

  mv temp_localization/fr-CA/pullPrint/oxpmfp/i18n/fr-CA.json temp_localization/fr-CA/pullPrint/oxpmfp/i18n/fr-FR.json
  mv temp_localization/pt-BR/pullPrint/oxpmfp/i18n/pt-BR.json temp_localization/pt-BR/pullPrint/oxpmfp/i18n/pt-PT.json

  mv temp_localization/fr-CA/proxyroot/oxpmfp/i18n/fr-CA.json temp_localization/fr-CA/proxyroot/oxpmfp/i18n/fr-FR.json
  mv temp_localization/pt-BR/proxyroot/oxpmfp/i18n/pt-BR.json temp_localization/pt-BR/proxyroot/oxpmfp/i18n/pt-PT.json
}

# unzip delivery into a temp directory
temp_dir=$temp_dir_name
# if temp directory already exists then delete it
if [ -d $temp_dir ] ; then
  rm -rf $temp_dir
fi
mkdir $temp_dir

for delivery_file in $delivery_folder/*
do
  if [[ $delivery_file = *$delivery*.** ]]
  then
    unzip $delivery_file -d $temp_dir
    delivery_file=$(basename "$delivery_file")
    delivery_folder="${delivery_file%.*}"
    delivery_dir=$temp_dir
  fi
done

if [ -z $delivery_dir ] ; then
  echo "ERROR: Delivery directory not set"
  exit 1
fi

# clean up temp delivery
for locale_dir in $delivery_dir/*
do
  if [[ "$locale_dir" =~ [A-Z][A-Z](_|-)[A-Z][A-Z] ]]
  then
    locale=$(basename "$locale_dir")
    echo "Found invalid locale: $locale"
    dirBase=${locale_dir%$locale}
    correctedLocale=$(sed -e 's/../\L&\E/' <<< $locale)
    correctedDir=$dirBase$correctedLocale
    echo "Corrected dir: $correctedDir"
    mv $locale_dir $correctedDir
    locale_dir=$correctedDir
  fi
  if [[ "$locale_dir" =~ [a-z][a-z]_[A-Z][A-Z] ]]
  then
    # fix any invalid locale formats
    locale=$(basename "$locale_dir")
    echo "Found invalid locale: $locale"
    dirBase=${locale_dir%$locale}
    correctedLocale=${locale//_/-}
    # we currently use fr-CA instead of fr-FR
    if [[ "$correctedLocale" =~ fr-FR ]]
    then
        correctedLocale="fr-CA"
    fi
    correctedDir=$dirBase$correctedLocale
    mv $locale_dir $correctedDir
    # find and correct any files in the directory
    renameBadLocaleFiles $correctedDir $locale $correctedLocale
  elif [[ "$locale_dir" =~ [a-z][a-z]-[A-Z][A-Z] ]]
  then
    # fix any invalid locale formats
    locale=$(basename "$locale_dir")
    dirBase=${locale_dir%$locale}
    badLocale=${locale//-/_}
    # we currently use fr-CA instead of fr-FR
    if [[ "$locale_dir" =~ fr-FR ]]
    then
        locale="fr-CA"
        correctedDir=$dirBase$locale
        mv $locale_dir $correctedDir
        locale_dir=$correctedDir
    fi
    echo "Searching for files with bad locale: $badLocale"
    # find and correct any files in the directory
    renameBadLocaleFiles $locale_dir $badLocale $locale
  fi

  createFileStructure $locale_dir
done

renameOxpmfpFiles

for locale_dir in $delivery_dir/*
do
  if [[ "$locale_dir" =~ [a-z][a-z]-[A-Z][A-Z] ]]
  then
    echo "Processing directory: $locale_dir"
    locale=$(basename "$locale_dir")
    # copy emails to deployment directory
    processVM $locale_dir $locale
    # clean up json file resources
    processI18N $locale_dir/portal $locale
    processI18N $locale_dir/pullPrint $locale
    processI18N $locale_dir/proxyroot $locale
    processI18N $locale_dir/scanSendUI $locale
    processI18N $locale_dir/scanSendBootstrap $locale
    processI18N $locale_dir/platformBootstrap $locale
    processI18N $locale_dir/pullPrintBootstrap $locale
    processFramework $locale_dir framework $locale
    processFramework $locale_dir scanSendFW $locale

  fi
done

# run grunt localize to copy strings to deployed json files
echo "!!! Running grunt localize"
grunt localize

# remove temp directories
rm -rf $temp_dir
