#~/bin/bash

#  This script will gather all of the current english localization resources for the storefront
#  and place them in a "localizationDrop_+day+month+year" folder on the desktop.
#
#  When localization files are added or removed from the project this script should be updated.

# Add localization resources folders here

# vm email template directory
vm_dir="../../../../../titan-portal/HPServices/src/main/resources/vm/en-US"

# framework strings directory
framework_dir="../../../../../titan-framework/HPFramework/src/main/resources/localization/en-US"

# bootstrap resources
platform_bootstrap_i18n_dir="../Bootstrap/json/src/main/assets/i18n"
scansend_bootstrap_i18n_dir="../Bootstrap/json/ScanSend/src/main/assets/i18n"
pullPrint_bootstrap_i18n_dir="../Bootstrap/json/PullPrint/src/main/assets/i18n"

# i18n directories
portal_common_i18n_dir="../../../../../titan-portal/Clients/Web/src/app/common/i18n"
pullPrint_i18n_dir="../../../../../titan-pullprint/PullPrintService/src/main/resources/i18n"
pullPrint_home_i18n_dir="../../../../../titan-pullprint/PullPrintUI/src/app/home/i18n"
pullPrint_oxpmfp_i18n_dir="../../../../../titan-pullprint/PullPrintService/src/main/oxpmfp/v3/src/resources/i18n"
proxyroot_home_i18n_dir="../../../../../titan-proxyroot/ProxyRootService/src/main/resources/i18n"
proxyroot_oxpmfp_i18n_dir="../../../../../titan-proxyroot/ProxyRootService/src/main/oxpmfp/v3/src/resources/i18n"
scanSendUI_i18n_dir="../../../../../titan-scansend/CloudRepoProxy/CloudRepoProxyUI/src/app/i18n"
scanSend_i18n_dir="../../../../../titan-scansend/CloudRepoProxy/CrpServices/src/main/resources/localization/en-US"

i18n_file="locale-en-US.json"
i18n_file_short="locale-en.json"

# End localization resources

LOCALIZATION_FOLDER="localizationDrop_"$(date +"%d%b%Y")
CURRENT_DIR=${PWD}

# create the drop folder
DROP_DIR=~/Desktop/$LOCALIZATION_FOLDER
mkdir -p $DROP_DIR

# move the i18n json files
# Portal apps
cp -v $portal_common_i18n_dir/$i18n_file $DROP_DIR/"portal__common__i18n__"$i18n_file

# Pullprint i18n resources
cp -v $pullPrint_home_i18n_dir/$i18n_file $DROP_DIR/"pullPrint__home__i18n__"$i18n_file

# Scansend apps
cp -v $scanSendUI_i18n_dir/$i18n_file $DROP_DIR/"scanSendUI__i18n__"$i18n_file

# move the vm files
for file in $vm_dir/*
do
  cp -v $file $DROP_DIR/"vm__${file##*/}"
done

#rename all .vm files to be .html
cd $DROP_DIR
for file in *.vm
do
  mv $file "${file/%vm/html}"
done
cd $CURRENT_DIR

# move the framework json files
for file in $framework_dir/*
do
  cp -v $file $DROP_DIR/"framework__LOCALE__${file##*/}"
done
#cp -v -r $framework_dir $tmp_dir

# move the bootstrap resources
# platform bootstrap
# i18n
for file in $platform_bootstrap_i18n_dir/*
do
  if [[ $file = *.*en-US.** ]]
  then
    cp -v $file $DROP_DIR/"platformBootstrap__i18n__${file##*/}"
  fi
done

# pullprint bootstrap

# i18n
for file in $pullPrint_bootstrap_i18n_dir/*
do
  if [[ $file = *.*en-US.** ]]
  then
    cp -v $file $DROP_DIR/"pullPrintBootstrap__i18n__${file##*/}"
  fi
done

# pullprint resources
for file in $pullPrint_i18n_dir/*
do
  if [[ $file = *.*messages.properties ]]
    then
      cp -v $file $DROP_DIR/"pullPrint__resources__i18n__${file##*/}"
  fi
done

#pullprint mfp
for file in $pullPrint_oxpmfp_i18n_dir/*
do
  if [[ $file = *.*en-US.** ]]
    then
      cp -v $file $DROP_DIR/"pullPrint__oxpmfp__i18n__${file##*/}"
  fi
done

# proxyroot resources
for file in $proxyroot_home_i18n_dir/*
do
  if [[ $file = *.*messages.properties ]]
    then
      cp -v $file $DROP_DIR/"proxyroot__resources__i18n__${file##*/}"
  fi
done

#proxyroot mfp
for file in $proxyroot_oxpmfp_i18n_dir/*
do
  if [[ $file = *.*en-US.** ]]
    then
      cp -v $file $DROP_DIR/"proxyroot__oxpmfp__i18n__${file##*/}"
  fi
done

# scansend bootstrap

# i18n
for file in $scanSend_bootstrap_i18n_dir/*
do
  if [[ $file = *.*en-US.** ]]
  then
    cp -v $file $DROP_DIR/"scansendBootstrap__i18n__${file##*/}"
  fi
done

# Cloud Repo Proxy
for file in $scanSend_i18n_dir/*
do
  cp -v $file $DROP_DIR/"scanSendFW__LOCALE__${file##*/}"
done

# compress the drop folder
cd $DROP_DIR
zip -r ../$LOCALIZATION_FOLDER *

rm -rf $DROP_DIR
