-- MySQL dump 10.13  Distrib 5.5.42, for Linux (x86_64)
--
-- Host: pxc-titan-dev2.hpccp.net    Database: pullprint_stable
-- ------------------------------------------------------
-- Server version	5.6.20-68.0-56-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `schema_version_expansion`
--

LOCK TABLES `schema_version_expansion` WRITE;
/*!40000 ALTER TABLE `schema_version_expansion` DISABLE KEYS */;
INSERT INTO `schema_version_expansion` VALUES (1,1,'1','<< Flyway Init >>','INIT','<< Flyway Init >>',NULL,'migration_user','2014-11-20 18:13:58',0,1),(2,2,'201411030339','ConfigData add entityId settingType mobilePrintEnabled','JDBC','db.migration.expansion.pullprint.V201411030339__ConfigData_add_entityId_settingType_mobilePrintEnabled',NULL,'migration_user','2014-11-20 18:14:01',2753,1);
/*!40000 ALTER TABLE `schema_version_expansion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `schema_version_contraction`
--

LOCK TABLES `schema_version_contraction` WRITE;
/*!40000 ALTER TABLE `schema_version_contraction` DISABLE KEYS */;
INSERT INTO `schema_version_contraction` VALUES (1,1,'1','<< Flyway Init >>','INIT','<< Flyway Init >>',NULL,'migration_user','2014-11-26 18:15:14',0,1),(2,2,'201501070217','ConfigData add parent','JDBC','db.migration.contraction.pullprint.V201501070217__ConfigData_add_parent',NULL,'migrations','2015-02-12 23:11:41',253007,1);
/*!40000 ALTER TABLE `schema_version_contraction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `schema_version_git`
--

LOCK TABLES `schema_version_git` WRITE;
/*!40000 ALTER TABLE `schema_version_git` DISABLE KEYS */;
INSERT INTO `schema_version_git` VALUES (2,'868f50d','expansion'),(2,'cbd6412','expansion'),(1,'cbd6412','contraction'),(2,'1be254e','expansion'),(1,'1be254e','contraction'),(2,'e678f63','expansion'),(2,'07fb755','expansion'),(1,'07fb755','contraction'),(2,'50db990','expansion'),(1,'e72bd7b','contraction'),(2,'568882e','expansion'),(1,'568882e','contraction'),(2,'edd21b6','expansion'),(2,'de0d1b1','expansion'),(2,'4ebf7c2','expansion'),(2,'49a1ba3','contraction'),(2,'4faab4e','contraction'),(2,'38573eb','contraction'),(2,'74b6dc4','expansion'),(2,'74b6dc4','contraction'),(2,'cd46390','expansion'),(2,'cd46390','contraction'),(2,'0b85ba3','expansion'),(2,'0b85ba3','contraction'),(2,'1bbe81b','contraction'),(2,'2212bdc','expansion'),(2,'e37c075','expansion'),(2,'e37c075','contraction'),(2,'042bc37','contraction'),(2,'5e59cd8','expansion'),(2,'6b58f97','contraction'),(2,'77fd0eb','expansion'),(2,'77fd0eb','contraction'),(2,'b1c1675','expansion'),(2,'b1c1675','contraction'),(2,'b1c1675','contraction'),(2,'57744dc','expansion'),(2,'57744dc','contraction'),(2,'c4644b6','contraction'),(2,'0a6d83b','contraction');
/*!40000 ALTER TABLE `schema_version_git` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-24 21:28:20
