'use strict';

var fs = require('fs');

function processPair(grunt, filePair) {
  grunt.log.subhead('Combining SVGs: "' + filePair.orig.src + '" into "' + filePair.orig.dest + '"');

  // The output need to be valid XML
  // Start out with our basic document headers
  var output = '<?xml version="1.0" encoding="utf-8"?>\
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">\
    <svg version="1.1" id="hp-blah-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" preserveAspectRatio="xMidYMid" viewBox="0 0 25 25" width="100%" height="100%">';

  filePair.src.forEach(function (src) {
    grunt.log.write(' > Adding: ' + src.cyan + ' ... ');

    // Skip if not an SVG
    if (!src.match(/.*\.svg/)) {
      grunt.log.writeln('SKIPPED'.red.bold);
      grunt.fail.warn('File does not have the SVG extension');
    }

    // Determine the name of the icon
    var name = src.match(/\/([a-zA-Z0-9\-]+)\.svg/);
    if (!name) {
      grunt.log.writeln('FAILED'.red.bold);
      grunt.fail.fatal('File name is invalid (only alphanumeric and dashes permitted)');
    }
    name = name[1];

    // Pull out everything inside the <svg> tag
    var contents = fs.readFileSync(src).toString();
    var matches = contents.match(/<svg[\W\w]*">([\W\w]*)<\/svg>/);
    if (!matches) {
      grunt.log.writeln('FAILED'.red.bold);
      grunt.fail.fatal('Could not parse SVG');
    }
    contents = matches[1];

    // Wrap the contents with an ID and append them to our output document
    output += '<g id="hp-' + name + '-icon" fill="currentColor">' + contents + '</g>';

    grunt.log.ok();
  });

  // Add in the closing SVG tag
  output += '</svg>\n'; // Hooray for trailing newlines

  // Write the new SVG file back out
  var dir = filePair.dest.substring(0, filePair.dest.lastIndexOf('/'));
  if (!fs.existsSync(dir)) {
    grunt.log.write(' > Creating directory: ' + dir.cyan + ' ... ');
    fs.mkdirSync(dir, parseInt('0766', 8));
    grunt.log.ok();
  }

  grunt.log.write(' > Creating: ' + filePair.dest.cyan + ' ... ');
  fs.writeFileSync(filePair.dest, output);
  grunt.log.ok();
}

module.exports = function (grunt) {
  grunt.registerMultiTask('svgIcons', 'Combine SVG icons', function () {
    this.files.forEach(processPair.bind(this, grunt));
  });
};
