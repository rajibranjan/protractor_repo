'use strict';

describe('[Module: hp.portal.home.appManagement]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.appManagement'));

  describe('controller: AppManagementCtrl', function () {
    var $scope, $q, $timeout, AppManagementCtrl;
    var mockSelf = {
      accountId: 'mock-account-id'
    };
    var mockOffering = {
      fqName: 'mock-offering-fq-name'
    };
    var mockQueries = {
      ProductsAndUsersByOfferingAndAccount: {
        get: function (params) {
          // Capture the passed in params.
          this.params = params;
          var dfd = $q.defer();
          if (this.shouldResolve) {
            dfd.resolve(mockProductAndUsers);
          }
          else {
            dfd.reject();
          }
          return dfd.promise;
        }
      }
    };
    var mockProductAndUsers = {
      productAndUsers: ['mock-product-and-users']
    };
    var mockAddUserToProductModalResult = {
      addedUsers: 'added-users',
      removedUsers: 'removed-users'
    };
    var mockAddUserToProductModal = {
      open: function (params) {
        // Capture the passed in params.
        mockAddUserToProductModal.params = params;
        var dfd = $q.defer();
        dfd.resolve(mockAddUserToProductModalResult);
        return dfd.promise;
      }
    };

    var mockUpdateEntitlementsModal = {
      open: function (params) {
        // Capture the passed in params.
        mockUpdateEntitlementsModal.params = params;
        var dfd = $q.defer();
        dfd.resolve();
        return dfd.promise;
      }
    };

    var mockConfirmEntitlementChangesModal = {
      open: function (params) {
        // Capture the passed in params.
        mockConfirmEntitlementChangesModal.params = params;
        var dfd = $q.defer();
        dfd.resolve();
        return dfd.promise;
      }
    };

    beforeEach(inject(function ($rootScope, $controller, _$q_, _$timeout_) {
      $scope = $rootScope.$new();
      $q = _$q_;
      $timeout = _$timeout_;

      // Reset Mocks
      delete mockQueries.ProductsAndUsersByOfferingAndAccount.params;
      mockQueries.ProductsAndUsersByOfferingAndAccount.shouldResolve = true;
      delete mockAddUserToProductModal.params;
      delete mockUpdateEntitlementsModal.params;

      // Load the controller
      AppManagementCtrl = $controller('AppManagementCtrl', {
        $scope: $scope,
        Self: mockSelf,
        Queries: mockQueries,
        AddUserToProductModal: mockAddUserToProductModal,
        UpdateEntitlementsModal: mockUpdateEntitlementsModal,
        ConfirmEntitlementChangesModal: mockConfirmEntitlementChangesModal
      });
    }));

    beforeEach(function () {
      spyOn(mockAddUserToProductModal, 'open').and.callThrough();
      spyOn(mockUpdateEntitlementsModal, 'open').and.callThrough();
      spyOn(mockConfirmEntitlementChangesModal, 'open').and.callThrough();
      spyOn($scope, 'fetchProductsByOffering').and.callThrough();
    });

    it('should set all of the necessary $scope fields', function () {
      expect($scope.fetchProductsByOffering).toBeDefined();
      expect($scope.manageUsers).toBeFunction();
      expect($scope.removeAccess).toBeFunction();
    });

    describe('$scope.fetchProductsByOffering(offering)', function () {
      it('should query ProductsAndUsersByOfferingAndAccount with the expected params', function () {
        expect($scope.productAndUsers).toBeUndefined();
        expect(mockQueries.ProductsAndUsersByOfferingAndAccount.params).toBeUndefined();

        $scope.fetchProductsByOffering(mockOffering);
        $timeout.flush();

        expect($scope.fetchProductsByOffering).toHaveBeenCalled();
        expect($scope.productAndUsers).toBe(mockProductAndUsers.productAndUsers);
        expect(mockQueries.ProductsAndUsersByOfferingAndAccount.params).toEqual({
          accountId: mockSelf.accountId,
          offeringFqName: mockOffering.fqName
        });
      });

      it('should remove $scope.productAndUsers if the query fails', function () {
        expect($scope.productAndUsers).toBeUndefined();

        $scope.fetchProductsByOffering(mockOffering);
        $timeout.flush();

        expect($scope.fetchProductsByOffering).toHaveBeenCalled();
        expect($scope.productAndUsers).toBe(mockProductAndUsers.productAndUsers);

        mockQueries.ProductsAndUsersByOfferingAndAccount.shouldResolve = false;
        $scope.fetchProductsByOffering(mockOffering);
        $timeout.flush();

        expect($scope.productAndUsers).toBeUndefined();
      });
    });

    describe('$scope.manageUsers(productItem)', function () {
      it('should open the AddUserToProductModal with the correct params', function () {
        expect(mockAddUserToProductModal.params).toBeUndefined();

        $scope.selectedOffering = {
          fqName: 'selected-offering'
        };
        var productItem = {
          license: 'product-item-license'
        };

        $scope.manageUsers(productItem);

        expect(mockAddUserToProductModal.open).toHaveBeenCalled();
        expect(mockAddUserToProductModal.params).toEqual({
          selectedOffering: $scope.selectedOffering,
          productItem: productItem
        });
      });

      it('should chain the result of the AddUserToProductModal to the UpdateEntitlementsModal', function () {
        expect(mockUpdateEntitlementsModal.params).toBeUndefined();

        $scope.selectedOffering = {
          fqName: 'selected-offering'
        };
        var productItem = {
          license: 'product-item-license'
        };

        $scope.manageUsers(productItem);

        expect(mockUpdateEntitlementsModal.params).toBeUndefined();
        $timeout.flush();
        expect(mockUpdateEntitlementsModal.open).toHaveBeenCalled();
        var expectedParams = angular.extend({
          productItem: productItem
        }, mockAddUserToProductModalResult);
        expect(mockUpdateEntitlementsModal.params).toEqual(expectedParams);
      });

      it('should fetchProductsByOffering after updating users\' entitlements', function () {
        expect($scope.productAndUsers).toBeUndefined();
        expect(mockQueries.ProductsAndUsersByOfferingAndAccount.params).toBeUndefined();

        $scope.selectedOffering = {
          fqName: 'selected-offering'
        };
        var productItem = {
          license: 'product-item-license'
        };

        $scope.manageUsers(productItem);
        expect(mockAddUserToProductModal.open).toHaveBeenCalled();
        $timeout.flush();
        expect(mockUpdateEntitlementsModal.open).toHaveBeenCalled();

        expect($scope.fetchProductsByOffering).toHaveBeenCalled();
        expect($scope.productAndUsers).toBe(mockProductAndUsers.productAndUsers);
        expect(mockQueries.ProductsAndUsersByOfferingAndAccount.params).toEqual({
          accountId: mockSelf.accountId,
          offeringFqName: $scope.selectedOffering.fqName
        });
      });
    });

    describe('$scope.removeAccess(productItem, userItem)', function () {
      it('should open the ConfirmEntitlementChangesModal with the correct params', function () {
        expect(mockConfirmEntitlementChangesModal.params).toBeUndefined();

        var userItem = 'user-item';
        var productItem = {
          license: 'product-item-license'
        };

        $scope.removeAccess(productItem, userItem);

        expect(mockConfirmEntitlementChangesModal.open).toHaveBeenCalled();
        expect(mockConfirmEntitlementChangesModal.params).toEqual({
          removedUsers: [userItem]
        });
      });

      it('should chain the result of the ConfirmEntitlementChangesModal to the UpdateEntitlementsModal', function () {
        var userItem = 'user-item';
        var productItem = {
          license: 'product-item-license'
        };

        $scope.removeAccess(productItem, userItem);

        expect(mockUpdateEntitlementsModal.params).toBeUndefined();
        $timeout.flush();
        expect(mockUpdateEntitlementsModal.open).toHaveBeenCalled();
        var expectedParams = {
          removedUsers: [userItem],
          productItem: productItem
        };
        expect(mockUpdateEntitlementsModal.params).toEqual(expectedParams);
      });

      it('should fetchProductsByOffering after updating users\' entitlements', function () {
        expect($scope.productAndUsers).toBeUndefined();
        expect(mockQueries.ProductsAndUsersByOfferingAndAccount.params).toBeUndefined();

        $scope.selectedOffering = {
          fqName: 'selected-offering'
        };
        var userItem = 'user-item';
        var productItem = {
          license: 'product-item-license'
        };

        $scope.removeAccess(productItem, userItem);

        expect(mockConfirmEntitlementChangesModal.open).toHaveBeenCalled();
        $timeout.flush();
        expect(mockUpdateEntitlementsModal.open).toHaveBeenCalled();

        expect($scope.fetchProductsByOffering).toHaveBeenCalled();
        expect($scope.productAndUsers).toBe(mockProductAndUsers.productAndUsers);
        expect(mockQueries.ProductsAndUsersByOfferingAndAccount.params).toEqual({
          accountId: mockSelf.accountId,
          offeringFqName: $scope.selectedOffering.fqName
        });
      });
    });

  });

  describe('directive: licensedAppSelector', function () {
    var selfAccountId = 'self-account-id';
    beforeEach(module(function ($provide) {
      $provide.constant('Self', {
        accountId: selfAccountId
      });
      $provide.constant('i18n', {
        getBySource: function (source) {
          return {
            source: source
          };
        }
      });
      $provide.constant('LicenseService', {
        get: function (params) {
          var dfd = $q.defer();
          mockLicensesResponse.params = params;
          $timeout(function () {
            dfd.resolve(mockLicensesResponse);
          });
          return dfd.promise;
        }
      });
    }));

    var mockLicensesResponse = {
      data: [{
        offering: {
          fqName: 'mock-offering-fq-name'
        }
      }, {
        offering: {
          fqName: 'mock-offering-fq-name'
        }
      }]
    };

    var html = '<licensed-app-selector selected-model="selectedOffering" on-select="fetchProductsByOffering"></licensed-app-selector>';

    var $q, $timeout, element, $scope, elementScope;
    beforeEach(inject(function ($rootScope, $compile, _$q_, _$timeout_) {
      delete mockLicensesResponse.params;
      $q = _$q_;
      $timeout = _$timeout_;
      $scope = $rootScope.$new();
      element = $compile(html)($scope);
      $scope.$digest();
      elementScope = element.find('select').scope();
    }));

    it('should get licenses with the expected params', function () {
      expect(mockLicensesResponse.params).toEqual({
        qs: 'account.id==' + selfAccountId + '&&status==ACTIVE',
        expand: 'offering'
      });
    });

    it('should set a list of offerings on the scope based on licenses response', function () {
      expect(elementScope.offerings).toBeUndefined();
      $timeout.flush();

      var expectedOfferings = [];
      expectedOfferings[0] = mockLicensesResponse.data[0].offering;
      expect(elementScope.offerings).toEqual(expectedOfferings);
    });

    it('should fetch i18n for each offering fqName', function () {
      expect(elementScope.i18n).toEqual({});
      $timeout.flush();

      var expectedI18n = {};
      mockLicensesResponse.data.forEach(function (license) {
        expectedI18n[license.offering.fqName] = {
          source: license.offering.fqName
        };
      });
      expect(elementScope.i18n).toEqual(expectedI18n);
    });

  });
});
