'use strict';

describe('[Module: hp.portal.home.appManagement.updateEntitlementsModal]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.appManagement.updateEntitlementsModal'));

  describe('Factory: UpdateEntitlementsModal', function () {
    beforeEach(module(function ($provide) {
      $provide.constant('$uibModal', {
        open: function (params) {
          return {
            result: {
              params: params
            }
          };
        }
      });
    }));

    var UpdateEntitlementsModal;
    beforeEach(inject(function (_UpdateEntitlementsModal_) {
      UpdateEntitlementsModal = _UpdateEntitlementsModal_;
    }));

    describe('.open', function () {
      it('should open the $uibModal with the correct params', function () {
        var data = 'UpdateEntitlementsModal-data';

        var result = UpdateEntitlementsModal.open(data);

        expect(result).toBeDefined();
        expect(result.params).toBeDefined();
        var params = result.params;
        expect(params.resolve).toBeDefined();
        var resolve = params.resolve;
        expect(resolve.data).toBe(data);

        delete params.resolve;
        expect(params).toEqual({
          templateUrl: 'app/home/app-management/update-entitlements-modal.html',
          controller: 'UpdateEntitlementsModalCtrl',
          backdrop: 'static',
          size: 'lg',
          keyboard: false
        });
      });
    });
  });

  describe('Controller: UpdateEntitlementsModalCtrl', function () {
    var mockUibModalInstance = {
      dismiss: function () {
        this.dismissed = true;
      },
      close: function (result) {
        this.closed = true;
        this.result = result;
      }
    };

    var mockEntitlementService = {
      create: function () {
        var newEntitlement = {
          save: function () {
            var dfd = $q.defer();

            newEntitlement.saveSuccess = function () {
              dfd.resolve();
              $timeout.flush();
            };

            newEntitlement.saveFail = function (statusCode) {
              dfd.reject({
                status: statusCode
              });
              $timeout.flush();
            };

            return dfd.promise;
          }
        };
        mockEntitlementService.newEntitlements.push(newEntitlement);
        return newEntitlement;
      },
      fromExisting: function (existing) {
        var deletedEntitlement = {
          existingEntitlement: existing,
          delete: function () {
            var dfd = $q.defer();

            deletedEntitlement.deleteSuccess = function () {
              dfd.resolve();
              $timeout.flush();
            };

            deletedEntitlement.deleteFail = function () {
              dfd.reject();
              $timeout.flush();
            };

            return dfd.promise;
          }
        };
        mockEntitlementService.deletedEntitlements.push(deletedEntitlement);
        return deletedEntitlement;
      }
    };

    var mockLicenseId = 'mock-license-id';
    var mockData = {
      addedUsers: [],
      removedUsers: [],
      productItem: {
        license: {
          links: [{
            rel: 'self',
            href: mockLicenseId
          }]
        }
      }
    };

    function UserToAdd(id) {
      this.links = [{
        rel: 'self',
        href: '/api/v2/users/' + id
      }];
    }

    function generateUsersToAdd(count) {
      for (var i = 0; i < count; i++) {
        mockData.addedUsers.push(new UserToAdd(i));
      }
      return count;
    }

    function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    }

    var mockEntitlement = 'mock-entitlement-';

    function UserToRemove(userIndex, numEntitlements) {
      this.user = 'mock-user-to-remove-' + userIndex;
      this.entitlements = [];
      for (var i = 0; i < numEntitlements; i++) {
        this.entitlements.push(mockEntitlement + i);
      }
    }

    function generateUsersToRemove(count, expectedSequence) {
      var numEntitlementsToDelete = 0;
      for (var i = 0; i < count; i++) {
        var numEntitlements = getRandomInt(1, i + 1);
        mockData.removedUsers.push(new UserToRemove(i, numEntitlements));
        numEntitlementsToDelete += numEntitlements;
        if (expectedSequence) {
          for (var s = 0; s < numEntitlements; s++) {
            expectedSequence.push(s);
          }
        }
      }
      return numEntitlementsToDelete;
    }

    var $scope, $controller, $timeout, $q, UpdateEntitlementsModalCtrl, LinkParser;
    beforeEach(inject(function ($rootScope, _$controller_, _$timeout_, _$q_, _LinkParser_) {
      $scope = $rootScope.$new();
      $controller = _$controller_;
      $timeout = _$timeout_;
      $q = _$q_;
      LinkParser = _LinkParser_;

      // Reset Mocks
      mockUibModalInstance.dismissed = false;
      mockUibModalInstance.closed = false;
      delete mockUibModalInstance.result;
      mockEntitlementService.newEntitlements = [];
      mockEntitlementService.deletedEntitlements = [];
      mockData.addedUsers = [];
      mockData.removedUsers = [];
    }));

    function initController() {
      UpdateEntitlementsModalCtrl = $controller('UpdateEntitlementsModalCtrl', {
        $scope: $scope,
        $uibModalInstance: mockUibModalInstance,
        EntitlementService: mockEntitlementService,
        data: mockData
      });
    }

    describe('initialization', function () {
      it('should set all of the necessary $scope fields', function () {
        generateUsersToAdd(1);

        initController();
        expect($scope.percentComplete).toBe(0);
        expect($scope.close).toBeFunction();
        expect($scope.productItem).toBe(mockData.productItem);
        expect($scope.addUserErrors).toEqual({});
        expect($scope.completedOperations).toEqual({
          removedUsers: [],
          addedUsers: {
            success: [],
            failed: []
          }
        });
      });

      it('should just close the modal if there are no added or removed users', function () {
        delete mockData.removedUsers;
        initController();
        expect(mockUibModalInstance.closed).toBe(true);
      });
    });

    describe('$scope.close()', function () {
      it('should close the modal dialog', function () {
        generateUsersToAdd(1);
        initController();

        expect(mockUibModalInstance.closed).toBe(false);
        $scope.close();
        expect(mockUibModalInstance.closed).toBe(true);
      });
    });

    describe('adding user entitlements', function () {
      it('should create a new entitlement with user and license links for each addedUser', function () {
        var numUsersToAdd = generateUsersToAdd(getRandomInt(10, 100));

        expect(mockEntitlementService.newEntitlements.length).toBe(0);

        initController();
        $timeout.flush();

        expect(mockEntitlementService.newEntitlements.length).toBe(numUsersToAdd);

        mockEntitlementService.newEntitlements.forEach(function (newEntitlement, index) {
          var userLinkId = LinkParser.getLinkId(newEntitlement, 'user');
          expect(userLinkId).toBe(index.toString());
          var licenseLinkId = LinkParser.getLinkId(newEntitlement, 'license');
          expect(licenseLinkId).toBe(mockLicenseId);
        });
      });

      it('should set $scope.completedOperations.addedUsers.success when each addedUser\'s entitlement is saved successfully', function () {
        var numUsersToAdd = generateUsersToAdd(getRandomInt(10, 100));

        expect(mockEntitlementService.newEntitlements.length).toBe(0);

        initController();
        $timeout.flush();

        expect(mockEntitlementService.newEntitlements.length).toBe(numUsersToAdd);

        mockEntitlementService.newEntitlements.forEach(function (newEntitlement, index) {
          expect($scope.completedOperations.addedUsers.success.length).toBe(index);
          newEntitlement.saveSuccess();
          expect($scope.completedOperations.addedUsers.success.length).toBe(index + 1);
          expect($scope.completedOperations.addedUsers.success[index]).toBe(mockData.addedUsers[index]);
        });
      });

      it('should set $scope.completedOperations.addedUsers.failed when addedUser\'s entitlement fail to be saved', function () {
        var numUsersToAdd = generateUsersToAdd(getRandomInt(10, 100));

        expect(mockEntitlementService.newEntitlements.length).toBe(0);

        initController();
        $timeout.flush();

        expect(mockEntitlementService.newEntitlements.length).toBe(numUsersToAdd);

        mockEntitlementService.newEntitlements.forEach(function (newEntitlement, index) {
          expect($scope.completedOperations.addedUsers.failed.length).toBe(index);
          newEntitlement.saveFail();
          expect($scope.completedOperations.addedUsers.failed.length).toBe(index + 1);
          expect($scope.completedOperations.addedUsers.failed[index]).toBe(mockData.addedUsers[index]);
        });
      });

      it('should update the percentComplete when each addedUser\'s entitlement is saved successfully', function () {
        var numUsersToAdd = generateUsersToAdd(getRandomInt(10, 100));

        expect(mockEntitlementService.newEntitlements.length).toBe(0);

        initController();
        $timeout.flush();

        expect(mockEntitlementService.newEntitlements.length).toBe(numUsersToAdd);

        mockEntitlementService.newEntitlements.forEach(function (newEntitlement, index) {
          expect($scope.percentComplete).toBe(index / numUsersToAdd * 100);
          newEntitlement.saveSuccess();
          expect($scope.percentComplete).toBe((index + 1) / numUsersToAdd * 100);
        });
      });

      it('should update the percentComplete when some addedUser\'s entitlements fail to be saved', function () {
        var numUsersToAdd = generateUsersToAdd(getRandomInt(10, 100));

        expect(mockEntitlementService.newEntitlements.length).toBe(0);

        initController();
        $timeout.flush();

        expect(mockEntitlementService.newEntitlements.length).toBe(numUsersToAdd);

        mockEntitlementService.newEntitlements.forEach(function (newEntitlement, index) {
          expect($scope.percentComplete).toBe(index / numUsersToAdd * 100);
          newEntitlement.saveFail(409);
          expect($scope.percentComplete).toBe((index + 1) / numUsersToAdd * 100);
        });
      });

      it('should set addUserErrors when some entitlements fail to be saved', function () {
        var numUsersToAdd = generateUsersToAdd(2);

        expect(mockEntitlementService.newEntitlements.length).toBe(0);

        initController();
        $timeout.flush();

        expect(mockEntitlementService.newEntitlements.length).toBe(numUsersToAdd);

        expect($scope.addUserErrors.cMaxEntitlementsReached).toBeUndefined();
        expect($scope.addUserErrors.cSaveFailed).toBeUndefined();

        // Cause the first entitlement save to fail for non-409
        mockEntitlementService.newEntitlements[0].saveFail(400);
        expect($scope.addUserErrors.cSaveFailed).toBe(true);

        // Cause the first entitlement save to fail for 409
        mockEntitlementService.newEntitlements[1].saveFail(409);
        expect($scope.addUserErrors.cMaxEntitlementsReached).toBe(true);
      });
    });

    describe('removing user entitlements', function () {
      it('should delete all entitlements for each removedUser', function () {
        var numUsersToRemove = getRandomInt(10, 100);
        var expectedSequence = [];
        var numEntitlementsToDelete = generateUsersToRemove(numUsersToRemove, expectedSequence);

        expect(mockEntitlementService.deletedEntitlements.length).toBe(0);

        initController();

        expect(mockEntitlementService.deletedEntitlements.length).toBe(numEntitlementsToDelete);

        mockEntitlementService.deletedEntitlements.forEach(function (deletedEntitlement, index) {
          expect(deletedEntitlement.existingEntitlement).toBe(mockEntitlement + expectedSequence[index]);
        });
      });

      it('should set $scope.completedOperations.removedUsers when each removedUser\'s entitlement is deleted successfully', function () {
        var numUsersToRemove = getRandomInt(10, 100);
        var expectedSequence = [];
        var numEntitlementsToDelete = generateUsersToRemove(numUsersToRemove, expectedSequence);

        expect(mockEntitlementService.deletedEntitlements.length).toBe(0);

        initController();

        expect(mockEntitlementService.deletedEntitlements.length).toBe(numEntitlementsToDelete);

        mockEntitlementService.deletedEntitlements.forEach(function (deletedEntitlement) {
          deletedEntitlement.deleteSuccess();
        });
        expect($scope.completedOperations.removedUsers.length).toBe(numUsersToRemove);

        $scope.completedOperations.removedUsers.forEach(function (removedUser, index) {
          expect(removedUser).toBe(mockData.removedUsers[index].user);
        });
      });

      it('should NOT set $scope.completedOperations.removedUsers when each removedUser\'s entitlement fail to be deleted', function () {
        var numUsersToRemove = getRandomInt(10, 100);
        var expectedSequence = [];
        var numEntitlementsToDelete = generateUsersToRemove(numUsersToRemove, expectedSequence);

        expect(mockEntitlementService.deletedEntitlements.length).toBe(0);

        initController();

        expect(mockEntitlementService.deletedEntitlements.length).toBe(numEntitlementsToDelete);

        mockEntitlementService.deletedEntitlements.forEach(function (deletedEntitlement) {
          deletedEntitlement.deleteFail();
        });
        expect($scope.completedOperations.removedUsers.length).toBe(0);
      });

      it('should update the percentComplete when each removedUser\'s entitlement is deleted successfully', function () {
        var numUsersToRemove = getRandomInt(10, 100);
        var numEntitlementsToDelete = generateUsersToRemove(numUsersToRemove);

        expect(mockEntitlementService.deletedEntitlements.length).toBe(0);

        initController();

        expect(mockEntitlementService.deletedEntitlements.length).toBe(numEntitlementsToDelete);

        mockEntitlementService.deletedEntitlements.forEach(function (deletedEntitlement, index) {
          expect($scope.percentComplete).toBe(index / numEntitlementsToDelete * 100);
          deletedEntitlement.deleteSuccess();
          expect($scope.percentComplete).toBe((index + 1) / numEntitlementsToDelete * 100);
        });
      });

      it('should update the percentComplete when some removedUser\'s entitlements fail to be deleted', function () {
        var numUsersToRemove = getRandomInt(10, 100);
        var numEntitlementsToDelete = generateUsersToRemove(numUsersToRemove);

        expect(mockEntitlementService.deletedEntitlements.length).toBe(0);

        initController();

        expect(mockEntitlementService.deletedEntitlements.length).toBe(numEntitlementsToDelete);

        mockEntitlementService.deletedEntitlements.forEach(function (deletedEntitlement, index) {
          expect($scope.percentComplete).toBe(index / numEntitlementsToDelete * 100);
          deletedEntitlement.deleteFail();
          expect($scope.percentComplete).toBe((index + 1) / numEntitlementsToDelete * 100);
        });
      });
    });

    describe('adding and removing users together', function () {
      it('should create entitlements for added users and delete entitlements for removed users', function () {
        var numUsersToAdd = generateUsersToAdd(getRandomInt(10, 100));
        var numUsersToRemove = getRandomInt(10, 100);
        var expectedSequence = [];
        var numEntitlementsToDelete = generateUsersToRemove(numUsersToRemove, expectedSequence);

        expect(mockEntitlementService.deletedEntitlements.length).toBe(0);
        expect(mockEntitlementService.newEntitlements.length).toBe(0);

        initController();

        expect(mockEntitlementService.deletedEntitlements.length).toBe(numEntitlementsToDelete);

        // newEntitlements are still empty until all deleted entitlements are done.
        expect(mockEntitlementService.newEntitlements.length).toBe(0);

        mockEntitlementService.deletedEntitlements.forEach(function (deletedEntitlement, index) {
          deletedEntitlement.deleteSuccess();
          expect(deletedEntitlement.existingEntitlement).toBe(mockEntitlement + expectedSequence[index]);
        });

        expect(mockEntitlementService.newEntitlements.length).toBe(numUsersToAdd);

        mockEntitlementService.newEntitlements.forEach(function (newEntitlement, index) {
          var userLinkId = LinkParser.getLinkId(newEntitlement, 'user');
          expect(userLinkId).toBe(index.toString());
          var licenseLinkId = LinkParser.getLinkId(newEntitlement, 'license');
          expect(licenseLinkId).toBe(mockLicenseId);
        });
      });

      it('should update the percentComplete when each added or removed user\'s entitlement is updated', function () {
        var numUsersToAdd = generateUsersToAdd(getRandomInt(10, 10));
        var numUsersToRemove = getRandomInt(10, 10);
        var numEntitlementsToDelete = generateUsersToRemove(numUsersToRemove);

        expect(mockEntitlementService.newEntitlements.length).toBe(0);
        expect(mockEntitlementService.deletedEntitlements.length).toBe(0);

        initController();

        var totalOperations = numUsersToAdd + numEntitlementsToDelete;

        function expectPercentCompleteToUpdatePerOperation(operations, offset) {
          offset = offset || 0;
          operations.forEach(function (operation, index) {
            expect($scope.percentComplete).toBe((index + offset) / totalOperations * 100);
            operation();
            expect($scope.percentComplete).toBe((index + offset + 1) / totalOperations * 100);
          });
        }

        // Resolve all of the 'delete' oparations.
        var operations = [];
        mockEntitlementService.deletedEntitlements.forEach(function (deletedEntitlement) {
          operations.push(deletedEntitlement.deleteSuccess);
        });
        expectPercentCompleteToUpdatePerOperation(operations);

        // Resolve all of the 'add' operations.
        operations = [];
        mockEntitlementService.newEntitlements.forEach(function (newEntitlement) {
          operations.push(newEntitlement.saveSuccess);
        });
        expectPercentCompleteToUpdatePerOperation(operations, numEntitlementsToDelete);
      });
    });
  });
});
