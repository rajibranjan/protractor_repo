'use strict';

describe('[Module: hp.portal.home.error]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.error'));

  describe('PermissionDeniedCtrl', function () {
    var scope, PermissionDeniedCtrl;
    var testTitle = 'cPermissionDeniedTitle';
    var testMsg = 'cPermissionDeniedMessage';

    beforeEach(inject(function ($rootScope, $controller) {
      scope = $rootScope.$new();
      PermissionDeniedCtrl = $controller('PermissionDeniedCtrl', {
        $scope: scope
      });
    }));

    it('should display permissionDenied title and message', function () {
      expect(scope.pageError.title).toBe(testTitle);
      expect(scope.pageError.msg).toBe(testMsg);
    });
  });

  describe('InternalErrorCtrl', function () {
    var scope, InternalErrorCtrl;
    var testTitle = 'cInternalErrorTitle';
    var testMsg = 'cInternalErrorMessage';

    beforeEach(inject(function ($rootScope, $controller) {
      scope = $rootScope.$new();
      InternalErrorCtrl = $controller('InternalErrorCtrl', {
        $scope: scope
      });
    }));

    it('should display Internal Error title and message', function () {
      expect(scope.pageError.title).toBe(testTitle);
      expect(scope.pageError.msg).toBe(testMsg);
    });
  });
});
