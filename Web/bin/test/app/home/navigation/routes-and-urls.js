'use strict';

describe('[Module: hp.portal.home.navigation.routesAndUrls]', function () {

  beforeEach(module('unitTestInit'));

  var mockSelf = {
    $promise: null,
    roles: null,
    links: [{
      rel: 'account',
      href: 'https://127.0.0.1/api/v2/resellers/25020aab-ff98-4cb0-90dd-15361af903cb'
    }]
  };

  var mockPermissions = {
    isHPAdmin: false,
    isReseller: false,
    isCompanyAdmin: false,
    isUser: false
  };

  beforeEach(module(function ($provide) {
    $provide.constant('Self', mockSelf);
    $provide.constant('Globals', {
      permissions: mockPermissions
    });
  }));

  beforeEach(module('hp.portal.home.navigation.routesAndUrls'));

  var rootScope, q, timeout;
  beforeEach(inject(function ($rootScope, $q, $timeout) {
    // Reset the roles on every test run.
    mockPermissions.isHPAdmin = false;
    mockPermissions.isReseller = false;
    mockPermissions.isCompanyAdmin = false;
    mockPermissions.isUser = false;

    rootScope = $rootScope;
    q = $q;
    timeout = $timeout;

    mockSelf.$promise = q.resolve(mockSelf);
  }));

  describe('Factory: RoutePermissions', function () {

    var RoutePermissionsFactory, RoutesConstant;

    beforeEach(inject(function (RoutePermissions, Routes) {
      RoutePermissionsFactory = RoutePermissions;
      RoutesConstant = Routes;
    }));

    function containsDefaultRoutes(availableRoutes) {
      expect(availableRoutes).toContain(RoutesConstant.dashboard);
      expect(availableRoutes).toContain(RoutesConstant.help);
      expect(availableRoutes).toContain(RoutesConstant.devices);
      expect(availableRoutes).toContain(RoutesConstant.utilities);
      expect(availableRoutes).toContain(RoutesConstant.welcome);
      expect(availableRoutes).toContain(RoutesConstant.permissionDenied);
      expect(availableRoutes).toContain(RoutesConstant.internalError);
    }

    function containsUserRoutes(availableRoutes) {
      expect(availableRoutes).toContain(RoutesConstant.profile);
    }

    function containsCompanyAdminRoutes(availableRoutes) {
      expect(availableRoutes).toContain(RoutesConstant.appCatalog);
      expect(availableRoutes).toContain(RoutesConstant.appManagement);
      containsCompanyAdminResellerRoutes(availableRoutes);
    }

    function containsCompanyAdminResellerRoutes(availableRoutes) {
      expect(availableRoutes).toContain(RoutesConstant.myCompany);
      expect(availableRoutes).toContain(RoutesConstant.accountUsers);
      expect(availableRoutes).toContain(RoutesConstant.addUser);
      expect(availableRoutes).toContain(RoutesConstant.userDetails);
      expect(availableRoutes).toContain(RoutesConstant.csvImport);
    }

    function containsResellerRoutes(availableRoutes) {
      expect(availableRoutes).toContain(RoutesConstant.resellerAccounts);
      expect(availableRoutes).toContain(RoutesConstant.resellerDetails);
      expect(availableRoutes).toContain(RoutesConstant.addAccount);
      expect(availableRoutes).toContain(RoutesConstant.accountDetails);
    }

    it('should contain all routes for HPAdmin', function () {
      mockPermissions.isHPAdmin = true;
      mockPermissions.isReseller = true;
      mockPermissions.isCompanyAdmin = true;
      mockPermissions.isUser = true;
      rootScope.$apply();
      RoutePermissionsFactory.then(function (availableRoutes) {
        expect(availableRoutes.length).toBe(30);

        containsDefaultRoutes(availableRoutes);
        containsUserRoutes(availableRoutes);
        containsCompanyAdminRoutes(availableRoutes);
        containsResellerRoutes(availableRoutes);
      });
      timeout.flush();
    });

    it('should only contain reseller specific routes', function () {
      mockPermissions.isReseller = true;

      rootScope.$apply();
      RoutePermissionsFactory.then(function (availableRoutes) {
        expect(availableRoutes.length).toBe(13);

        containsDefaultRoutes(availableRoutes);
        containsResellerRoutes(availableRoutes);
      });
      timeout.flush();
    });

    it('should contain all reseller routes, including company adming and user routes that resellers can access', function () {
      mockPermissions.isUser = true;
      mockPermissions.isCompanyAdmin = true;
      mockPermissions.isReseller = true;

      rootScope.$apply();
      RoutePermissionsFactory.then(function (availableRoutes) {
        expect(availableRoutes.length).toBe(19);

        containsDefaultRoutes(availableRoutes);
        containsUserRoutes(availableRoutes);
        containsCompanyAdminResellerRoutes(availableRoutes);
        containsResellerRoutes(availableRoutes);
      });
      timeout.flush();
    });

    it('should only contain company admin specific routes', function () {
      mockPermissions.isCompanyAdmin = true;

      rootScope.$apply();
      RoutePermissionsFactory.then(function (availableRoutes) {
        expect(availableRoutes.length).toBe(15);

        containsDefaultRoutes(availableRoutes);
        containsCompanyAdminRoutes(availableRoutes);
      });
      timeout.flush();
    });

    it('should contain all company admin routes, including user routes', function () {
      mockPermissions.isUser = true;
      mockPermissions.isCompanyAdmin = true;

      rootScope.$apply();
      RoutePermissionsFactory.then(function (availableRoutes) {
        expect(availableRoutes.length).toBe(16);

        containsDefaultRoutes(availableRoutes);
        containsUserRoutes(availableRoutes);
        containsCompanyAdminRoutes(availableRoutes);
      });
      timeout.flush();
    });

    it('should contain only the user accessible routes', function () {
      mockPermissions.isUser = true;

      rootScope.$apply();
      RoutePermissionsFactory.then(function (availableRoutes) {
        expect(availableRoutes.length).toBe(8);

        containsDefaultRoutes(availableRoutes);
        containsUserRoutes(availableRoutes);
      });
      timeout.flush();
    });

  });

  describe('Factory: Urls', function () {

    var UrlsFactory;

    beforeEach(inject(function (Urls) {
      UrlsFactory = Urls;
    }));

    it('should contain all routes', function () {
      mockPermissions.isHPAdmin = true;
      mockPermissions.isReseller = true;
      mockPermissions.isCompanyAdmin = true;
      mockPermissions.isUser = true;
      expect(UrlsFactory.manageUsers()).toBe('/users');
      expect(UrlsFactory.manageAccounts()).toBe('/accounts');
      expect(UrlsFactory.manageResellers()).toBe('/resellers');
    });

    it('should contain only the default routes', function () {
      mockPermissions.isUser = true;
      expect(UrlsFactory.manageUsers()).toBe('/accounts/25020aab-ff98-4cb0-90dd-15361af903cb/users');
      expect(UrlsFactory.manageAccounts()).toBe('/resellers/25020aab-ff98-4cb0-90dd-15361af903cb/accounts');
    });

  });

});
