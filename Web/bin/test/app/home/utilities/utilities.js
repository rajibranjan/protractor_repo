'use strict';

describe('[Module: hp.portal.home.utilities]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.utilities'));

  describe('controller:UtilitiesCtrl', function () {

    var scope, utilitiesCtrl, controller;

    var mockDownloadService = {
      getPager: function (opts) {
        mockDownloadsPager.opts = opts;
        return mockDownloadsPager;
      }
    };

    var allOfferings = 'allOfferings';
    var mockOfferingService = {
      getAllOfferings: function () {
        return allOfferings;
      }
    };

    var mockDownloadsPager = {
      getItemsPerPage: function () {
        return this.opts.itemsPerPage;
      },
      loadPage: function () {}
    };

    var initController = function () {
      utilitiesCtrl = controller('UtilitiesCtrl', {
        $scope: scope,
        OfferingService: mockOfferingService,
        DownloadService: mockDownloadService
      });
    };

    beforeEach(inject(function ($rootScope, $controller) {
      scope = $rootScope.$new();
      controller = $controller;
    }));

    it('should set expected items on the scope', function () {
      initController();
      scope.$apply();
      expect(scope.offerings).toBe(allOfferings);
      expect(scope.downloads).toBe(mockDownloadsPager);
      expect(mockDownloadsPager.getItemsPerPage()).toBe(10);
    });
  });
});
