'use strict';

describe('[Module: hp.portal.home.users.addUser]', function () {

  var alerts = [];
  var shouldFail = false;
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.users.addUser'));

  var httpBackend, q, timeout, linkParser;
  beforeEach(inject(function ($httpBackend, $q, $timeout, LinkParser) {
    httpBackend = $httpBackend;
    q = $q;
    timeout = $timeout;
    linkParser = LinkParser;
  }));

  var mockUserService = {
    create: function () {
      var deferred = q.defer();
      timeout(function () {
        if (shouldFail) {
          var response = {
            status: 'Test Response: Failure Adding User'
          };
          deferred.reject(response);
        }
        else {
          deferred.resolve();
        }
      }, 100);
      return deferred.promise;
    }
  };

  var pageAlerts = {
    add: function (alert) {
      return alerts.push(alert);
    },
    timeout: {
      short: 3000,
      medium: 5000,
      long: 10000
    }
  };

  var mockSelf = {
    account: {
      country: 'US'
    },
    locale: 'en-US',
    getLocale: function () {
      return mockSelf.locale;
    },
    links: [{
      rel: 'account',
      href: 'https://localhost:9000/api/v2/accounts/d8c0d964-43c8-4047-9f88-f8eec2c970e3'
    }]
  };

  var testUrls = {
    manageUsers: function () {
      return '/manageUsersUrl';
    }
  };

  describe('AddUserCtrl', function () {

    var scope, AddUserCtrl, location, emitFunctionRan;


    beforeEach(inject(function ($rootScope, $controller, $location) {
      scope = $rootScope.$new();
      alerts = [];
      emitFunctionRan = false;
      shouldFail = false;
      location = $location;
      AddUserCtrl = $controller('AddUserCtrl', {
        $scope: scope,
        UserService: mockUserService,
        PageAlerts: pageAlerts,
        Urls: testUrls,
        Self: mockSelf
      });
      scope.user.firstName = 'testFirst';
      scope.user.lastName = 'testLast';

      //Verify controller default values
      expect(scope.user.locale).toBe('en-US');
      expect(scope.user.account).toBeUndefined();
      expect(linkParser.getLink(scope.user, 'account')).toBeDefined();
    }));

    it('should return a single user', function () {
      scope.$on('hpDisableAddUserForm', function (event, addUser) {
        emitFunctionRan = true;
        addUser();
        timeout.flush();
        expect(scope.user.locale).toBe('en-US');
        expect(scope.user.account).toBeUndefined();
        expect(linkParser.getLink(scope.user, 'account').href).toBeDefined();
        expect(scope.user.firstName).toBe('testFirst');
        expect(scope.user.lastName).toBe('testLast');
        expect(alerts.length).toBe(1);
        expect(alerts[0].path).toBe('/manageUsersUrl');
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cAddUserSuccess');
        expect(alerts[0].translateValues.name).toBe(scope.user.firstName + ' ' + scope.user.lastName);
        expect(alerts[0].timeout).toBe(pageAlerts.timeout.medium);
      });
      scope.addUserSubmit();
      expect(emitFunctionRan).toBe(true);

    });

    it('should not add a user and error out', function () {
      shouldFail = true;
      scope.$on('hpDisableAddUserForm', function (event, addUser) {
        emitFunctionRan = true;
        addUser();
        timeout.flush();
        expect(scope.user.locale).toBe('en-US');
        expect(scope.user.account).toBeUndefined();
        expect(linkParser.getLink(scope.user, 'account').href).toBeDefined();
        expect(scope.user.firstName).toBe('testFirst');
        expect(scope.user.lastName).toBe('testLast');
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
        expect(alerts[0].path).toBeUndefined();
        expect(alerts[0].msg).toBe('cSaveFailed');
        expect(alerts[0].translateValues.name).toBeUndefined();
        expect(alerts[0].translateValues.errorCode).toBe('Test Response: Failure Adding User');
        expect(alerts[0].timeout).toBe(pageAlerts.timeout.medium);
      });
      scope.addUserSubmit();
      expect(emitFunctionRan).toBe(true);
    });

    it('should add one user and be ready for a second', function () {
      scope.$on('hpDisableAddUserForm', function (event, addUser) {
        emitFunctionRan = true;
        addUser();
        timeout.flush();
        expect(scope.user.locale).toBe('en-US');
        expect(scope.user.account).toBeUndefined();
        expect(linkParser.getLink(scope.user, 'account').href).toBeDefined();
        expect(scope.user.firstName).toBeUndefined();
        expect(scope.user.lastName).toBeUndefined();
        expect(alerts.length).toBe(1);
        expect(alerts[0].path).toBeUndefined();
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cAddUserSuccess');
        expect(alerts[0].translateValues.name).toBe('testFirst testLast');
        expect(alerts[0].timeout).toBe(pageAlerts.timeout.medium);
      });
      scope.addUserAndNewSubmit();
      expect(emitFunctionRan).toBe(true);
    });

    it('should cancel AddUser and return testUrl', function () {
      scope.cancelAddUser();
      expect(location.url()).toBe('/manageUsersUrl');
    });

  });
});
