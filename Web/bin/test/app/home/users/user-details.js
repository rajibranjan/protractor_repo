'use strict';

describe('[Module: userDetails]', function () {

  var alerts = [];
  beforeEach(module(function ($provide) {
    $provide.constant('PageAlerts', {
      add: function (alert) {
        alerts.push(alert);
      },
      timeout: {
        short: 33000,
        medium: 5000,
        long: 10000
      }
    });
  }));

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.users.userDetails'));

  var httpBackend, $q, timeout;
  beforeEach(inject(function ($httpBackend, _$q_, $timeout) {
    httpBackend = $httpBackend;
    $q = _$q_;
    timeout = $timeout;
  }));

  var mockPromise = function () {
    var deferred = $q.defer();
    if (Math.floor(statusCode / 100) !== 2) {
      var error = [];
      if (addInvalidPasswordError) {
        error.push({
          serviceCode: 5,
          errorCode: 3
        });
      }
      deferred.reject({
        httpStatus: statusCode,
        errors: error
      });
    }
    else {
      deferred.resolve(this);
    }
    return deferred.promise;
  };

  var statusCode = 200;

  var mockUser, mockSelf, addInvalidPasswordError;

  var getMockUser = function () {
    return {
      createdBy: 'd265f528-f20c-4529-9e7f-133b75b45941',
      createdDate: '2014-03-25T16:29:47.000+0000',
      lastModifiedBy: '0666df1f-8b05-4b64-8408-9b840ec889f6',
      lastModifiedDate: '2014-03-25T16:29:52.000+0000',
      tenantId: null,
      firstName: 'User',
      lastName: '4',
      poNumber: null,
      crsId: null,
      registrationId: null,
      activatedDate: '2014-03-25T16:29:51.000+0000',
      status: 'SUSPENDED',
      crmId: null,
      addressLine: null,
      addressLine2: null,
      city: null,
      state: null,
      locale: 'en-US',
      zipcode: null,
      phoneNumber: '2081234567',
      phoneExtension: '123',
      productIds: [],
      paymentMethod: 'CHECK',
      account: {
        country: 'US',
        links: [{
          rel: 'adminContact',
          href: 'https://localhost:9000/api/v2/users/13189ae1-b63b-49db-8539-21fd3361c099'
        }, {
          rel: 'self',
          href: 'https://localhost:9000/api/v2/accounts/47e1b4a9-cfba-4c7f-b4d6-b9c49dd81de8'
        }]
      },
      roles: ['ROLE_USER'],
      links: [{
        rel: 'self',
        href: 'https://localhost:9000/api/v2/users/042dec32-f7eb-4b27-b840-c82ab721e213'
      }, {
        rel: 'account',
        href: 'https://localhost:9000/api/v2/accounts/47e1b4a9-cfba-4c7f-b4d6-b9c49dd81de8'
      }],
      getLocale: function () {
        return mockSelf.locale;
      }
    };
  };

  var mockTaskService = {
    startTask: function () {
      return {
        $promise: mockPromise()
      };
    }
  };

  var mockResendActivationEmail = {
    startTask: function () {
      return {
        $promise: mockPromise()
      };
    }
  };

  var mockUpdateCredentialsFactory = {
    changePassword: mockTaskService.startTask,
    deletePins: mockTaskService.startTask
  };


  var testUrls = {
    manageUsers: function () {
      return '/detailUsersUrl';
    }
  };

  var getMockSelf = function () {
    return {
      id: 0,
      locale: 'en-US',
      extend: function () {},
      links: [{
        rel: 'self',
        href: 'https://localhost/api/v2/users/0'
      }],
      getLocale: function () {
        return mockSelf.locale;
      }
    };
  };

  var UserDetailsCtrl, scope, element, location, mockChangeLang, emitFunctionRan, controllerLoader;

  var initializeController = function (testAsSelf) {
    mockUser = getMockUser();
    if (testAsSelf === true) {
      mockSelf = getMockUser();
    }
    else {
      mockSelf = getMockSelf();
    }

    // Override the Entity.save and Entity.delete functions so we can mock the promise they return.
    Object.defineNonEnumerableProperty = function (obj, propName, prop) {
      if (propName === 'save' || propName === 'delete') {
        prop = mockPromise;
      }
      Object.defineProperty(obj, propName, {
        value: prop
      });
    };

    UserDetailsCtrl = controllerLoader('UserDetailsCtrl', {
      changeLanguage: mockChangeLang,
      $scope: scope,
      user: mockUser,
      Self: mockSelf,
      Urls: testUrls,
      UpdateCredentials: mockUpdateCredentialsFactory,
      ResendActivationEmail: mockResendActivationEmail
    });
  };

  beforeEach(inject(function ($rootScope, $controller, $compile, $location) {
    scope = $rootScope.$new();
    location = $location;
    controllerLoader = $controller;
    mockChangeLang = function () {};
    initializeController(false);
    alerts = [];
    emitFunctionRan = false;
    statusCode = 200;
    addInvalidPasswordError = false;
    element = angular.element('<form name="userProfileForm" role="form"></form>');
    $compile(element)(scope);
    scope.$digest();
  }));

  describe('Controller: UserDetailsCtrl', function () {

    it('should put the user on the scope', function () {
      // scope.user should be a clone of mockUser.
      expect(scope.user).not.toBe(mockUser);
      expect(scope.user).toEqual(jasmine.objectContaining(mockUser));
    });

    describe('.setStatusClass', function () {

      it('should return the correct label string', function () {
        expect(scope.setStatusClass()).toBe('label-danger');
        scope.user.status = 'CREATED';
        expect(scope.setStatusClass()).toBe('label-warning');
        scope.user.status = 'ACTIVE';
        expect(scope.setStatusClass()).toBe('label-success');
      });
    });

    describe('.resetForm', function () {

      it('should reset the form back to the original state', function () {
        expect(scope.user).toEqual(jasmine.objectContaining(mockUser));

        scope.user.firstName = 'changed user';
        scope.user.roles.push('ANOTHER_ROLE');

        scope.resetForm();

        // scope.user should reset to a clone of mockUser.
        expect(scope.user).not.toBe(mockUser);
        expect(scope.user).toEqual(jasmine.objectContaining(mockUser));
      });
    });

    describe('.updateUser', function () {

      it('should successfully update the user', function () {
        scope.$on('hpDisableUserDetailsForm', function (event, updateUser, checkIsInvalid) {
          expect(checkIsInvalid).toEqual(true);

          var originalUser = scope.user;
          scope.user.firstName = 'changed user';

          var promise = updateUser();
          expect(promise).toBePromise();

          timeout.flush();
          expect(scope.user).not.toBe(originalUser);
          expect(scope.user).toEqual(jasmine.objectContaining(originalUser));
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('success');
          emitFunctionRan = true;
        });
        scope.updateUserSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should successfully update the user with same update.id and self.id', function () {
        scope.user.id = mockUser.id;
        scope.$on('hpDisableUserDetailsForm', function (event, updateUser, checkIsInvalid) {
          expect(checkIsInvalid).toEqual(true);

          var originalUser = scope.user;
          scope.user.firstName = 'changed user';

          var promise = updateUser();
          expect(promise).toBePromise();

          timeout.flush();
          expect(scope.user).not.toBe(originalUser);
          expect(scope.user).toEqual(jasmine.objectContaining(originalUser));
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('success');
          emitFunctionRan = true;
        });
        scope.updateUserSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should deal with an unsuccessful update', function () {
        statusCode = 409;
        scope.$on('hpDisableUserDetailsForm', function (event, updateUser, checkIsInvalid) {
          expect(checkIsInvalid).toEqual(true);
          var promise = updateUser();
          expect(promise).toBePromise();

          timeout.flush();
          expect(scope.user).toEqual(jasmine.objectContaining(mockUser));

          // They still need to be different objects
          expect(scope.user).not.toBe(mockUser);
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('danger');
          emitFunctionRan = true;
        });
        scope.updateUserSubmit();
        expect(emitFunctionRan).toEqual(true);
      });
    });

    describe('deleteUser', function () {
      it('should successfully delete a user', function () {
        scope.$on('hpDisableUserDetailsForm', function (event, deleteUser, checkIsInvalid) {
          expect(checkIsInvalid).toEqual(false);
          var promise = deleteUser();
          expect(promise).toBePromise();

          timeout.flush();
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('success');
          expect(alerts[0].path).toBe(testUrls.manageUsers());
          expect(alerts[0].translateValues.name).toBe('User 4');
          expect(location.url()).toBe(testUrls.manageUsers());
          emitFunctionRan = true;
        });
        scope.deleteUserSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should error out and not delete a user', function () {
        statusCode = 409;
        scope.$on('hpDisableUserDetailsForm', function (event, deleteUser, checkIsInvalid) {
          expect(checkIsInvalid).toEqual(false);
          var promise = deleteUser();
          expect(promise).toBePromise();

          timeout.flush();
          expect(alerts.length).toBe(0);
          expect(alerts[0]).toBe(undefined);
          expect(location.url()).toBe('');
          emitFunctionRan = true;
        });
        scope.deleteUserSubmit();
        expect(emitFunctionRan).toEqual(true);
      });
    });

    describe('changePassword', function () {

      it('should change the password and add a success alert', function () {
        scope.$on('hpDisablePasswordForm', function (event, updatePassword, checkIsInvalid) {
          expect(checkIsInvalid).toEqual(true);
          updatePassword();

          timeout.flush();
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('success');
          expect(alerts[0].msg).toBe('cPasswordSuccessChange');
          expect(alerts[0].timeout).toBe(5000);
          expect(scope.accordionStatus.showPasswordForm).toBeFalsy();
          expect(scope.changePasswordFormData).toEqual({});
          emitFunctionRan = true;
        });
        scope.changePasswordSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should not change the password without 409 error code', function () {
        statusCode = 408;
        scope.$on('hpDisablePasswordForm', function (event, updatePassword, checkIsInvalid) {
          expect(checkIsInvalid).toEqual(true);
          updatePassword();

          timeout.flush();
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('danger');
          expect(alerts[0].msg).toBe('cPasswordError');
          expect(alerts[0].timeout).toBe(5000);
          expect(scope.accordionStatus.showPasswordForm).toBeFalsy();
          expect(scope.changePasswordFormData).toEqual({});
          emitFunctionRan = true;
        });
        scope.changePasswordSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should not change the password and respond with 409 error code', function () {
        statusCode = 409;
        scope.$on('hpDisablePasswordForm', function (event, updatePassword, checkIsInvalid) {
          expect(checkIsInvalid).toEqual(true);
          updatePassword();

          timeout.flush();
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('danger');
          expect(alerts[0].msg).toBe('cPasswordErrorBadPassword');
          expect(alerts[0].timeout).toBe(5000);
          expect(scope.accordionStatus.showPasswordForm).toBeFalsy();
          expect(scope.changePasswordFormData).toEqual({});
          emitFunctionRan = true;
        });
        scope.changePasswordSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should not change the password and respond with password invalid error code', function () {
        statusCode = 400;
        addInvalidPasswordError = true;
        scope.$on('hpDisablePasswordForm', function (event, updatePassword, checkIsInvalid) {
          expect(checkIsInvalid).toEqual(true);
          updatePassword();

          timeout.flush();
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('danger');
          expect(alerts[0].msg).toBe('cPasswordValidationFailed');
          expect(scope.accordionStatus.showPasswordForm).toBeFalsy();
          expect(scope.changePasswordFormData).toEqual({});
          emitFunctionRan = true;
        });
        scope.changePasswordSubmit();
        expect(emitFunctionRan).toEqual(true);
      });
    });

    describe('resendActivationEmail', function () {
      it('should send a page alert success for resend activation email', function () {
        scope.resendActivationEmail('testId');
        timeout.flush();
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cActivationEmailSent');
        expect(alerts[0].timeout).toBe(5000);
      });

      it('should send a page alert failure for resend activation email', function () {
        statusCode = 409;
        scope.resendActivationEmail();
        timeout.flush();
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
        expect(alerts[0].msg).toBe('cFailureToSendActivationEmail');
        expect(alerts[0].timeout).toBe(5000);
      });
    });

    describe('changeLocale', function () {
      it('should not change the value in the locale input when self.locale changes', function () {
        expect(scope.user.locale).toBe('en-US');
        mockSelf.locale = 'fr-CA';
        scope.$digest();
        expect(scope.user.locale).toBe('en-US');
      });

      it('should change the value in the locale input when self.locale changes if user is self', function () {
        initializeController(true);
        expect(scope.user.locale).toBe('en-US');
        mockSelf.locale = 'fr-CA';
        scope.$digest();
        expect(scope.user.locale).toBe('fr-CA');
      });
    });

    describe('deletePins', function () {
      it('should delete all PINs successfully and add a page alert', function () {
        scope.deletePins();
        timeout.flush();
        expect(scope.accordionStatus.deletePinSessions).toBe(false);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cDeletePinsSuccess');
        expect(alerts[0].timeout).toBe(5000);
      });

      it('should handle nonexistent PINs successfully and add a page alert', function () {
        statusCode = 404;
        scope.deletePins();
        timeout.flush();
        expect(scope.accordionStatus.deletePinSessions).toBe(false);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cDeletePinsSuccess');
        expect(alerts[0].timeout).toBe(5000);
      });

      it('should fail to delete all PINs and add a page alert error', function () {
        statusCode = 400;
        scope.deletePins();
        timeout.flush();
        expect(scope.accordionStatus.deletePinSessions).toBeUndefined();
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
        expect(alerts[0].msg).toBe('cDeletePinsError');
        expect(alerts[0].timeout).toBe(5000);
      });
    });

  });

});
