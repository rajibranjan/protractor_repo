'use strict';

describe('[Module: hp.portal.home.dashboardV2]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.dashboardV2'));

  describe('controller:DashboardCtrl-v2', function () {

    var scope, $q, DashboardCtrlV2, controller, mockLinkParser, mockGlobals, httpBackend, mockSelf, mockAccountServiceRes, mockLicenseService, mockRouteParams, accountsJson, licenseJson;
    accountsJson = {
      'limit': 1,
      'offset': 0,
      'total': 1,
      'data': [{
        'adminContact': {
          'status': 'ACTIVE',
          'accountId': '25020aab-ff98-4cb0-90dd-15361af903cb',
        },
        'name': 'Company #1',
        'activatedDate': '2014-03-25T16:29:51.000+0000',
        'status': 'ACTIVE'
      }]
    };

    licenseJson = {
      'total': 1,
      'data': [{
        'startDate': '2015-10-28T17:04:53.000+0000',
        'endDate': '9999-01-01T00:00:00.000+0000',
        'quantity': 5,
        'status': 'ACTIVE',
        'isTrial': false
      }]
    };
    mockAccountServiceRes = {
      get: function (opts) {
        mockAccountServiceRes.opts = opts;
        var dfd = $q.defer();
        dfd.resolve(accountsJson);
        return dfd.promise;
      }
    };

    mockLicenseService = {
      get: function (params) {
        var dfd = $q.defer();
        mockLicenseService.params = params;
        if (params !== undefined && params.qs !== undefined && params.expand === 'offering' && params.qs.indexOf('account.id==') >= 0) {
          mockLicenseService.getShouldResolve = true;
        }
        else {
          mockLicenseService.getShouldResolve = false;
        }
        if (mockLicenseService.getShouldResolve) {
          dfd.resolve(licenseJson);
        }
        else {
          dfd.reject();
        }
        return dfd.promise;
      }
    };

    mockRouteParams = {};
    mockSelf = {
      account: {
        name: 'Hp Admin'
      }
    };
    mockGlobals = {
      permissions: {}
    };

    var initController = function (testGlobals) {
      if (!testGlobals) {
        testGlobals = mockGlobals;
      }
      DashboardCtrlV2 = controller('DashboardCtrl-v2', {
        $scope: scope,
        LicenseService: mockLicenseService,
        AccountServiceRes: mockAccountServiceRes,
        $routeParams: mockRouteParams,
        Self: mockSelf,
        Globals: testGlobals,
      });
    };

    beforeEach(inject(function ($rootScope, $controller, LinkParser, _$q_, $httpBackend) {
      scope = $rootScope.$new();
      controller = $controller;
      mockLinkParser = LinkParser;
      $q = _$q_;
      httpBackend = $httpBackend;

      initController();
      scope.$apply();
    }));

    it('should set expected items on the scope', function () {
      expect(scope.DashboardInfo.companyName).toBeDefined();
      expect(scope.DashboardInfo.permission).toBeDefined();
      expect(scope.DashboardInfo.isDashboard).toBeDefined();
      expect(scope.accountInfo.paidLicenses).toBeDefined();
      expect(scope.accountInfo.licensesNearEnd).toBeDefined();
      expect(scope.accountInfo.activeLicenseCount).toBeDefined();
      expect(scope.accountInfo.trialLicenses).toBeDefined();
      expect(scope.accountInfo.activeAccountPercent).toBeDefined();
      expect(scope.accountInfo.accountCount).toBeDefined();
      expect(scope.DashboardInfo.companyName).toBe(mockSelf.account.name);
      expect(scope.DashboardInfo.isDashboard).toBe(true);
      expect(scope.accountInfo.paidLicenses).toEqual(1);
      expect(scope.accountInfo.licensesNearEnd).toEqual(0);
      expect(scope.accountInfo.activeLicenseCount).toEqual(1);
      expect(scope.accountInfo.trialLicenses).toEqual(0);
      expect(scope.accountInfo.activeAccountPercent).toEqual(100);
      expect(scope.accountInfo.accountCount).toEqual(1);
    });


    it('should set permission true', function () {
      mockGlobals.permissions.isHPAdmin = true;
      mockGlobals.permissions.isReseller = true;
      initController(mockGlobals);
      expect(scope.DashboardInfo.permission).toBe(true);
    });
    it('should set permission false', function () {
      mockGlobals.permissions.isHPAdmin = false;
      mockGlobals.permissions.isReseller = false;
      initController(mockGlobals);
      expect(scope.DashboardInfo.permission).toBe(false);
    });
  });
});
