'use strict';

describe('[Module: entityRepair]', function () {

  var alerts = [];
  beforeEach(module(function ($provide) {
    $provide.constant('PageAlerts', {
      add: function (alert) {
        alerts.push(alert);
      },
      timeout: {
        short: 3000,
        medium: 5000,
        long: 10000
      }
    });
  }));

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.adminDashboard.entityRepair'));

  var httpBackend, $q, timeout;
  beforeEach(inject(function ($httpBackend, _$q_, $timeout) {
    httpBackend = $httpBackend;
    $q = _$q_;
    timeout = $timeout;
  }));

  var statusCode = 200;
  var mockPromise = function () {
    var deferred = $q.defer();
    if (Math.floor(statusCode / 100) !== 2) {
      deferred.reject({
        status: statusCode
      });
    }
    else {
      deferred.resolve();
    }
    return deferred.promise;
  };

  var mockEntityRepair = {
    startTask: function () {
      return {
        $promise: mockPromise()
      };
    }
  };

  var EntityRepairCtrl, scope;

  beforeEach(inject(function ($rootScope, $controller) {
    scope = $rootScope.$new();
    alerts = [];
    EntityRepairCtrl = $controller('EntityRepairCtrl', {
      $scope: scope,
      EntityRepair: mockEntityRepair
    });
    scope.$digest();
  }));

  describe('Controller: EntityRepairCtrl', function () {
    it('should make sure entityRepair is defined.', function () {
      expect(scope.entityRepair).toBeDefined();
    });

    it('should queue the correct alert on success.', function () {
      statusCode = 200;
      scope.submit();
      timeout.flush();
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('success');
      expect(alerts[0].msg).toBe('cSuccessfullyFixedMessage');
    });

    it('should queue the correct alert on error.', function () {
      statusCode = 401;
      scope.submit();
      timeout.flush();
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('danger');
      expect(alerts[0].msg).toBe('cFailToFixMessage');
    });
  });
});
