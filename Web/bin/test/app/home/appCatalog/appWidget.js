'use strict';

describe('[Module: hp.portal.home.appCatalog.appWidget]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.appCatalog.appWidget'));
  beforeEach(module('hp.templates'));

  var mockUserId = 'mock-user-id';
  var mockEulaPageUrl = 'http://localhost/eula';
  var mockRestrictionCodes = {
    provisioning: -1592735507,
    eulaAcceptance: 1340150173,
    licenseExpired: 219523870,
    licenseTerminated: 1071278268
  };

  var getByUserAndOfferingDeferred;
  var mockEntitlementService = {
    getByUserAndOffering: function () {
      getByUserAndOfferingDeferred = q.defer();
      return getByUserAndOfferingDeferred.promise;
    }
  };

  beforeEach(module(function ($provide) {
    $provide.provider('PortalConfigurations', function () {
      // Mock the PortalConfigurations factory to return the eulaPageUrl.
      this.$get = function () {
        return {
          'com.portal.eulaPageUrl': mockEulaPageUrl,
          $promise: {
            then: function (callback) {
              callback();
            }
          }
        };
      };
    });

    // Mock the RestrictionCodes constant with the provisioning restriction.
    $provide.constant('RestrictionCodes', mockRestrictionCodes);

    $provide.provider('EntitlementService', function () {
      this.$get = function () {
        return mockEntitlementService;
      };
    });

    $provide.provider('LinkParser', function () {
      this.$get = function () {
        return {
          getSelfId: function () {
            return mockUserId;
          }
        };
      };
    });
  }));

  var compile, q;
  beforeEach(inject(function ($compile, $q) {
    compile = $compile;
    q = $q;
  }));

  describe('Directive: app-widget', function () {
    var element, scope, isolateScope, timeout, moment;
    var elementText = '<app-widget product="product" offering="offering" entitlements="entitlements" license="license"></app-widget>';

    var mockOffering = {
      fqName: 'mock-offering',
      urls: {
        browserLandingUrl: 'test-url'
      }
    };

    var mockProduct = {
      name: 'mock-product'
    };

    var mockLicense = {
      status: 'ACTIVE',
      terminateDate: new Date().setDate(new Date().getDate() + 45),
      daysToExpired: 30,
      expirationNotificationDays: 15
    };

    // By default the mock entitlement has a provisioning restriction, but EULA has been accepted.
    function generateMockEntitlement() {
      return {
        isEntitled: false,
        termsAccepted: true,
        privacyAccepted: true,
        restrictions: [{
          code: mockRestrictionCodes.provisioning,
        }]
      };
    }

    beforeEach(inject(function ($rootScope, $timeout, Globals, amMoment) {
      timeout = $timeout;
      scope = $rootScope.$new();
      element = compile(elementText)(scope);
      scope.product = mockProduct;
      scope.offering = mockOffering;
      mockLicense.status = 'ACTIVE';
      scope.license = mockLicense;
      scope.entitlements = [generateMockEntitlement()];
      moment = amMoment;

      Globals.LicenseStatus = {
        EXPIRED: 'EXPIRED'
      };
    }));

    // This function allows tests to decide when to run scope.digest.
    function scopeDigest() {
      scope.$digest();
      isolateScope = element.isolateScope();
    }

    it('should set appDisabledMessage for the provisioning restriction', function () {
      scopeDigest();

      expect(isolateScope.appMessage).toBe('cAccessPending');
    });

    it('should set href for the eula acceptance restriction', function () {
      scope.entitlements[0].restrictions = [{
        code: mockRestrictionCodes.eulaAcceptance
      }];
      scopeDigest();

      expect(isolateScope.href).toBe(mockEulaPageUrl + '#/' + mockOffering.fqName);
    });

    it('should set scope correctly when only entitlement is valid', function () {
      var secondEntitlement = generateMockEntitlement();
      secondEntitlement.isEntitled = true;
      secondEntitlement.restrictions = [];
      scope.entitlements.push(secondEntitlement);

      scopeDigest();

      expect(isolateScope.appMessage).toBe('cAccessPending');
    });

    it('should set scope correctly when there are multiple entitlements an none are entitled', function () {
      var secondEntitlement = generateMockEntitlement();
      scope.entitlements.push(secondEntitlement);

      scopeDigest();
      expect(isolateScope.appMessage).toBe('cAccessPending');
    });

    it('should set scope correctly when there are multiple restriction types', function () {
      var secondEntitlement = generateMockEntitlement();
      var anotherLameRestriction = 'another lame restriction';
      secondEntitlement.restrictions = [{
        code: anotherLameRestriction
      }];
      scope.entitlements.push(secondEntitlement);

      scopeDigest();

      expect(isolateScope.appMessage).toBe('cAccessPending');
    });

    it('should set appDisabledMessage for unknown entitlement restriction', function () {
      scope.entitlements[0].restrictions[0] = {
        code: 'super-crazy-restriction-code'
      };

      scopeDigest();

      expect(isolateScope.appMessage).toBe('cUnknownEntitlementRestriction');
    });

    it('should set appDisabledMessage when license status is EXPIRED', function () {
      scope.license.status = 'EXPIRED';
      scope.license.endDate = new Date();
      scope.entitlements[0].restrictions.push({
        code: mockRestrictionCodes.licenseExpired
      });

      var midnightTonight = new Date();
      midnightTonight.setDate(midnightTonight.getDate() + 1);
      midnightTonight.setUTCHours(0, 0, 0, 0);
      var daysTilTermination = moment.preprocessDate(mockLicense.terminateDate).from(midnightTonight);

      scopeDigest();
      expect(isolateScope.license.daysTilTermination).toBe(daysTilTermination);
      expect(isolateScope.appMessage).toBe('cLicenseExpired');
      delete scope.license.status;
      delete scope.license.endDate;
    });

    it('should set appDisabledMessage when license is EXPIRING', function () {
      scope.license.daysToExpired = 1;
      scope.entitlements[0].restrictions = [];

      scopeDigest();

      expect(isolateScope.appMessage).toBe('cLaunchAppLicenseExpiring');
      delete scope.license.status;
      scope.license.daysToExpired = 30;
    });

    it('should set appDisabledMessage when license status is past the number of days between expired and terminated', function () {
      scope.license.status = 'TERMINATED';
      scope.license.terminateDate = new Date();
      scope.entitlements[0].restrictions.push({
        code: mockRestrictionCodes.licenseTerminated
      });

      scopeDigest();

      expect(isolateScope.appMessage).toBe('cLicenseTerminated');
      delete scope.license.status;
      delete scope.license.endDate;
    });

    it('should call EntitlementService.getByUserAndOffering with correct arguments', function () {
      scopeDigest();

      expect(isolateScope.appMessage).toBe('cAccessPending');

      spyOn(mockEntitlementService, 'getByUserAndOffering').and.callThrough();

      // Flush the timeout to cause the polling to happen.
      timeout.flush();

      expect(mockEntitlementService.getByUserAndOffering).toHaveBeenCalledWith(mockUserId, mockOffering.fqName);
    });

    it('should poll once for entitlements and then stop', function () {
      scopeDigest();

      var entitlement = isolateScope.entitlements[0];
      expect(entitlement.termsAccepted).toBe(true);
      expect(entitlement.privacyAccepted).toBe(true);
      expect(entitlement.restrictions).toBeDefined();
      expect(isolateScope.appMessage).toBe('cAccessPending');

      // Flush the timeout to cause the polling to happen.
      timeout.flush();

      var newEntitlement = generateMockEntitlement();
      newEntitlement.isEntitled = true;
      delete newEntitlement.restrictions;
      getByUserAndOfferingDeferred.resolve({
        data: [newEntitlement]
      });

      // Flush the timeout for the promise.
      timeout.flush();

      entitlement = isolateScope.entitlements[0];
      expect(entitlement.termsAccepted).toBe(true);
      expect(entitlement.privacyAccepted).toBe(true);
      expect(isolateScope.appMessage).toBe('cLaunchApp');

      timeout.verifyNoPendingTasks();
    });

    it('should poll twice for entitlements and then stop', function () {
      scopeDigest();

      var entitlement = isolateScope.entitlements[0];
      expect(entitlement.termsAccepted).toBe(true);
      expect(entitlement.privacyAccepted).toBe(true);
      expect(entitlement.restrictions).toBeDefined();
      expect(isolateScope.appMessage).toBe('cAccessPending');

      // Flush the timeout to cause the polling to happen.
      timeout.flush();

      var newEntitlement = generateMockEntitlement();
      getByUserAndOfferingDeferred.resolve({
        data: [newEntitlement]
      });

      // Flush the timeout for the promise.
      timeout.flush();

      entitlement = isolateScope.entitlements[0];
      expect(entitlement.termsAccepted).toBe(true);
      expect(entitlement.privacyAccepted).toBe(true);
      expect(entitlement.restrictions).toBeDefined();
      expect(isolateScope.appMessage).toBe('cAccessPending');

      // Flush the timeout to cause the polling to happen.
      timeout.flush();

      newEntitlement = generateMockEntitlement();
      newEntitlement.isEntitled = true;
      delete newEntitlement.restrictions;
      getByUserAndOfferingDeferred.resolve({
        data: [newEntitlement]
      });

      // Flush the timeout for the promise.
      timeout.flush();
      entitlement = isolateScope.entitlements[0];
      expect(entitlement.termsAccepted).toBe(true);
      expect(entitlement.privacyAccepted).toBe(true);
      expect(isolateScope.appMessage).toBe('cLaunchApp');

      timeout.verifyNoPendingTasks();
    });

    it('should poll once for entitlements but the promise is rejected', function () {
      scopeDigest();

      var entitlement = isolateScope.entitlements[0];
      expect(entitlement.isEntitled).toBe(false);
      expect(entitlement.termsAccepted).toBe(true);
      expect(entitlement.privacyAccepted).toBe(true);
      expect(entitlement.restrictions).toBeDefined();
      expect(isolateScope.appMessage).toBe('cAccessPending');

      // Flush the timeout to cause the polling to happen.
      timeout.flush();

      // Reject the promise.
      getByUserAndOfferingDeferred.reject();

      // Flush the timeout for the promise.
      timeout.flush();

      entitlement = isolateScope.entitlements[0];
      expect(entitlement.isEntitled).toBe(false);
      expect(entitlement.termsAccepted).toBe(true);
      expect(entitlement.privacyAccepted).toBe(true);
      expect(entitlement.restrictions).toBeDefined();
      expect(isolateScope.appMessage).toBe('cInternalErrorTitle');

      timeout.verifyNoPendingTasks();
    });

  });
});
