'use strict';

describe('Module: hp.portal.home.webservices-v2', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.webservices.endpoints'));
  beforeEach(module('hp.portal.home.webservices-v2'));

  var httpBackend;

  describe('Factory: AccountService', function () {

    var accountService, linkParser;

    var accountsResourceEndpoint = '/api/v2/accounts';

    var resellerAccounts = {
      Company1: {
        name: 'Company1'
      },
      Company2: {
        name: 'Company2'
      }
    };

    var testAccount = {
      name: 'Test Company',
      customerId: 'cust_ID_1234',
      status: 'ACTIVE',
      state: 'ID',
      phoneNumber: '1234561234',
      links: [{
        rel: 'self',
        href: 'https://localhost:8443/HPStoreWeb/api/v2/accounts/ea28c75d-5de9-426d-a6d0-8e408f1e7c31'
      }]
    };

    beforeEach(inject(function ($httpBackend, AccountService, LinkParser) {
      httpBackend = $httpBackend;
      accountService = AccountService;
      linkParser = LinkParser;
    }));

    afterEach(function () {
      httpBackend.verifyNoOutstandingExpectation();
      httpBackend.verifyNoOutstandingRequest();
    });

    it('should get the accounts under a resller Id', function () {
      httpBackend.expectGET(accountsResourceEndpoint + '?resellerId=' + testAccount.customerId).respond(200, resellerAccounts);
      var accountsPromise = accountService.get({
        resellerId: testAccount.customerId
      });

      // Assert that we got back the data we were expecting.
      accountsPromise.then(function (data) {
        expect(data.Company1.name).toBe('Company1');
        expect(data.Company2.name).toBe('Company2');
      });
      httpBackend.flush();
    });

    it('should get the accounts without a reseller Id', function () {
      httpBackend.expectGET(accountsResourceEndpoint).respond(200, resellerAccounts);
      var accountsPromise = accountService.get();

      // Assert that we got back the data we were expecting.
      accountsPromise.then(function (data) {
        expect(data.Company1.name).toBe('Company1');
        expect(data.Company2.name).toBe('Company2');
      });
      httpBackend.flush();
    });

    it('should get the Account through REST with an ID.', function () {
      // Expect a GET with the URL regex, when received return 200 with mock data.
      httpBackend.expectGET(accountsResourceEndpoint + '/' + linkParser.getSelfId(testAccount)).respond(200, testAccount);
      var accountEntity = accountService.getById(linkParser.getSelfId(testAccount));

      // Assert that we got back the data we were expecting.
      accountEntity.$promise.then(function (data) {
        expect(data.name).toBe('Test Company');
        expect(data.state).toBe('ID');
        expect(data.status).toBe('ACTIVE');
      });
      httpBackend.flush();
    });

    it('should perform a PUT with REST when updating an account.', function () {
      httpBackend.expectPUT(accountsResourceEndpoint + '/' + linkParser.getSelfId(testAccount)).respond(200, testAccount);

      accountService.fromExisting(testAccount).save();
      httpBackend.flush();
    });

    it('should call POST through REST when performing a save on the account..', function () {
      httpBackend.expectPOST(accountsResourceEndpoint + '/' + linkParser.getSelfId(testAccount)).respond(200, testAccount);

      accountService.create(testAccount).save();
      httpBackend.flush();
    });

    it('should call DELETE through REST when performing a delete on the account.', function () {
      httpBackend.expectDELETE(accountsResourceEndpoint + '/' + linkParser.getSelfId(testAccount)).respond(200);

      accountService.fromExisting(testAccount).delete();
      httpBackend.flush();
    });

  });

});
