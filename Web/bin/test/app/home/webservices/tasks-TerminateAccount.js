'use strict';

describe('Module: hp.portal.home.webservices.tasks', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.webservices.tasks'));

  describe('Factory: TerminateAccount.startTask', function () {

    var httpBackend, TerminateAccount, Endpoints;

    beforeEach(inject(function ($httpBackend, _TerminateAccount_, _Endpoints_) {
      httpBackend = $httpBackend;
      TerminateAccount = _TerminateAccount_;
      Endpoints = _Endpoints_;
    }));

    it('should post to the right endpoint', function () {
      var sampleData = {
        foo: 'bar'
      };

      httpBackend.expect('POST', Endpoints.terminateAccount, function (data) {
        expect(JSON.parse(data)).toEqual(sampleData);
        return true;
      }).respond(200, {});

      TerminateAccount.startTask(sampleData);
    });

    it('should return a task object', function () {
      httpBackend.expect('POST', Endpoints.terminateAccount).respond(200, {});

      var task = TerminateAccount.startTask();
      expect(task.$promise).toBePromise();
      expect(task.waitForComplete).toBeFunction();
      expect(task.getStatus).toBeFunction();
    });

  });

});
