'use strict';

describe('[Module: hp.portal.common.webservices-v2]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.webservices-v2'));

  describe('Queries.OfferingsAndProductsByUser.get(params)', function () {
    var offeringsAndProductsByUser, $httpBackend;
    var params = {
      param1: 'foo',
      param2: 'bar'
    };

    var mockResponseData = 'mock-response-data';

    beforeEach(inject(function (_$httpBackend_, Queries) {
      $httpBackend = _$httpBackend_;
      offeringsAndProductsByUser = Queries.OfferingsAndProductsByUser;
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should make GET request to endpoint without params', function () {
      $httpBackend.expectGET('/api/v2/queries/offeringsAndProductsByUser').respond(200, mockResponseData);
      var promiseResolved = false;
      offeringsAndProductsByUser.get().then(function (data) {
        expect(data).toEqual(mockResponseData);
        promiseResolved = true;
      });
      expect(promiseResolved).toBe(false);
      $httpBackend.flush();
      expect(promiseResolved).toBe(true);
    });

    it('should make GET request to endpoint with params', function () {
      $httpBackend.expectGET('/api/v2/queries/offeringsAndProductsByUser?param1=' + params.param1 + '&param2=' + params.param2).respond(200, mockResponseData);
      var promiseResolved = false;
      offeringsAndProductsByUser.get(params).then(function (data) {
        expect(data).toEqual(mockResponseData);
        promiseResolved = true;
      });
      expect(promiseResolved).toBe(false);
      $httpBackend.flush();
      expect(promiseResolved).toBe(true);
    });
  });

  describe('Queries.ProductsAndUsersByOfferingAndAccount.get(params)', function () {
    var productsAndUsersByOfferingAndAccount, $httpBackend;
    var params = {
      param1: 'boo',
      param2: 'far'
    };

    var mockResponseData = 'mock-response-data';

    beforeEach(inject(function (_$httpBackend_, Queries) {
      $httpBackend = _$httpBackend_;
      productsAndUsersByOfferingAndAccount = Queries.ProductsAndUsersByOfferingAndAccount;
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should make GET request to endpoint without params', function () {
      $httpBackend.expectGET('/api/v2/queries/productsAndUsersByOfferingAndAccount').respond(200, mockResponseData);
      var promiseResolved = false;
      productsAndUsersByOfferingAndAccount.get().then(function (data) {
        expect(data).toEqual(mockResponseData);
        promiseResolved = true;
      });
      expect(promiseResolved).toBe(false);
      $httpBackend.flush();
      expect(promiseResolved).toBe(true);
    });

    it('should make GET request to endpoint with params', function () {
      $httpBackend.expectGET('/api/v2/queries/productsAndUsersByOfferingAndAccount?param1=' + params.param1 + '&param2=' + params.param2).respond(200, mockResponseData);
      var promiseResolved = false;
      productsAndUsersByOfferingAndAccount.get(params).then(function (data) {
        expect(data).toEqual(mockResponseData);
        promiseResolved = true;
      });
      expect(promiseResolved).toBe(false);
      $httpBackend.flush();
      expect(promiseResolved).toBe(true);
    });
  });
});
