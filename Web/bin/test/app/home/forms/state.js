'use strict';

describe('[Module: hp.portal.home.forms.state]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.forms.state'));
  beforeEach(module('hp.templates'));

  var scope, compile;
  var stateTag = '<div hp-state-field="account.state" country-settings="countrySettings" required></div>';

  var mockCountrySettingsUS = {
    country: 'US',
    isSupported: true,
    $resolved: true
  };
  var mockCountrySettingsCanada = {
    country: 'CA',
    isSupported: true,
    $resolved: true
  };
  var mockCountrySettingsGermany = {
    country: 'DE',
    isSupported: true,
    $resolved: true
  };
  var mockCountrySettingsNetherlands = {
    country: 'NL',
    isSupported: true,
    $resolved: true
  };
  var mockCountrySettingsSpain = {
    country: 'ES',
    isSupported: true,
    $resolved: true
  };
  var mockCountrySettingsFrance = {
    country: 'FR',
    isSupported: true,
    $resolved: true
  };
  var mockCountrySettingsItaly = {
    country: 'IT',
    isSupported: true,
    $resolved: true
  };

  var mockUnsupportedCountrySettings = {
    country: 'unsupportedCountry',
    isSupported: false,
    $resolved: true
  };
  var mockAccount = {
    state: null,
    country: 'US'
  };

  beforeEach(inject(function ($compile) {
    compile = $compile;
  }));

  describe('Direcitive: hp-state-field scope methods', function () {
    var element;
    beforeEach(inject(function ($rootScope) {
      scope = $rootScope.$new();
      scope.stateSupportedCountry = true;
      scope.countrySettings = mockCountrySettingsUS;
      element = compile(stateTag)(scope);
      scope.$digest();
    }));

    it('should return the state name only if the modelValue is a correct abbreviation', function () {
      var inputModel = element.find('input').data('$ngModelController');
      inputModel.$modelValue = 'AL';
      var elementScope = element.scope();
      expect(elementScope.formatStateModel()).toBe('Alabama');
      inputModel.$modelValue = 'FOO';
      expect(elementScope.formatStateModel()).toBeUndefined();
      inputModel.$modelValue = 'AL';
      expect(elementScope.formatStateModel()).toBe('Alabama');
    });

    it('should have the correct states and disabled state for different countries with required', function () {
      var elementScope = element.scope();
      // scope.countrySettings initialized to US
      expect(elementScope.formatStateModel()).toBe(undefined);
      expect(elementScope.disableState).toBe(false);

      elementScope.countrySettings = undefined;
      scope.$digest();
      expect(elementScope.disableState).toBe(true);
      expect(elementScope.countryStates.length).toBe(0);

      elementScope.countrySettings = mockUnsupportedCountrySettings;
      elementScope.stateSupportedCountry = false;
      scope.$digest();
      expect(elementScope.disableState).toBe(true);
      expect(elementScope.countryStates.length).toBe(0);
    });
  });

  describe('Directive: hp-state-field in a form', function () {
    var element;
    beforeEach(inject(function ($rootScope) {
      scope = $rootScope.$new();
      scope.account = mockAccount;
      scope.stateSupportedCountry = true;
      element = compile('<form name="form">' + stateTag + '</form>')(scope);
      scope.$digest();
    }));

    it('should validate the field', function () {
      var inputModel = element.find('input').data('$ngModelController');
      scope.countrySettings = mockCountrySettingsUS;
      scope.$digest();
      inputModel.$setViewValue('Alabama');
      expect(scope.account.state).toBe('AL');
      expect(inputModel.$valid).toBe(true);
    });

    it('should validate the field for a canadian country', function () {
      var inputModel = element.find('input').data('$ngModelController');
      scope.countrySettings = mockCountrySettingsCanada;
      scope.$digest();
      inputModel.$setViewValue('Ontario');
      expect(scope.account.state).toBe('CA_ON');
    });

    it('should validate the field for a german region', function () {
      var inputModel = element.find('input').data('$ngModelController');
      scope.countrySettings = mockCountrySettingsGermany;
      scope.$digest();
      inputModel.$setViewValue('Berlin');
      expect(scope.account.state).toBe('DE_BE');
    });

    it('should validate the field for a netherlands region', function () {
      var inputModel = element.find('input').data('$ngModelController');
      scope.countrySettings = mockCountrySettingsNetherlands;
      scope.$digest();
      inputModel.$setViewValue('Drenthe');
      expect(scope.account.state).toBe('NL_DR');
    });

    it('should validate the field for a spain region', function () {
      var inputModel = element.find('input').data('$ngModelController');
      scope.countrySettings = mockCountrySettingsSpain;
      scope.$digest();
      inputModel.$setViewValue('Andalucía');
      expect(scope.account.state).toBe('ES_AN');
    });

    it('should validate the field for a french region', function () {
      var inputModel = element.find('input').data('$ngModelController');
      scope.countrySettings = mockCountrySettingsFrance;
      scope.$digest();
      inputModel.$setViewValue('Alsace');
      expect(scope.account.state).toBe('FR_A');
    });

    it('should validate the field for an Italy region', function () {
      var inputModel = element.find('input').data('$ngModelController');
      scope.countrySettings = mockCountrySettingsItaly;
      scope.$digest();
      inputModel.$setViewValue('Abruzzo');
      expect(scope.account.state).toBe('IT_65');
    });


    it('should not match the country with the state', function () {
      var inputModel = element.find('input').data('$ngModelController');
      scope.countrySettings = mockCountrySettingsUS;
      scope.$digest();
      inputModel.$setViewValue('Ontario');
      expect(scope.account.state).toBe('Ontario');
      expect(inputModel.$valid).toBe(false);
    });
  });
});
