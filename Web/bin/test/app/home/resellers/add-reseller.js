'use strict';

describe('[Module: hp.home.resellers.addReseller]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.resellers.addReseller'));

  var httpBackend, q, timeout, linkParser;
  beforeEach(inject(function ($httpBackend, $q, $timeout, LinkParser) {
    httpBackend = $httpBackend;
    q = $q;
    timeout = $timeout;
    linkParser = LinkParser;
  }));

  var addResellerSuccess;

  var ResellerFactory = function () {
    this.$save = function (success, failure) {
      if (addResellerSuccess) {
        success();
      }
      else {
        failure({
          status: 'Test Response: Failure Adding Reseller'
        });
      }
    };
  };

  var alerts, mockPageAlerts = {
    add: function (alert) {
      alerts.push(alert);
    },
    timeout: {
      short: 3000,
      medium: 5000,
      long: 10000
    }
  };

  var mockCountrySettings = {
    get: function () {
      if (isSupportedCountry) {
        return countrySettingsSupported;
      }
      else {
        return countrySettingsUnsupported;
      }
    }
  };

  var isSupportedCountry;

  var countrySettingsSupported = {
    isSupported: true
  };

  var countrySettingsUnsupported = {
    isSupported: false
  };

  var mockUrls = {
    manageResellers: function () {
      return '/manageResellersUrl';
    }
  };

  var mockSelf = {
    links: [{
      rel: 'account',
      href: 'https://localhost:9000/api/v2/accounts/d8c0d964-43c8-4047-9f88-f8eec2c970e3'
    }],
    account: {
      country: 'US'
    },
    getLocale: function () {
      return 'en-US';
    }
  };

  describe('AddResellerCtrl', function () {

    var scope, AddResellerCtrl, addResellerRan, location;

    beforeEach(inject(function ($rootScope, $controller, $location) {
      scope = $rootScope.$new();
      alerts = [];
      addResellerRan = false;
      addResellerSuccess = true;
      location = $location;
      isSupportedCountry = true;
      AddResellerCtrl = $controller('AddResellerCtrl', {
        $scope: scope,
        Urls: mockUrls,
        Self: mockSelf,
        PageAlerts: mockPageAlerts,
        AddReseller: ResellerFactory,
        CountrySettings: mockCountrySettings
      });
      scope.reseller.name = 'Reseller';
      scope.reseller.country = 'US';
      httpBackend.when('GET', '/api/v2/users/self').respond('This GET was handled');
    }));

    it('should have all the correct initial properties for a reseller', function () {
      expect(scope.reseller).toBeDefined();
      expect(scope.companyAdmin).toBeDefined();
      expect(scope.adminContact).toBeDefined();
      expect(scope.user).toBeDefined();
      expect(scope.locales).toBeDefined();
      expect(scope.countries).toBeDefined();
    });

    it('should add a single reseller', function () {
      scope.$on('hpDisableAddResellerForm', function (event, addReseller) {
        addResellerRan = true;
        addReseller();
        expect(linkParser.getLink(scope.reseller, 'reseller').href).toBeDefined();
        expect(scope.reseller.name).toBe('Reseller');
        expect(location.url()).toBe('');
        expect(alerts.length).toBe(1);
        expect(alerts[0].path).toBe(mockUrls.manageResellers());
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cAddResellerSuccess');
        expect(alerts[0].translateValues.name).toBe('Reseller');
        expect(alerts[0].timeout).toBe(mockPageAlerts.timeout.medium);
      });
      scope.addResellerSubmit();
      expect(addResellerRan).toBe(true);
    });

    it('should not add a reseller and error out', function () {
      addResellerSuccess = false;
      scope.$on('hpDisableAddResellerForm', function (event, addReseller) {
        addResellerRan = true;
        addReseller();
        expect(linkParser.getLink(scope.reseller, 'reseller').href).toBeDefined();
        expect(scope.reseller.name).toBe('Reseller');
        expect(location.url()).toBe('');
        expect(alerts.length).toBe(1);
        expect(alerts[0].path).toBe(mockUrls.manageResellers());
        expect(alerts[0].type).toBe('danger');
        expect(alerts[0].msg).toBe('cAddResellerError');
        expect(alerts[0].translateValues.errorCode).toBe('Test Response: Failure Adding Reseller');
        expect(alerts[0].timeout).toBe(mockPageAlerts.timeout.medium);
      });
      scope.addResellerSubmit();
      expect(addResellerRan).toBe(true);
    });

    it('should add one reseller and be ready for a second', function () {
      scope.$on('hpDisableAddResellerForm', function (event, addReseller) {
        addResellerRan = true;
        addReseller();
        expect(linkParser.getLink(scope.reseller, 'reseller').href).toBeDefined();
        expect(scope.reseller.name).toBe(undefined);
        expect(location.url()).toBe('');
        expect(alerts.length).toBe(1);
        expect(alerts[0].path).toBe(undefined);
        expect(alerts[0].type).toBe('success');
        expect(alerts[0].msg).toBe('cAddResellerSuccess');
        expect(alerts[0].translateValues.name).toBe('Reseller');
        expect(alerts[0].timeout).toBe(mockPageAlerts.timeout.medium);
      });
      scope.addResellerAndNewSubmit();
      expect(addResellerRan).toBe(true);
    });

    it('should cancel AddReseller and return mockUrl', function () {
      scope.cancelAdd();
      expect(location.url()).toEqual(mockUrls.manageResellers());
    });

    //To be enabled when we support regionalized languages.
    // it('should set up locales correctly for US', function() {
    //   scope.updateLocales();
    //   expect(scope.user.locale).toBe('en-US');
    // });

    // it('should set up locales correctly for CA', function() {
    //   scope.reseller.country = 'CA';
    //   scope.updateLocales();
    //   expect(scope.user.locale).toBe('en-CA');
    // });

    it('should ensure that we have the same Locale selections for all countries', function () {
      scope.updateLocales();
      var oldLocales = scope.locales;
      scope.reseller.country = 'CA';
      scope.updateLocales();
      expect(scope.locales).toBe(oldLocales);
    });

    it('should not show the state and zip code fields with unsupported country and null fields on country switch', function () {
      isSupportedCountry = false;
      scope.reseller.phoneNumber = 123456789;
      scope.reseller.country = undefined;
      scope.$digest();
      expect(scope.countrySettings).toBe(undefined);
      expect(scope.reseller.state).toBe(null);
      expect(scope.reseller.zipcode).toBe(null);
      expect(scope.reseller.phoneNumber).toBe(123456789);

      isSupportedCountry = true;
      scope.reseller.country = 'CA';
      scope.reseller.state = 'testState';
      scope.reseller.zipcode = 123456789;
      scope.$digest();
      expect(scope.countrySettings.isSupported).toBe(true);
      expect(scope.reseller.state).toBe(null);
      expect(scope.reseller.zipcode).toBe(null);
      expect(scope.reseller.phoneNumber).toBe(123456789);

      isSupportedCountry = false;
      scope.reseller.country = 'Antarctica';
      scope.$digest();
      expect(scope.countrySettings.isSupported).toBe(false);
      expect(scope.reseller.state).toBe(null);
      expect(scope.reseller.zipcode).toBe(null);
      expect(scope.reseller.phoneNumber).toBe(123456789);
    });
  });
});
