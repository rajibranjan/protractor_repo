'use strict';

describe('[Module: hp.portal.home.accounts.accountDetails]', function () {
  beforeEach(module('unitTestInit'));

  var alerts = [];
  var resendEmailSuccess;
  var mockSelf = {
    status: 'CREATED',
    roles: [],
    account: {
      metaTags: ['TEST_TAG', 'AMERICAS_DIRECT']
    },
    links: [{
      rel: 'account',
      href: 'https://localhost:9443/HPStoreWeb/api/v2/accounts/042dec32-f7eb-4b27-b840-c82ab721e213'
    }]
  };
  var mockGlobals = {
    permissions: {
      isHPAdmin: true,
      isReseller: false,
      isCompanyAdmin: false
    }
  };

  var mockPageAlerts = {
    add: function (alert) {
      alerts.push(alert);
    },
    timeout: {
      short: 3000,
      medium: 5000,
      long: 10000
    }
  };

  beforeEach(module(function ($provide) {
    $provide.constant('PageAlerts', mockPageAlerts);
  }));

  beforeEach(module('hp.portal.home.accounts.accountDetails'));

  var httpBackend, q, timeout, filter;
  beforeEach(inject(function ($httpBackend, $q, $timeout, $filter) {
    httpBackend = $httpBackend;
    q = $q;
    timeout = $timeout;
    filter = $filter;
  }));

  describe('Controller: AccountDetailsCtrl', function () {

    var shouldFail = false;
    var deleteFail = false;
    var testAccount = {
      createdBy: 'd265f528-f20c-4529-9e7f-133b75b45941',
      createdDate: '2014-03-25T16:29:47.000+0000',
      lastModifiedBy: '0666df1f-8b05-4b64-8408-9b840ec889f6',
      lastModifiedDate: '2014-03-25T16:29:52.000+0000',
      tenantId: null,
      name: 'Account 4',
      poNumber: null,
      crsId: null,
      registrationId: null,
      activatedDate: '2014-03-25T16:29:51.000+0000',
      status: 'ACTIVE',
      crmId: null,
      addressLine: null,
      addressLine2: null,
      city: null,
      state: null,
      country: 'US',
      zipcode: null,
      phoneNumber: '2081234567',
      phoneExtension: '123',
      productIds: [],
      paymentMethod: 'CHECK',
      metaTags: ['EXISTING_TAG', 'TEST_TAG'],
      links: [{
        rel: 'self',
        href: 'https://localhost:9000/api/v2/accounts/042dec32-f7eb-4b27-b840-c82ab721e213'
      }, {
        rel: 'adminContact',
        href: 'https://localhost:9000/api/v2/users/13189ae1-b63b-49db-8539-21fd3361c099'
      }],
      save: function () {
        var deferred = q.defer();
        timeout(function () {
          if (shouldFail) {
            deferred.reject(new Error('I just don\'t like you exception'));
          }
          else {
            deferred.resolve(updatedAccount);
          }
        }, 100);
        return deferred.promise;
      },
      delete: function () {
        var deferred = q.defer();
        timeout(function () {
          if (deleteFail) {
            deferred.reject(new Error('I just don\'t like you exception'));
          }
          else {
            deferred.resolve();
          }
        }, 100);
        return deferred.promise;
      }
    };

    var testUser = {
      email: 'user2@eggnogcookie.com',
      firstName: 'User2First',
      lastName: 'en-US',
      phoneNumber: '1-208-396-6000',
      phoneExtension: null,
      links: [{
        rel: 'self',
        href: 'https://localhost:9000/HPStoreWeb/api/v2/users/testUser'
      }]
    };

    var updatedUser = angular.copy(testUser);
    updatedUser.links[0] = {
      rel: 'self',
      href: 'https://localhost:9000/HPStoreWeb/api/v2/users/updatedUser'
    };

    var mockUserService = {
      getById: function () {
        var deferred = q.defer();
        timeout(function () {
          if (adminGetFail) {
            deferred.reject(new Error('I just don\'t like you exception'));
          }
          else {
            angular.extend(returnVal, testUser);
            delete returnVal.$promise;
            deferred.resolve(testUser);
          }
        }, 100);
        var returnVal = {
          $promise: deferred.promise
        };
        return returnVal;
      }
    };

    var mockCountrySettings = {
      get: function () {
        if (isSupportedCountry) {
          return countrySettingsSupported;
        }
        else {
          return countrySettingsUnsupported;
        }
      }
    };

    var isSupportedCountry;

    var countrySettingsSupported = {
      isSupported: true
    };

    var countrySettingsUnsupported = {
      isSupported: false
    };

    var setIsMyCompany = function (isMyCompany) {
      if (isMyCompany) {
        mockSelf.links = [{
          rel: 'account',
          href: 'https://localhost:9443/HPStoreWeb/api/v2/accounts/042dec32-f7eb-4b27-b840-c82ab721e213'
        }];
      }
      else {
        mockSelf.links = [{
          rel: 'account',
          href: 'https://localhost:9443/HPStoreWeb/api/v2/accounts/Something-else'
        }];
      }
    };

    var hasStatusCreated = function (hasStatus) {
      if (hasStatus) {
        testAccount.status = 'CREATED';
      }
      else {
        testAccount.status = 'NOT-CREATED';
      }
    };

    var setIsHPAdmin = function (isHPAdmin) {
      mockGlobals.permissions.isHPAdmin = isHPAdmin;
    };

    var setIsCompanyAdmin = function (isCompanyAdmin) {
      mockGlobals.permissions.isCompanyAdmin = isCompanyAdmin;
    };

    var setIsReseller = function (isReseller) {
      mockGlobals.permissions.isReseller = isReseller;
    };

    var testModalResult, modalInstanceSuccess;

    var mockModal = {
      open: function (modalInfo) {
        this.templateUrl = modalInfo.templateUrl;
        this.controller = modalInfo.controller;
        return {
          result: {
            then: function (success) {
              if (modalInstanceSuccess) {
                success(testModalResult);
              }
            }
          }
        };
      },
      templateUrl: undefined,
      controller: undefined
    };

    var mockWindow = {
      location: {
        href: undefined
      }
    };

    var mockResendActivationEmail = {
      startTask: function () {
        var deferred = $q.defer();
        if (resendEmailSuccess) {
          deferred.resolve();
        }
        else {
          deferred.reject();
        }
        return {
          $promise: deferred.promise
        };
      }
    };

    var mockPortalConfigurations = {};

    var meteringInfoId = 'abc123';
    var mockEnrollmentInfos = [{
      licenseKey: 'mock-key',
      dirty: true,
      contractId: 'mockContractId',
      billingCategory: 'INDIA',
      meteringInfo: {
        i18n: 'cNumberOfDevice',
        links: [{
          rel: 'self',
          href: 'api/v2/meteringInfo/' + meteringInfoId
        }]
      },
      isTrial: false,
      licenseEndDate: '2016-08-24T12:15:10.123',
      licenseAdditionalFields: [{
        value: 'mockAddlFieldValue'
      }],
      product: {
        checkoutCommerceProvider: 'MOCK_CHECKOUT_PROVIDER',
        supportedMeteringInfo: [{
          i18n: 'cNumberOfDevice',
          links: [{
            rel: 'self',
            href: 'api/v2/meteringInfo/' + meteringInfoId
          }]
        }, {
          i18n: 'cNumberOfUser',
          links: [{
            rel: 'self',
            href: 'api/v2/meteringInfo/abc123'
          }]
        }],
        links: [{
          rel: 'self',
          href: 'mock-product-id'
        }]
      }
    }];

    var mockShoppingCartId = 'abc123ShoppingCartId';
    var mockShoppingCartService = {
      create: function (payload) {
        mockShoppingCartService.payload = payload;
        payload.links.push({
          rel: 'self',
          href: 'api/v2/shoppingCarts/' + mockShoppingCartId
        });

        if (!payload.items) {
          payload.items = [];
        }

        payload.save = function () {
          var dfd = $q.defer();

          mockShoppingCartService.resolveSave = function () {
            dfd.resolve();
            $timeout.flush();
          };
          mockShoppingCartService.rejectSave = function () {
            dfd.reject();
            $timeout.flush();
          };

          return dfd.promise;
        };
        return payload;
      }
    };

    var actualPollingMessages = {
      pleaseWaitMsg: 'cCreatingLicenseToProduct',
      pleaseWaitLongerMsg: 'cCreatingLicenseTimeout',
      successMsg: 'cSuccessAddingLicense',
      fallbackErrorMsg: 'cErrorCreatingLicense',
      timeoutMsg: 'cCreateLicenseTimeoutFailed'
    };

    var checkoutTaskShouldResolve = true;
    var mockCheckout = {
      startTask: function (payload) {

        // Store off the payload so we can test against it.
        mockCheckout.payload = payload;
        return {
          $promise: $q.when(),
          pollWithMessages: function (pollingMessages) {
            mockCheckout.pollingMessages = pollingMessages;

            var dfd = $q.defer();
            if (checkoutTaskShouldResolve) {
              dfd.resolve();
            }
            else {
              dfd.reject();
            }
            return dfd.promise;
          }
        };
      },
    };

    var mockProductName = 'mock-product-name';
    var mockProductId = 'mock-product-id';
    var mockOfferingFqName = 'mock-offering-fq-name';
    var getMockHPInternalProduct = function () {
      return {
        name: mockProductName,
        isFree: false,
        supportedCommerceProviders: ['GLIS', 'HP_INTERNAL'],
        additionalFields: [{
          name: 'Field 1',
          applicableCommerceProviders: [
            'GLIS'
          ]
        }, {
          name: 'Field 2',
          applicableCommerceProviders: [
            'GLIS'
          ]
        }, {
          name: 'Field 3',
          applicableCommerceProviders: [
            'HP_INTERNAL'
          ]
        }],
        offering: {
          fqName: mockOfferingFqName
        },
        links: [{
          rel: 'self',
          href: mockProductId
        }]
      };
    };

    var getMockBetaProduct = function () {
      return {
        name: mockProductName + '2',
        isFree: false,
        supportedCommerceProviders: ['GLIS', 'BETA'],
        additionalFields: [{
          name: 'Field 1',
          applicableCommerceProviders: [
            'GLIS'
          ]
        }, {
          name: 'Field 2',
          applicableCommerceProviders: [
            'GLIS'
          ]
        }, {
          name: 'Field 4',
          applicableCommerceProviders: [
            'BETA'
          ]
        }],
        offering: {
          fqName: mockOfferingFqName
        },
        links: [{
          rel: 'self',
          href: mockProductId
        }]
      };
    };


    var mockProductService = {
      get: function (params) {
        var products = {};
        var dfd = $q.defer();
        products.data = [getMockHPInternalProduct(), getMockBetaProduct()];
        mockProductService.params = params;
        if (mockProductService.getShouldResolve) {
          dfd.resolve(products);
        }
        else {
          dfd.reject();
        }

        return dfd.promise;
      }
    };

    var mockOffering = 'mock-offering';

    function getMockHPInternalLicense() {
      return {
        product: getMockHPInternalProduct(),
        offering: mockOffering,
        additionalFields: {
          'Field 3': ['HP-Internal-additional-field-value']
        }
      };
    }

    function getMockBetaLicense() {
      return {
        product: getMockBetaProduct(),
        offering: mockOffering,
        additionalFields: {
          'Field 4': ['Beta-additional-field-value']
        }
      };
    }

    var mockLicenseService = {
      get: function (params) {
        var licenses = {};
        var dfd = $q.defer();
        licenses.data = mockLicenseService.licenses;
        mockLicenseService.params = params;
        if (mockLicenseService.getShouldResolve) {
          dfd.resolve(licenses);
        }
        else {
          dfd.reject();
        }

        return dfd.promise;
      }
    };

    var updatedAccount = angular.copy(testAccount);
    updatedAccount.name = 'Changed Man';

    var initMocks = function () {
      mockWindow.location.href = undefined;
      mockModal.templateUrl = undefined;
      mockModal.controller = undefined;
      mockPortalConfigurations['com.portal.logoutUrl'] = '/testLogoutUrl';
      alerts = [];
      modalInstanceSuccess = true;
      testModalResult = 'success';
      emitFunctionRan = false;
      resendEmailSuccess = true;
      adminGetFail = false;
      checkoutTaskShouldResolve = true;
      delete mockShoppingCartService.payload;
      delete mockCheckout.pollingMessages;
      mockProductService.getShouldResolve = true;
      mockLicenseService.getShouldResolve = true;
      delete mockCheckout.payload;
      delete mockCheckout.pollOptions;
      mockLicenseService.licenses = [getMockHPInternalLicense(), getMockBetaLicense()];
      isSupportedCountry = true;
    };

    var initController = function (testSelf) {
      scope = $rootScope.$new();
      if (!testSelf) {
        testSelf = mockSelf;
      }

      AccountDetailsCtrl = controllerLoader('AccountDetailsCtrl', {
        $scope: scope,
        account: testAccount,
        Urls: {
          manageAccounts: function () {
            return '/blah';
          }
        },
        $window: mockWindow,
        $uibModal: mockModal,
        Globals: mockGlobals,
        PortalConfigurations: mockPortalConfigurations,
        UserService: mockUserService,
        ResendActivationEmail: mockResendActivationEmail,
        ProductService: mockProductService,
        LicenseService: mockLicenseService,
        CountrySettings: mockCountrySettings,
        ShoppingCartService: mockShoppingCartService,
        ShoppingCartCheckout: mockCheckout,
        Self: testSelf
      });
    };

    var $rootScope, $q, $timeout, AccountDetailsCtrl, scope, location, controllerLoader, emitFunctionRan, linkParser, adminGetFail;
    beforeEach(inject(function (_$rootScope_, $controller, $location, _$q_, _$timeout_, LinkParser) {
      $rootScope = _$rootScope_;
      controllerLoader = $controller;
      $q = _$q_;
      $timeout = _$timeout_;
      linkParser = LinkParser;
      location = $location;
      initMocks();
    }));

    describe('initController in beforeEach', function () {
      beforeEach(function () {
        initController();
      });

      it('should filter metaTags correctly based on self', function () {
        expect(scope.metaTags.length).toBe(2);
        expect(scope.metaTags.indexOf('TEST_TAG') > -1).toBe(true);
        expect(scope.metaTags.indexOf('EXISTING_TAG') > -1).toBe(true);
      });

      it('should put the account and companyContact on the scope', function () {
        expect(scope.account).toEqual(testAccount);
      });

      it('should successfully update the account and checkout', function () {
        scope.$on('hpDisableAccountDetailsForm', function (event, updateAccount, checkIsInvalid) {
          scope.bindings.enrollmentInfos = angular.copy(mockEnrollmentInfos);
          var promise = updateAccount();
          expect(promise).toBePromise();

          expect(scope.bindings.enrollmentInfos[0].existing).toEqual(true);
          expect(scope.bindings.enrollmentInfos[0].dirty).toEqual(true);
          timeout.flush();
          expect(scope.user).toEqual(testUser);
          expect(checkIsInvalid).toEqual(true);

          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('success');
          expect(alerts[0].msg).toBe('cAccountSaveSuccess');

          mockShoppingCartService.resolveSave();

          expect(scope.bindings.enrollmentInfos[0].dirty).toEqual(false);
          expect(linkParser.getLinkId(mockCheckout.payload, 'shoppingCart')).toBe(mockShoppingCartId);

          var shoppingCartItem = mockShoppingCartService.payload.items[0];
          var mockEnrollmentInfo = mockEnrollmentInfos[0];
          expect(shoppingCartItem.commerceProvider).toEqual(mockEnrollmentInfo.product.checkoutCommerceProvider);
          expect(shoppingCartItem.licenseKey).toEqual(mockEnrollmentInfo.licenseKey);
          expect(shoppingCartItem.contractId).toEqual(mockEnrollmentInfo.contractId);
          expect(shoppingCartItem.billingCategory).toEqual(mockEnrollmentInfo.billingCategory);
          expect(shoppingCartItem.isTrial).toEqual(mockEnrollmentInfo.isTrial);
          expect(shoppingCartItem.licenseAdditionalFields).toEqual(mockEnrollmentInfo.licenseAdditionalFields);
          expect(shoppingCartItem.endDate).toEqual(filter('date')(mockEnrollmentInfo.licenseEndDate, 'yyyy-MM-ddTHH:mm:ss.sssZ'));
          expect(shoppingCartItem.links).toBeDefined();
          expect(shoppingCartItem.links.length).toEqual(2);
          expect(linkParser.getLinkId(shoppingCartItem, 'meteringInfo')).toBe(meteringInfoId);
          emitFunctionRan = true;
        });
        scope.updateAccountSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should fail to save the shoppingCart', function () {
        scope.$on('hpDisableAccountDetailsForm', function (event, updateAccount, checkIsInvalid) {
          scope.bindings.enrollmentInfos = angular.copy(mockEnrollmentInfos);
          var promise = updateAccount();
          expect(promise).toBePromise();

          expect(scope.bindings.enrollmentInfos[0].existing).toEqual(true);
          expect(scope.bindings.enrollmentInfos[0].dirty).toEqual(true);
          timeout.flush();
          expect(scope.user).toEqual(testUser);
          expect(checkIsInvalid).toEqual(true);

          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('success');
          expect(alerts[0].msg).toBe('cAccountSaveSuccess');

          mockShoppingCartService.rejectSave();

          expect(alerts.length).toBe(2);
          expect(alerts[1].type).toBe('danger');
          expect(alerts[1].msg).toBe('cErrorCreatingLicense');

          expect(scope.bindings.enrollmentInfos[0].dirty).toEqual(true);
          emitFunctionRan = true;
        });
        scope.updateAccountSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should fail to checkout the shoppingCart', function () {
        scope.$on('hpDisableAccountDetailsForm', function (event, updateAccount, checkIsInvalid) {
          scope.bindings.enrollmentInfos = angular.copy(mockEnrollmentInfos);
          var promise = updateAccount();
          expect(promise).toBePromise();

          expect(scope.bindings.enrollmentInfos[0].existing).toEqual(true);
          expect(scope.bindings.enrollmentInfos[0].dirty).toEqual(true);
          timeout.flush();
          expect(scope.user).toEqual(testUser);
          expect(checkIsInvalid).toEqual(true);

          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('success');
          expect(alerts[0].msg).toBe('cAccountSaveSuccess');

          mockShoppingCartService.resolveSave();

          expect(scope.bindings.enrollmentInfos[0].dirty).toEqual(true);
          expect(mockCheckout.pollingMessages).toEqual(actualPollingMessages);

          emitFunctionRan = true;
        });
        checkoutTaskShouldResolve = false;
        scope.updateAccountSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should successfully update the account with 5 digit zipcode', function () {
        scope.$on('hpDisableAccountDetailsForm', function (event, updateAccount, checkIsInvalid) {
          testAccount.zipcode = 83646;
          var promise = updateAccount();
          expect(promise).toBeDefined();
          expect(typeof promise).toBe('object');
          expect(typeof promise.then).toBe('function');
          expect(typeof promise.finally).toBe('function');
          expect(typeof promise.catch).toBe('function');
          timeout.flush();
          expect(scope.user).toEqual(testUser);
          expect(checkIsInvalid).toEqual(true);
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('success');
          emitFunctionRan = true;
        });
        scope.updateAccountSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should successfully update the account with 9 digit zipcode', function () {
        scope.$on('hpDisableAccountDetailsForm', function (event, updateAccount, checkIsInvalid) {
          testAccount.zipcode = 836461868;
          var promise = updateAccount();
          expect(promise).toBeDefined();
          expect(typeof promise).toBe('object');
          expect(typeof promise.then).toBe('function');
          expect(typeof promise.finally).toBe('function');
          expect(typeof promise.catch).toBe('function');
          timeout.flush();
          expect(scope.user).toEqual(testUser);
          expect(checkIsInvalid).toEqual(true);
          expect(scope.account).toEqual(testAccount);
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('success');
          emitFunctionRan = true;
        });
        scope.updateAccountSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should successfully update the account with Canadian zipcode', function () {
        scope.$on('hpDisableAccountDetailsForm', function (event, updateAccount, checkIsInvalid) {
          testAccount.country = 'CA';
          testAccount.zipcode = 'A4A 5C5';
          var promise = updateAccount();
          expect(promise).toBeDefined();
          expect(typeof promise).toBe('object');
          expect(typeof promise.then).toBe('function');
          expect(typeof promise.finally).toBe('function');
          expect(typeof promise.catch).toBe('function');
          timeout.flush();
          expect(scope.user).toEqual(testUser);
          expect(checkIsInvalid).toEqual(true);
          expect(scope.account).toEqual(testAccount);
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('success');
          emitFunctionRan = true;
        });
        scope.updateAccountSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should deal with an unsuccessful update', function () {
        shouldFail = true;
        scope.$on('hpDisableAccountDetailsForm', function (event, updateAccount, checkIsInvalid) {
          var promise = updateAccount();
          expect(promise).toBeDefined();
          expect(typeof promise).toBe('object');
          expect(typeof promise.then).toBe('function');
          expect(typeof promise.finally).toBe('function');
          expect(typeof promise.catch).toBe('function');
          timeout.flush();
          expect(scope.user).toEqual(testUser);
          expect(checkIsInvalid).toEqual(true);
          expect(scope.account).toEqual(testAccount);
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('danger');
          emitFunctionRan = true;
        });
        scope.updateAccountSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should deal with an unsuccessful update with 7 digit zipcode', function () {
        shouldFail = true;
        scope.$on('hpDisableAccountDetailsForm', function (event, updateAccount, checkIsInvalid) {

          testAccount.zipcode = 8364618;
          var promise = updateAccount();
          expect(promise).toBeDefined();
          expect(typeof promise).toBe('object');
          expect(typeof promise.then).toBe('function');
          expect(typeof promise.finally).toBe('function');
          expect(typeof promise.catch).toBe('function');
          timeout.flush();
          expect(scope.user).toEqual(testUser);
          expect(checkIsInvalid).toEqual(true);
          expect(scope.account).toEqual(testAccount);
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('danger');
          emitFunctionRan = true;
        });
        scope.updateAccountSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should deal with an unsuccessful update with Canadian zipcode', function () {
        shouldFail = true;
        scope.$on('hpDisableAccountDetailsForm', function (event, updateAccount, checkIsInvalid) {
          testAccount.country = 'CA';
          testAccount.zipcode = 'A4A';
          var promise = updateAccount();
          expect(promise).toBeDefined();
          expect(typeof promise).toBe('object');
          expect(typeof promise.then).toBe('function');
          expect(typeof promise.finally).toBe('function');
          expect(typeof promise.catch).toBe('function');
          timeout.flush();
          expect(scope.user).toEqual(testUser);
          expect(checkIsInvalid).toEqual(true);
          expect(scope.account).toEqual(testAccount);
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('danger');
          emitFunctionRan = true;
        });
        scope.updateAccountSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should deal with an unsuccessful update with 4 digit zipcode', function () {
        shouldFail = true;
        scope.$on('hpDisableAccountDetailsForm', function (event, updateAccount, checkIsInvalid) {

          testAccount.zipcode = 8364;
          var promise = updateAccount();
          expect(promise).toBeDefined();
          expect(typeof promise).toBe('object');
          expect(typeof promise.then).toBe('function');
          expect(typeof promise.finally).toBe('function');
          expect(typeof promise.catch).toBe('function');
          timeout.flush();
          expect(scope.user).toEqual(testUser);
          expect(checkIsInvalid).toEqual(true);
          expect(scope.account).toEqual(testAccount);
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('danger');
          emitFunctionRan = true;
        });
        scope.updateAccountSubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should successfully delete the account and redirect to accounts list', function () {
        scope.$on('hpDisableAccountDetailsForm', function (event, deleteCompany, checkIsInvalid) {
          var promise = deleteCompany();
          expect(promise).toBeDefined();
          expect(typeof promise).toBe('object');
          expect(typeof promise.then).toBe('function');
          expect(typeof promise.finally).toBe('function');
          expect(typeof promise.catch).toBe('function');
          timeout.flush();
          expect(scope.user).toEqual(testUser);
          expect(checkIsInvalid).toEqual(false);
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('success');
          expect(location.url()).toBe('/blah');
          emitFunctionRan = true;
        });
        scope.deleteCompanySubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should fail on delete company', function () {
        deleteFail = true;
        scope.$on('hpDisableAccountDetailsForm', function (event, deleteCompany, checkIsInvalid) {
          var promise = deleteCompany();
          expect(promise).toBeDefined();
          expect(typeof promise).toBe('object');
          expect(typeof promise.then).toBe('function');
          expect(typeof promise.finally).toBe('function');
          expect(typeof promise.catch).toBe('function');
          timeout.flush();
          expect(scope.user).toEqual(testUser);
          expect(checkIsInvalid).toEqual(false);
          expect(alerts.length).toBe(1);
          expect(alerts[0].type).toBe('danger');
          emitFunctionRan = true;
        });
        scope.deleteCompanySubmit();
        expect(emitFunctionRan).toEqual(true);
      });

      it('should use ResendActivationEmail task to resend activation email', function () {
        scope.resendActivationEmail();
        timeout.flush();
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('success');
      });

      it('should use attempt to use the ResendActivationEmail task to resend a activation email and fail', function () {
        resendEmailSuccess = false;
        scope.resendActivationEmail();
        timeout.flush();
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
      });

      it('should open a modal instance with a result of success', function () {
        scope.openTerminateModal();
        expect(scope.modalInstance).toBeDefined();
        expect(mockModal.templateUrl).toBe('app/home/accounts/terminate-modal.html');
        expect(mockModal.controller).toBe('TerminateAccountModalCtrl');
        expect(mockWindow.location.href).toBeDefined();
      });

      it('should silently close the modal window', function () {
        testModalResult = 'cancel';
        scope.openTerminateModal();
        expect(scope.modalInstance).toBeDefined();
        expect(mockModal.templateUrl).toBe('app/home/accounts/terminate-modal.html');
        expect(mockModal.controller).toBe('TerminateAccountModalCtrl');
        expect(mockWindow.location.href).toBeUndefined();
      });

      it('should not redirect to login for incorrect password', function () {
        testModalResult = 409;
        scope.openTerminateModal();
        expect(scope.modalInstance).toBeDefined();
        expect(mockModal.templateUrl).toBe('app/home/accounts/terminate-modal.html');
        expect(mockModal.controller).toBe('TerminateAccountModalCtrl');
        expect(mockWindow.location.href).toBeUndefined();
        expect(alerts.length).toBe(1);
        expect(alerts[0].msg).toBe('cIncorrectPassword');
      });

      it('should default the modal resolution to Terminate Account Error', function () {
        testModalResult = undefined;
        scope.openTerminateModal();
        expect(scope.modalInstance).toBeDefined();
        expect(mockModal.templateUrl).toBe('app/home/accounts/terminate-modal.html');
        expect(mockModal.controller).toBe('TerminateAccountModalCtrl');
        expect(mockWindow.location.href).toBeUndefined();
        expect(alerts.length).toBe(1);
        expect(alerts[0].msg).toBe('cTerminateAccountError');
      });

      it('should successfully change the company contact associated with an account and update the links', function () {
        scope.companyContactChange(updatedUser);
        expect(scope.user).toBe(updatedUser);
        expect(linkParser.getLinkId(scope.account, 'adminContact')).toEqual('updatedUser');
      });

      it('should not show the state and zip code fields with unsupported country and null fields on country switch', function () {
        scope.account.country = 'CA';
        scope.account.state = 'testState';
        scope.account.zipcode = 123456789;
        scope.account.phoneNumber = 123456789;
        scope.$digest();
        expect(scope.countrySettings.isSupported).toBe(true);
        expect(scope.account.state).toBe('testState');
        expect(scope.account.zipcode).toBe(123456789);
        expect(scope.account.phoneNumber).toBe(123456789);

        isSupportedCountry = false;
        scope.account.phoneNumber = 987654321;
        scope.account.country = 'Antarctica';
        scope.$digest();
        scope.account.zipcode = 88888;
        scope.account.state = 'coolState';
        expect(scope.countrySettings.isSupported).toBe(false);
        expect(scope.account.state).toBe('coolState');
        expect(scope.account.zipcode).toBe(88888);
        expect(scope.account.phoneNumber).toBe(987654321);
      });
    });


    it('should only have HP_INTERNAL additionalFields', function () {
      var testSelf = angular.copy(mockSelf);
      testSelf.roles = ['ROLE_HP_RESELLER'];
      mockLicenseService.licenses.length = 0; // Clear out all licenses
      initController(testSelf);

      $timeout.flush();
      expect(scope.bindings.availableProducts.length).toBe(1);
      expect(scope.bindings.availableProducts[0].checkoutCommerceProvider).toBe('HP_INTERNAL');
      expect(scope.bindings.availableProducts[0].additionalFields.length).toBe(1);
      expect(scope.bindings.availableProducts[0].additionalFields[0]).toEqual({
        name: 'Field 3',
        applicableCommerceProviders: [
          'HP_INTERNAL'
        ]
      });
    });

    it('should only have BETA additionalFields', function () {
      var testSelf = angular.copy(mockSelf);
      testSelf.roles = ['ROLE_OFFERING_ADMIN'];
      mockLicenseService.licenses.length = 0; // Clear out all licenses
      initController(testSelf);

      $timeout.flush();
      expect(scope.bindings.availableProducts.length).toBe(1);
      expect(scope.bindings.availableProducts[0].checkoutCommerceProvider).toBe('BETA');
      expect(scope.bindings.availableProducts[0].additionalFields.length).toBe(1);
      expect(scope.bindings.availableProducts[0].additionalFields[0]).toEqual({
        name: 'Field 4',
        applicableCommerceProviders: [
          'BETA'
        ]
      });
    });

    it('should have both HP_INTERNAL and BETA products and additionalFields', function () {
      var testSelf = angular.copy(mockSelf);
      testSelf.roles = ['ROLE_HPADMIN', 'ROLE_HP_RESELLER', 'ROLE_OFFERING_ADMIN'];
      mockLicenseService.licenses.length = 0; // Clear out all licenses
      initController(testSelf);

      timeout.flush();
      expect(scope.bindings.availableProducts.length).toBe(2);

      expect(scope.bindings.availableProducts[0].checkoutCommerceProvider).toBe('HP_INTERNAL');
      expect(scope.bindings.availableProducts[0].additionalFields.length).toBe(1);
      expect(scope.bindings.availableProducts[0].additionalFields[0]).toEqual({
        name: 'Field 3',
        applicableCommerceProviders: [
          'HP_INTERNAL'
        ]
      });

      expect(scope.bindings.availableProducts[1].checkoutCommerceProvider).toBe('BETA');
      expect(scope.bindings.availableProducts[1].additionalFields.length).toBe(1);
      expect(scope.bindings.availableProducts[1].additionalFields[0]).toEqual({
        name: 'Field 4',
        applicableCommerceProviders: [
          'BETA'
        ]
      });
    });

    it('should add existing paid licenses as enrollmentInfos for HP_INTERNAL products', function () {
      var testSelf = angular.copy(mockSelf);
      testSelf.roles = ['ROLE_HP_RESELLER'];
      initController(testSelf);
      timeout.flush();

      expect(scope.bindings.enrollmentInfos.length).toBe(1);
      expect(scope.bindings.enrollmentInfos[0].product.checkoutCommerceProvider).toBe('HP_INTERNAL');
      expect(scope.bindings.enrollmentInfos[0].product.additionalFields.length).toBe(1);
      expect(scope.bindings.enrollmentInfos[0].product.additionalFields[0]).toEqual({
        name: 'Field 3',
        applicableCommerceProviders: [
          'HP_INTERNAL'
        ]
      });
      expect(scope.bindings.enrollmentInfos[0].additionalFieldValues).toEqual({
        'Field 3': 'HP-Internal-additional-field-value'
      });
    });

    it('should add existing paid licenses as enrollmentInfos for BETA products', function () {
      var testSelf = angular.copy(mockSelf);
      testSelf.roles = ['ROLE_OFFERING_ADMIN'];
      initController(testSelf);
      timeout.flush();

      expect(scope.bindings.enrollmentInfos.length).toBe(1);
      expect(scope.bindings.enrollmentInfos[0].product.checkoutCommerceProvider).toBe('BETA');
      expect(scope.bindings.enrollmentInfos[0].product.additionalFields.length).toBe(1);
      expect(scope.bindings.enrollmentInfos[0].product.additionalFields[0]).toEqual({
        name: 'Field 4',
        applicableCommerceProviders: [
          'BETA'
        ]
      });
      expect(scope.bindings.enrollmentInfos[0].additionalFieldValues).toEqual({
        'Field 4': 'Beta-additional-field-value'
      });
    });


    it('should add existing paid licenses as enrollmentInfos for both HP_INTERNAL and BETA products', function () {
      var testSelf = angular.copy(mockSelf);
      testSelf.roles = ['ROLE_HPADMIN', 'ROLE_HP_RESELLER', 'ROLE_OFFERING_ADMIN'];
      initController(testSelf);
      timeout.flush();

      expect(scope.bindings.enrollmentInfos.length).toBe(2);
      expect(scope.bindings.enrollmentInfos[0].product.checkoutCommerceProvider).toBe('HP_INTERNAL');
      expect(scope.bindings.enrollmentInfos[0].product.additionalFields.length).toBe(1);
      expect(scope.bindings.enrollmentInfos[0].product.additionalFields[0]).toEqual({
        name: 'Field 3',
        applicableCommerceProviders: [
          'HP_INTERNAL'
        ]
      });
      expect(scope.bindings.enrollmentInfos[0].additionalFieldValues).toEqual({
        'Field 3': 'HP-Internal-additional-field-value'
      });

      expect(scope.bindings.enrollmentInfos[1].product.checkoutCommerceProvider).toBe('BETA');
      expect(scope.bindings.enrollmentInfos[1].product.additionalFields.length).toBe(1);
      expect(scope.bindings.enrollmentInfos[1].product.additionalFields[0]).toEqual({
        name: 'Field 4',
        applicableCommerceProviders: [
          'BETA'
        ]
      });
      expect(scope.bindings.enrollmentInfos[1].additionalFieldValues).toEqual({
        'Field 4': 'Beta-additional-field-value'
      });
    });


    it('should show delete button', function () {
      setIsMyCompany(false);
      setIsHPAdmin(true);
      initController();
      expect(scope.canDelete).toBe(true);

      setIsMyCompany(false);
      setIsReseller(true);
      setIsHPAdmin(false);
      hasStatusCreated(true);
      initController();
      expect(scope.canDelete).toBe(true);
    });

    it('should show terminate button', function () {
      setIsMyCompany(true);
      setIsHPAdmin(false);
      setIsCompanyAdmin(true);
      setIsReseller(false);
      initController();
      expect(scope.canTerminate).toBe(true);
    });

    it('should not show delete button', function () {
      setIsMyCompany(true);
      initController();
      expect(scope.canDelete).toBe(false);

      setIsMyCompany(false);
      setIsHPAdmin(false);
      setIsReseller(false);
      initController();
      expect(scope.canDelete).toBe(false);

      setIsMyCompany(false);
      setIsHPAdmin(false);
      initController();
      expect(scope.canDelete).toBe(false);

      setIsMyCompany(false);
      setIsHPAdmin(false);
      setIsReseller(true);
      hasStatusCreated(false);
      initController();
      expect(scope.canDelete).toBe(false);

      setIsMyCompany(false);
      setIsHPAdmin(false);
      setIsReseller(false);
      hasStatusCreated(true);
      initController();
      expect(scope.canDelete).toBe(false);
    });

    it('should not show terminate button', function () {
      setIsMyCompany(false);
      initController();
      expect(scope.canTerminate).toBe(false);

      setIsMyCompany(true);
      setIsCompanyAdmin(false);
      initController();
      expect(scope.canTerminate).toBe(false);

      setIsMyCompany(true);
      setIsCompanyAdmin(true);
      setIsHPAdmin(true);
      initController();
      expect(scope.canTerminate).toBe(false);

      setIsMyCompany(true);
      setIsHPAdmin(false);
      setIsReseller(true);
      setIsCompanyAdmin(true);
      initController();
      expect(scope.canTerminate).toBe(false);

      setIsMyCompany(false);
      setIsHPAdmin(false);
      setIsReseller(false);
      hasStatusCreated(true);
      initController();
      expect(scope.canTerminate).toBe(false);
    });
  });

  describe('Controller: TerminateAccountModalCtrl', function () {

    var $q, $timeout, scope, terminateAccountModalCtrl, terminateSuccess;

    var mockModalInstance = {
      close: function (closeReason) {
        this.closed = closeReason;
      },
      closed: undefined,
      dismiss: function () {
        this.dismissed = true;
      },
      dismissed: false
    };

    var mockTerminateAccount = {
      startTask: function () {
        var deferred = $q.defer();
        if (terminateSuccess) {
          deferred.resolve();
        }
        else {
          deferred.reject({
            httpStatus: 403
          });
        }
        return {
          $promise: deferred.promise
        };
      }
    };

    beforeEach(inject(function ($rootScope, $controller, _$q_, _$timeout_) {
      scope = $rootScope.$new();
      $q = _$q_;
      $timeout = _$timeout_;
      mockModalInstance.closed = undefined;
      mockModalInstance.dismissed = false;
      terminateSuccess = true;
      terminateAccountModalCtrl = $controller('TerminateAccountModalCtrl', {
        $scope: scope,
        $uibModalInstance: mockModalInstance,
        TerminateAccount: mockTerminateAccount
      });
    }));

    it('should define all the necessary properties on the scope', function () {
      expect(scope.currentPassword).toBeDefined();
      expect(scope.reason).toBeDefined();
      expect(scope.terminate).toBeDefined();
      expect(scope.cancel).toBeDefined();
    });

    it('should cancel terminating account', function () {
      scope.cancel();
      expect(mockModalInstance.dismissed).toBe(true);
    });

    it('should successfully terminate an account and close the modal instance with a success', function () {
      scope.terminate();
      $timeout.flush();
      expect(mockModalInstance.closed).toBe('success');
    });

    it('should fail to terminate an account and close the modal instance with a 403', function () {
      terminateSuccess = false;
      scope.terminate();
      $timeout.flush();
      expect(mockModalInstance.closed).toBe(403);
    });
  });

});
