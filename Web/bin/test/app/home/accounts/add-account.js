'use strict';

describe('[Module: hp.portal.home.accounts.addAccount]', function () {

  var alerts = [];
  var removedAlerts = [];
  var statusCode = 200;
  var errorCode = null;
  var mockPageAlerts = {
    add: function (alert) {
      alerts.push(alert);
    },
    remove: function (alert) {
      removedAlerts.push(alert);
    },
    timeout: {
      short: 3000,
      medium: 5000,
      long: 10000
    }
  };

  var testProductId = 'abc123';
  var meteringInfoId = 'meteringInfo123';
  var getMockEnrollmentInfo = function () {
    return {
      licenseEndDate: '2016-08-24T12:36:40.423',
      licenseKey: 'mockLicenseKey',
      commerceProvider: 'MOCK_COMMERCE_PROVIDER',
      contractId: 'mockContractId',
      billingCategory: 'US',
      meteringInfo: {
        i18n: 'cNumberOfUser',
        links: [{
          rel: 'self',
          href: 'api/v2/meteringInfo/' + meteringInfoId
        }]
      },
      isTrial: false,
      licenseAdditionalFields: 'test-license-additional-fields',
      product: {
        checkoutCommerceProvider: 'mockCommerceProvider',
        name: 'abc',
        supportedMeteringInfo: [{
          i18n: 'cNumberOfDevice',
          links: [{
            rel: 'self',
            href: 'api/v2/meteringInfos/testId123'
          }]
        }, {
          i18n: 'cNumberOfUser',
          links: [{
            rel: 'self',
            href: 'api/v2/meteringInfos/' + meteringInfoId
          }]
        }],
        links: [{
          rel: 'self',
          href: '/api/v2/products/' + testProductId
        }]
      }
    };
  };

  var mockProductName = 'mock-product-name';
  var mockProductId = 'mock-product-id';
  var mockOfferingFqName = 'mock-offering-fq-name';
  var getMockHPInternalProduct = function () {
    return {
      name: mockProductName,
      isFree: false,
      supportedCommerceProviders: ['GLIS', 'HP_INTERNAL'],
      additionalFields: [{
        name: 'Field 1',
        applicableCommerceProviders: [
          'GLIS'
        ]
      }, {
        name: 'Field 2',
        applicableCommerceProviders: [
          'GLIS'
        ]
      }, {
        name: 'Field 3',
        applicableCommerceProviders: [
          'HP_INTERNAL'
        ]
      }],
      offering: {
        fqName: mockOfferingFqName
      },
      links: [{
        rel: 'self',
        href: mockProductId
      }]
    };
  };

  var getMockBetaProduct = function () {
    return {
      name: mockProductName + '2',
      isFree: false,
      supportedCommerceProviders: ['GLIS', 'BETA'],
      additionalFields: [{
        name: 'Field 1',
        applicableCommerceProviders: [
          'GLIS'
        ]
      }, {
        name: 'Field 2',
        applicableCommerceProviders: [
          'GLIS'
        ]
      }, {
        name: 'Field 4',
        applicableCommerceProviders: [
          'BETA'
        ]
      }],
      offering: {
        fqName: mockOfferingFqName
      },
      links: [{
        rel: 'self',
        href: mockProductId
      }]
    };
  };

  var mockData = {};
  var mockProductService = {
    get: function (params) {
      var dfd = $q.defer();
      mockData.$promise = dfd.promise;
      mockData.data = [getMockHPInternalProduct(), getMockBetaProduct()];
      mockProductService.params = params;
      if (mockProductService.getShouldResolve) {
        dfd.resolve(mockData);
      }
      else {
        dfd.reject();
      }

      return dfd.promise;
    }
  };

  beforeEach(module(function ($provide) {
    $provide.constant('PageAlerts', mockPageAlerts);
  }));

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.home.accounts.addAccount'));

  var httpBackend, $q, timeout, linkParser, filter;
  beforeEach(inject(function ($httpBackend, _$q_, $timeout, LinkParser, $filter) {
    httpBackend = $httpBackend;
    $q = _$q_;
    timeout = $timeout;
    linkParser = LinkParser;
    filter = $filter;
  }));

  var mockCreateAccount = {
    startTask: function (payload) {
      // Store off the payload so we can test against it.
      mockCreateAccount.payload = payload;

      var deferred = $q.defer();
      if (statusCode / 100 !== 2) {
        var task = {
          httpStatus: statusCode
        };

        if (errorCode) {
          var serviceCode = Math.floor(errorCode / 1000);
          task.errors = [{
            serviceCode: serviceCode,
            errorCode: errorCode % 1000
          }];
        }

        deferred.reject(task);
      }
      else {
        deferred.resolve();
      }

      return {
        $promise: deferred.promise
      };
    }
  };

  var actualPollingMessages = {
    pleaseWaitMsg: 'cAddAccountProcessing',
    pleaseWaitLongerMsg: 'cAddAccountTimeout',
    successMsg: 'cAddAccountSuccess',
    fallbackErrorMsg: 'cAddAccountError',
    timeoutMsg: 'cAddAccountTimeoutFailed'
  };

  var waitForCompleteShouldResolve = true;
  var mockCreateAccountAndCheckout = {
    startTask: function (payload) {
      // Store off the payload so we can test against it.
      mockCreateAccountAndCheckout.payload = payload;

      var deferred = $q.defer();
      if (statusCode / 100 !== 2) {
        deferred.reject({
          httpStatus: statusCode
        });
      }
      else {
        deferred.resolve();
      }

      return {
        $promise: deferred.promise,
        pollWithMessages: function (pollingMessages) {
          expect(pollingMessages).toEqual(actualPollingMessages);
          var dfd = $q.defer();
          if (waitForCompleteShouldResolve) {
            dfd.resolve();
          }
          else {
            dfd.reject();
          }
          return dfd.promise;
        }
      };
    }
  };

  var isSupportedCountry;

  var countrySettingsSupported = {
    isSupported: true
  };

  var countrySettingsUnsupported = {
    isSupported: false
  };

  var mockCountrySettings = {
    get: function () {
      if (isSupportedCountry) {
        return countrySettingsSupported;
      }
      else {
        return countrySettingsUnsupported;
      }
    }
  };

  var testUrls = {
    manageAccounts: function () {
      return '/manageAccountsUrl';
    }
  };

  var testResellerId = 'd8c0d964-43c8-4047-9f88-f8eec2c970e3';
  var getTestSelf = function () {
    return {
      links: [{
        rel: 'account',
        href: 'https://localhost:9000/api/v2/accounts/' + testResellerId
      }],
      account: {
        country: 'US',
        metaTags: ['OTHER_DIRECT', 'TEST_TAG']
      },
      getLocale: function () {
        return this.locale;
      },
      locale: 'en-US',
      roles: []
    };
  };

  var mockGlobals = {
    permissions: {
      isHPAdmin: false,
      isReseller: true
    }
  };

  var AddAccountCtrl, controller, scope, location;
  var initializeController = function (testSelf) {
    // Load the controller.
    AddAccountCtrl = controller('AddAccountCtrl', {
      $scope: scope,
      $location: location,
      CreateAccount: mockCreateAccount,
      CreateAccountAndCheckout: mockCreateAccountAndCheckout,
      ProductService: mockProductService,
      Urls: testUrls,
      Self: testSelf,
      CountrySettings: mockCountrySettings,
      Globals: mockGlobals
    });

    scope.account.name = 'Company 2';
    scope.account.country = 'US';
  };

  describe('AddAccountCtrl', function () {

    var emitFunctionRan;
    var fakeEmail = 'fake-email@fake-domain.com';

    beforeEach(inject(function ($rootScope, $controller, $location) {
      scope = $rootScope.$new();
      controller = $controller;
      location = $location;
      mockProductService.getShouldResolve = true;

      spyOn($location, 'search').and.callFake(function () {
        return {
          email: fakeEmail
        };
      });

      // Reset to default state.
      delete mockCreateAccount.payload;
      delete mockCreateAccountAndCheckout.payload;
      delete mockCreateAccountAndCheckout.pollOptions;

      errorCode = null;
      alerts = [];
      removedAlerts = [];
      isSupportedCountry = true;
      emitFunctionRan = false;
      statusCode = 200;
      waitForCompleteShouldResolve = true;

      initializeController(getTestSelf());
    }));

    it('should set the default scope values and functions', function () {
      expect(scope.account.reseller).toBeUndefined();
      expect(linkParser.getLinkId(scope.account, 'reseller')).toBe(testResellerId);
      expect(scope.user.locale).toBeDefined();
      expect(location.search).toHaveBeenCalled();
      expect(scope.user.email).toBeDefined();
      expect(scope.user.email).toBe(fakeEmail);
      expect(scope.locales).toBeDefined();
      expect(scope.updateLocales).toBeDefined();
    });

    it('should filter metaTags correctly based on self', function () {
      expect(scope.metaTags.length).toBe(1);
      expect(scope.metaTags.indexOf('TEST_TAG') > -1).toBe(true);
    });

    it('should only have HP_INTERNAL products and additionalFields', function () {
      var testSelf = getTestSelf();
      testSelf.roles = ['ROLE_HP_RESELLER'];
      initializeController(testSelf);

      timeout.flush();
      expect(scope.bindings.availableProducts.length).toBe(1);
      expect(scope.bindings.availableProducts[0].checkoutCommerceProvider).toBe('HP_INTERNAL');
      expect(scope.bindings.availableProducts[0].additionalFields.length).toBe(1);
      expect(scope.bindings.availableProducts[0].additionalFields[0]).toEqual({
        name: 'Field 3',
        applicableCommerceProviders: [
          'HP_INTERNAL'
        ]
      });
    });

    it('should only have BETA products and additionalFields', function () {
      var testSelf = getTestSelf();
      testSelf.roles = ['ROLE_OFFERING_ADMIN'];
      initializeController(testSelf);

      timeout.flush();
      expect(scope.bindings.availableProducts.length).toBe(1);
      expect(scope.bindings.availableProducts[0].checkoutCommerceProvider).toBe('BETA');
      expect(scope.bindings.availableProducts[0].additionalFields.length).toBe(1);
      expect(scope.bindings.availableProducts[0].additionalFields[0]).toEqual({
        name: 'Field 4',
        applicableCommerceProviders: [
          'BETA'
        ]
      });
    });

    it('should have both HP_INTERNAL and BETA products and additionalFields', function () {
      var testSelf = getTestSelf();
      testSelf.roles = ['ROLE_HPADMIN', 'ROLE_HP_RESELLER', 'ROLE_OFFERING_ADMIN'];
      initializeController(testSelf);

      timeout.flush();
      expect(scope.bindings.availableProducts.length).toBe(2);

      expect(scope.bindings.availableProducts[0].checkoutCommerceProvider).toBe('HP_INTERNAL');
      expect(scope.bindings.availableProducts[0].additionalFields.length).toBe(1);
      expect(scope.bindings.availableProducts[0].additionalFields[0]).toEqual({
        name: 'Field 3',
        applicableCommerceProviders: [
          'HP_INTERNAL'
        ]
      });

      expect(scope.bindings.availableProducts[1].checkoutCommerceProvider).toBe('BETA');
      expect(scope.bindings.availableProducts[1].additionalFields.length).toBe(1);
      expect(scope.bindings.availableProducts[1].additionalFields[0]).toEqual({
        name: 'Field 4',
        applicableCommerceProviders: [
          'BETA'
        ]
      });
    });

    it('should add a single account', function () {
      scope.$on('hpDisableAddAccountForm', function (event, addAccount) {
        addAccount();
        timeout.flush();
        emitFunctionRan = true;
      });
      scope.addAccountSubmit();

      expect(emitFunctionRan).toBe(true);
      expect(scope.account.reseller).toBeUndefined();
      expect(linkParser.getLink(scope.account, 'reseller').href).toBeDefined();
      expect(scope.account.name).toBe('Company 2');
      expect(location.url()).toBe(testUrls.manageAccounts());
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('success');
      expect(alerts[0].msg).toBe('cAddAccountSuccess');
      expect(alerts[0].timeout).toBe(mockPageAlerts.timeout.medium);
    });

    it('should not add an account and error out', function () {
      statusCode = 409;
      scope.$on('hpDisableAddAccountForm', function (event, addAccount) {
        addAccount();
        timeout.flush();
        emitFunctionRan = true;
      });
      scope.addAccountSubmit();

      expect(emitFunctionRan).toBe(true);
      expect(scope.account.reseller).toBeUndefined();
      expect(linkParser.getLink(scope.account, 'reseller').href).toBeDefined();
      expect(scope.account.name).toBe('Company 2');
      expect(location.url()).toBe('');
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('danger');
      expect(alerts[0].path).toBe(undefined);
      expect(alerts[0].msg).toBe('cAddAccountError');
      expect(alerts[0].translateValues.name).toBe(undefined);
      expect(alerts[0].translateValues.errorCode).toBe(statusCode);
      expect(alerts[0].timeout).toBe(mockPageAlerts.timeout.long);
      expect(mockCreateAccount.payload.productIdsAndLicenseKeys).toBeUndefined();
    });


    it('should not add a account and error out with errorCode', function () {
      statusCode = 409;
      errorCode = 1001;
      scope.$on('hpDisableAddAccountForm', function (event, addAccount) {
        addAccount();
        timeout.flush();
        emitFunctionRan = true;
      });
      scope.addAccountSubmit();

      expect(emitFunctionRan).toBe(true);
      expect(scope.account.reseller).toBeUndefined();
      expect(linkParser.getLink(scope.account, 'reseller').href).toBeDefined();
      expect(scope.account.name).toBe('Company 2');
      expect(location.url()).toBe('');
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('danger');
      expect(alerts[0].path).toBe(undefined);
      expect(alerts[0].msg).toBe('cLicenseKeyNotFound');
      expect(alerts[0].timeout).toBe(mockPageAlerts.timeout.long);
    });

    it('should add one account and be ready for a second', function () {
      scope.$on('hpDisableAddAccountForm', function (event, addAccount) {
        addAccount();
        timeout.flush();
        emitFunctionRan = true;
      });
      scope.addAccountAndNewSubmit();

      expect(emitFunctionRan).toBe(true);
      expect(scope.account.reseller).toBeUndefined();
      expect(linkParser.getLink(scope.account, 'reseller').href).toBeDefined();
      expect(scope.account.name).toBe(undefined);
      expect(location.url()).toBe('');
      expect(alerts.length).toBe(1);
      expect(alerts[0].path).toBe(undefined);
      expect(alerts[0].type).toBe('success');
      expect(alerts[0].msg).toBe('cAddAccountSuccess');
      expect(alerts[0].timeout).toBe(mockPageAlerts.timeout.medium);
      emitFunctionRan = true;
    });

    it('should add an account with pre-licensed products', function () {
      scope.$on('hpDisableAddAccountForm', function (event, addAccount) {
        addAccount();
        timeout.flush();
        emitFunctionRan = true;
      });

      var mockEnrollmentInfo = getMockEnrollmentInfo();
      scope.bindings.enrollmentInfos.push(mockEnrollmentInfo);
      scope.addAccountSubmit();

      expect(emitFunctionRan).toBe(true);
      expect(scope.account.reseller).toBeUndefined();
      expect(linkParser.getLink(scope.account, 'reseller').href).toBeDefined();
      expect(scope.account.name).toBe('Company 2');
      expect(location.url()).toBe(testUrls.manageAccounts());

      expect(mockCreateAccountAndCheckout.payload.shoppingCartItems).toBeDefined();
      expect(mockCreateAccountAndCheckout.payload.shoppingCartItems instanceof Array).toBe(true);

      var payloadProductEnrollment = mockCreateAccountAndCheckout.payload.shoppingCartItems[0];
      expect(payloadProductEnrollment.commerceProvider).toEqual(mockEnrollmentInfo.product.checkoutCommerceProvider);
      expect(payloadProductEnrollment.licenseKey).toEqual(mockEnrollmentInfo.licenseKey);
      expect(payloadProductEnrollment.contractId).toEqual(mockEnrollmentInfo.contractId);
      expect(payloadProductEnrollment.billingCategory).toEqual(mockEnrollmentInfo.billingCategory);
      expect(payloadProductEnrollment.isTrial).toEqual(mockEnrollmentInfo.isTrial);
      expect(payloadProductEnrollment.licenseAdditionalFields).toEqual(mockEnrollmentInfo.licenseAdditionalFields);
      expect(payloadProductEnrollment.endDate).toEqual(filter('date')(mockEnrollmentInfo.licenseEndDate, 'yyyy-MM-ddTHH:mm:ss.sssZ'));
      expect(payloadProductEnrollment.links).toBeDefined();
      expect(payloadProductEnrollment.links.length).toEqual(2);
      expect(linkParser.getLinkId(payloadProductEnrollment, 'meteringInfo')).toBe(meteringInfoId);
    });

    it('should not show the state and zip code fields with unsupported country and null fields on country switch', function () {
      isSupportedCountry = false;
      scope.account.phoneNumber = 123456789;
      scope.account.country = undefined;
      scope.$digest();
      expect(scope.countrySettings).toBe(undefined);
      expect(scope.account.state).toBe(null);
      expect(scope.account.zipcode).toBe(null);
      expect(scope.account.phoneNumber).toBe(123456789);

      isSupportedCountry = true;
      scope.account.country = 'CA';
      scope.$digest();
      scope.account.state = 'testState';
      scope.account.zipcode = 123456789;
      expect(scope.countrySettings.isSupported).toBe(true);
      expect(scope.account.state).toBe('testState');
      expect(scope.account.zipcode).toBe(123456789);
      expect(scope.account.phoneNumber).toBe(123456789);

      isSupportedCountry = false;
      scope.account.country = 'Antarctica';
      scope.$digest();
      expect(scope.countrySettings.isSupported).toBe(false);
      expect(scope.account.state).toBe(null);
      expect(scope.account.zipcode).toBe(null);
      expect(scope.account.phoneNumber).toBe(123456789);
    });

    it('should cancel AddAccount and return testUrl', function () {
      scope.cancelAddAccount();
      expect(location.url()).toEqual(testUrls.manageAccounts());
    });

    //To be enabled when we support regionalized languages.
    // it('should set up locales correctly for US', function () {
    //   scope.updateLocales();
    //   expect(scope.user.locale).toBe('en-US');
    // });

    // it('should set up locales correctly for CA', function () {
    //   scope.account.country = 'CA';
    //   scope.updateLocales();
    //   expect(scope.user.locale).toBe('en-CA');
    // });

    it('should ensure that we have the same Locale selections for all countries', function () {
      scope.updateLocales();
      var oldLocales = scope.locales;
      scope.account.country = 'CA';
      scope.updateLocales();
      expect(scope.locales).toBe(oldLocales);
    });
  });
});
