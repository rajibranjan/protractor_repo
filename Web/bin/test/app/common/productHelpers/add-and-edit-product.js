'use strict';

describe('Factory: AddAndEditProduct', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.productHelpers.addAndEditProduct'));

  beforeEach(module(function ($provide) {
    $provide.constant('AddProductToAccountModal', mockAddProductToAccountModal);
    $provide.constant('ValidateLicenseKey', mockValidateLicenseKey);
  }));

  var AddAndEditProduct, $timeout, $q;

  beforeEach(inject(function (_$timeout_, _AddAndEditProduct_, _$q_) {
    $timeout = _$timeout_;
    AddAndEditProduct = _AddAndEditProduct_;
    $q = _$q_;
    delete mockAddProductToAccountModal.data;
    delete mockAddProductToAccountModal.shouldDelete;
    delete mockAddProductToAccountModal.shouldResolve;
    delete mockValidateLicenseKey.payload;
  }));

  var mockAddProductToAccountModal = {
    open: function (data) {
      mockAddProductToAccountModal.data = data;

      var dfd = $q.defer();
      if (this.shouldResolve) {
        dfd.resolve(data.enrollmentInfo);
      }
      else {
        var operation;
        if (this.shouldDelete) {
          operation = 'delete';
        }
        else {
          operation = 'cancel';
        }

        dfd.reject({
          operation: operation,
          enrollmentInfo: data.enrollmentInfo
        });
      }
      return dfd.promise;
    }
  };

  var mockProduct1 = {
    name: 'Product 1'
  };

  var mockProduct2 = {
    name: 'Product 2'
  };

  var mockEnrollmentInfo = {
    product: mockProduct1,
    licenseKey: 'abc-123'
  };

  var mockValidateLicenseKey = {
    startTask: function (payload) {
      mockValidateLicenseKey.payload = payload;

      var dfd = $q.defer();
      if (this.validateLicenseKeyShouldResolve) {
        dfd.resolve();
      }
      else {
        dfd.reject();
      }

      return {
        $promise: dfd.promise
      };
    }
  };

  it('should openProductModal with given parameters', function () {
    var availableProducts = 'mock-available-products';
    var shoppingCartItems = [];

    AddAndEditProduct.editProduct(mockEnrollmentInfo, availableProducts, shoppingCartItems);

    expect(mockAddProductToAccountModal.data.enrollmentInfo).toBe(mockEnrollmentInfo);
    expect(mockAddProductToAccountModal.data.products).toBe(availableProducts);
  });

  it('should add the new enrollmentInfo with pre-licensed products with valid commerce provider', function () {
    mockAddProductToAccountModal.shouldResolve = true;
    mockValidateLicenseKey.validateLicenseKeyShouldResolve = true;

    var availableProducts = [mockProduct1, mockProduct2];
    var shoppingCartItems = [];
    var testLicenseKey = 'test-license-key';
    var testProductId = 'abc123';

    var enrollmentInfo = angular.copy(mockEnrollmentInfo);
    enrollmentInfo.licenseKey = testLicenseKey;
    enrollmentInfo.product = {
      links: [{
        rel: 'self',
        href: '/api/v2/products/' + testProductId
      }]
    };

    AddAndEditProduct.editProduct(enrollmentInfo, availableProducts, shoppingCartItems);
    expect(enrollmentInfo.isLicenseKeyValid).toBeUndefined();

    $timeout.flush();

    expect(enrollmentInfo.isLicenseKeyValid).toBe(true);
    expect(enrollmentInfo.validating).toBe(false);
    expect(enrollmentInfo.licenseKey).toBe(testLicenseKey);
    expect(mockValidateLicenseKey.payload.licenseKey).toBe(testLicenseKey);
    expect(mockValidateLicenseKey.payload.productId).toBe(testProductId);
  });

  it('should not ValidateLicenseKey if no licenseKey is found in enrollmentInfo', function () {
    mockAddProductToAccountModal.shouldResolve = true;
    mockValidateLicenseKey.validateLicenseKeyShouldResolve = true;

    var availableProducts = [mockProduct1, mockProduct2];
    var shoppingCartItems = [];

    var enrollmentInfo = angular.copy(mockEnrollmentInfo);
    delete enrollmentInfo.licenseKey;

    AddAndEditProduct.editProduct(enrollmentInfo, availableProducts, shoppingCartItems);
    expect(enrollmentInfo.isLicenseKeyValid).toBeUndefined();

    $timeout.flush();

    expect(enrollmentInfo.isLicenseKeyValid).toBeUndefined();
  });

  it('should edit product with pre-licensed products with invalid commerce provider', function () {
    mockAddProductToAccountModal.shouldResolve = true;
    mockValidateLicenseKey.validateLicenseKeyShouldResolve = false;

    var availableProducts = [mockProduct1, mockProduct2];
    var shoppingCartItems = [];

    var testLicenseKey = 'test-license-key';
    var testProductId = 'abc123';

    var enrollmentInfo = angular.copy(mockEnrollmentInfo);
    enrollmentInfo.licenseKey = testLicenseKey;
    enrollmentInfo.product = {
      links: [{
        rel: 'self',
        href: '/api/v2/products/' + testProductId
      }]
    };

    AddAndEditProduct.editProduct(enrollmentInfo, availableProducts, shoppingCartItems);
    expect(enrollmentInfo.isLicenseKeyValid).toBeUndefined();

    $timeout.flush();
    expect(enrollmentInfo.isLicenseKeyValid).toBe(false);
    expect(mockValidateLicenseKey.payload.licenseKey).toBe(testLicenseKey);
    expect(mockValidateLicenseKey.payload.productId).toBe(testProductId);
  });

  it('should remove product from availableProducts and add it to shoppingCartItems on productModal save', function () {
    mockAddProductToAccountModal.shouldResolve = true;
    mockValidateLicenseKey.validateLicenseKeyShouldResolve = true;

    var availableProducts = [mockProduct1, mockProduct2];
    var shoppingCartItems = [];

    AddAndEditProduct.editProduct(mockEnrollmentInfo, availableProducts, shoppingCartItems);

    $timeout.flush();

    expect(availableProducts.length).toBe(1);
    expect(availableProducts[0]).toBe(mockProduct2);
    expect(shoppingCartItems.length).toBe(1);
    expect(shoppingCartItems[0]).toBe(mockEnrollmentInfo);
  });

  it('should remove product from shoppingCartItems and add it back to availableProducts on productModal delete', function () {
    mockAddProductToAccountModal.shouldResolve = false;
    mockAddProductToAccountModal.shouldDelete = true;

    var availableProducts = [mockProduct2];
    var shoppingCartItems = [mockEnrollmentInfo];

    AddAndEditProduct.editProduct(mockEnrollmentInfo, availableProducts, shoppingCartItems);

    $timeout.flush();

    expect(availableProducts.length).toBe(2);
    expect(availableProducts[1]).toBe(mockEnrollmentInfo.product);
    expect(shoppingCartItems.length).toBe(0);
  });

  it('should not remove product from shoppingCartItems on productModal cancel', function () {
    mockAddProductToAccountModal.shouldResolve = false;
    mockAddProductToAccountModal.shouldDelete = false;

    var availableProducts = [mockProduct2];
    var shoppingCartItems = [mockEnrollmentInfo];

    AddAndEditProduct.editProduct(mockEnrollmentInfo, availableProducts, shoppingCartItems);

    $timeout.flush();

    expect(availableProducts.length).toBe(1);
    expect(shoppingCartItems.length).toBe(1);
    expect(shoppingCartItems[0]).toBe(mockEnrollmentInfo);
  });
});
