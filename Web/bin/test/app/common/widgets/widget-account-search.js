'use strict';

describe('[Module: hp.portal.common.widgets.widgetAccountSearch]', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.widgets.widgetAccountSearch'));

  var mockCompanyAdmin = {
    limit: 3,
    offset: 0,
    total: 1,
    data: [{
      createdBy: 'a47c78e8-32f1-4517-9af6-19ac266d161a',
      createdDate: '2014-05-08T21:29:51.000+0000',
      lastModifiedBy: 'a47c78e8-32f1-4517-9af6-19ac266d161a',
      lastModifiedDate: '2014-05-08T21:29:51.000+0000',
      tenantId: null,
      email: 'admin@company.com',
      firstName: 'Joe',
      lastName: 'Administrator',
      roles: [
        'ROLE_USER',
        'ROLE_COMPANY_ADMIN'
      ],
      status: 'ACTIVE',
      accountId: '25020aab-ff98-4cb0-90dd-15361af903cb',
      locale: 'en-US',
      optedIntoEmail: false,
      phoneNumber: '1-208-396-6000',
      phoneExtension: null,
      links: [{
        rel: 'self',
        href: 'http://localhost:9000/api/v2/users/1c4ede71-55b9-4cd1-a918-5e0329677f0f'
      }, {
        rel: 'account',
        href: 'http://localhost:9000/api/v2/resellers/25020aab-ff98-4cb0-90dd-15361af903cb'
      }]
    }]
  };

  var mockUser = {
    limit: 3,
    offset: 0,
    total: 1,
    data: [{
      createdBy: 'a47c78e8-32f1-4517-9af6-19ac266d161a',
      createdDate: '2014-05-08T21:29:51.000+0000',
      lastModifiedBy: 'a47c78e8-32f1-4517-9af6-19ac266d161a',
      lastModifiedDate: '2014-05-08T21:29:51.000+0000',
      tenantId: null,
      email: 'user@company.com',
      firstName: 'Joe',
      lastName: 'Average User',
      roles: [
        'ROLE_USER'
      ],
      status: 'ACTIVE',
      accountId: '25020aab-ff98-4cb0-90dd-15361af903cb',
      locale: 'en-US',
      optedIntoEmail: false,
      phoneNumber: '1-208-396-6000',
      phoneExtension: null,
      links: [{
        rel: 'self',
        href: 'http://localhost:9000/api/v2/users/1c4ede71-55b9-4cd1-a918-5e0329677f0f'
      }, {
        rel: 'account',
        href: 'http://localhost:9000/api/v2/resellers/25020aab-ff98-4cb0-90dd-15361af903cb'
      }]
    }]
  };

  describe('Controller: AccountSearchWidgetCtrl', function () {

    var redirectPath;
    var fakeEmail = 'doesnt@matter.com';

    function MockUserService($q) {
      this.deferred = $q.defer();
      this.get = function () {
        return this.deferred.promise;
      };
    }

    var scope, location, userService, rolesHelper, controller;
    beforeEach(inject(function ($rootScope, $location, $q, $controller, RolesHelper) {
      scope = $rootScope.$new();
      location = $location;
      userService = new MockUserService($q);
      controller = $controller;
      rolesHelper = RolesHelper;
      spyOn(userService, 'get').and.callThrough();
      spyOn($location, 'path').and.callFake(function (path) {
        redirectPath = path;
        return $location;
      });
      spyOn($location, 'search').and.callThrough();
    }));

    var initController = function () {
      controller('AccountSearchWidgetCtrl', {
        $scope: scope,
        $location: location,
        UserService: userService,
        RolesHelper: rolesHelper
      });
    };

    it('should search for admin user', function () {
      initController();
      scope.searchAccounts('admin@company.com');

      userService.deferred.resolve(mockCompanyAdmin);
      scope.$apply();

      expect(scope.user).toBeDefined();
      expect(scope.user.isAdmin).toBe(true);
      expect(scope.user.isReseller).toBe(false);
      expect(scope.user.isActive).toBe(true);

      scope.goToAccount();

      expect(location.path).toHaveBeenCalled();
      expect(redirectPath).toBe('/accounts/' + scope.user.accountId);

      expect(userService.get).toHaveBeenCalled();
    });

    it('should search for regular user', function () {
      initController();

      scope.searchAccounts('user@company.com');

      userService.deferred.resolve(mockUser);
      scope.$apply();

      expect(scope.user).toBeDefined();
      expect(scope.user.isAdmin).toBe(false);
      expect(scope.user.isReseller).toBe(false);
      expect(scope.user.isActive).toBe(true);

      expect(userService.get).toHaveBeenCalled();
    });

    it('should not find any users', function () {
      var users = angular.copy(mockUser);
      users.data.shift();
      initController();

      scope.searchAccounts(fakeEmail);

      userService.deferred.resolve(users);
      scope.$apply();

      expect(scope.user).toBeNull();
      expect(userService.get).toHaveBeenCalled();
    });

    it('should redirect to create account page', function () {
      initController();
      scope.createAccount(fakeEmail);
      expect(location.path).toHaveBeenCalled();
      expect(redirectPath).toBe('/accounts/add-account');
      expect(location.search).toHaveBeenCalled();
      expect(location.search().email).toBeDefined();
      expect(location.search().email).toBe(fakeEmail);
    });

    it('should not be an active user', function () {
      mockUser.data[0].status = 'SUSPENDED';

      initController();

      scope.searchAccounts('user@company.com');
      userService.deferred.resolve(mockUser);
      scope.$apply();

      expect(scope.user).toBeDefined();
      expect(scope.user.isAdmin).toBe(false);
      expect(scope.user.isActive).toBe(false);

      expect(userService.get).toHaveBeenCalled();
    });

    it('should be a reseller account', function () {
      mockCompanyAdmin.data[0].roles.push('ROLE_RESELLER');

      initController();

      scope.searchAccounts('reseller@company.com');
      userService.deferred.resolve(mockCompanyAdmin);
      scope.$apply();

      expect(scope.user).toBeDefined();
      expect(scope.user.isAdmin).toBe(true);
      expect(scope.user.isReseller).toBe(true);
      expect(scope.user.isActive).toBe(true);

      expect(userService.get).toHaveBeenCalled();
    });
  });
});
