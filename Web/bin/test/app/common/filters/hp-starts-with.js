'use strict';

describe('[Module: hp.common.filters.hpStartsWith]', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.common.filters.hpStartsWith'));

  describe('Filter: hp-state-field', function () {

    var stateTexas = {
      name: 'Texas',
      country: 'US',
      abbreviation: 'TX'
    };
    var stateAlabama = {
      name: 'Alabama',
      country: 'US',
      abbreviation: 'AL'
    };
    var stateAlberta = {
      name: 'Alberta',
      country: 'CA',
      abbreviation: 'AL'
    };

    var testStates = [
      stateTexas,
      stateAlabama,
      stateAlberta
    ];

    var testStatesObjects = {
      texas: stateTexas,
      alabama: stateAlabama,
      alberta: stateAlberta
    };

    var testArrayElements = ['uTestElement', 'aTestElement'];

    var testInteger = 123;

    var testIntArray = [1, 2];

    it('should only find values that start with a lower case letter', inject(function ($filter) {
      expect($filter('hpStartsWith')(testStates, 't', 'name')).toEqual([stateTexas]);
    }));

    it('should only find values that start with a capital letter and more than one returned object', inject(function ($filter) {
      expect($filter('hpStartsWith')(testStates, 'A', 'name')).toEqual([stateAlabama, stateAlberta]);
    }));

    it('should not return any states when U or C is entered', inject(function ($filter) {
      expect($filter('hpStartsWith')(testStates, 'u', 'name')).toEqual([]);
      expect($filter('hpStartsWith')(testStates, 'U', 'name')).toEqual([]);
      expect($filter('hpStartsWith')(testStates, 'c', 'name')).toEqual([]);
      expect($filter('hpStartsWith')(testStates, 'C', 'name')).toEqual([]);
    }));

    it('should return no objects if nothing is entered into the filter', inject(function ($filter) {
      expect($filter('hpStartsWith')(testStates, ' ', 'name')).toEqual([]);
    }));

    it('should return elements even if the list of values is only an array, not an array of objects', inject(function ($filter) {
      expect($filter('hpStartsWith')(testArrayElements, 'u')).toEqual(['uTestElement']);
      expect($filter('hpStartsWith')(testArrayElements, 'a')).toEqual(['aTestElement']);
      expect($filter('hpStartsWith')(testArrayElements, 'b')).toEqual([]);
    }));

    it('should return no objects if the values are not strings', inject(function ($filter) {
      expect($filter('hpStartsWith')(testIntArray, 1)).toEqual([]);
    }));

    it('should return no objects if the key does not exist on the object', inject(function ($filter) {
      expect($filter('hpStartsWith')(testStates, 'u', 'testInvalidKey')).toEqual([]);
    }));

    it('should return no objects if the values are not iterable', inject(function ($filter) {
      expect($filter('hpStartsWith')(testInteger, 'u')).toEqual([]);
    }));

    it('should return the correct objects if the values used are objects with objects', inject(function ($filter) {
      expect($filter('hpStartsWith')(testStatesObjects, 'a', 'name')).toEqual([stateAlabama, stateAlberta]);
    }));
  });
});
