'use strict';

describe('[Module: hp.portal.common.forms.country]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.forms.country'));
  beforeEach(module('hp.templates'));

  var scope, compile;
  var elementText = '<div id="countryValue" hp-country-field="account.country" required></div>';
  var mockAccount = {
    country: 'US'
  };

  beforeEach(inject(function ($compile) {
    compile = $compile;
  }));

  describe('Directive: hp-country-field', function () {
    var element;
    beforeEach(inject(function ($rootScope) {
      scope = $rootScope.$new();
      element = compile(elementText)(scope);
      scope.account = mockAccount;
      scope.$digest();
    }));

    it('should have a selectedValue of US', function () {
      var elementScope = element.scope();
      expect(elementScope.selectedValue).toBe('US');
    });
  });
});
