'use strict';

describe('[Module: hp.portal.common.forms.userForm]', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.forms.userForm'));
  beforeEach(module('hp.templates'));

  beforeEach(module(function ($provide) {
    $provide.provider('Globals', function () {
      this.$get = function () {
        return {
          permissions: {}
        };
      };
    });
  }));

  var compile, scope;
  var testUser = {
    phoneNumber: '1234567890',
    phoneExtension: '',
    locale: 'en-US',
    firstName: 'name',
    lastName: 'lName',
    email: 'email@email.com',
    status: 'ACTIVE'

  };

  var defaultInitParameters = {
    user: testUser,
    showEmail: true,
    showLocales: true,
    showStatus: true,
    showRoles: true,
    isAdding: true
  };

  // Could not pass in Locale to an inject function, so manually listing all locales at this time.
  var locales = [{
    lang: 'en-US',
    name: 'English'
  }, {
    lang: 'fr-CA',
    name: 'français'
  }, {
    lang: 'it-IT',
    name: 'italiano'
  }, {
    lang: 'de-DE',
    name: 'Deutsch'
  }, {
    lang: 'es-ES',
    name: 'español'
  }, {
    lang: 'pt-BR',
    name: 'português (BR)'
  }, {
    lang: 'pt-PT',
    name: 'português (PT)'
  }, {
    lang: 'zh-CN',
    name: '中国 (CN)'
  }, {
    lang: 'zh-TW',
    name: '中國 (TW)'
  }, ];

  beforeEach(inject(function ($compile) {
    compile = $compile;
  }));

  var mockElement, RolesConst, globals;
  describe('Directive: hp-user-form', function () {
    beforeEach(inject(function ($rootScope, Roles, Globals) {
      scope = $rootScope.$new();
      RolesConst = Roles;
      globals = Globals;
    }));

    var initDirective = function (initParams) {
      initParams = angular.copy(initParams);
      scope.user = initParams.user;

      var testElemString = '<div hp-user-form show-email="' + initParams.showEmail + '" show-locales="' + initParams.showLocales + '" show-status="' + initParams.showStatus + '" show-roles="' + initParams.showRoles + '" user="user" is-adding="' + initParams.isAdding + '"></div>';
      mockElement = compile(testElemString)(scope);

      //-- TODO: Need to possibly add this functionality to unitTestInit module to extend all angular elements with the method (in unit tests only)//
      mockElement.directiveElementAttributeTestHelper = function (elementType, attributeToMatch, expectedValue) {
        var elementTestResult = false;
        var elementTestSearch = this.find(elementType);
        angular.forEach(elementTestSearch, function (matchedTag) {
          elementTestResult = angular.element(matchedTag).attr(attributeToMatch) === expectedValue || elementTestResult;
        });
        return elementTestResult;
      };
      scope.$digest();
    };

    it('should set up roles correctly with hpadmin true', function () {
      globals.permissions.isHPAdmin = true;

      initDirective(defaultInitParameters);

      var elementScope = mockElement.isolateScope();
      expect(elementScope._showRoles).toBe(true);

      elementScope.roles.forEach(function (role) {
        switch (role.id) {
          case 'CompanyAdmin':
            expect(role.show).toBe(true);
            expect(role.value).toBe(RolesConst.COMPANY_ADMIN);
            break;
          case 'Reseller':
            expect(role.show).toBe(false);
            expect(role.value).toBe(RolesConst.RESELLER);
            break;
          case 'OfferingAdmin':
            expect(role.show).toBe(true);
            expect(role.value).toBe(RolesConst.OFFERING_ADMIN);
            break;
          case 'HPAdmin':
            expect(role.show).toBe(true);
            expect(role.value).toBe(RolesConst.HP_ADMIN);
            break;
        }
      });
    });

    it('should set up roles correctly with hpadmin false', function () {
      globals.permissions.isHPAdmin = false;

      initDirective(defaultInitParameters);

      var elementScope = mockElement.isolateScope();
      expect(elementScope._showRoles).toBe(true);

      elementScope.roles.forEach(function (role) {
        switch (role.id) {
          case 'CompanyAdmin':
            expect(role.show).toBe(true);
            expect(role.value).toBe(RolesConst.COMPANY_ADMIN);
            break;
          case 'Reseller':
            expect(role.show).toBe(false);
            expect(role.value).toBe(RolesConst.RESELLER);
            break;
          case 'OfferingAdmin':
            expect(role.show).toBe(false);
            expect(role.value).toBe(RolesConst.OFFERING_ADMIN);
            break;
          case 'HPAdmin':
            expect(role.show).toBe(false);
            expect(role.value).toBe(RolesConst.HP_ADMIN);
            break;
        }
      });
    });

    it('should set up roles correctly with hpadmin true but isAdding false', function () {
      globals.permissions.isHPAdmin = true;

      initDirective(angular.extend({}, defaultInitParameters, {
        isAdding: false
      }));

      var elementScope = mockElement.isolateScope();
      expect(elementScope._showRoles).toBe(true);

      elementScope.roles.forEach(function (role) {
        switch (role.id) {
          case 'CompanyAdmin':
            expect(role.show).toBe(true);
            expect(role.value).toBe(RolesConst.COMPANY_ADMIN);
            break;
          case 'Reseller':
            expect(role.show).toBe(false);
            expect(role.value).toBe(RolesConst.RESELLER);
            break;
          case 'OfferingAdmin':
            expect(role.show).toBe(true);
            expect(role.value).toBe(RolesConst.OFFERING_ADMIN);
            break;
          case 'HPAdmin':
            expect(role.show).toBe(false);
            expect(role.value).toBe(RolesConst.HP_ADMIN);
            break;
        }
      });
    });

    it('should set up roles correctly with reseller true', function () {
      globals.permissions.isHPAdmin = false;
      globals.permissions.isReseller = true;

      initDirective(defaultInitParameters);
      var elementScope = mockElement.isolateScope();

      expect(elementScope._showRoles).toBe(false);

      elementScope.roles.forEach(function (role) {
        switch (role.id) {
          case 'CompanyAdmin':
            expect(role.show).toBe(true);
            expect(role.value).toBe(RolesConst.COMPANY_ADMIN);
            break;
          case 'Reseller':
            expect(role.show).toBe(true);
            expect(role.value).toBe(RolesConst.RESELLER);
            break;
          case 'OfferingAdmin':
            expect(role.show).toBe(false);
            expect(role.value).toBe(RolesConst.OFFERING_ADMIN);
            break;
          case 'HPAdmin':
            expect(role.show).toBe(false);
            expect(role.value).toBe(RolesConst.HP_ADMIN);
            break;
        }
      });

      expect(scope.user.roles).toEqual([RolesConst.COMPANY_ADMIN, RolesConst.RESELLER]);
    });

    it('should set up roles correctly with reseller true and isAdding false', function () {
      globals.permissions.isHPAdmin = false;
      globals.permissions.isReseller = true;

      initDirective(angular.extend({}, defaultInitParameters, {
        isAdding: false
      }));
      var elementScope = mockElement.isolateScope();

      expect(elementScope._showRoles).toBe(false);

      elementScope.roles.forEach(function (role) {
        switch (role.id) {
          case 'CompanyAdmin':
            expect(role.show).toBe(true);
            expect(role.value).toBe(RolesConst.COMPANY_ADMIN);
            break;
          case 'Reseller':
            expect(role.show).toBe(true);
            expect(role.value).toBe(RolesConst.RESELLER);
            break;
          case 'OfferingAdmin':
            expect(role.show).toBe(false);
            expect(role.value).toBe(RolesConst.OFFERING_ADMIN);
            break;
          case 'HPAdmin':
            expect(role.show).toBe(false);
            expect(role.value).toBe(RolesConst.HP_ADMIN);
            break;
        }
      });

      expect(scope.user.roles).toBeUndefined();
    });

    it('should display the full form, without new user functionality, when a user is passed and all arguments except isAdding are set to true', function () {
      var initParameters = {
        user: testUser,
        showEmail: true,
        showLocales: true,
        showStatus: true,
        showRoles: true,
        isAdding: false
      };
      initDirective(initParameters);

      var elementScope = mockElement.isolateScope();
      var statusIncluded = mockElement.directiveElementAttributeTestHelper('label', 'translate', 'cStatus');
      var HPAdminRoleIncluded = mockElement.directiveElementAttributeTestHelper('input', 'name', 'roleHPAdmin');
      var emailIncluded = mockElement.directiveElementAttributeTestHelper('label', 'translate', 'cEmail');
      var emailVerifyIncluded = mockElement.directiveElementAttributeTestHelper('label', 'translate', 'cVerifyEmail');
      expect(elementScope.user.status).toEqual('ACTIVE');
      expect(statusIncluded).toEqual(true);
      expect(HPAdminRoleIncluded).toEqual(false);
      expect(emailIncluded).toEqual(true);
      expect(emailVerifyIncluded).toEqual(false);
    });
    it('should display basic info and status, but not email, locales and roles', function () {
      var initParameters = {
        user: testUser,
        showEmail: false,
        showLocales: false,
        showStatus: true,
        showRoles: false,
        isAdding: false
      };
      initDirective(initParameters);

      var elementScope = mockElement.isolateScope();
      var statusIncluded = mockElement.directiveElementAttributeTestHelper('label', 'translate', 'cStatus');
      var HPAdminRoleIncluded = mockElement.directiveElementAttributeTestHelper('input', 'name', 'roleHPAdmin');
      var emailIncluded = mockElement.directiveElementAttributeTestHelper('label', 'translate', 'cEmail');
      var emailVerifyIncluded = mockElement.directiveElementAttributeTestHelper('label', 'translate', 'cVerifyEmail');
      var basicInfoIncluded = mockElement.directiveElementAttributeTestHelper('label', 'for', 'firstNameValue');
      expect(elementScope.user.status).toEqual('ACTIVE');
      expect(statusIncluded).toEqual(true);
      expect(HPAdminRoleIncluded).toEqual(false);
      expect(emailIncluded).toEqual(false);
      expect(emailVerifyIncluded).toEqual(false);
      expect(basicInfoIncluded).toEqual(true);
    });
    it('should display only the most basic information', function () {
      var initParameters = {
        user: testUser,
        showEmail: false,
        showLocales: false,
        showStatus: false,
        showRoles: false,
        isAdding: false
      };
      initDirective(initParameters);

      var elementScope = mockElement.isolateScope();
      var statusIncluded = mockElement.directiveElementAttributeTestHelper('label', 'translate', 'cStatus');
      var HPAdminRoleIncluded = mockElement.directiveElementAttributeTestHelper('input', 'name', 'roleHPAdmin');
      var emailIncluded = mockElement.directiveElementAttributeTestHelper('label', 'translate', 'cEmail');
      var emailVerifyIncluded = mockElement.directiveElementAttributeTestHelper('label', 'translate', 'cVerifyEmail');
      var basicInfoIncluded = mockElement.directiveElementAttributeTestHelper('label', 'for', 'firstNameValue');
      expect(elementScope.user.status).toEqual('ACTIVE');
      expect(statusIncluded).toEqual(false);
      expect(HPAdminRoleIncluded).toEqual(false);
      expect(emailIncluded).toEqual(false);
      expect(emailVerifyIncluded).toEqual(false);
      expect(basicInfoIncluded).toEqual(true);

    });
    it('should show basic information only and have isAdding == true', function () {
      var initParameters = {
        user: testUser,
        showEmail: false,
        showLocales: false,
        showStatus: false,
        showRoles: false,
        isAdding: true
      };
      initDirective(initParameters);

      var elementScope = mockElement.isolateScope();
      var statusIncluded = mockElement.directiveElementAttributeTestHelper('label', 'translate', 'cStatus');
      var HPAdminRoleIncluded = mockElement.directiveElementAttributeTestHelper('input', 'name', 'roleHPAdmin');
      var emailIncluded = mockElement.directiveElementAttributeTestHelper('label', 'translate', 'cEmail');
      var emailVerifyIncluded = mockElement.directiveElementAttributeTestHelper('label', 'translate', 'cVerifyEmail');
      var basicInfoIncluded = mockElement.directiveElementAttributeTestHelper('label', 'for', 'firstNameValue');
      expect(elementScope.user.status).toEqual('ACTIVE');
      expect(elementScope.isAdding).toEqual(true);
      expect(statusIncluded).toEqual(false);
      expect(HPAdminRoleIncluded).toEqual(false);
      expect(emailIncluded).toEqual(false);
      expect(emailVerifyIncluded).toEqual(false);
      expect(basicInfoIncluded).toEqual(true);

    });

    it('should require a locale to be set', function () {
      var initParameters = {
        user: testUser,
        showEmail: true,
        showLocales: true,
        showStatus: true,
        showRoles: true,
        isAdding: false
      };
      initDirective(initParameters);

      var selectElement = mockElement.find('#userLocaleValue');
      expect(selectElement.attr('class')).toContain('ng-valid');
      delete scope.user.locale;
      scope.$digest();
      expect(selectElement.attr('class')).toContain('ng-invalid');
    });

    it('should show all Locales in select field', function () {
      var initParameters = {
        user: testUser,
        showEmail: true,
        showLocales: true,
        showStatus: true,
        showRoles: true,
        isAdding: false
      };
      initDirective(initParameters);

      angular.forEach(locales, function (value) {
        expect(mockElement.find('#userLocaleValue option[value="string:' + value.lang + '"]').length).toBe(1);
        expect(mockElement.find('#userLocaleValue option[value="string:' + value.lang + '"]').text()).toBe(value.name);
      });
    });

    it('should disable email when not adding or HPAdmin', function () {
      globals.permissions.isHPAdmin = false;
      globals.permissions.isReseller = true;

      initDirective(angular.extend({}, defaultInitParameters, {
        isAdding: false
      }));
      var elementScope = mockElement.isolateScope();
      expect(elementScope.disableEmail).toBe(true);
    });

    it('should enable email when adding', function () {
      globals.permissions.isHPAdmin = false;
      globals.permissions.isReseller = true;

      initDirective(defaultInitParameters);
      var elementScope = mockElement.isolateScope();
      expect(elementScope.disableEmail).toBe(false);
    });

    it('should enable email when hpadmin', function () {
      globals.permissions.isHPAdmin = true;
      globals.permissions.isReseller = true;

      initDirective(angular.extend({}, defaultInitParameters, {
        isAdding: false
      }));
      var elementScope = mockElement.isolateScope();
      expect(elementScope.disableEmail).toBe(false);
    });
  });
});
