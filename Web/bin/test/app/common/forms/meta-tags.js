'use strict';

describe('[Module: hp.portal.common.forms.metaTags] Directive: hp-country-field', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.forms.metaTags'));
  beforeEach(module('hp.templates'));

  var scope, element, elementScope;
  var elementText = '<hp-meta-tags-form meta-tags="testMetaTags" free-form="testFreeForm" available-tags="testAvailableTags"></hp-meta-tags-form>';

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(elementText)(scope);
    scope.$digest();

    elementScope = scope.$$childHead;

    scope.testFreeForm = false;
    scope.testMetaTags = ['TEST_TAG'];
    scope.testAvailableTags = ['PARENT_TAG_ONE', 'PARENT_TAG_TWO'];
    scope.$digest();
  }));

  it('should have certain expected functions', function () {
    expect(typeof elementScope.edit).toBe('function');
    expect(typeof elementScope.save).toBe('function');
    expect(typeof elementScope.remove).toBe('function');
    expect(typeof elementScope.add).toBe('function');
    expect(typeof elementScope.toggle).toBe('function');
  });

  describe('(free form version)', function () {

    beforeEach(function () {
      scope.testFreeForm = true;
      scope.$digest();
    });

    it('should allow editing of existing meta tags', function () {
      elementScope.edit(scope.testMetaTags[0]);
      expect(elementScope.editing).toBe(true);
      elementScope.newValue = 'blah';
      elementScope.save();
      expect(elementScope.editing).toBe(false);
      expect(scope.testMetaTags.length).toBe(1);
      expect(scope.testMetaTags[0]).toBe('blah');
    });

    it('should allow the edits to be canceled', function () {
      elementScope.edit(scope.testMetaTags[0]);
      expect(elementScope.editing).toBe(true);
      elementScope.newValue = 'blah';
      elementScope.resetForm();
      expect(elementScope.editing).toBe(false);
      expect(scope.testMetaTags[0]).toBe('TEST_TAG');
    });

    it('should allow adding of new meta tags', function () {
      elementScope.add();
      expect(elementScope.adding).toBe(true);
      elementScope.newValue = 'magic';
      elementScope.add();
      expect(elementScope.adding).toBe(false);
      expect(scope.testMetaTags.length).toBe(2);
      expect(scope.testMetaTags[0]).toBe('TEST_TAG');
      expect(scope.testMetaTags[1]).toBe('magic');
    });

    it('should prevent blank meta tags', function () {
      elementScope.add();
      expect(elementScope.adding).toBe(true);
      elementScope.add();
      expect(elementScope.adding).toBe(true);
    });

    it('should allow removal of meta tags', function () {
      elementScope.edit(scope.testMetaTags[0]);
      expect(elementScope.editing).toBe(true);
      elementScope.remove();
      expect(elementScope.editing).toBe(false);
      expect(scope.testMetaTags.length).toBe(0);
    });

    it('should allow switching from editing to adding', function () {
      elementScope.edit(scope.testMetaTags[0]);
      expect(elementScope.editing).toBe(true);
      expect(elementScope.adding).toBe(false);
      elementScope.add();
      expect(elementScope.editing).toBe(false);
      expect(elementScope.adding).toBe(true);
    });

    it('should allow switching from adding to editing', function () {
      elementScope.add();
      expect(elementScope.editing).toBe(false);
      expect(elementScope.adding).toBe(true);
      elementScope.edit(scope.testMetaTags[0]);
      expect(elementScope.editing).toBe(true);
      expect(elementScope.adding).toBe(false);
    });

  });

  describe('(toggler version)', function () {

    it('should allow toggling of available meta tags', function () {
      expect(scope.testMetaTags.length).toBe(1);
      expect(scope.testMetaTags[0]).toBe('TEST_TAG');
      elementScope.toggle('PARENT_TAG_ONE');
      expect(scope.testMetaTags.length).toBe(2);
      expect(scope.testMetaTags[1]).toBe('PARENT_TAG_ONE');
      elementScope.toggle('PARENT_TAG_ONE');
      expect(scope.testMetaTags.length).toBe(1);
      expect(scope.testMetaTags[0]).toBe('TEST_TAG');
    });

    it('should prevent toggling of unavailable meta tags', function () {
      expect(scope.testMetaTags.length).toBe(1);
      expect(scope.testMetaTags[0]).toBe('TEST_TAG');
      elementScope.toggle('TEST_TAG');
      expect(scope.testMetaTags.length).toBe(1);
      expect(scope.testMetaTags[0]).toBe('TEST_TAG');
    });

  });

});
