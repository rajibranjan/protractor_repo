'use strict';

describe('[Module: hp.portal.common.forms.companyContact]', function () {
  beforeEach(module('unitTestInit'));

  var testAdmins = {
    data: [{
      name: 'test admin 1'
    }, {
      name: 'test admin 2'
    }]
  };

  var UserService;

  function MockUserService() {
    this.getShouldResolve = false;
    this.get = function (params) {
      // Store off the params to be able to assert them.
      this.params = params;

      var dfd = $q.defer();
      if (this.getShouldResolve) {
        dfd.resolve(this.getShouldResolve);
      }
      else {
        dfd.reject();
      }

      return dfd.promise;
    };
  }

  beforeEach(module(function ($provide) {
    UserService = new MockUserService();
    $provide.constant('UserService', UserService);
  }));

  beforeEach(module('hp.portal.common.forms.companyContact'));
  beforeEach(module('hp.templates'));

  var $q, $timeout, $compile;
  beforeEach(inject(function (_$q_, _$timeout_, _$compile_) {
    $q = _$q_;
    $timeout = _$timeout_;
    $compile = _$compile_;
  }));

  var testAccount = {
    links: [{
      rel: 'self',
      href: 'https://localhost:9000/api/v2/accounts/testAccount'
    }]
  };

  var testUser = {
    name: 'testUser name'
  };

  var scope;
  var elementText = '<div hp-company-contact-field id="userValue" ng-change="testChangeFunction" ng-model="user" company="testAccount"></div>';
  describe('Directive: hp-company-contact-field', function () {
    var element, linkParser, emitFunctionRan;
    beforeEach(inject(function ($rootScope, LinkParser) {
      scope = $rootScope.$new();
      UserService.getShouldResolve = testAdmins;
      emitFunctionRan = false;
      linkParser = LinkParser;
      scope.testAccount = testAccount;
      scope.user = testUser;
    }));

    function compileElement() {
      element = $compile(elementText)(scope);
      scope.$digest();
    }

    it('should get the company admins when the company changes', function () {
      compileElement();
      var elementScope = element.isolateScope();
      $timeout.flush();
      expect(elementScope.admins.length).toBe(2);
      expect(elementScope.uiSelectModel).toBe(testUser);
    });

    it('should not add any company admins if there is a problem with the GET', function () {
      UserService.getShouldResolve = false;
      compileElement();
      var elementScope = element.isolateScope();
      $timeout.flush();
      expect(elementScope.admins).toBeUndefined();
      expect(elementScope.uiSelectModel).toBe(testUser);
    });
  });
});
