'use strict';

describe('[hp.portal.common.webservices] Factory: ProductService', function () {
  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.webservices'));

  var httpBackend,
    ProductService;

  var mockShoppingCart = {
    id: 'test-shopping-cart-id',
    accountId: 'test-account-id'
  };

  beforeEach(inject(function ($httpBackend, _ProductService_) {
    httpBackend = $httpBackend;
    ProductService = _ProductService_;
  }));

  it('should POST to endpoint', function () {
    httpBackend.expectPOST('/api/v2/products').respond(200, mockShoppingCart);
    ProductService.create({
      accountId: 'test-account-id'
    }).save().then(function (data) {
      expect(data.id).toBeDefined();
      expect(data.id).toEqual(mockShoppingCart.id);
      expect(data.accountId).toBeDefined();
      expect(data.accountId).toEqual(mockShoppingCart.accountId);
    });
    httpBackend.flush();
  });

  describe('ProductService.generateLicenseAdditionalFields', function () {
    function createProductAdditionalField(name, href) {
      return {
        name: name,
        links: [{
          rel: 'self',
          href: href
        }]
      };
    }

    it('should create LicenseAdditionalFields using the passed in product and additionalFieldValues', function () {
      var product = {
        additionalFields: []
      };
      var additionalFieldValues = {};

      [0, 1, 2, 3].forEach(function (index) {
        product.additionalFields.push(createProductAdditionalField('field-' + index + '-name', 'href-' + index));
        additionalFieldValues['field-' + index + '-name'] = 'field-' + index + '-value';
      });

      var licenseAdditionalFields = ProductService.generateLicenseAdditionalFields(product, additionalFieldValues);

      licenseAdditionalFields.forEach(function (licenseAdditionalField, index) {
        expect(licenseAdditionalField.value).toBe('field-' + index + '-value');
        expect(licenseAdditionalField.links).toEqual([{
          rel: 'productAdditionalField',
          href: 'href-' + index
        }]);
      });
    });

    it('should return empty if the product or additionalFieldValues are missing', function () {
      var licenseAdditionalFields = ProductService.generateLicenseAdditionalFields();
      expect(licenseAdditionalFields).toBeUndefined();

      licenseAdditionalFields = ProductService.generateLicenseAdditionalFields({});
      expect(licenseAdditionalFields).toBeUndefined();

      licenseAdditionalFields = ProductService.generateLicenseAdditionalFields({}, {});
      expect(licenseAdditionalFields).toBeUndefined();

      licenseAdditionalFields = ProductService.generateLicenseAdditionalFields({
        additionalFields: []
      }, {});
      expect(licenseAdditionalFields).toEqual([]);
    });
  });
});
