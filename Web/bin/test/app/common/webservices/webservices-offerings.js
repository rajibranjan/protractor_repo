'use strict';

describe('[hp.portal.common.webservices] Factory: OfferingService', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.webservices'));

  var OfferingService, httpBackend, timeout;

  beforeEach(inject(function ($httpBackend, $timeout, _OfferingService_) {
    httpBackend = $httpBackend;
    httpBackend.expectGET(/\/api\/v2\/offerings/).respond(200, sampleOfferingPage);
    timeout = $timeout;
    OfferingService = _OfferingService_;
  }));

  var sampleOfferingPage = {
    total: 2,
    data: [{
      fqName: 'testOffering'
    }, {
      fqName: 'testOffering2'
    }]
  };

  it('Tests the getByFqName method', function () {
    var found = false;
    OfferingService.getByFqName('testOffering').then(function (offering) {
      expect(offering.fqName).toBe('testOffering');
      found = true;
    });
    httpBackend.flush();
    expect(found).toBe(true);

    // When we query OfferingService a second time the value should come from the cache not
    // an Http request. With no second http request expected this will fail if it does not come from
    // the cache.
    found = false;
    OfferingService.getByFqName('testOffering').then(function (offering) {
      expect(offering.fqName).toBe('testOffering');
      found = true;
    });
    // We do a timeout.flush here instead of a httpBackend.flush, because this will only
    // flush outstanding promises but not any http requests.
    timeout.flush();
    expect(found).toBe(true);
  });

  it('Should handle non-existent offerings', function () {
    var realOfferingResolved = false;
    var fakeOfferingRejected = false;

    OfferingService.getByFqName('testOffering').then(function (result) {
      realOfferingResolved = true;
      expect(result.fqName).toBe('testOffering');
    }).then(function () {
      OfferingService.getByFqName('testOffering2').then(function (result) {
        expect(result.fqName).toBe('testOffering2');
      });
    });
    httpBackend.flush();

    OfferingService.getByFqName('doesNotExist').catch(function () {
      fakeOfferingRejected = true;
    });

    // This throws if a second request is not generated
    expect(httpBackend.flush).toThrow();

    timeout.flush();
    expect(realOfferingResolved).toBe(true);
    expect(fakeOfferingRejected).toBe(true);
  });

  afterEach(function () {
    return httpBackend.verifyNoOutstandingRequest();
  });

});
