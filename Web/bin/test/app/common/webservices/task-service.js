'use strict';

describe('[Module: hp.portal.common.webservices.task-service]', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.common.webservices.task-service'));

  var alerts = [];
  var removedAlerts = [];
  var mockPageAlerts = {
    add: function (alert) {
      alerts.push(alert);
    },
    remove: function (alert) {
      removedAlerts.push(alert);
    },
    timeout: {
      short: 3000,
      medium: 5000,
      long: 10000
    }
  };

  beforeEach(module(function ($provide) {
    $provide.constant('PageAlerts', mockPageAlerts);
  }));

  // Injected services
  var httpBackend, $timeout, mockTask;
  var mockTaskEndpoint = 'https://localhost/api/v2/tasks/mockTask';
  var mockTaskSelfEndpoint = mockTaskEndpoint + '/12345';
  var currentStatus = 'PROCESSING';
  var errors = [];

  var testMessages = {
    pleaseWaitMsg: 'cPleaseWait',
    pleaseWaitLongerMsg: 'cPleaseWaitLonger',
    successMsg: 'cSuccess',
    fallbackErrorMsg: 'cFallbackError',
    timeoutMsg: 'cTimeout'
  };

  var getResult = function () {
    return {
      status: currentStatus,
      links: [{
        rel: 'self',
        href: mockTaskSelfEndpoint
      }],
      errors: errors,
    };
  };

  beforeEach(inject(function ($httpBackend, _$timeout_, TaskService) {
    httpBackend = $httpBackend;
    $timeout = _$timeout_;

    //Setup mock task
    currentStatus = 'PROCESSING';
    var mockTaskService = new TaskService(mockTaskEndpoint);
    mockTask = mockTaskService.startTask();

    alerts = [];
    removedAlerts = [];
    errors = [];
  }));

  describe('pollWithMessages tests', function () {

    it('should show the error message when an unknown error was encountered', function () {
      httpBackend.whenPOST(mockTaskEndpoint).respond(200, getResult());
      mockTask.$promise.then(function (task) {
        task.pollWithMessages(testMessages, {
          maxPollTimeMs: 0
        });
      });

      httpBackend.flush();
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('info');
      expect(alerts[0].msg).toBe(testMessages.pleaseWaitMsg);

      currentStatus = 'FAILED';
      httpBackend.whenGET(mockTaskSelfEndpoint).respond(200, getResult());

      $timeout.flush();
      httpBackend.flush();

      expect(alerts.length).toBe(2);
      expect(alerts[1].type).toBe('danger');
      expect(alerts[1].msg).toBe(testMessages.fallbackErrorMsg);

      expect(removedAlerts.length).toBe(1);
    });

    it('should use the errorCodeMapper when a known error was encountered', function () {
      httpBackend.whenPOST(mockTaskEndpoint).respond(200, getResult());
      mockTask.$promise.then(function (task) {
        task.pollWithMessages(testMessages, {
          maxPollTimeMs: 0
        });
      });

      httpBackend.flush();
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('info');
      expect(alerts[0].msg).toBe(testMessages.pleaseWaitMsg);

      currentStatus = 'FAILED';
      errors = [{
        serviceCode: 1,
        errorCode: 1
      }];
      httpBackend.whenGET(mockTaskSelfEndpoint).respond(200, getResult());

      $timeout.flush();
      httpBackend.flush();

      expect(alerts.length).toBe(2);
      expect(alerts[1].type).toBe('danger');
      expect(alerts[1].msg).toBe('cLicenseKeyNotFound');

      expect(removedAlerts.length).toBe(1);
    });

    it('should show the correct messages when polling twice', function () {
      httpBackend.whenPOST(mockTaskEndpoint).respond(200, getResult());
      mockTask.$promise.then(function (task) {
        task.pollWithMessages(testMessages, {
          maxPollTimeMs: 0
        });
      });

      httpBackend.flush();
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('info');
      expect(alerts[0].msg).toBe(testMessages.pleaseWaitMsg);

      $timeout.flush();

      expect(alerts.length).toBe(2);
      expect(alerts[1].type).toBe('warning');
      expect(alerts[1].msg).toBe(testMessages.pleaseWaitLongerMsg);

      expect(removedAlerts.length).toBe(1);
    });


    it('should show the correct messages when succeeding on the first poll', function () {
      httpBackend.whenPOST(mockTaskEndpoint).respond(200, getResult());
      mockTask.$promise.then(function (task) {
        task.pollWithMessages(testMessages, {
          maxPollTimeMs: 0
        });
      });

      httpBackend.flush();
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('info');
      expect(alerts[0].msg).toBe(testMessages.pleaseWaitMsg);

      currentStatus = 'COMPLETED';
      httpBackend.whenGET(mockTaskSelfEndpoint).respond(200, getResult());

      $timeout.flush();
      httpBackend.flush();

      expect(alerts.length).toBe(2);
      expect(alerts[1].type).toBe('success');
      expect(alerts[1].msg).toBe(testMessages.successMsg);

      expect(removedAlerts.length).toBe(1);
    });

    it('should show the correct messages when succeeding on the initial post', function () {
      currentStatus = 'COMPLETED';
      httpBackend.whenPOST(mockTaskEndpoint).respond(200, getResult());

      mockTask.$promise.then(function (task) {
        task.pollWithMessages(testMessages, {
          maxPollTimeMs: 0
        });
      });

      httpBackend.flush();
      expect(alerts.length).toBe(2);
      expect(alerts[0].type).toBe('info');
      expect(alerts[0].msg).toBe(testMessages.pleaseWaitMsg);

      expect(alerts[1].type).toBe('success');
      expect(alerts[1].msg).toBe(testMessages.successMsg);

      expect(removedAlerts.length).toBe(1);
    });

    it('should timeout after 6 tries', function () {
      httpBackend.whenPOST(mockTaskEndpoint).respond(200, getResult());

      var promise;
      mockTask.$promise.then(function (task) {
        promise = task.pollWithMessages(testMessages, {
          maxPollTimeMs: 0
        });
      });

      httpBackend.flush();

      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('info');
      expect(alerts[0].msg).toBe(testMessages.pleaseWaitMsg);

      $timeout.flush();

      expect(alerts.length).toBe(2);
      expect(alerts[1].type).toBe('warning');
      expect(alerts[1].msg).toBe(testMessages.pleaseWaitLongerMsg);

      //Make sure the promise is resolved after a reasonable amount of time, even if we are timing out.
      var resolved = false;
      promise.then(function () {
        resolved = true;
      });
      $timeout.flush();
      expect(resolved).toBe(true);
      expect(alerts.length).toBe(3);

      $timeout.flush();
      expect(alerts.length).toBe(4);

      $timeout.flush();
      expect(alerts.length).toBe(5);

      $timeout.flush();
      expect(alerts.length).toBe(6);

      $timeout.flush();
      expect(alerts.length).toBe(7);


      expect(alerts[6].type).toBe('danger');
      expect(alerts[6].msg).toBe(testMessages.timeoutMsg);

      expect(removedAlerts.length).toBe(6);
    });

  });
});
