'use strict';

describe('[Module: hp.portal.eula.content] ContentCtrl', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.eula.content'));

  var $scope, ContentCtrl, $controller, $q, $timeout, element;

  var mockServerURL = 'server-url';

  var mockRouteParams = {
    app: 'testApp'
  };

  var mockWindow = {};

  var alerts;
  var mockPageAlerts = {
    add: function (alert) {
      return alerts.push(alert);
    },
    timeout: {
      short: 3000,
      medium: 5000,
      long: 10000
    }
  };

  var mockOffering = {
    title: 'testOfferingTitle',
    fqName: 'testOfferingFqname',
    termsOfUse: 'testTermsOfUse',
    privacyPolicy: 'testPrivacyPolicy',
  };

  var mockSelfId = 'mock-user-id';
  var mockSelf = {
    links: [{
      rel: 'self',
      href: mockSelfId
    }],
    $promise: {
      then: function (success) {
        success(mockSelf);
      }
    }
  };

  var testEntitlements = {};
  var mockEntitlementService = {
    getByUserAndOffering: function (userId, offeringFqName) {
      expect(userId).toBe(mockSelfId);
      expect(offeringFqName).toBe(mockRouteParams.app);
      return mockEntitlementService.getByUserAndOfferingShouldResolve ? $q.when(testEntitlements) : $q.reject();
    },
    saveAll: function (entitlements) {
      mockEntitlementService.entitlements = angular.copy(entitlements);
      return mockEntitlementService.saveAllShouldResolve ? $q.when() : $q.reject();
    }
  };

  var mocki18nFactory;

  function Mocki18nFactory() {
    this.getBySourceShouldResolve = true;
    this.localeStrings = {
      testOfferingTitle: 'Here is the Title!',
      testTermsOfUse: 'With great power comes great responsibility...',
      testPrivacyPolicy: 'Mind your own business.'
    };
    this.getBySource = function (source) {
      this.source = source;
      return {
        $promise: this.getBySourceShouldResolve ? $q.when(this.localeStrings) : $q.reject()
      };
    };
  }

  var mockIdentityRedirectUrl = '/mock/identity/redirect';
  var mockPortalConfigurations = {
    $promise: {
      then: function (successHandler) {
        successHandler();
      }
    },
    'com.portal.identityRedirectUrl': mockIdentityRedirectUrl,
  };

  var loadController = function () {
    ContentCtrl = $controller('ContentCtrl', {
      $scope: $scope,
      offering: mockOffering,
      Self: mockSelf,
      i18n: mocki18nFactory,
      EntitlementService: mockEntitlementService,
      $routeParams: mockRouteParams,
      $window: mockWindow,
      PageAlerts: mockPageAlerts,
      PortalConfigurations: mockPortalConfigurations,
      serverURL: mockServerURL
    });
    $timeout.flush();
  };

  var submitButtonHtml = '<button id="submitButton" class="btn btn-primary" type="submit" translate="cSubmit" ng-click="submit()" ng-disabled="!checkboxResults.termsAccepted || !checkboxResults.privacyAccepted"></button>';

  beforeEach(inject(function ($rootScope, _$controller_, $compile, _$q_, _$timeout_) {
    $q = _$q_;
    $timeout = _$timeout_;
    $scope = $rootScope.$new();
    $controller = _$controller_;

    element = $compile(submitButtonHtml)($scope);
    $scope.$digest();

    testEntitlements.data = [{
      entitlement1: 'testEntitlement1'
    }];

    mocki18nFactory = new Mocki18nFactory();

    mockEntitlementService.getByUserAndOfferingShouldResolve = true;
    mockEntitlementService.saveAllShouldResolve = true;
    mockWindow.location = {};
    alerts = [];
  }));

  it('submit button should be disabled', function () {
    loadController();
    $scope.checkboxResults.termsAccepted = false;
    $scope.checkboxResults.privacyAccepted = false;
    $scope.$digest();

    expect(element[0].disabled).toBe(true);
  });

  it('submit button should be enabled', function () {
    loadController();
    $scope.checkboxResults.termsAccepted = true;
    $scope.checkboxResults.privacyAccepted = true;
    $scope.$digest();

    expect(element[0].disabled).toBe(false);
  });

  it('should have all the necessary attributes for the $scope defined', function () {
    loadController();
    expect($scope.offering).toBeDefined();
    expect($scope.checkboxResults.termsAccepted).toBe(false);
    expect($scope.checkboxResults.privacyAccepted).toBe(false);
    expect($scope.offering).toBe(mockOffering);
    expect($scope.localeStrings).toBeDefined();
    expect($scope.eulaDescriptionTranslateValues.offeringTitle).toBeDefined();
    expect($scope.self).toBe(mockSelf);
    expect($scope.submit).toBeDefined();
    expect($scope.printTextArea).toBeDefined();
  });

  it('should fetch i18n strings by the offeringFqName', function () {
    loadController();
    expect(mocki18nFactory.source).toBe(mockOffering.fqName);
  });

  it('should have brackets around strings if the i18n factory returns an error', function () {
    mocki18nFactory.getBySourceShouldResolve = false;
    loadController();
    expect($scope.eulaDescriptionTranslateValues.offeringTitle).toBe('{{testOfferingTitle}}');
  });

  it('should redirect to serverURL if there are no entitlements', function () {
    testEntitlements.data.length = 0;
    loadController();
    expect(mockWindow.location.href).toBe(mockServerURL);
  });

  it('should redirect to identityRedirectUrl terms and privacy are already accepted', function () {
    testEntitlements.data[0] = {
      entitlement1: 'testEntitlement1',
      termsAccepted: true,
      privacyAccepted: true
    };
    loadController();
    expect(mockWindow.location.href).toBe(mockIdentityRedirectUrl + '?redirect=' + $scope.offering.fqName);
  });

  it('should submit the eula content form with the user accepting both the terms and privacy statements', function () {
    var submissionRan = false;
    loadController();
    $scope.$on('hpDisableEulaContentForm', function (event, submitFn, checkIsValid) {
      expect(checkIsValid).toBe(true);
      expect(mockEntitlementService.entitlements).toBeUndefined();
      submitFn();
      $timeout.flush();

      expect(mockEntitlementService.entitlements.length).toBe(1);
      angular.forEach(mockEntitlementService.entitlements, function (entitlement) {
        expect(entitlement.termsAccepted).toBe($scope.checkboxResults.termsAccepted);
        expect(entitlement.privacyAccepted).toBe($scope.checkboxResults.privacyAccepted);
      });

      expect($scope.eulaDescriptionTranslateValues.offeringTitle).toBe(mocki18nFactory.localeStrings.testOfferingTitle);
      expect(mockWindow.location.href).toBe(mockIdentityRedirectUrl + '?redirect=' + $scope.offering.fqName);
      submissionRan = true;
    });
    $scope.checkboxResults.termsAccepted = true;
    $scope.checkboxResults.privacyAccepted = true;
    $scope.submit();
    expect(submissionRan).toBe(true);
  });

  it('should fail to submit the Entitlements and display a page alert', function () {
    var submissionRan = false;
    testEntitlements.data[0] = {
      entitlement1: 'testEntitlement1',
      termsAccepted: false,
      privacyAccepted: false
    };
    loadController();
    mockEntitlementService.saveAllShouldResolve = false;
    $scope.$on('hpDisableEulaContentForm', function (event, submitFn, checkIsValid) {
      expect(checkIsValid).toBe(true);
      submitFn();
      $timeout.flush();
      expect(mockWindow.location.href).toBeUndefined();
      expect($scope.eulaDescriptionTranslateValues.offeringTitle).toBe(mocki18nFactory.localeStrings.testOfferingTitle);
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('danger');
      expect(alerts[0].msg).toBe('cErrorSubmittingEula');
      submissionRan = true;
    });
    $scope.checkboxResults.termsAccepted = true;
    $scope.checkboxResults.privacyAccepted = true;
    $scope.submit();
    expect(submissionRan).toBe(true);
  });

  it('should automatically set termsAccepted if termsOfUse is missing', function () {
    delete mocki18nFactory.localeStrings.testTermsOfUse;
    loadController();
    expect($scope.checkboxResults.termsAccepted).toBe(true);
    expect($scope.checkboxResults.privacyAccepted).toBe(false);
  });

  it('should automatically set privacyAccepted if privacyPolicy is missing', function () {
    delete mocki18nFactory.localeStrings.testPrivacyPolicy;
    loadController();
    expect($scope.checkboxResults.termsAccepted).toBe(false);
    expect($scope.checkboxResults.privacyAccepted).toBe(true);
  });

});
