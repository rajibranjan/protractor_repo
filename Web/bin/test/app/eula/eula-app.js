'use strict';

describe('[Module: hp.portal.eula.eulaApp]', function () {

  beforeEach(module(function ($provide) {
    // Used by ResetPassword
    $provide.constant('$route', mockRoute);
    $provide.constant('Self', {
      $promise: undefined
    });
  }));

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.eula.eulaApp'));

  var mockRoute = {
    routes: [{}]
  };

  describe('Run: EulaRunMethod', function () {

    // For the run method to be called in a module, there must be a inject statement to trigger it.
    // This is so developers have a reliable way to test just the run method with different inputs.
    beforeEach(inject(function () {}));

    it('should initialze the route stuff', function () {
      expect(mockRoute.routes[0].resolve).toBeDefined();
      expect(mockRoute.routes[0].resolve.getSelf).toBeDefined();
    });
  });

  describe('Controller: EulaCtrl', function () {

    var mockPageLoadingMonitor = {
      start: function (whatToStart) {
        this.started = whatToStart;
      },
      started: undefined
    };

    var scope, EulaCtrl;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($rootScope, $controller) {
      scope = $rootScope.$new();
      mockPageLoadingMonitor.started = undefined;
      EulaCtrl = $controller('EulaCtrl', {
        $scope: scope,
        pageLoadingMonitor: mockPageLoadingMonitor,
      });
    }));

    it('should initialize the Eula controller', function () {
      expect(mockPageLoadingMonitor.started).toBe('eula');
    });
  });
});
