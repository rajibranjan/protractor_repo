describe('Controller: LoginCtrl', function () {
  'use strict';


  beforeEach(module('hp.common.login.loginApp'));

  var LoginCtrl, scope, httpBackend;

  var mockHttpErrorLogger = {};
  var mockPageLoadingMonitor = {
    start: function (appName) {
      this.startingModule = appName;
    }
  };

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $httpBackend) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;

    LoginCtrl = $controller('LoginCtrl', {
      $scope: scope,
      pageLoadingMonitor: mockPageLoadingMonitor,
      httpErrorLogger: mockHttpErrorLogger
    });

  }));

  // Check scope properties
  it('should attach all the correct objects to scope', function () {
    expect(scope.self).toBeDefined(true);
  });

  // Check httpErrorLoger and page
  it('should handle httpErrorLogger and pageLoadingMonitor', function () {
    expect(mockHttpErrorLogger.redirectOnUnauthenticated).toBe(false);
    expect(mockPageLoadingMonitor.startingModule).toBe('login');
  });

});
