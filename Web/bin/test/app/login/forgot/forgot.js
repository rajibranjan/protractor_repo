describe('Controller: ForgotCtrl', function () {
  'use strict';

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.login.forgot'));

  var $q, timeout;
  beforeEach(inject(function (_$q_, $timeout) {
    $q = _$q_;
    timeout = $timeout;
  }));

  var ForgotCtrl, scope, forgotPasswordShouldReturnSuccess, alerts;

  var mockForgotPasswordFactory = {
    startTask: function () {
      var deferred = $q.defer();
      if (forgotPasswordShouldReturnSuccess) {
        deferred.resolve();
      }
      else {
        deferred.reject();
      }
      return {
        $promise: deferred.promise
      };
    }
  };

  var mockPageAlerts = {
    add: function (alert) {
      return alerts.push(alert);
    },
    timeout: {
      short: 3000,
      medium: 5000,
      long: 10000
    }
  };

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    alerts = [];

    ForgotCtrl = $controller('ForgotCtrl', {
      $scope: scope,
      ForgotPassword: mockForgotPasswordFactory,
      PageAlerts: mockPageAlerts
    });
  }));


  // Check scope properties
  it('should attach all the correct objects to scope', function () {
    expect(scope.formData).toBeDefined(true);
    expect(scope.forgotPasswordSubmit).toBeDefined(true);
  });


  it('should successfully submit forgot password request', function () {
    forgotPasswordShouldReturnSuccess = true;
    var submitFunctionRan = false;

    scope.$on('hpDisableForgotForm', function (event, submitFn, checkIsInvalid) {
      submitFn();
      timeout.flush();
      expect(checkIsInvalid).toBe(true);
      expect(scope.hasSuccess).toBe(true);
      submitFunctionRan = true;
    });

    scope.forgotPasswordSubmit();
    expect(submitFunctionRan).toBe(true);
  });

  it('should display an error', function () {
    forgotPasswordShouldReturnSuccess = false;
    var submitFunctionRan = false;

    scope.$on('hpDisableForgotForm', function (event, submitFn, checkIsInvalid) {
      submitFn();
      timeout.flush();
      expect(checkIsInvalid).toBe(true);
      expect(alerts.length).toBe(1);
      expect(alerts[0].type).toBe('danger');
      expect(alerts[0].msg).toBe('cErrorUnknown');
      submitFunctionRan = true;
    });

    scope.forgotPasswordSubmit();
    expect(submitFunctionRan).toBe(true);
  });

});
