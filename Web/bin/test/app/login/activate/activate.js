'use strict';

describe('[Module: hp.portal.login.activate]', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.login.activate'));

  var alerts = [];
  beforeEach(module(function ($provide) {
    $provide.constant('PageAlerts', {
      add: function (alert) {
        alerts.push(alert);
      },
      timeout: {
        short: 3000,
        medium: 5000,
        long: 10000
      }
    });
  }));

  var $q, timeout;
  beforeEach(inject(function (_$q_, $timeout) {
    $q = _$q_;
    timeout = $timeout;
  }));

  var mockRouteParams = {
    id: 'testId',
    OptIn: 'true',
    Locale: 'testLocale',
    cc: 'CA'
  };

  var mockSelf = {
    locale: undefined
  };

  var testChangeLanguage = function (locale) {
    currentLanguage = 'we changed the language!';
    return locale;
  };

  var mockActivateFactory = {
    startTask: function () {
      var deferred = $q.defer();
      if (activateShouldReturnSuccess) {
        deferred.resolve();
      }
      else {

        var payload = {
          errors: []
        };

        if (addPasswordError) {
          payload.errors.push({
            serviceCode: 5,
            errorCode: 3
          });
        }

        deferred.reject(payload);
      }
      return {
        $promise: deferred.promise
      };
    }
  };

  var localStorageKey = 'titanSDKLocale';
  var activateShouldReturnSuccess, addPasswordError, currentLanguage, $locker;

  describe('ActivateCtrl', function () {

    var scope, ActivateCtrl, controller;

    var initActivateCtr = function (routParams) {
      ActivateCtrl = controller('ActivateCtrl', {
        $scope: scope,
        $routeParams: routParams,
        ActivateUser: mockActivateFactory,
        Self: mockSelf,
        changeLanguage: testChangeLanguage,
        passwordPolicy: 'STRICT'
      });
    };

    beforeEach(inject(function ($rootScope, $controller, locker) {
      scope = $rootScope.$new();
      alerts = [];
      addPasswordError = false;
      currentLanguage = undefined;
      $locker = locker;
      controller = $controller;
    }));

    it('should successfully activate a new user', function () {
      var disableFormFunctionRan = false;
      activateShouldReturnSuccess = true;
      initActivateCtr(mockRouteParams);
      expect(scope.formData.verificationHash).toBe('testId');
      expect(scope.formData.optedIntoEmail).toBeUndefined();
      expect(scope.formData.agreeToPrivacy).toBe(true);
      expect(mockSelf.locale).toBe('testLocale');
      expect(currentLanguage).toBe('we changed the language!');
      scope.$on('hpDisableActivateForm', function (event, submitFn, checkIsInvalid) {
        submitFn();
        timeout.flush();
        expect(checkIsInvalid).toBe(true);
        expect(scope.hasSuccess).toBe(true);
        disableFormFunctionRan = true;
      });
      scope.submit();
      expect(disableFormFunctionRan).toBe(true);
    });

    it('should throw an error while activating a user', function () {
      var disableFormFunctionRan = false;
      activateShouldReturnSuccess = false;
      initActivateCtr(mockRouteParams);
      expect(scope.formData.verificationHash).toBe('testId');
      expect(scope.formData.optedIntoEmail).toBeUndefined();
      expect(scope.formData.agreeToPrivacy).toBe(true);
      expect(mockSelf.locale).toBe('testLocale');
      expect(currentLanguage).toBe('we changed the language!');
      scope.$on('hpDisableActivateForm', function (event, submitFn, checkIsInvalid) {
        submitFn();
        timeout.flush();
        expect(checkIsInvalid).toBe(true);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
        expect(alerts[0].msg).toBe('cErrorActivateAccount');
        disableFormFunctionRan = true;
      });
      scope.submit();
      expect(disableFormFunctionRan).toBe(true);
    });

    it('should throw an invalid password error when an invalid password error comes back from the server', function () {
      var disableFormFunctionRan = false;
      activateShouldReturnSuccess = false;
      addPasswordError = true;
      initActivateCtr(mockRouteParams);
      expect(scope.formData.verificationHash).toBe('testId');
      expect(scope.formData.optedIntoEmail).toBeUndefined();
      expect(scope.formData.agreeToPrivacy).toBe(true);
      expect(mockSelf.locale).toBe('testLocale');
      expect(currentLanguage).toBe('we changed the language!');
      scope.$on('hpDisableActivateForm', function (event, submitFn, checkIsInvalid) {
        submitFn();
        timeout.flush();
        expect(checkIsInvalid).toBe(true);
        expect(alerts.length).toBe(1);
        expect(alerts[0].type).toBe('danger');
        expect(alerts[0].msg).toBe('cPasswordValidationFailed');
        disableFormFunctionRan = true;
      });
      scope.submit();
      expect(disableFormFunctionRan).toBe(true);
    });

    it('should set the local to routeParams cc value', function () {
      mockRouteParams.Locale = 'it-IT';
      initActivateCtr(mockRouteParams);
      expect(mockSelf.locale).toBe(mockRouteParams.Locale);
      expect($locker.get(localStorageKey)).toBe(mockRouteParams.Locale);
    });

    it('should set the local to localStorage value', function () {
      mockRouteParams.Locale = undefined;
      mockSelf.locale = undefined;
      $locker.put(localStorageKey, 'ab-AB');
      initActivateCtr(mockRouteParams);
      expect(mockSelf.locale).toBe('ab-AB');
      expect($locker.get(localStorageKey)).toBe('ab-AB');
    });

    it('should set the local to self value', function () {
      mockRouteParams.Locale = undefined;
      mockSelf.locale = 'ca-CA';
      $locker.forget(localStorageKey);
      initActivateCtr(mockRouteParams);
      expect(mockSelf.locale).toBe('ca-CA');
      expect($locker.get(localStorageKey)).toBe('ca-CA');
    });
  });
});
