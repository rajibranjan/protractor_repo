'use strict';

describe('hp.portal.login.unsubscribe', function () {

  beforeEach(module('unitTestInit'));
  beforeEach(module('hp.portal.login.unsubscribe'));

  describe('UnsubscribeCtrl', function () {


    var statusCode = 200;

    var httpBackend, q, timeout;
    beforeEach(inject(function ($httpBackend, $q, $timeout) {
      httpBackend = $httpBackend;
      q = $q;
      timeout = $timeout;
    }));

    var mockUnsubscribe = {
      validate: function () {
        return {
          $promise: {
            then: function (success, failure) {
              var deferred = q.defer();
              var response;
              timeout(function () {
                if (statusCode / 100 !== 2) {
                  response = {
                    status: 'could not validate ID'
                  };
                  deferred.reject(response);
                }
                else {
                  response = {
                    status: 'ID validated'
                  };
                  deferred.resolve(response);
                }
              }, 100);
              return deferred.promise.then(success, failure);
            }
          }
        };
      },
      save: function () {
        return {
          $promise: {
            then: function (success, failure) {
              var deferred = q.defer();
              var response;
              timeout(function () {
                if (statusCode / 100 !== 2) {
                  response = {
                    status: 'could not save unsubscribe status'
                  };
                  deferred.reject(response);
                }
                else {
                  response = {
                    status: 'saved unsubscribe status'
                  };
                  deferred.resolve(response);
                }
              }, 100);
              return deferred.promise.then(success, failure);
            }
          }
        };
      }
    };


    var UnsubscribeCtrl, scope, controllerLoader;


    beforeEach(inject(function ($rootScope, $controller) {
      scope = $rootScope.$new();
      controllerLoader = $controller;
      scope.response = {
        email: 'a@a'
      };
      UnsubscribeCtrl = controllerLoader('UnsubscribeCtrl', {
        $scope: scope,
        Unsubscribe: mockUnsubscribe
      });

    }));


    it('should check for unsubscribe object', function () {
      mockUnsubscribe.validate();
      expect(scope.messageBodyParams.unsubscribeData).toBeDefined();

    });

    it('should click submit button', function () {
      scope.submit();
    });
  });

});
