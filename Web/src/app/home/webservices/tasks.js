(function () {
  'use strict';

  angular.module('hp.portal.home.webservices.tasks', [
    'hp.portal.common.webservices.task-service',
    'hp.portal.common.webservices.endpoints',
  ])

  .factory('UserCSVUpload', function (TaskService, Endpoints) {
    return new TaskService(Endpoints.userCSVUpload);
  })

  .factory('CreateReseller', function (TaskService, Endpoints) {
    return new TaskService(Endpoints.createReseller);
  })

  .factory('ResendActivationEmail', function (TaskService, Endpoints) {
    return new TaskService(Endpoints.resendCreatedEmail);
  })

  .factory('EntityRepair', function (TaskService, Endpoints) {
    return new TaskService(Endpoints.entityRepair);
  })

  .factory('TerminateAccount', function (TaskService, Endpoints) {
    return new TaskService(Endpoints.deleteAccount);
  })

  .factory('UpdateCredentials', function (TaskService, Endpoints, Self, LinkParser) {
    var service = new TaskService(Endpoints.updateCredentials);

    service.changePassword = function (oldPassword, newPassword) {
      return service.startTask({
        oldPassword: oldPassword,
        newPassword: newPassword,
        resetPin: false,
        links: [{
          rel: 'user',
          href: LinkParser.getSelf(Self).href
        }]
      });
    };

    service.deletePins = function () {
      return service.startTask({
        resetPin: true,
        links: [{
          rel: 'user',
          href: LinkParser.getSelf(Self).href
        }]
      });
    };

    return service;
  })

  .factory('ValidateLicenseKey', function (TaskService, Endpoints) {
    return new TaskService(Endpoints.validateLicenseKey);
  });

})();
