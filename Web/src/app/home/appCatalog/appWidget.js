(function () {
  'use strict';

  angular.module('hp.portal.home.appCatalog.appWidget', [
    'hp.portal.common.webservices.entitlements',
    'hp.portal.common.modals.licenseCountdownModal',
    'angularMoment'
  ])

  .constant('LaunchBehaviors', {
    NEW_TAB: 'NEW_TAB',
    SAME_TAB: 'SAME_TAB'
  })

  .directive('appWidget', function ($timeout, $q, EntitlementService, RestrictionCodes, LinkParser, Self, PortalConfigurations, LicenseCountdownModal, LaunchBehaviors, amMoment) {
    var userId = LinkParser.getSelfId(Self);
    var entitlementPollingInterval = 5000;
    var restrictionCodes;

    function parseEntitlements(entitlements, scope) {
      // Look for restrictions
      restrictionCodes = [];
      angular.forEach(entitlements, function (entitlement) {
        angular.forEach(entitlement.restrictions, function (restriction) {
          restrictionCodes.push(restriction.code);
        });
      });

      scope.buttonDisabled = false;
      scope.appMessage = 'cLaunchApp';
      scope.href = scope.offering.urls.browserLandingUrl;

      if (scope.license.daysToExpired <= scope.license.expirationNotificationDays) {
        scope.appMessage = 'cLaunchAppLicenseExpiring';
      }
      if (restrictionCodes.length > 0) {
        if (hasRestrictionCode(RestrictionCodes.licenseTerminated, restrictionCodes)) {
          scope.appMessage = 'cLicenseTerminated';
          scope.buttonDisabled = true;
        }
        else if (hasRestrictionCode(RestrictionCodes.licenseExpired, restrictionCodes)) {
          var midnightTonight = new Date();
          midnightTonight.setDate(midnightTonight.getDate() + 1);
          midnightTonight.setUTCHours(0, 0, 0, 0);
          scope.license.daysTilTermination = amMoment.preprocessDate(scope.license.terminateDate).from(midnightTonight);
          scope.appMessage = 'cLicenseExpired';
          scope.buttonLarge = true;
          scope.buttonDisabled = true;
        }
        else if (hasRestrictionCode(RestrictionCodes.provisioning, restrictionCodes)) {
          scope.buttonDisabled = true;
          scope.appMessage = 'cAccessPending';
        }
        else if (hasRestrictionCode(RestrictionCodes.eulaAcceptance, restrictionCodes)) {
          PortalConfigurations.$promise.then(function () {
            scope.href = PortalConfigurations['com.portal.eulaPageUrl'] + '#/' + scope.offering.fqName;
          });
        }
        else {
          scope.appMessage = 'cUnknownEntitlementRestriction';
          scope.buttonDisabled = true;
        }
      }
    }

    function hasRestrictionCode(restrictionCode, restrictions) {
      return restrictions && restrictions.indexOf(restrictionCode) >= 0;
    }

    return {
      restrict: 'E',
      templateUrl: 'app/home/appCatalog/appWidget.html',
      scope: {
        product: '=',
        offering: '=',
        entitlements: '=',
        license: '='
      },
      link: function (scope) {
        if (scope.product.launchBehavior === LaunchBehaviors.NEW_TAB) {
          scope.launchBehavior = '_blank';
        }
        else if (scope.product.launchBehavior === LaunchBehaviors.SAME_TAB) {
          scope.launchBehavior = '_self';
        }
        if (scope.license.endDate) {
          if (scope.license.daysToExpired <= scope.license.expirationNotificationDays) {
            var entitlement = scope.entitlements.find(function (e) {
              return e.offering.fqName === scope.offering.fqName;
            });

            scope.modalData = {
              daysTillExpiration: scope.license.daysToExpired,
              offering: scope.offering,
              entitlement: entitlement,
            };
          }
        }

        scope.launchModal = function () {
          LicenseCountdownModal.open(scope.modalData);
        };

        function pollEntitlements() {
          pollingTimeout = null;
          checkOfferingEntitlement().then(shouldContinuePolling);
        }

        var pollingTimeout;

        function shouldContinuePolling() {
          if (hasRestrictionCode(RestrictionCodes.provisioning, restrictionCodes)) {
            pollingTimeout = $timeout(pollEntitlements, entitlementPollingInterval);
          }
        }

        function checkOfferingEntitlement() {
          return EntitlementService.getByUserAndOffering(userId, scope.offering.fqName).then(function success(result) {
            parseEntitlements(result.data, scope);
          }, function error() {
            scope.appMessage = 'cInternalErrorTitle';
            return $q.reject();
          });
        }
        parseEntitlements(scope.entitlements, scope);
        shouldContinuePolling();
        scope.$on('$destroy', function () {
          if (pollingTimeout) {
            $timeout.cancel(pollingTimeout);
          }
        });
      }
    };
  });

})();
