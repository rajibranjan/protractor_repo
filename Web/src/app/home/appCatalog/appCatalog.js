(function () {
  'use strict';

  angular.module('hp.portal.home.appCatalog', [
    'hp.portal.home.webservices-v2',
    'hp.portal.home.webservices.tasks',
    'hp.portal.home.appCatalog.appWidget',
    'hp.portal.home.navigation.routesAndUrls',
    'hp.portal.common.constants.commerceProviders'
  ])

  .controller('AppCatalogCtrl', function ($scope, $uibModal, Queries, Self, LinkParser, PageAlerts, $timeout) {

    var userId = LinkParser.getSelfId(Self);

    var hasEntitlement = function (app) {
      if (app.products) {
        return app.products.some(function (productItem) {
          // This does not check whether entitlement.isEntitled is true, it only checks if an entitlement is present.
          return productItem.entitlement;
        });
      }
      return false;
    };

    var mapEntitlements = function (app) {
      // Set all of the entitlements in an array on the app.
      app.entitlements = app.products.map(function (productItem) {
        return productItem.entitlement || null;
      }).filter(function (entitlement) {
        return entitlement !== null;
      });
      return app;
    };

    var loadApps = function () {
      return Queries.OfferingsAndProductsByUser.get({
        userId: userId
      }).then(function (queryResult) {
        var apps = queryResult.offeringsAndProducts || [];
        $scope.entitledApps = apps.filter(hasEntitlement).map(mapEntitlements);
        $scope.unentitledApps = apps.filter(function (app) {
          return !hasEntitlement(app);
        });
      });
    };

    var allProductsProvisioned = function (productIdsToWatch) {
      var entitledProductIds = $scope.entitledApps.map(function (app) {
        return app.products.map(function (productItem) {
          return LinkParser.getSelfId(productItem.product);
        });
      }).reduce(function (a, b) {
        return a.concat(b);
      });

      return productIdsToWatch.every(function (productId) {
        return entitledProductIds.some(function (entitledProductId) {
          return productId === entitledProductId;
        });
      });
    };

    $scope.signUp = function (offering, product) {
      var modal = $uibModal.open({
        templateUrl: 'app/home/appCatalog/shopping-cart-modal.html',
        controller: 'ShoppingCartModalCtrl',
        windowClass: 'shopping-cart-modal',
        resolve: {
          product: function () {
            return product;
          },
          offering: function () {
            return offering;
          }
        }
      });

      modal.result.then(function success(shoppingCart) {
        PageAlerts.add({
          type: 'success',
          timeout: PageAlerts.timeout.medium,
          templateUrl: 'shopping-cart-add-success.html',
          scope: {
            product: product,
            offering: offering
          }
        });

        var productIdsToWatch = shoppingCart.items.map(function (shoppingCartItem) {
          return shoppingCartItem.productId;
        });

        var tries = 0,
          maxRetries = 10,
          retryInterval = 1000;
        var shouldContinuePolling = function () {
          if (tries < maxRetries && !allProductsProvisioned(productIdsToWatch)) {
            $timeout(pollOfferingsAndProducts, retryInterval);
          }
        };

        var pollOfferingsAndProducts = function () {
          tries++;
          loadApps().then(shouldContinuePolling);
        };

        pollOfferingsAndProducts();
      }, function error(result) {
        if (result && result === 'error') {
          PageAlerts.add({
            type: 'danger',
            msg: 'cShoppingCartCreatedError',
            timeout: PageAlerts.timeout.medium
          });
        }
      });
    };

    loadApps();
  })

  .controller('ShoppingCartModalCtrl', function ($scope, $uibModalInstance, Self, LinkParser, RolesHelper, Roles, ShoppingCartService, ShoppingCartCheckout, offering, product, CommerceProviders) {
    $scope.offering = offering;
    $scope.product = product;
    $scope.quantity = 1;

    $scope.close = function () {
      $uibModalInstance.dismiss('close');
    };

    $scope.ok = function () {
      var accountId = LinkParser.getLinkId(Self, 'account');
      var shoppingCart = ShoppingCartService.create({
        accountId: accountId,
        items: [{
          productId: LinkParser.getSelfId($scope.product),
          quantity: $scope.quantity,
          commerceProvider: CommerceProviders.GLIS
        }]
      });
      shoppingCart.save().then(function () {
        ShoppingCartCheckout.startTask({
          shoppingCart: shoppingCart
        }).$promise.then(function success() {
          $uibModalInstance.close(shoppingCart);
        }, function error() {
          $uibModalInstance.dismiss('error');
        });
      });
    };
  });
})();
