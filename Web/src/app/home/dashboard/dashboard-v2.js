(function () {
  'use strict';

  angular.module('hp.portal.home.dashboardV2', [
    'hp.portal.common.widgets',
    'angularMoment'
  ])

  .controller('DashboardCtrl-v2', function ($scope, Self, $routeParams, LicenseService, LinkParser, AccountServiceRes, Globals, moment) {

    $scope.DashboardInfo = {};
    var accountInfo = {
      'paidLicenses': 0,
      'licensesNearEnd': 0,
      'activeLicenseCount': 0,
      'trialLicenses': 0,
      'activeAccountPercent': 0
    };
    var activeAccounts = 0;
    $scope.DashboardInfo.isDashboard = true;

    $scope.DashboardInfo.companyName = Self.account.name;
    if (Globals.permissions.isHPAdmin || Globals.permissions.isReseller) {
      $scope.DashboardInfo.permission = true;
    }
    else {
      $scope.DashboardInfo.permission = false;
    }

    function setAccountInfo(total) {
      AccountServiceRes.get({
        expand: 'adminContact',
        limit: total,
        qs: $routeParams.resellerId ? 'resellerId==' + $routeParams.resellerId : ''
      }).then(function (result) {
        angular.forEach(result.data, function (account) {
          LicenseService.get({
            qs: 'account.id==' + LinkParser.getSelfId(account),
            expand: 'offering',
            latest: true
          }).then(function (result) {
            setLicenseInfo(result);
          });
        });
      });
    }

    AccountServiceRes.get({
      expand: 'adminContact',
      qs: $routeParams.resellerId ? 'resellerId==' + $routeParams.resellerId : ''
    }).then(function (result) {
      accountInfo.accountCount = result.total;
      setAccountInfo(result.total);
    });

    function setLicenseInfo(licenses) {
      var isActive = false;
      var currentDate = moment();
      angular.forEach(licenses.data, function (license) {

        if (license.status === 'ACTIVE') {
          isActive = true;
          // no of solutions
          accountInfo.activeLicenseCount++;
          var endDate = moment(license.endDate);

          if (license.isTrial === true) {
            accountInfo.trialLicenses++;
            if (endDate.diff(currentDate, 'days') <= 15) {
              accountInfo.licensesNearEnd++;
            }
          }
          else {
            accountInfo.paidLicenses++;
            if (endDate.diff(currentDate, 'days') <= 60) {
              accountInfo.licensesNearEnd++;
            }
          }
        }
      });
      if (isActive === true) {
        activeAccounts++;
      }
      accountInfo.activeAccountPercent = (activeAccounts / accountInfo.accountCount) * 100;
      $scope.accountInfo = accountInfo;
    }
  });

})();
