(function () {
  'use strict';
  angular.module('hp.portal.home.welcome', [
      'hp.portal.common.webservices.entitlements',
      'hp.portal.home.appCatalog.appWidget',
      'hp.portal.home.navigation.routesAndUrls'
    ])
    .controller('WelcomeCtrl', function ($scope, $filter, $location, SdkConfig, EntitlementService, LinkParser, Self, PortalConfigurations, Profiles, RolesHelper, Roles, Routes, Globals) {

      // Redirect to the dashboard for HPAdmins and Resellers.
      if (!Profiles.LOCAL_TEST.isActive) {
        if (Globals.permissions.isHPAdmin || Globals.permissions.isReseller) {
          $location.url(Routes.dashboard);
          return;
        }
      }

      $scope.welcomeMessage = 'cWelcomeMessageCommon';

      SdkConfig.isWelcomePage = true;
      $scope.$on('$routeChangeStart', function () {
        SdkConfig.isWelcomePage = false;
      });

      PortalConfigurations.$promise.then(function () {
        $scope.dashboardUrl = PortalConfigurations['com.portal.dashboardUrl'];
      });

      EntitlementService.getWithOfferingAndProductAndLicense(LinkParser.getSelfId(Self)).then(function success(response) {
        var order = {
          'insights': 1,
          'hp-pullprint': 2,
          'hp-scansend': 3
        };

        $scope.products = [];

        $filter('orderBy')(response.data, function (entitlement) {
          // Order the entitlements.
          return order[entitlement.offering.fqName] || 1000;
        }).filter(function (entitlement) {
          // Filter out devicemanager.
          return entitlement.offering.fqName !== 'hp-devicemanager';
        }).forEach(function (entitlement) {
          var product = entitlement.product;
          $scope.products.push(product);
          product.offering = entitlement.offering;
          product.license = entitlement.license;

          if (!product.entitlements) {
            product.entitlements = [];
          }
          product.entitlements.push(entitlement);

        });
        if ($scope.products.length === 1 && $scope.products[0].offering.fqName === 'hp-pullprint') {
          // Note: this is a kludgey way to display special welcome messages for specific offerings.
          // A better approach may be to push strings into the i18n service for the offering, but that idea needs to be fleshed out.
          $scope.welcomeMessage = 'cWelcomeMessagePullPrintOnly' + (Globals.permissions.isCompanyAdmin ? 'Admin' : 'User');
        }
      });
    });
})();
