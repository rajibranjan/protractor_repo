(function () {
  'use strict';

  // Load all constants for dependencies
  angular.module('hp.portal.home.appConfig', ['init'])
    .constant('i18nPath', 'app/home/i18n/');

  angular.module('hp.portal.home.homeApp', [
    'hp.portal.home.appConfig', // This needs to be first for all apps
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'angulartics', 'angulartics.google.analytics',
    'ui.bootstrap',
    'hp.common.sdk',
    'hp.portal.common.init.cdn',
    'hp.portal.common.helpers',
    'hp.portal.home.accounts',
    'hp.portal.home.accounts.addAccount',
    'hp.portal.home.dashboard',
    'hp.portal.home.users.userDetails',
    'hp.portal.home.resellers',
    'hp.portal.home.resellers.addReseller',
    'hp.portal.home.navigation',
    'hp.portal.home.appCatalog',
    'hp.portal.home.appManagement',
    'hp.portal.home.users',
    'hp.portal.home.users.addUser',
    'hp.portal.home.users.csvImport',
    'hp.portal.home.accounts.accountDetails',
    'hp.portal.home.resellers.resellerDetails',
    'hp.portal.home.utilities',
    'hp.portal.home.navigation.routesAndUrls',
    'hp.portal.home.welcome',
    'hp.portal.home.serviceAccounts',
    'hp.portal.home.adminDashboard.dbMigrations',
    'hp.portal.home.adminDashboard',
    'hp.portal.home.adminDashboard.deadMessages',
    'hp.portal.home.adminDashboard.entityRepair',
    'hp.portal.home.adminDashboard.amqpGlobalConfigurations',
    'hp.portal.home.adminDashboard.accountGroups',
    'hp.portal.home.adminDashboard.accountGroups.accountGroupsDetails',
    'hp.portal.home.error',
    'hp.portal.home.help',
    'iOxpwSdkApp',
    'angularMoment',
    'hp.portal.home.accountsV2',
    'hp.portal.home.dashboardV2',
    'hp.portal.home.devicesV2'
  ])

  .run(function (Globals, Self, $route, $location, Roles, RolesHelper, LinkParser, cdnURL, serverURL, PortalConfigurations, Profiles, OfferingService) {
    Globals.cdnURL = cdnURL;
    Globals.serverURL = serverURL;
    Globals.portalConfigurations = PortalConfigurations;
    Globals.linkParser = LinkParser;
    Globals.profiles = Profiles;

    Globals.zipcodeRegex = /^[0-9]{5}(?:-____)?(?:[0-9]{4})?(?:-[0-9]{4})?$/;

    Globals.permissions = {
      isHPAdmin: false,
      isOfferingAdmin: false,
      isCompanyAdmin: false,
      isUser: false,
      isReseller: false
    };

    Globals.LicenseStatus = {
      EXPIRED: 'EXPIRED'
    };

    // Pre-loads and chaches all offerings when the app loads.
    OfferingService.getAllOfferings();

    Self.$promise.then(function success() {
      Globals.permissions.isHPAdmin = RolesHelper.selfHasRole(Roles.HP_ADMIN);
      Globals.permissions.isOfferingAdmin = RolesHelper.selfHasRole(Roles.OFFERING_ADMIN);
      Globals.permissions.isReseller = RolesHelper.selfHasRole(Roles.RESELLER);
      Globals.permissions.isCompanyAdmin = RolesHelper.selfHasRole(Roles.COMPANY_ADMIN);
      Globals.permissions.isUser = RolesHelper.selfHasRole(Roles.USER);
    });

    angular.forEach($route.routes, function (route) {
      if (route.resolve === undefined) {
        route.resolve = {};
      }
      route.resolve.getSelf = ['Self', function (Self) {
        return Self.$promise;
      }];

      route.resolve.getRoutePermissions = ['RoutePermissions', function (RoutePermissions) {
        return RoutePermissions.then(function (RoutePermissions) {
          // HP Admin is exempt from route restrictions
          if (Globals.permissions.isHPAdmin) {
            return; // Skip role permissions check
          }
          // Redirects don't need to be role restricted because the route
          // each redirects to will have its own permission wall
          else if (route.redirectTo) {
            return; // Skip role permissions check
          }
          // If the route being accessed is not whitelisted for this user
          else if (RoutePermissions.indexOf(route.originalPath) === -1) {
            // Redirect to the permission denied page.
            $location.path('/permission-denied');
          }
        });
      }];
    });
  })

  .config(function ($analyticsProvider, $routeProvider, $animateProvider, Routes) {
    // Setting this classNameFilter makes it so that animation will only run
    // when we explicitely specify this class on an element. This prevents animation
    // from running by default in places such as ng-views, ng-repeats, ng-hide/show, etc.
    $animateProvider.classNameFilter(/hp-animate/);

    $analyticsProvider.withAutoBase(true); // Add the app-name to analytics pageviews

    var usersPage = {
      templateUrl: 'app/home/users/users.html',
      controller: 'UsersCtrl'
    };

    var userDetailsPage = {
      templateUrl: 'app/home/users/user-details.html',
      controller: 'UserDetailsCtrl',
      resolve: {
        user: ['UserService', 'Self', '$route', function (UserService, Self, $route) {
          if ($route.current.params.id) {
            return UserService.getOne({
              id: $route.current.params.id,
              expand: 'account'
            }).$promise;
          }
          else {
            return Self.$promise;
          }
        }]
      }
    };

    var accountsPage = {
      templateUrl: 'app/home/accounts/accounts.html',
      controller: 'AccountsCtrl'
    };

    var accountsPageV2 = {
      templateUrl: 'app/home/accounts/accounts-v2.html',
      controller: 'AccountsCtrl-v2'
    };

    var accountDetailsPage = {
      templateUrl: 'app/home/accounts/account-details.html',
      controller: 'AccountDetailsCtrl',
      resolve: {
        account: ['AccountService', '$route', 'LinkParser', 'Self', 'ResellerService', 'Globals', function (AccountService, $route, LinkParser, Self, ResellerService, Globals) {

          var accountId = $route.current.params.id;
          if (accountId) {
            return AccountService.getById(accountId).$promise;
          }
          else {
            return Self.$promise.then(function () {
              var resource = AccountService;
              if (Globals.permissions.isReseller) {
                resource = ResellerService;
              }
              return resource.getById(LinkParser.getLinkId(Self, 'account')).$promise;
            });
          }
        }]
      }
    };

    var resellerDetailsPage = {
      templateUrl: 'app/home/resellers/reseller-details.html',
      controller: 'ResellerDetailsCtrl',
      resolve: {
        reseller: ['ResellerService', '$route', function (ResellerService, $route) {
          return ResellerService.getById($route.current.params.id).$promise;
        }]
      }
    };

    var accountGroupsDetailsPage = {
      templateUrl: 'app/home/admin-dashboard/account-groups-details.html',
      controller: 'AccountGroupsDetailsCtrl',
      resolve: {
        accountGroup: ['AccountGroupService', '$route', function (AccountGroupService, $route) {
          var accountGroupId = $route.current.params.id;
          return AccountGroupService.getById(accountGroupId).$promise;
        }]
      }
    };

    $routeProvider
      .when(Routes.dashboard, {
        templateUrl: 'app/home/dashboard/dashboard.html',
        controller: 'DashboardCtrl'
      })
      .when(Routes.dashboardV2, {
        templateUrl: 'app/home/dashboard/dashboard-v2.html',
        controller: 'DashboardCtrl-v2'
      })
      .when(Routes.resellerDashboardV2, {
        templateUrl: 'app/home/dashboard/dashboard-v2.html',
        controller: 'DashboardCtrl-v2'
      })
      .when(Routes.welcome, {
        templateUrl: 'app/home/welcome/welcome.html',
        controller: 'WelcomeCtrl'
      })
      .when(Routes.appCatalog, {
        templateUrl: 'app/home/appCatalog/appCatalog.html',
        controller: 'AppCatalogCtrl'
      })
      .when(Routes.appManagement, {
        templateUrl: 'app/home/app-management/app-management.html',
        controller: 'AppManagementCtrl'
      })
      .when(Routes.help, {
        templateUrl: 'app/home/help/help.html',
        controller: 'HelpCtrl'
          //redirectTo: Routes.dashboard // Remove this when we have turned on the Help page again
      })
      .when(Routes.adminDashboard, {
        templateUrl: 'app/home/admin-dashboard/admin-dashboard.html',
        controller: 'AdminDashboardCtrl'
      })
      .when(Routes.devices, {
        templateUrl: 'app/home/devices/devices.html',
        controller: 'EnableDeviceCtrl'
      })
      .when(Routes.devicesV2, {
        templateUrl: 'app/home/devices/devices-v2.html',
        controller: 'DevicesV2Ctrl'
      })
      .when(Routes.addUser, {
        templateUrl: 'app/home/users/add-user.html',
        controller: 'AddUserCtrl'
      })
      .when(Routes.csvImport, {
        templateUrl: 'app/home/users/csv-import.html',
        controller: 'CsvImportCtrl'
      })
      .when(Routes.addAccount, {
        templateUrl: 'app/home/accounts/add-account.html',
        controller: 'AddAccountCtrl'
      })
      .when(Routes.addReseller, {
        templateUrl: 'app/home/resellers/add-reseller.html',
        controller: 'AddResellerCtrl'
      })
      .when(Routes.permissionDenied, {
        templateUrl: 'app/home/error/error.html',
        controller: 'PermissionDeniedCtrl'
      })
      .when(Routes.internalError, {
        templateUrl: 'app/home/error/error.html',
        controller: 'InternalErrorCtrl'
      })
      .when(Routes.entityRepair, {
        templateUrl: 'app/home/admin-dashboard/entity-repair.html',
        controller: 'EntityRepairCtrl'
      })
      .when(Routes.profile, userDetailsPage)
      .when(Routes.users, usersPage)
      .when(Routes.accountUsers, usersPage)
      .when(Routes.userDetails, userDetailsPage)
      .when(Routes.accounts, accountsPage)
      .when(Routes.resellerAccounts, accountsPage)
      .when(Routes.accountsV2, accountsPageV2)
      .when(Routes.resellerAccountsV2, accountsPageV2)
      .when(Routes.resellers, {
        templateUrl: 'app/home/resellers/resellers.html',
        controller: 'ResellersCtrl'
      })
      .when(Routes.resellerDetails, resellerDetailsPage)
      .when(Routes.myCompany, accountDetailsPage)
      .when(Routes.accountDetails, accountDetailsPage)
      .when(Routes.utilities, {
        templateUrl: 'app/home/utilities/utilities.html',
        controller: 'UtilitiesCtrl'
      })
      .when(Routes.deadMessages, {
        templateUrl: 'app/home/admin-dashboard/dead-messages.html',
        controller: 'DeadMessagesCtrl'
      })
      .when(Routes.dbMigrations, {
        templateUrl: 'app/home/admin-dashboard/db-migrations.html',
        controller: 'DBMigrationsCtrl'
      })
      .when(Routes.amqpGlobalConfigurations, {
        templateUrl: 'app/home/admin-dashboard/amqpGlobalConfigurations.html',
        controller: 'AMQPGlobalConfigurationsCtrl'
      })
      .when(Routes.accountGroups, {
        templateUrl: 'app/home/admin-dashboard/account-groups.html',
        controller: 'AccountGroupsCtrl'
      })
      .when(Routes.stackCleanup, {
        templateUrl: 'app/home/admin-dashboard/stack-cleanup/stack-cleanup.html',
        controller: 'StackCleanupCtrl'
      })
      .when(Routes.accountGroupsDetails, accountGroupsDetailsPage)
      .when(Routes.serviceAccounts, {
        templateUrl: 'app/home/service-accounts/service-accounts.html',
        controller: 'ServiceAccountsCtrl'
      })
      .otherwise({
        redirectTo: Routes.welcome
      });
  })

  .controller('HomeCtrl', function ($scope, $translate, LinkParser, Self, testHookService, amMoment, $rootScope) {
    testHookService.init('home');

    $scope.self = Self;

    $rootScope.$on('changeLanguage', function (event, locale) {
      amMoment.changeLocale(locale);
    });


    // This is an example of how any component could listen for 'hp_httpError' events.
    // $scope.$on('hp_httpError', function (event, response) {
    //   $scope.alerts.push({
    //     type: 'danger',
    //     msg: response.config.url + ' returned ' + response.status
    //   });
    // });
  });


})();
