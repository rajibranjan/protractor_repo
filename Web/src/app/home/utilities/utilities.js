(function () {
  'use strict';

  angular.module('hp.portal.home.utilities', [
    'hp.portal.common.webservices',
    'hp.portal.home.webservices-v2'
  ])

  .controller('UtilitiesCtrl', function ($scope, OfferingService, DownloadService) {
    $scope.offerings = OfferingService.getAllOfferings();
    $scope.downloads = DownloadService.getPager({
      itemsPerPage: 10
    });
    $scope.downloads.loadPage();
  });
})();
