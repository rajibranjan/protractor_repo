(function () {
  'use strict';

  angular.module('hp.portal.home.adminDashboard.deadMessages', [
    'hp.portal.common.webservices',
    'hp.portal.home.webservices-v2'
  ])

  .controller('DeadMessagesCtrl', function ($scope, DeadMessageService, PageAlerts, $q) {
    $scope.deadMessages = DeadMessageService.getPager();
    $scope.deadMessages.loadPage();

    $scope.getCausality = function (deadMessage) {
      if (deadMessage.causality) {
        return deadMessage.causality;
      }
      else if (deadMessage.payload) {
        deadMessage.causality = angular.fromJson(deadMessage.payload).causality;
        return deadMessage.causality;
      }
      return '';
    };

    $scope.setAllSelected = function (shouldSelect) {
      angular.forEach($scope.deadMessages.list, function (deadMessage) {
        deadMessage.selected = shouldSelect;
      });
    };


    $scope.someAreSelected = function () {
      return $scope.deadMessages.list.some(function (deadMessage) {
        return deadMessage && deadMessage.selected;
      });
    };

    $scope.formatErrorPayload = function (stackTrace, numLines) {
      if (!stackTrace) {
        return stackTrace;
      }
      var at = 'at ';
      var stackTraceArray = stackTrace.split(at);
      if (!numLines) {
        numLines = stackTraceArray.length;
      }

      var result = stackTraceArray[0];
      var maxLineLength = 200;

      angular.forEach(stackTraceArray, function (line, index) {
        if (index === 0 || index >= numLines) {
          return;
        }
        result = result + at;
        if (line.length >= maxLineLength) {
          //We don't want our lines to be too long.
          result = result + line.substring(0, maxLineLength) + '\n\t\t' + line.substring(maxLineLength);
        }
        else {
          result = result + line;
        }
      });

      return result;
    };

    $scope.setClicked = function (object) {
      object.clicked = !object.clicked;
    };

    var success = function (successMessage) {
      PageAlerts.add({
        type: 'success',
        msg: successMessage,
        timeout: PageAlerts.timeout.medium
      });
      $scope.deadMessages.refresh();
    };

    var error = function (errorMessage) {
      PageAlerts.add({
        type: 'danger',
        msg: errorMessage,
        timeout: PageAlerts.timeout.long
      });
    };

    var deleteDeadMessage = function (deadMessage) {
      return deadMessage.delete();
    };

    var requeueDeadMessage = function (deadMessage) {
      deadMessage.requeue = true;
      return deadMessage.save();
    };

    var processSelectedMessages = function (operation, successMessage, errorMessage) {
      var promiseArray = [];
      angular.forEach($scope.deadMessages.list, function (deadMessage) {
        if (deadMessage.selected) {
          var promise = operation(deadMessage);
          promiseArray.push(promise);
        }
      });
      if (promiseArray.length) {
        $q.all(promiseArray)
          .then(function () {
            success(successMessage);
          }, function () {
            error(errorMessage);
          });
      }
    };

    $scope.deleteSelected = function () {
      processSelectedMessages(deleteDeadMessage, 'cSuccessfullyDeletedMessages', 'cErrorDeletingMessages');
    };

    $scope.requeueSelected = function () {
      processSelectedMessages(requeueDeadMessage, 'cSuccessfullyRequeuedMessages', 'cErrorRequeuingMessages');
    };
  });
})();
