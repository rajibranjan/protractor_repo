(function () {
  'use strict';

  angular.module('hp.portal.home.adminDashboard.amqpGlobalConfigurations', [
      'hp.portal.common.webservices',
      'hp.portal.home.webservices-v2'
    ])
    .controller('AMQPGlobalConfigurationsCtrl', function ($scope, AMQPGlobalConfigurationService, currentGitHash, PageAlerts, $q) {
      $scope.globalConfigs = AMQPGlobalConfigurationService.getPager();
      $scope.globalConfigs.loadPage();

      $scope.currentGitHash = currentGitHash;

      $scope.setAllSelected = function (shouldSelect) {
        angular.forEach($scope.globalConfigs.list, function (globalConfig) {
          globalConfig.selected = shouldSelect;
        });
      };

      // Disable/enable the enable/disable buttons as appropriate
      $scope.someHaveEnabledSetTo = function (shouldBeEnabled) {
        return $scope.globalConfigs.list.some(function (globalConfig) {
          return globalConfig && Boolean(globalConfig.consumersEnabled) === shouldBeEnabled && globalConfig.selected;
        });
      };

      $scope.setSelectedConsumersEnabled = function (areConsumersEnabled) {
        var promiseArray = [];
        angular.forEach($scope.globalConfigs.list, function (globalConfig) {
          if (globalConfig.selected && globalConfig.consumersEnabled !== areConsumersEnabled) {
            globalConfig.consumersEnabled = areConsumersEnabled;
            promiseArray.push(globalConfig.save());
          }
        });
        if (promiseArray.length) {
          $q.all(promiseArray)
            .then(success, error);
        }
      };

      var success = function () {
        PageAlerts.add({
          type: 'success',
          msg: 'cGlobalConfigUpdateSuccessful',
          timeout: PageAlerts.timeout.medium
        });
        $scope.globalConfigs.refresh();
      };

      var error = function () {
        PageAlerts.add({
          type: 'error',
          msg: 'cGlobalConfigUpdateError',
          timeout: PageAlerts.timeout.long
        });
      };

    });

})();
