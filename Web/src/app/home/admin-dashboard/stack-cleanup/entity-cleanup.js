(function () {
  'use strict';

  angular.module('hp.portal.home.adminDashboard.stackCleanup.entityCleanup', [
    'hp.portal.home.adminDashboard.stackCleanup.entityCleanup.fetchEntitiesModal',
    'hp.portal.home.adminDashboard.stackCleanup.entityCleanup.deleteEntitiesModal',
  ])

  .directive('entityCleanup', function ($q, copyToClipboard, mockPager, FetchEntitiesModal, DeleteEntitiesModal) {
    return {
      restrict: 'E',
      templateUrl: 'app/home/admin-dashboard/stack-cleanup/entity-cleanup.html',
      scope: {
        entityName: '=',
        pager: '=servicePager',
        trigger: '@',
        parentLogger: '=',
      },
      link: function (scope) {
        scope.trimmedEntityName = scope.entityName.replace(/ /g, '');
        scope.logs = [];

        function log(status, logEntry) {
          if (angular.isString(logEntry)) {
            logEntry = {
              msg: logEntry
            };
          }
          else {
            logEntry = angular.copy(logEntry);
            delete logEntry.entity;
          }

          logEntry.entityName = scope.entityName;
          logEntry.status = status;
          scope.logs.push(logEntry);
          scope.parentLogger.add(logEntry);
        }

        var logger = {
          info: log.bindArgs('default'),
          success: log.bindArgs('success'),
          warn: log.bindArgs('warning'),
          error: log.bindArgs('danger'),
        };

        function findTestEntities(startTime) {
          return FetchEntitiesModal.open({
            entityName: scope.entityName,
            pager: scope.pager,
            // pager: mockPager, // IMPORTANT: only uncomment this for local debugging/testing!!!
            logger: logger
          }).then(function (result) {
            if (startTime) {
              scope.entities = result.entities.filter(function (entityInfo) {
                return !entityInfo.entity.createdDate || entityInfo.entity.createdDate < startTime;
              });
            }
            else {
              scope.entities = result.entities;
            }

            if (result.cancelled) {
              return $q.reject(result);
            }
          });
        }

        function deleteTestEntities() {
          return DeleteEntitiesModal.open({
            entityName: scope.entityName,
            entities: scope.entities,
            logger: logger
          }).then(function (result) {
            // Initialize the deleted and errors arrays.
            scope.deleted = scope.deleted ? scope.deleted : [];
            scope.errors = scope.errors ? scope.errors : [];

            // Concatenate the modal's deleted and error arrays.
            scope.deleted.push.apply(scope.deleted, result.deleted);
            scope.errors.push.apply(scope.errors, result.errors);

            return result;
          });
        }

        function findAndDeleteEntities(startTime) {
          return findTestEntities(startTime).then(deleteTestEntities);
        }

        scope.$on(scope.trigger, function (event, callback, startTime) {
          findAndDeleteEntities(startTime).then(callback, callback);
        });

        scope.copyLogs = copyToClipboard;
        scope.findTestEntities = findTestEntities;
        scope.deleteTestEntities = deleteTestEntities;
        scope.findAndDeleteEntities = findAndDeleteEntities;
      }
    };

  })

  // This is a dummy mockPager factory that will be utilized when the mockPager below gets stripped out of production code.
  .factory('mockPager', function () {
    return {};
  })

  /*test-code*/
  .factory('mockPager', function ($q, $timeout) {
    // This is some TEST-ONLY code that is useful to fake the fetch & delete operations for development purposes.
    var pager = {
      pageNumber: 1,
      getPageNumber: function () {
        return this.pageNumber;
      },
      getTotalPages: function () {
        return 10;
      },
      getTotal: function () {
        return 1000;
      }
    };

    function MockEntity() {
      /* jshint ignore:start */
      var guidMask = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
      var randomGuid = guidMask.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
          v = c === 'x' ? r : r & 0x3 | 0x8;
        return v.toString(16);
      });
      this.links = [{
        rel: 'self',
        href: randomGuid
      }];
      /* jshint ignore:end */

      this.delete = function () {
        var dfd = $q.defer();

        $timeout(function () {
          if (Math.random() > 0.3) {
            dfd.resolve();
          }
          else {
            var response = {
              data: {
                message: 'Something blowed up!',
                exceptionType: 'nuclear',
              },
              status: 409
            };
            dfd.reject(response);
          }
        }, Math.floor(Math.random() * (20 - 1)) + 1);

        return dfd.promise;
      };
    }


    pager.refresh = function (pageNumber) {
      pager.pageNumber = pageNumber;

      var dfd = $q.defer();

      $timeout(function () {
        var entities = [];
        for (var i = 0; i < 100; i++) {
          entities.push(new MockEntity());
        }
        dfd.resolve(entities);
      }, 300);

      return dfd.promise;
    };
    pager.loadNextPage = function () {
      return pager.refresh(pager.pageNumber + 1);
    };

    return pager;
  });
  /*end-test-code*/

})();
