(function () {
  'use strict';

  angular.module('hp.portal.home.adminDashboard.accountGroups', [
      'hp.portal.home.webservices-v2'
    ])
    .controller('AccountGroupsCtrl', function ($scope, AccountGroupService, PageAlerts) {
      $scope.accountGroups = AccountGroupService.getPager();
      $scope.accountGroups.loadPage();

      var deleteGroup = function () {
        return $scope.accountGroupSelected.delete().then(function success() {
          PageAlerts.add({
            type: 'success',
            msg: 'cDeleteSuccessful',
            timeout: PageAlerts.timeout.short
          });
          $scope.accountGroups.refresh();
        }, function error() {
          PageAlerts.add({
            type: 'danger',
            msg: 'cDeleteAccountGroupError',
            timeout: PageAlerts.timeout.long
          });
        });
      };

      $scope.deleteGroupSubmit = function (accountGroup) {
        $scope.accountGroupSelected = accountGroup;
        $scope.$broadcast('hpDisableAccountGroupsForm', deleteGroup, false);
      };

    });
})();
