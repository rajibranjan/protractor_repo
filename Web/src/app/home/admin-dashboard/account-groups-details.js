(function () {
  'use strict';

  angular.module('hp.portal.home.adminDashboard.accountGroups.accountGroupsDetails', [
    'hp.portal.common.webservices'
  ])

  .controller('AccountGroupsDetailsCtrl', function ($scope, PageAlerts, accountGroup) {

    $scope.accountIdSelected = '';
    $scope.accountGroup = accountGroup.clone();
    $scope.newAccountId = {
      id: 'xxxxxxxx-yyyy-yyyy-yyyy-zzzzzzzzzzzz'
    };

    var addNewAccountId = function () {
      // This method MUST return a promise in order to work with the
      // hpDisableOn directive.
      if ($scope.newAccountId.id.length === 36 &&
        $scope.newAccountId.id.split('-').length - 1 === 4 &&
        $scope.accountGroup.accountIds.indexOf($scope.newAccountId.id) === -1) {
        // Add the new Account Id to the array.
        $scope.accountGroup.accountIds.push($scope.newAccountId.id);

        return $scope.accountGroup.save().then(function success() {
          PageAlerts.add({
            type: 'success',
            msg: 'cSaveSuccessful',
            timeout: PageAlerts.timeout.short
          });
        }, function error() {
          PageAlerts.add({
            type: 'danger',
            msg: 'cAddAccountIdError',
            timeout: PageAlerts.timeout.long
          });
        });
      }
      else {
        PageAlerts.add({
          type: 'danger',
          msg: 'cAddAccountIdError',
          timeout: PageAlerts.timeout.long
        });
      }
    };

    $scope.addNewAccountIdSubmit = function () {
      $scope.$broadcast('hpDisableAccountGroupsDetailsForm', addNewAccountId, false);
    };

    var deleteAccountId = function () {
      // This method MUST return a promise in order to work with the
      // hpDisableOn directive.
      var index = $scope.accountGroup.accountIds.indexOf($scope.accountIdSelected);
      if (index !== -1) {
        $scope.accountGroup.accountIds.splice(index, 1);
      }

      return $scope.accountGroup.save().then(function success() {
        PageAlerts.add({
          type: 'success',
          msg: 'cDeleteSuccessful',
          timeout: PageAlerts.timeout.short
        });
      }, function error() {
        PageAlerts.add({
          type: 'danger',
          msg: 'cDeleteAccountIdError',
          timeout: PageAlerts.timeout.long
        });
      });
    };

    $scope.deleteAccountIdSubmit = function (accountId) {
      $scope.accountIdSelected = accountId;
      $scope.$broadcast('hpDisableAccountGroupsDetailsForm', deleteAccountId, false);
    };
  });
})();
