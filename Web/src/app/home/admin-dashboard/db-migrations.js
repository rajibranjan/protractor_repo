(function () {
  'use strict';

  angular.module('hp.portal.home.adminDashboard.dbMigrations', [
    'hp.portal.home.webservices-v2'
  ])

  .controller('DBMigrationsCtrl', function ($scope, SchemaVersionService) {
    $scope.schemaVersions = SchemaVersionService.getPager({
      itemsPerPage: 5
    });
    $scope.schemaVersions.loadPage();
  });

})();
