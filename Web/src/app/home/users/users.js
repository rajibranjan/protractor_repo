(function () {
  'use strict';

  angular.module('hp.portal.home.users', [
    'hp.portal.common.pagination.itemsPerPage',
    'hp.portal.home.webservices-v2'
  ])

  .controller('UsersCtrl', function ($routeParams, $scope, UserService) {
    $scope.users = UserService.getPager({
      queryString: $routeParams.accountId ? 'accountId==' + $routeParams.accountId : ''
    });
    $scope.users.loadPage();

    var searchFields = ['email', 'firstName', 'lastName'];
    $scope.search = function (searchString) {
      var searchExpressions = searchFields.map(function (param) {
        return param + '*="*' + searchString + '*"';
      });

      var fullSearchString = searchExpressions.join(' || ');

      // This will trigger the current page to refresh with the new search.
      $scope.users.searchBy(fullSearchString);
    };
  });

})();
