(function () {
  'use strict';

  angular.module('hp.portal.home.users.addUser', [
    'ui.mask',
    'hp.portal.home.navigation.routesAndUrls',
    'hp.portal.common.webservices',
    'hp.portal.home.webservices-v2',
    'checklist-model',
    'hp.portal.common.forms.userForm'

  ])

  .controller('AddUserCtrl', function ($scope, $location, LinkParser, UserService, Self, PageAlerts, Urls, ErrorCodeMapper) {
    var setupUser = function () {
      $scope.user = {
        links: [
          LinkParser.getLink(Self, 'account')
        ],
        locale: Self.getLocale()
      };

    };
    setupUser();

    var addUser = function (saveAndNew) {
      var user = UserService.create($scope.user);
      return user.save().then(redirectOrResetForm.bindArgs(saveAndNew))
        .then(addUserSuccess.bindArgs($scope.user), addUserError);
    };

    var addUserSuccess = function (user) {
      var alert = {
        type: 'success',
        msg: 'cAddUserSuccess',
        translateValues: {
          name: user.firstName + ' ' + user.lastName,
          email: user.email
        },
        timeout: PageAlerts.timeout.medium
      };

      PageAlerts.add(alert);
    };

    var addUserError = function (user) {
      if (!ErrorCodeMapper.hasErrorCodes(user.data)) {
        PageAlerts.add({
          type: 'danger',
          msg: 'cSaveFailed',
          timeout: PageAlerts.timeout.long
        });
      }
    };

    function redirectOrResetForm(saveAndNew) {
      if (saveAndNew) {
        // Re-initialize the models to be able to create a new user.
        setupUser();
      }
      else {
        // Redirect to the manage account apge.
        $location.url(Urls.manageUsers());
      }
    }

    $scope.addUserSubmit = function () {
      $scope.$broadcast('hpDisableAddUserForm', addUser.bindArgs(false));
    };

    $scope.addUserAndNewSubmit = function () {
      $scope.$broadcast('hpDisableAddUserForm', addUser.bindArgs(true));
    };


    $scope.cancelAddUser = function () {
      $location.url(Urls.manageUsers());
    };

  });
})();
