(function () {
  'use strict';
  angular.module('hp.portal.home.users.csvImport', [
    'angularFileUpload',
    'hp.portal.home.navigation.routesAndUrls',
    'hp.portal.common.webservices',
    'hp.portal.common.webservices.endpoints',
    'hp.portal.home.webservices.tasks',
  ])

  // We may want to use a different fileUploader if we need to upload a file again.
  .controller('CsvImportCtrl', function ($scope, $location, $timeout, $window, LinkParser, Self, PageAlerts, UserCSVUpload, FileUploader, Urls, PortalConfigurations, Endpoints, TaskStatus) {
    $scope.isImportingUsers = false;

    // create a uploader with options
    var uploader = $scope.uploader = new FileUploader({
      scope: $scope, // to automatically update the html. Default: $rootScope
      method: 'POST',
      url: Endpoints.userCSVUpload,
      alias: 'csvFile',
      formData: [{
        accountId: LinkParser.getLinkId(Self, 'account')
      }],
      filters: []
    });

    var selectedItem;
    uploader.onAfterAddingFile = function (item) {
      selectedItem = item;
      restoreVisibility();
    };

    var uploadComplete = function (pageAlertArgs) {
      $scope.isImportingUsers = false;
      PageAlerts.add(pageAlertArgs);
    };

    uploader.onCompleteItem = function (item, response, httpStatus) {
      if (httpStatus === 400) {
        uploadComplete({
          type: 'danger',
          msg: 'cCSVImportParseError',
          timeout: PageAlerts.timeout.long
        });
      }
      else if (httpStatus === 401) {
        //Redirect user to login page if unauthorized.
        $scope.isImportingUsers = false;
        $window.location.href = PortalConfigurations['com.portal.logoutUrl'];
      }
      else if (httpStatus === 409) {
        uploadComplete({
          type: 'danger',
          msg: 'cCSVImportConstraintError',
          timeout: PageAlerts.timeout.long
        });
      }
      else if (httpStatus > 400 || response === undefined) {
        uploadComplete({
          type: 'danger',
          msg: 'cCSVImportError',
          timeout: PageAlerts.timeout.long
        });
      }
      else {
        var uploadAlert = {
          type: 'info',
          msg: 'cCSVUploadSuccess',
          timeout: PageAlerts.timeout.long
        };
        PageAlerts.add(uploadAlert);

        var error = function (task) {
          if (task.status === TaskStatus.TIMEOUT) {
            uploadComplete({
              type: 'warning',
              msg: 'cCSVImportTimeout',
              timeout: PageAlerts.timeout.long
            });
          }
          else if (task.status === TaskStatus.FAILED) {
            uploadComplete({
              type: 'warning',
              templateUrl: 'csv-import-error.html',
              scope: {
                errorUsers: task.failedCSVFileUsers
              },
              timeout: PageAlerts.timeout.long
            });
          }
          else {
            uploadComplete({
              type: 'danger',
              msg: 'cCSVImportError',
              timeout: PageAlerts.timeout.long
            });
          }
        };

        var done = function (task) {
          if (task.status === TaskStatus.COMPLETED) {
            uploadComplete({
              type: 'success',
              msg: 'cAddUsersSuccess',
              timeout: PageAlerts.timeout.medium
            });
            redirectToManageUsers();
          }
          else {
            error(task);
          }
        };

        var taskId = LinkParser.getSelfId(response);
        UserCSVUpload.getExisting(taskId)
          .waitForComplete({
            maxPollCount: -1, // infinite
            maxPollTimeMs: 300000 // 5 minutes
          }).then(done, error)
          .finally(PageAlerts.remove.bindArgs(uploadAlert));
      }
    };

    var newField;
    $scope.uploadFile = function () {
      if (selectedItem) {
        selectedItem.upload();
        $scope.isImportingUsers = true;

        //The rest is ie9 stuff.
        // Show the new box (see restoreVisibility)
        if (newField) {
          newField.css('display', 'block');
          newField = null;
          selectedItem = null;
        }
      }
    };

    // Defect in ie9. Because it doesn't support HTML5, the angular-file-upload library supports multiple files by duplicating the input.
    // When duplicating file input, the old input box is "covered" with the new one, which also covers the file name, so the user can't see it.
    // This method hides the new input and shows the old input.
    function restoreVisibility() {
      //This function is only applied to ie9.
      if (uploader.isHTML5) {
        return;
      }
      //In case the user picks multiple file.
      if (newField) {
        newField.remove();
        newField = null;
      }
      //Timeout because there is no event triggered when the new file input is created, but we do know it's immediately after the "afteraddingfile" event.
      $timeout(function () {
        var inputFields = document.getElementsByName('csvFile');
        //
        for (var i = 0; i < inputFields.length; i++) {
          var isVisible = (inputFields[i].offsetWidth > 0 || inputFields[i].offsetHeight > 0);
          if (isVisible) {
            //The display field was set as inline in the library so must be set inline here.
            newField = angular.element(inputFields[i]);
            newField.css('display', 'none');
          }
          else {
            var tempField = angular.element(inputFields[i]);
            tempField.css('display', 'block');
          }
        }
      }, 100);
    }

    function redirectToManageUsers() {
      $location.url(Urls.manageUsers());
    }

    $scope.cancelCsvImport = function () {
      redirectToManageUsers();
    };
  });
})();
