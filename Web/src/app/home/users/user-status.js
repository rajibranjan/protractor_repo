(function () {
  'use strict';

  angular.module('hp.portal.home.users.userStatus', [])

  .constant('UserStatus', {
    CREATED: 'CREATED',
    ACTIVE: 'ACTIVE',
    SUSPENDED: 'SUSPENDED'
  });

})();
