(function () {
  'use strict';

  angular.module('hp.portal.home.users.userDetails', [
    'ui.mask',
    'hp.portal.common.webservices',
    'hp.portal.home.webservices-v2',
    'hp.portal.home.webservices.tasks',
    'hp.portal.home.users.userStatus',
    'hp.portal.home.navigation.routesAndUrls',
    'hp.portal.common.webservices.endpoints',
    'checklist-model',
    'hp.portal.common.forms.userForm'
  ])

  .controller('UserDetailsCtrl', function ($scope, $location, LinkParser, Self, UserService, UserStatus, user, PageAlerts, ResendActivationEmail, Urls, UpdateCredentials, changeLanguage, CountrySettings, ErrorCodeMapper, PasswordPolicy) {

    user = UserService.fromExisting(user);
    $scope.user = user.clone(); // Copy the user so we are updating a copy of the user in the form.
    $scope.isSelf = (LinkParser.getSelfId(Self) === LinkParser.getSelfId($scope.user));
    $scope.accordionStatus = {};
    $scope.hasStatusCreated = (user.status === UserStatus.CREATED);
    $scope.passwordPolicy = PasswordPolicy.getForUser(Self);

    var isCompanyContact = (LinkParser.getLinkId($scope.user.account, 'adminContact') === LinkParser.getSelfId($scope.user));
    $scope.showDeleteButton = (!$scope.isSelf && !isCompanyContact);

    $scope.setStatusClass = function () {
      if ($scope.user.status === UserStatus.CREATED) {
        return 'label-warning';
      }
      else if ($scope.user.status === UserStatus.ACTIVE) {
        return 'label-success';
      }
      else if ($scope.user.status === UserStatus.SUSPENDED) {
        return 'label-danger';
      }
    };

    $scope.resetForm = function () {
      $scope.user = user.clone();
    };

    if ($scope.isSelf === true) {
      $scope.$watch(
        function () {
          return Self.getLocale();
        },
        function () {
          $scope.user.locale = Self.getLocale();
        }
      );
      CountrySettings.get(Self.account.country).$promise.then(function success(response) {
        $scope.isEmailOptInDisplayed = response.isEmailOptInDisplayed;
      });
    }

    var updateUser = function () {
      // This method MUST return a promise in order to work with the
      // hpDisableOnSubmit directive.
      return $scope.user.save().then(function success(updatedUser) {

        if (LinkParser.getSelfId(Self) === LinkParser.getSelfId(updatedUser)) {
          changeLanguage($scope.user.locale);
          Self.extend(angular.copy(updatedUser));
        }

        PageAlerts.add({
          type: 'success',
          msg: 'cSaveSuccessful',
          timeout: PageAlerts.timeout.medium
        });
        user = updatedUser;
        $scope.user = user.clone();

      }, function error(response) {
        if (response.status === 422) {
          PageAlerts.add({
            type: 'danger',
            msg: response.data.message,
            timeout: PageAlerts.timeout.medium
          });
        }
        else {
          PageAlerts.add({
            type: 'danger',
            msg: 'cSaveFailed',
            timeout: PageAlerts.timeout.medium
          });
        }
      });
    };

    $scope.updateUserSubmit = function () {
      $scope.$broadcast('hpDisableUserDetailsForm', updateUser, true);
    };

    var deleteUser = function () {
      // This method MUST return a promise in order to work with the
      // hpDisableOn directive.
      return $scope.user.delete().then(function success() {
        PageAlerts.add({
          path: Urls.manageUsers(),
          type: 'success',
          msg: 'cSuccessfullyDeletedUser',
          translateValues: {
            name: user.firstName + ' ' + user.lastName
          },
          timeout: PageAlerts.timeout.medium
        });
        $location.url(Urls.manageUsers());
      });
    };

    $scope.deleteUserSubmit = function () {
      $scope.$broadcast('hpDisableUserDetailsForm', deleteUser, false);
    };

    var resetChangePasswordForm = function () {
      // Hide the change password form and clear the form
      $scope.accordionStatus.showPasswordForm = false;
      $scope.changePasswordFormData = {};
    };
    resetChangePasswordForm();
    var changePassword = function () {
      return UpdateCredentials.changePassword($scope.changePasswordFormData.oldPassword, $scope.changePasswordFormData.password)
        .$promise.then(function success() {
          PageAlerts.add({
            type: 'success',
            msg: 'cPasswordSuccessChange',
            timeout: PageAlerts.timeout.medium
          });

          resetChangePasswordForm();
        }, function error(task) {
          if (task.httpStatus === 409) {
            PageAlerts.add({
              type: 'danger',
              msg: 'cPasswordErrorBadPassword',
              timeout: PageAlerts.timeout.medium
            });
          }
          else if (!ErrorCodeMapper.hasErrorCodes(task)) {
            PageAlerts.add({
              type: 'danger',
              msg: 'cPasswordError',
              timeout: PageAlerts.timeout.medium
            });
          }
        });
    };

    $scope.changePasswordSubmit = function () {
      $scope.$broadcast('hpDisablePasswordForm', changePassword, true);
    };

    $scope.resendActivationEmail = function () {
      var userId = LinkParser.getSelfId($scope.user);
      ResendActivationEmail.startTask({
        userId: userId
      }).$promise.then(
        function success() {
          PageAlerts.add({
            type: 'success',
            msg: 'cActivationEmailSent',
            timeout: PageAlerts.timeout.medium
          });
        },
        function error() {
          PageAlerts.add({
            type: 'danger',
            msg: 'cFailureToSendActivationEmail',
            timeout: PageAlerts.timeout.medium
          });
        });
    };

    $scope.deletePins = function () {
      var success = function () {
        $scope.accordionStatus.deletePinSessions = false;
        PageAlerts.add({
          type: 'success',
          msg: 'cDeletePinsSuccess',
          timeout: PageAlerts.timeout.medium
        });
      };
      UpdateCredentials.deletePins().$promise.then(success, function error(task) {
        if (task.httpStatus === 404) {
          return success();
        }
        PageAlerts.add({
          type: 'danger',
          msg: 'cDeletePinsError',
          timeout: PageAlerts.timeout.medium
        });
      });
    };
  });
})();
