(function () {
  'use strict';

  angular.module('hp.portal.home.accounts.accountStatus', [])

  .constant('AccountStatus', {
    CREATED: 'CREATED',
    ACTIVE: 'ACTIVE',
    SUSPENDED: 'SUSPENDED',
    CANCELLED: 'CANCELLED'
  });

})();
