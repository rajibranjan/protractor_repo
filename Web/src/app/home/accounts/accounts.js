(function () {
  'use strict';

  angular.module('hp.portal.home.accounts', [
    'hp.portal.home.webservices-v2'
  ])

  .controller('AccountsCtrl', function ($routeParams, $scope, AccountService) {
    $scope.accounts = AccountService.getPager({
      expand: 'adminContact',
      queryString: $routeParams.resellerId ? 'resellerId==' + $routeParams.resellerId : ''
    });
    $scope.accounts.loadPage();
  });
})();
