(function () {
  'use strict';

  angular.module('hp.portal.home.accountsV2', [
    'hp.portal.common.pagination.itemsPerPage',
    'hp.portal.common.pagination.sortList',
    'hp.portal.home.webservices-v2',
    'angularMoment'
  ])

  .controller('AccountsCtrl-v2', function ($routeParams, $scope, AccountService, LicenseService, LinkParser, moment) {
    $scope.accounts = AccountService.getPager({
      expand: 'adminContact',
      queryString: $routeParams.resellerId ? 'resellerId==' + $routeParams.resellerId : ''
    });
    $scope.accounts.loadPage();

    var searchFields = ['name'];
    $scope.search = function (searchString) {
      var searchExpressions = searchFields.map(function (param) {
        return param + '*="*' + searchString + '*"';
      });

      var fullSearchString = searchExpressions.join(' || ');

      // This will trigger the current page to refresh with the new search.
      $scope.accounts.searchBy(fullSearchString);
    };

    $scope.$watchCollection('accounts.list', function addAccounts(newVal) {
      if (newVal !== undefined) {
        angular.forEach(newVal, function (account) {
          LicenseService.get({
            qs: 'account.id==' + LinkParser.getSelfId(account),
            expand: 'offering',
            latest: true
          }).then(function (result) {
            setLicensesInfo(account, result);
          });

          account.address = getAccountAddress(account);
        });
      }
    });

    function setLicensesInfo(account, licenses) {

      account.licenses = licenses.data;

      var activeLicenseCount = 0;
      var trialsNearEnd = 0;
      var LicensesNearEnd = 0;

      var licenseEndsIn60Days = false;

      var currentDate = moment();

      angular.forEach(licenses.data, function (license) {
        var endDate = moment(license.endDate);
        if (license.status === 'ACTIVE') {
          activeLicenseCount++;
          if (license.isTrial === true && (endDate.diff(currentDate, 'days') <= 15)) {
            license.nearEnd = true;
            license.daysTilTermination = 15;
            trialsNearEnd++;
          }
          else if (license.isTrial === false) {
            if (endDate.diff(currentDate, 'days') <= 90) {
              license.nearEnd = true;
              LicensesNearEnd++;
              if (endDate.diff(currentDate, 'days') <= 60) {
                licenseEndsIn60Days = true;
                license.endIn60Days = licenseEndsIn60Days;
                license.daysTilTermination = 60;
              }
            }
          }
        }
      });

      account.totalLicenses = activeLicenseCount;
      account.trialsNearEnd = trialsNearEnd;
      account.licensesNearEnd = LicensesNearEnd;
      account.licensesEndsIn60Days = licenseEndsIn60Days;
    }

    function getAccountAddress(account) {
      var address = '';
      var addressLine1 = account.addressLine;
      if (addressLine1) {
        address = addressLine1 + ', ';
      }

      var addressLine2 = account.addressLine2;
      if (addressLine2) {
        address = address + addressLine2 + ', ';
      }

      var city = account.city;
      if (city) {
        address = address + city + ', ';
      }

      var state = account.state;
      if (state) {
        address = address + state + ' ';
      }

      var zipcode = account.zipcode;
      if (zipcode) {
        address = address + zipcode + ', ';
      }

      var country = account.country;
      if (country) {
        address = address + country;
      }

      if (address.substring(length - 2) === ', ') {
        address = address.substring(0, address.lastIndexof(','));
      }
      return address;
    }
  });
})();
