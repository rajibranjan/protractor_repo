(function () {
  'use strict';

  angular.module('hp.portal.home.devicesV2', [
    'hp.portal.home.webservices-v2'
  ])

  .controller('DevicesV2Ctrl', function ($scope, DeviceService, Self) {
    $scope.devices = DeviceService.getPager({
      queryString: Self.accountId ? 'accountId==' + Self.accountId : ''
    });
    $scope.devices.loadPage();
  });
})();
