(function () {
  'use strict';

  angular.module('hp.portal.home.resellers', [
    'hp.portal.common.pagination.itemsPerPage',
    'hp.portal.home.webservices-v2'
  ])

  .controller('ResellersCtrl', function ($scope, ResellerService) {
    $scope.resellers = ResellerService.getPager({
      expand: 'adminContact'
    });
    $scope.resellers.loadPage();
  });

})();
