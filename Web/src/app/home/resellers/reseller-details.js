(function () {
  'use strict';

  angular.module('hp.portal.home.resellers.resellerDetails', [
    'ngRoute',
    'hp.portal.home.navigation.routesAndUrls',
    'hp.portal.home.accounts.accountStatus',
    'hp.portal.common.forms.country',
    'hp.portal.common.webservices',
    'hp.portal.home.webservices-v2',
    'hp.portal.home.webservices.tasks',
    'hp.portal.common.forms.userForm',
    'hp.portal.common.forms.pattern'
  ])

  .controller('ResellerDetailsCtrl', function ($scope, $location, LinkParser, PageAlerts, reseller, ResendActivationEmail, AccountStatus, Routes, Countries, UserService, CountrySettings) {

    $scope.countries = Countries;
    $scope.reseller = reseller;
    $scope.hasStatusCreated = (reseller.status === AccountStatus.CREATED);

    var adminId = LinkParser.getLinkId(reseller, 'adminContact');
    UserService.getById(adminId).$promise.then(function success(admin) {
      $scope.user = admin;
    });
    var deleteReseller = function () {
      return reseller.delete().then(function success() {
        PageAlerts.add({
          path: Routes.resellers,
          type: 'success',
          msg: 'cSuccessfullyDeletedReseller',
          translateValues: {
            name: reseller.name
          },
          timeout: PageAlerts.timeout.medium
        });
        $location.url(Routes.resellers);
      }, function error() {
        PageAlerts.add({
          type: 'danger',
          msg: 'cResellerDeletionFailed',
          timeout: PageAlerts.timeout.medium
        });
      });
    };

    $scope.$watch('reseller.country', function (newValue, oldValue) {
      if (newValue !== oldValue) {
        reseller.state = null;
        reseller.zipcode = null;
      }

      $scope.countrySettings = CountrySettings.get(newValue);
    });

    $scope.deleteResellerSubmit = function () {
      $scope.$broadcast('hpDisableResellerDetailsForm', deleteReseller, false);
    };

    var updateReseller = function () {
      // This method MUST return a promise in order to work with the
      // hpDisableOn directive.
      return reseller.save().then(function () {
        PageAlerts.add({
          type: 'success',
          msg: 'cSaveSuccessful',
          timeout: PageAlerts.timeout.medium
        });
      }, function error() {
        PageAlerts.add({
          type: 'danger',
          msg: 'cSaveFailed',
          timeout: PageAlerts.timeout.medium
        });
      });
    };

    $scope.updateResellerSubmit = function () {
      $scope.$broadcast('hpDisableResellerDetailsForm', updateReseller, true);
    };

    $scope.changeCompanyContact = function (newUser) {
      $scope.user = newUser;
      LinkParser.updateLink(reseller, 'adminContact', LinkParser.getLink(newUser, 'self').href);
    };

    $scope.resendActivationEmail = function () {
      var userId = LinkParser.getLinkId(reseller, 'adminContact');
      ResendActivationEmail.startTask({
        userId: userId
      }).$promise.then(
        function success() {
          PageAlerts.add({
            type: 'success',
            msg: 'cActivationEmailSent',
            timeout: PageAlerts.timeout.medium
          });
        },
        function error() {
          PageAlerts.add({
            type: 'danger',
            msg: 'cFailureToSendActivationEmail',
            timeout: PageAlerts.timeout.medium
          });
        });
    };
  });
})();
