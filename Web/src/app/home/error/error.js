(function () {
  'use strict';

  angular.module('hp.portal.home.error', [])

  .controller('PermissionDeniedCtrl', function ($scope) {
    var pageError = {
      title: 'cPermissionDeniedTitle',
      msg: 'cPermissionDeniedMessage'
    };
    $scope.pageError = pageError;
  })

  .controller('InternalErrorCtrl', function ($scope) {
    var pageError = {
      title: 'cInternalErrorTitle',
      msg: 'cInternalErrorMessage'
    };
    $scope.pageError = pageError;
  });

})();
