(function () {
  'use strict';

  angular.module('hp.portal.login.forgot', [
    'hp.portal.login.webservices.tasks',
  ])

  .controller('ForgotCtrl', function ($scope, ForgotPassword, PageAlerts) {

    // Create a blank object to hold our form data
    $scope.formData = {};
    $scope.hasSuccess = false;

    var submitForgotPassword = function () {
      return ForgotPassword.startTask({
          email: $scope.formData.email
        })
        .$promise.then(function success() {
            $scope.hasSuccess = true;
          },
          function error() {
            PageAlerts.add({
              type: 'danger',
              msg: 'cErrorUnknown',
              timeout: 10000
            });
          });
    };

    // Submit the form data
    $scope.forgotPasswordSubmit = function () {
      $scope.$broadcast('hpDisableForgotForm', submitForgotPassword, true);
    };
  });
})();
