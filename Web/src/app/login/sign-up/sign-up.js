(function () {
  'use strict';

  angular.module('hp.portal.login.signUp', [
    'vcRecaptcha',
    'hp.portal.common.webservices.tasks',
    'hp.portal.login.signUp.signUpForm',
  ])

  .controller('SignUpCtrl', function ($scope, defaultAccount, PageAlerts, CreateAccount, vcRecaptchaService, Self, ErrorCodeMapper) {

    $scope.account = angular.copy(defaultAccount);
    $scope.adminContact = Self;
    $scope.bindings = {};

    var saveAccount = function () {
      var newAccount = {
        account: $scope.account,
        adminContact: $scope.adminContact,
        captcha: $scope.bindings.recaptcha
      };
      return new CreateAccount.startTask(newAccount).$promise.then(function success() {
        $scope.submitted = true;
      }, function error(task) {
        if (!ErrorCodeMapper.hasErrorCodes(task)) {
          PageAlerts.add({
            type: 'danger',
            msg: 'cErrorRequestAccount',
            timeout: PageAlerts.timeout.long
          });
        }

        vcRecaptchaService.reload();
      });
    };

    $scope.signUpSubmit = function () {
      //The email field for other form submissions on add User and add Accounts page has a user reenter their email
      //This is here to eliminate that requirement for this sign up page only.
      $scope.adminContact.emailVerify = $scope.adminContact.email;

      $scope.$broadcast('hpSubmitSignUpForm', saveAccount, true);
    };

  });

})();
