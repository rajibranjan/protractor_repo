(function () {
  'use strict';

  angular.module('hp.portal.login.signUp.signUpForm', [
    'vcRecaptcha',
    'hp.portal.common.forms.country'
  ])

  .directive('signUpForm', function (Self, CountrySettings, vcRecaptchaService, PortalConfigurations) {

    return {
      restrict: 'E',
      templateUrl: 'app/login/sign-up/sign-up-form.html',
      scope: {
        account: '=',
        adminContact: '=',
        recaptcha: '='
      },
      link: function (scope) {

        var widgetId;

        scope.setRecaptchaResponse = function (recaptchaValue) {
          scope.recaptcha = recaptchaValue;
        };

        function setRecaptchaLocale() {
          var iframe = angular.element('iframe');
          if (iframe.length) {
            var iframeSrc = iframe.attr('src');
            var selfLang = Self.getLang();

            // Only update the iframe src if the lang is different.
            if (!iframeSrc.match(new RegExp('hl=' + selfLang + '(&|$)', 'g'))) {
              iframeSrc = iframeSrc.replace(/hl=[^&]*(&|$)/, 'hl=' + selfLang + '&');
              iframe.attr('src', iframeSrc);
            }
          }
        }

        // Update recaptcha language when the user changes their language.
        scope.$on('changeLanguage', setRecaptchaLocale);

        scope.initRecaptcha = function (widgetIdValue) {
          widgetId = widgetIdValue;
          setRecaptchaLocale();
        };

        scope.recaptchaExpired = function () {
          vcRecaptchaService.reload(widgetId);
          scope.recaptcha = null;
        };

        PortalConfigurations.$promise.then(
          function success() {
            scope.captchaPublicKey = PortalConfigurations['com.portal.recaptchaPublicKey'];
          }
        );

        scope.unsupportedAcceptance = false;

        var countryChanged = function (newValue, oldValue) {

          // If this is hit on page load, then skip
          if (newValue === oldValue) {
            scope.countrySettings = {
              isSupported: true
            };
            return;
          }

          if (newValue !== undefined) {
            scope.countrySettings = CountrySettings.get(newValue.toLowerCase());
          }
        };
        scope.$watch('account.country', countryChanged);

      }
    };

  });

})();
