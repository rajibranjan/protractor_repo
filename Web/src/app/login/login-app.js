(function () {
  'use strict';

  // Load all constants for dependencies
  angular.module('hp.common.login.appConfig', ['init'])
    .constant('i18nPath', 'app/login/i18n/');

  angular.module('hp.common.login.loginApp', [
    'hp.common.login.appConfig', // This needs to be first for all apps
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'angulartics', 'angulartics.google.analytics',
    'ui.bootstrap',
    'hp.common.sdk',
    'hp.portal.common.init.cdn',
    'hp.portal.common.init.utils',
    'hp.portal.common.helpers',
    'hp.portal.login.signUp',
    'hp.portal.login.signUpWithProduct',
    'hp.portal.login.termsOfUse',
    'hp.portal.login.signIn',
    'hp.portal.login.activate',
    'hp.portal.login.forgot',
    'hp.portal.login.reset',
    'hp.portal.login.unsubscribe',
    'iOxpwSdkApp',
  ])

  .run(function (Globals, cdnURL, serverURL, PortalConfigurations) {
    Globals.cdnURL = cdnURL;
    Globals.serverURL = serverURL;
    Globals.portalConfigurations = PortalConfigurations;
  })


  .config(function ($analyticsProvider, $routeProvider, $animateProvider) {
    // Setting this classNameFilter makes it so that animation will only run
    // when we explicitely specify this class on an element. This prevents animation
    // from running by default in places such as ng-views, ng-repeats, ng-hide/show, etc.
    $animateProvider.classNameFilter(/hp-animate/);

    $analyticsProvider.withAutoBase(true); // Add the app-name to analytics pageviews

    var getPasswordPolicy = function (UserRole, route, PasswordPolicy, PasswordPolicies) {
      return UserRole.startTask({
        verificationHash: route.current.params.id
      }).$promise.then(PasswordPolicy.getForUser, function error() {
        return PasswordPolicies.DEFAULT;
      });
    };

    $routeProvider
      .when('/', {
        templateUrl: 'app/login/sign-in/sign-in.html',
        controller: 'SignInCtrl'
      })
      .when('/activate/:id', {
        templateUrl: 'app/login/activate/activate.html',
        controller: 'ActivateCtrl',
        resolve: {
          passwordPolicy: ['UserRole', '$route', 'PasswordPolicy', 'PasswordPolicies', getPasswordPolicy]
        }
      })
      .when('/forgot', {
        templateUrl: 'app/login/forgot/forgot.html',
        controller: 'ForgotCtrl'
      })
      .when('/reset/:id', {
        templateUrl: 'app/login/reset/reset.html',
        controller: 'ResetCtrl',
        resolve: {
          passwordPolicy: ['UserRole', '$route', 'PasswordPolicy', 'PasswordPolicies', getPasswordPolicy]
        }
      })
      .when('/sign-up', {
        templateUrl: 'app/login/sign-up/sign-up.html',
        controller: 'SignUpCtrl',
        resolve: {
          defaultAccount: function () {
            return {
              reseller: ''
            };
          }
        }
      })
      .when('/sign-up-with-product', {
        templateUrl: 'app/login/sign-up/sign-up-with-product.html',
        controller: 'SignUpWithProductCtrl',
        resolve: {
          defaultAccount: function () {
            return {
              reseller: ''
            };
          }
        }
      })
      .when('/sign-up/:referenceId', {
        templateUrl: 'app/login/sign-up/sign-up.html',
        controller: 'SignUpCtrl',
        resolve: {
          defaultAccount: ['$route', function ($route) {
            return {
              links: [{
                rel: 'reseller',
                href: $route.current.params.referenceId
              }]
            };
          }]
        }
      })
      .when('/terms-of-use', {
        templateUrl: 'app/login/terms-of-use/terms-of-use.html',
        controller: 'TermsOfUseCtrl'
      })
      .when('/unsubscribe/:id', {
        templateUrl: 'app/login/unsubscribe/unsubscribe.html',
        controller: 'UnsubscribeCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })

  .controller('LoginCtrl', function ($scope, pageLoadingMonitor, httpErrorLogger, Self, SdkConfig, cdnURL) {
    httpErrorLogger.redirectOnUnauthenticated = false;
    pageLoadingMonitor.start('login');
    $scope.self = Self;

    SdkConfig.loadFullSelf = false;

    var locale = {};
    locale.cc = Self.getLocale().split('-')[1].toLowerCase();
    locale.ll = Self.getLang();
    locale.mod = 'var';
    window.locale = locale;

    var euckScriptLocation = cdnURL + 'lib/deploy/privacy/hp-privacy.js';
    var euckFormScriptLocation = cdnURL + 'lib/deploy/privacy/hp-privacy-form.js';

    function euckLoadScripts() {
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = euckScriptLocation;
      document.getElementsByTagName('head')[0].appendChild(script);

      var formScript = document.createElement('script');
      formScript.type = 'text/javascript';
      formScript.src = euckFormScriptLocation;
      document.getElementsByTagName('head')[0].appendChild(formScript);
    }

    euckLoadScripts();

    $scope.loadAnalytics = false;

    document.addEventListener('HP.PrivacyBanner.Loaded', function () {
      $scope.isEUCK = true;
      // CSS injection on external banner code
      angular.element('#cookie_privacy_popup').css('position', 'relative');
      angular.element('#cookie_privacy_holder').css('padding-top', '0').css('margin-top', '-10px');

      // If cookie pref not set or set to true, track
      var cookieTrackingPref = window.hpeuck_prefs; // jshint ignore:line
      if (typeof cookieTrackingPref !== 'undefined' && cookieTrackingPref.substr(0, 1) === '1') {
        $scope.loadAnalytics = true;
      }
    });

    // Banner not loaded so load GA
    document.addEventListener('HP.PrivacyBanner.NotLoaded', function () {
      $scope.loadAnalytics = true;
    });
  })

  /**
   * On login page load, check for the server-side bootstrapped variables redirectUrl and licenseExpiresIn.
   * If they are present, then we need to pop up the LicenseCountdownModal and then redirect.
   */
  .run(function (LinkParser, Self, EntitlementService, LicenseCountdownModal, redirectUrl, licenseExpiresIn, queryParams) {
    if (!redirectUrl || !licenseExpiresIn) {
      return; // Nothin to do.
    }

    var redirectFqName = queryParams().redirect;
    if (!redirectFqName) {
      return;
    }

    Self.$promise.then(function () {
      EntitlementService.getByUser(LinkParser.getSelfId(Self), redirectFqName, 'offering,product,license').then(function (response) {
        var entitlement = response.data && response.data.length ? response.data[0] : undefined;
        if (!entitlement) {
          return;
        }

        var modalData = {
          redirectUrl: redirectUrl,
          offering: entitlement.offering,
          daysTillExpiration: licenseExpiresIn,
          backdrop: 'static',
          entitlement: entitlement,
        };
        LicenseCountdownModal.open(modalData);
      });

    });
  });

})();
