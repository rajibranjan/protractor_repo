(function () {
  'use strict';

  angular.module('hp.portal.login.activate', [
    'angular-locker',
    'hp.portal.login.webservices.tasks'
  ])

  .controller('ActivateCtrl', function ($scope, $routeParams, ActivateUser, PageAlerts, changeLanguage, Self, SdkConfig, CountrySettings, locker, ErrorCodeMapper, passwordPolicy) {
    var countryCode = $routeParams.cc;

    CountrySettings.get(countryCode).$promise.then(function success(response) {
      $scope.isEmailOptInDisplayed = response.isEmailOptInDisplayed;
      if (response.isEmailOptInDefault) {
        $scope.formData.optedIntoEmail = true;
      }
      $scope.isEmailTrackingDisplayed = response.isEmailTrackingDisplayed;
      if (response.isEmailTrackingDefault) {
        $scope.formData.optedIntoEmailTracking = true;
      }
    });

    $scope.passwordPolicy = passwordPolicy;

    $scope.formData = {
      verificationHash: $routeParams.id,
      agreeToPrivacy: true
    };

    var routeLocale = $routeParams.Locale;
    if (routeLocale) {
      Self.locale = routeLocale;
    }
    else if (!Self.locale && locker.has(SdkConfig.titanSDKLocale)) {
      Self.locale = locker.get(SdkConfig.titanSDKLocale);
    }

    if (Self.locale) {
      changeLanguage(Self.locale);
      locker.put(SdkConfig.titanSDKLocale, Self.locale);
    }

    var submitFn = function () {
      return ActivateUser.startTask($scope.formData)
        .$promise.then(function success() {
          $scope.hasSuccess = true;
        }, function error(task) {
          if (!ErrorCodeMapper.hasErrorCodes(task)) {
            PageAlerts.add({
              type: 'danger',
              msg: 'cErrorActivateAccount'
            });
          }
        });
    };

    $scope.submit = function () {
      $scope.$broadcast('hpDisableActivateForm', submitFn, true);
    };

    SdkConfig.fallbackToBrowserLanguage = false;
  });

})();
