(function () {
  'use strict';

  angular.module('hp.portal.login.termsOfUse', [])

  .controller('TermsOfUseCtrl', function ($scope, $window) {
    $scope.print = function () {
      $window.print();
    };
  });

})();
