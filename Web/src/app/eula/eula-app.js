(function () {
  'use strict';

  // Load all constants for dependencies
  angular.module('hp.portal.eula.appConfig', ['init'])
    .constant('i18nPath', 'app/eula/i18n/');

  angular.module('hp.portal.eula.eulaApp', [
    'hp.portal.eula.appConfig', // This needs to be first for all apps
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'angulartics', 'angulartics.google.analytics',
    'ui.bootstrap',
    'hp.common.sdk',
    'hp.portal.common.init.cdn',
    'hp.portal.common.webservices',
    'hp.portal.common.helpers',
    'hp.portal.eula.content',
    'iOxpwSdkApp'
  ])

  .run(function ($route, Globals, cdnURL, serverURL) {
    Globals.cdnURL = cdnURL;
    Globals.serverURL = serverURL;

    angular.forEach($route.routes, function (route) {
      if (route.resolve === undefined) {
        route.resolve = {};
      }
      route.resolve.getSelf = ['Self', function (Self) {
        return Self.$promise;
      }];
    });
  })

  .config(function ($analyticsProvider, $routeProvider, $animateProvider, serverURL) {
    // Setting this classNameFilter makes it so that animation will only run
    // when we explicitely specify this class on an element. This prevents animation
    // from running by default in places such as ng-views, ng-repeats, ng-hide/show, etc.
    $animateProvider.classNameFilter(/hp-animate/);

    $analyticsProvider.withAutoBase(true); // Add the app-name to analytics pageviews

    $routeProvider
      .when('/:app', {
        templateUrl: 'app/eula/content/content.html',
        controller: 'ContentCtrl',
        resolve: {
          offering: ['$route', 'OfferingService', function ($route, OfferingService) {
            return OfferingService.getByFqName($route.current.params.app);
          }]
        }
      })
      .otherwise({
        redirectTo: serverURL
      });
  })

  .controller('EulaCtrl', function ($scope, pageLoadingMonitor) {

    pageLoadingMonitor.start('eula');
  });

})();
