(function () {
  'use strict';

  angular.module('hp.portal.eula.content', [
    'hp.portal.common.webservices.entitlements'
  ])

  .controller('ContentCtrl', function ($scope, $routeParams, $window, $q, LinkParser, Self, PortalConfigurations, offering, EntitlementService, PageAlerts, i18n, serverURL) {

    var _entitlements = [];
    $scope.checkboxResults = {
      termsAccepted: false,
      privacyAccepted: false
    };

    $scope.offering = offering;

    $scope.eulaDescriptionTranslateValues = {
      offeringTitle: '{{' + offering.title + '}}'
    };
    $scope.localeStrings = i18n.getBySource(offering.fqName);
    $scope.localeStrings.$promise.then(function success(localeStrings) {
      $scope.eulaDescriptionTranslateValues.offeringTitle = localeStrings[offering.title];

      if (!localeStrings[offering.termsOfUse]) {
        $scope.checkboxResults.termsAccepted = true;
      }
      if (!localeStrings[offering.privacyPolicy]) {
        $scope.checkboxResults.privacyAccepted = true;
      }
    });

    $scope.printTextArea = function (idValue) {
      var printContents = document.getElementById(idValue).value;
      var newWin = window.open('', '_blank');
      newWin.document.open();
      newWin.document.write('<html><head></head><body>' + printContents + '</body></html>');
      newWin.document.close();
      newWin.focus();
      newWin.print();
      newWin.close();
    };

    function redirectToOffering() {
      return PortalConfigurations.$promise.then(function () {
        $window.location.href = PortalConfigurations['com.portal.identityRedirectUrl'] + '?redirect=' + offering.fqName;
      });
    }

    //Get the user that's creating this fuss
    $scope.self = Self;
    Self.$promise.then(function success(user) {
      //FIXME - what if there is more than one set of paginated entitlements?
      //Get the entitlements that are associated with this user and offering
      EntitlementService.getByUserAndOffering(LinkParser.getSelfId(user), $routeParams.app).then(function success(entitlements) {
        _entitlements = entitlements.data;

        if (!_entitlements || !_entitlements.length) {
          return $q.reject();
        }

        var eulaAlreadyAccepted = _entitlements.every(function (entitlement) {
          return entitlement.termsAccepted && entitlement.privacyAccepted;
        });
        if (eulaAlreadyAccepted) {
          redirectToOffering();
        }
      }).catch(function rejected() {
        // If there are no entitlements, redirect to the root server URL.
        $window.location.href = serverURL;
      });
    });

    var submitFn = function () {
      angular.forEach(_entitlements, function (entitlement) {
        entitlement.termsAccepted = $scope.checkboxResults.termsAccepted;
        entitlement.privacyAccepted = $scope.checkboxResults.privacyAccepted;
      });

      return EntitlementService.saveAll(_entitlements).then(function success() {
        redirectToOffering();
      }, function error() {
        PageAlerts.add({
          type: 'danger',
          msg: 'cErrorSubmittingEula',
          timeout: PageAlerts.timeout.long
        });
      });
    };

    //method for submitting the form data
    $scope.submit = function () {
      $scope.$broadcast('hpDisableEulaContentForm', submitFn, true);
    };
  });

})();
