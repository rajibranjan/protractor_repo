(function () {
  'use strict';

  if (!Array.prototype.find) {
    Array.prototype.find = function (predicate) {
      if (this === null) {
        throw new TypeError('Array.prototype.find called on null or undefined');
      }
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }
      var list = Object(this);
      /*jslint bitwise: true */
      var length = list.length >>> 0;
      /*jslint bitwise: false */
      var thisArg = arguments[1];
      var value;

      for (var i = 0; i < length; i++) {
        value = list[i];
        if (predicate.call(thisArg, value, i, list)) {
          return value;
        }
      }
      return undefined;
    };
  }

  angular.module('hp.portal.common.init.utils', [])

  .factory('queryParams', function ($window, $location) {

    return function () {
      // Get all the query params from before the hash/anchor.
      var queryParams = $window.location.search
        .split(/[?&]/) // Get all key=value pairs
        .filter(function (x) {
          // Filter out any empty results
          return x.indexOf('=') > -1;
        })
        .map(function (x) {
          // Split on the = sign
          return x.split(/=/);
        })
        .map(function (x) {
          // Convert plus to space
          x[1] = x[1].replace(/\+/g, ' ');
          return x;
        })
        .reduce(function (all, current) {
          // Reduce it all to one object
          all[current[0]] = current[1];
          return all;
        }, {});

      // Include the search params after the hash/anchor.
      return angular.extend(queryParams, $location.search());
    };
  });

})();
