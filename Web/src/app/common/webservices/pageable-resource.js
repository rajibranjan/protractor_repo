(function () {
  'use strict';

  angular.module('hp.portal.common.webservices.pageable-resource', [
    'hp.portal.common.webservices.resource-service'
  ])

  .factory('Cache', function () {
    var serializeKey = JSON.stringify;

    function Cache() {
      this._store = {};
    }

    Cache.prototype.add = function (key, value) {
      this._store[serializeKey(key)] = value;
    };

    Cache.prototype.remove = function (key) {
      delete this._store[serializeKey(key)];
    };

    Cache.prototype.get = function (key) {
      return this._store[serializeKey(key)];
    };

    Cache.prototype.exists = function (key) {
      return this.get(key) !== undefined;
    };

    return Cache;
  })

  /**
   * The Pageable Resource Service is a subclass of the Resource Service.
   * It adds additional paging functionality to a Resource Service.
   * The Pageable Resource Service adds the following method(s) to the Resource Service:
   *   - getPager(opts) : Returns a Pager instance.
   *       Available options:
   *         - itemsPerPage: Size of each page
   *         - expand: Requested related object to pass through
   *         - queryString: Supplemental qs to pass through
   *
   * Pager is a private class that exposes methods to page through a resource.
   *
   * The Pager maintains a cache of items as it fetches the pages, so that each page
   * can be pulled from the cache if it had already been loaded.
   *
   * All operations that trigger a resource call will wait on a "lock", which is a chain of any outstanding
   * promises for prior calls.  This way you could queue up any amount of loadPage calls, and they will
   * all execute serially, in the order they were triggered. If any promise in the chain is rejected,
   * (i.e. a page-out-of-bounds error), all queued promises will be rejected. This protects against the pager
   * getting into a bad state and additional operations being called on it.
   *
   * The Pager exposes the following methods/properties:
   *   - list: A readonly getter property to access the list of the current page, which is an array.
   *   - getPageNumber(): Gets the current page number.
   *   - getTotal(): Gets the total number of items across all pages.
   *   - getTotalPages(): Gets the total number of pages.
   *   - getItemsPerPage(): Gets the number of items per page.
   *   - setItemsPerPage(itemsPerPage): Sets the number of items per page. This will trigger a call to loadPage.
   *   - searchBy(searchString): Sets the search string and then reloads the page.
   *   - setSearchString(searchString): Sets the search string which will be appended to any queryString.
   *   - orderBy(sortBy, reverse): Changes the backend sorting. `sortBy` is the "sortable" field. Set `reverse` to `false` for ascending, and `true` for descending. Set to ascending by default.
   *   - refresh(pageNumber): Clears the cache and reloads the passed in page number, or the current page if none is specified.
   *   - loadPage(pageNumber): Loads a page by number. If no pageNumber is specified, this triggers a load of the current page.
   *   - loadNextPage(): Loads the next page.
   *   - loadPreviousPage(): Loads the previous page.
   *   - isLoading(): If the current page of data is still loading
   */
  .factory('PageableResourceService', function ($q, ResourceService, Cache, testHooks) {

    function Pager(resourceService, options) {
      var _resourceService = resourceService;
      var _options = angular.copy(options) || {};

      var _cache = new Cache(); // The cache for all pages.
      var _list = []; // The current page list.
      var _currentPageNumber = 1; // Default starting page.
      var _searchString;
      var _lock = $q.when(_list);
      var _isLoading = false;

      function getPageNumber() {
        return _currentPageNumber;
      }

      function getTotal() {
        return getCurrentCache().length;
      }

      function getTotalPages() {
        return Math.ceil(getTotal() / getItemsPerPage());
      }

      function getItemsPerPage() {
        return _options.itemsPerPage;
      }

      function setItemsPerPage(itemsPerPage) {
        return takeLockAndInvoke(function () {
          _options.itemsPerPage = itemsPerPage;
          return internalLoadPage();
        });
      }

      function searchBy(searchString) {
        return takeLockAndInvoke(function () {
          _searchString = searchString;
          return internalLoadPage();
        });
      }

      function setSearchString(searchString) {
        _searchString = searchString;
      }

      function calculateOffset(pageNumber) {
        return (pageNumber - 1) * _options.itemsPerPage;
      }

      function populateCurrentPageFromCache() {
        // Update the list for the current page.
        var list = getCurrentPageFromCache();
        _list.length = 0;
        _list.push.apply(_list, list);
        return _list;
      }

      function getCurrentPageFromCache() {
        var cache = getCurrentCache();
        var offset = calculateOffset(getPageNumber());
        return cache.slice(offset, offset + getItemsPerPage());
      }

      function orderBy(sortBy, reverse) {
        return takeLockAndInvoke(function () {
          var rev = reverse ? 'DESC' : 'ASC';
          _options.orderBy = sortBy + ' ' + rev;
          return internalLoadPage();
        });
      }

      function refresh(pageNumber) {
        _cache.remove(getCurrentParams());
        return loadPage(pageNumber);
      }

      function currentPageIsCached() {
        var page = getCurrentPageFromCache();
        if (getTotal() === 0) {
          return false;
        }

        // We have to count up the actual elements in the page, because page.length is not accurate.
        var count = 0;
        page.forEach(function () {
          count++;
        });

        if (count === getItemsPerPage()) {
          return true;
        }
        else {
          var itemsOnPreviousPages = (getPageNumber() - 1) * getItemsPerPage();
          var expectedCount = getTotal() - itemsOnPreviousPages;
          return count === expectedCount;
        }
      }

      function loadPage(pageNumber) {
        return takeLockAndInvoke(function () {
          return internalLoadPage(pageNumber);
        });
      }

      function loadNextPage() {
        return loadPage(getPageNumber() + 1);
      }

      function loadPreviousPage() {
        return loadPage(getPageNumber() - 1);
      }

      function takeLockAndInvoke(handler) {
        // The _lock enforces that only one operation can be outstanding at a time.
        // Any additional operations will be queued up waiting on the prior promise to resolve.
        _lock = _lock.finally(handler);
        return _lock;
      }

      function getCurrentParams() {
        // Each param will still be left out of the URL query string if they are undefined
        var config = {
          orderBy: _options.orderBy,
          expand: _options.expand,
          qs: _options.queryString
        };

        if (_searchString) {

          if (config.qs) {
            config.qs = '(' + config.qs + ') && (' + _searchString + ')';
          }
          else {
            // It might be undefined
            config.qs = _searchString;
          }
        }

        if (!config.qs) {
          delete config.qs;
        }
        if (!config.orderBy) {
          delete config.orderBy;
        }
        if (!config.expand) {
          delete config.expand;
        }

        return config;
      }

      function getCurrentCache() {
        var key = getCurrentParams();
        if (!_cache.exists(key)) {
          _cache.add(key, []);
        }
        return _cache.get(key);
      }

      function internalLoadPage(pageNumber) {
        // Floor the page number to something "reasonable"
        var lastPage = getTotalPages();
        pageNumber = pageNumber || getPageNumber(); // Use the current page number if none is specified
        pageNumber = Math.min(lastPage, pageNumber) || pageNumber; // Page cannot be after the last page
        pageNumber = Math.max(pageNumber, 1); // Page cannot be less than 1
        _currentPageNumber = pageNumber;

        // See if the data is already loaded
        if (currentPageIsCached()) {
          return populateCurrentPageFromCache();
        }

        var config = getCurrentParams();
        if (getItemsPerPage() !== undefined) {
          config.limit = getItemsPerPage();
          config.offset = calculateOffset(getPageNumber());
        }

        var cache = getCurrentCache();
        _isLoading = testHooks.isPaginatedDataLoading = true;
        _list.length = 0;

        return _resourceService.get(config)
          .then(function success(response) {
            _options.itemsPerPage = response.limit;
            cache.length = response.total;

            // Populate the specific indexes of the cache for the page we just loaded.
            angular.forEach(response.data, function (item, index) {
              cache[index + response.offset] = item;
            });

            return populateCurrentPageFromCache();
          }, function error() {}).finally(function () {
            _isLoading = testHooks.isPaginatedDataLoading = false;
          });
      }

      function isLoading() {
        return _isLoading;
      }

      Object.defineReadonlyGetter(this, 'list', function () {
        return _list;
      });
      this.getPageNumber = getPageNumber;
      this.getTotal = getTotal;
      this.getTotalPages = getTotalPages;
      this.getItemsPerPage = getItemsPerPage;
      this.setItemsPerPage = setItemsPerPage;
      this.searchBy = searchBy;
      this.setSearchString = setSearchString;
      this.orderBy = orderBy;
      this.refresh = refresh;
      this.loadPage = loadPage;
      this.loadNextPage = loadNextPage;
      this.loadPreviousPage = loadPreviousPage;
      this.isLoading = isLoading;
    }

    var PageableResourceService = function (apiUrl, options) {
      ResourceService.call(this, apiUrl, options);
      this.getPager = function (opts) {
        return new Pager(this, opts);
      };
    };
    PageableResourceService.prototype = Object.create(ResourceService.prototype);
    PageableResourceService.prototype.constructor = PageableResourceService;
    return PageableResourceService;
  });

})();
