(function () {
  'use strict';

  angular.module('hp.portal.common.webservices.tasks', [
    'hp.portal.common.webservices.task-service',
    'hp.portal.common.webservices.endpoints',
  ])

  .factory('CreateAccount', function (TaskService, Endpoints) {
    return new TaskService(Endpoints.createAccount);
  })

  .factory('CreateAccountAndCheckout', function (TaskService, Endpoints) {
    return new TaskService(Endpoints.createAccountAndCheckout);
  })

  .factory('ShoppingCartCheckout', function (TaskService, Endpoints) {
    return new TaskService(Endpoints.shoppingCartCheckout);
  });

})();
