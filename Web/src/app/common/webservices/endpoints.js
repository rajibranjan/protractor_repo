(function () {
  'use strict';

  angular.module('hp.portal.common.webservices.endpoints', [])

  .factory('Endpoints', function (LinkParser, Self) {
    var endpoints = {
      signIn: 'j_spring_security_check',
      deleteAccount: '/api/v2/tasks/deleteAccount',
      forgotPassword: '/api/v2/tasks/forgotPassword',
      resetPassword: '/api/v2/tasks/resetPassword',
      unsubscribe: '/api/v2/tasks/unsubscribe',
      unsubscribeValidate: '/api/v2/tasks/unsubscribe/validate',
      updateCredentials: '/api/v2/tasks/updateCredentials',
      activateUser: '/api/v2/tasks/activateUser',
      userRole: '/api/v2/tasks/userRole',
      userCSVUpload: 'api/v2/tasks/userCSVUpload',
      shoppingCartCheckout: '/api/v2/tasks/shoppingCartCheckout',
      createAccount: 'api/v2/tasks/createAccount',
      createAccountAndCheckout: 'api/v2/tasks/createAccountAndCheckout',
      createReseller: 'api/v2/tasks/createReseller',
      resendCreatedEmail: '/api/v2/tasks/resendCreatedEmail',
      entityRepair: '/api/v2/tasks/repair',
      validateLicenseKey: '/api/v2/tasks/validateLicenseKey',
    };
    var usersCountBase = '/api/v2/users?limit=1&qs=' + encodeURIComponent('accountId==' + LinkParser.getLinkId(Self, 'account') + ' && status==');
    endpoints.accountUsers = {
      Active: usersCountBase + 'ACTIVE',
      Suspended: usersCountBase + 'SUSPENDED',
      Created: usersCountBase + 'CREATED'
    };
    return endpoints;
  });
})();
