(function () {
  'use strict';

  angular.module('hp.portal.common.webservices', [
    'hp.portal.common.webservices.resource-service',
    'hp.portal.common.webservices.pageable-resource',
  ])

  .factory('ProductService', function (ResourceService, LinkParser) {
    var productService = new ResourceService('/api/v2/products');

    /**
     * Add a helper method to the service to generate an array of LicenseAdditionalFields
     * based on the passed in product and additionalFieldValues.
     */
    productService.generateLicenseAdditionalFields = function (product, additionalFieldValues) {
      if (product && product.additionalFields && additionalFieldValues) {
        return product.additionalFields.map(function (field) {
          return {
            value: additionalFieldValues[field.name],
            links: [{
              rel: 'productAdditionalField',
              href: LinkParser.getSelf(field).href
            }]
          };
        });
      }
    };

    return productService;
  })

  .factory('ShoppingCartService', function (ResourceService) {
    return new ResourceService('/api/v2/shoppingCarts');
  })

  .factory('OfferingService', function ($q, PageableResourceService) {
    var OfferingService = new PageableResourceService('/api/v2/offerings');
    var offerings = {};
    var offeringsFetched = false;

    OfferingService.getByFqName = function (fqName) {
      if (offerings[fqName]) {
        return $q.when(offerings[fqName]);
      }
      else {
        return OfferingService.getOne({
          qs: 'fqName==' + fqName
        }).$promise.then(function (offering) {
          offerings[offering.fqName] = offering;
          return offering;
        });
      }
    };

    OfferingService.getAllOfferings = function () {
      if (!offeringsFetched) {
        offeringsFetched = true;
        var promise = OfferingService.get({
          limit: 10000,
        }).then(function (response) {
          response.data.forEach(function (offering) {
            offerings[offering.fqName] = offering;
          });
          return offerings;
        });

        Object.defineNonEnumerableProperty(offerings, '$promise', promise);
      }

      return offerings;
    };

    return OfferingService;
  });

})();
