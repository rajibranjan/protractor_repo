(function () {
  'use strict';

  angular.module('hp.portal.common.webservices.resource-service', [
    'ngResource',
  ])

  /**
   * The ResourceService is a base class that can be used to do common operations on a resource.
   * The individual objects returned from Resource Service methods are called Entities.
   * For example, ResourceService.get() will return a list of Entities, each of which have
   * useful operations to update, delete, reload, etc.
   *
   * A Resource Service has the following constructor signature:
   *  - ResourceService(apiUrl, [options])
   * The apiUrl is the URL to the base resource endpoint, without including the trailing ":id".
   * The options object can contain any of the following properties:
   *  - extraActions [optional]: an object containing actions to be added to the Resource object.
   *      - The format of this object should match angular's $resource actions format.
   *  - entityPrototype [optional]: a function that will be injected into the Resource Entity's prototype chain.
   *      - This can be used to add specialized functionality to the Entity object for this Resource Service.
   *      - The entityPrototype function constructor will be invoked only once as the resource service is
   *          being instantiated. It will NOT be invoked every time an Entity is constructed.
   *      - The constructor will be passed the $resource instance.
   *
   * A Resource Service exposes the following methods:
   *  - get([parameters]): Gets a list of Entities from the resource.
   *      - A promise is returned which will later be resolved with the resource response.
   *      - The response.data array is replace with an array of Entity objects wrapping each individual object in the array.
   *      - The optional 'parameters' will be passed to the $resource.get call. See angular documentation.
   *  - getById(id): Alias for getOne({id: id})
   *  - getOne(params): Gets an Entity from the resource by the given parameters.
   *      - The Entity object is immediately returned and the data will later be extended onto that object.
   *  - fromExisting(obj): Creates an Entity wrapping the data in the passed in 'obj'.
   *      - This Entity object will have a $promise that is pre-resolved to itself, and $resolved = true.
   *  - create([obj]): Returns a new Entity object for this resource type.
   *      - This Entity object will NOT have a $promise until it is saved. $resolved is false until the object is successfully saved.
   *      - The optional [obj] parameter can be an object who's properties will be used to initialize the object.
   *
   *
   * An Entity object represents a single entity in a resource with a specific id.
   * An Entity is always tied to a specific Resource Service, and can therefore only be created or fetched using a Resource Service.
   * An Entity has the following methods/properties:
   *  - save(): Saves the data on the Entity to the Resource.
   *  - reload(): Reloads/refreshes the Entity data from the Resource.
   *  - delete(): Sends a delete request to the Resource for this specific entity ID.
   *  - clone(): Creates a clone of this Entity, with all of the functionaly and data as the original.
   *  - $resolved: Boolean indicating whether this Entity has been resolved with server-side data.
   *  - $promise: This promise is only applicable to the initial load of the entity, i.e. when $resolved === false.
   *      - Any subsequent operations, such as save, reload, delete, do NOT update this $promise, and instead
   *        each of those methods return the promise for that specific operation.
   */
  .factory('ResourceService', function ($resource, $q, LinkParser) {

    function EntityBase(resource) {
      var handleResponse = function (entity) {
        return function (response) {
          delete response.$promise;
          delete response.$resolved;
          angular.extend(entity, response);
          return entity;
        };
      };

      function reload() {
        var id = LinkParser.getSelfId(this);
        return resource.get({
          id: id
        }).$promise.then(handleResponse(this));
      }

      function clone() {
        var thisPrototype = Object.getPrototypeOf(this);
        var theClone = Object.create(thisPrototype);
        thisPrototype.constructor.call(theClone, {
          existing: this
        });
        return theClone;
      }

      function deleteAction() {
        var that = this;
        return resource['delete'](this).$promise.then(function () {
          return that;
        });
      }

      function save() {
        if (this.$resolved) {
          return resource.update(this).$promise.then(handleResponse(this));
        }
        else {
          var promise = resource.save(this).$promise.then(handleResponse(this));
          return this._setPromise(promise);
        }
      }

      Object.defineNonEnumerableProperty(this, 'save', save);
      Object.defineNonEnumerableProperty(this, 'reload', reload);
      Object.defineNonEnumerableProperty(this, 'clone', clone);
      Object.defineNonEnumerableProperty(this, 'delete', deleteAction);
    }


    function ResourceService(apiUrl, options) {
      options = options || {};
      options.extraActions = angular.extend({
        update: { // Add the update (PUT) action to every resource.
          method: 'PUT'
        }
      }, options.extraActions);


      /**********************************************************************
        Begin semi-hacky workaround:
          Angular documentation for $resource params states:
            params – {Object=} – Optional set of pre-bound parameters for this action.
            If any of the parameter value is a function, it will be executed every time when a
            param value needs to be obtained for a request (unless the param was overridden).
            (Taken from: https://code.angularjs.org/1.4.7/docs/api/ngResource/service/$resource)
          We are using this functionality to get the ':id' param from the self link.
          However, $resource does NOT pass the data object to this function when invoking
          it, so it is makes it impossible to get the self link for the object being used.
          To work around that, I'm wrapping the save, update, and delete methods to capture
          the data object before invoking the method. Then when the 'getSelfId' function
          gets called, we parse the self link from the captured object.
       **********************************************************************/
      var currentEntity;

      function getSelfId() {
        return LinkParser.getSelfId(currentEntity);
      }
      var resource = $resource(apiUrl + '/:id', {
        id: getSelfId
      }, options.extraActions);
      var actions = ['save', 'update', 'delete'];
      angular.forEach(actions, function (action) {
        var resourceAction = resource[action];
        resource[action] = function (data) {
          currentEntity = data;
          var response = resourceAction(action === 'delete' ? undefined : data);
          currentEntity = undefined;
          return response;
        };
      });
      /**********************************************************************
       *  End semi-hacky workaround
       **********************************************************************/


      function Entity(config) {
        EntityBase.call(this, resource);
        var _promise, _resolved;
        Object.defineReadonlyGetter(this, '$promise', function () {
          return _promise;
        });
        Object.defineReadonlyGetter(this, '$resolved', function () {
          return _resolved;
        });

        var that = this;
        if (config.existing) {
          var data = config.existing;
          // Delete existing promise/resolution state
          if (!(data instanceof Entity)) { // Can't delete our own readonly properties if the existing object is not an Entity and has these properties already.
            data = angular.copy(data);
            if (data.$promise) {
              delete data.$promise;
            }
            if (data.$resolved) {
              delete data.$resolved;
            }
          }
          angular.merge(this, data); // angular.merge is a deep copy, whereas angular.extend is a shallow copy.
          _promise = $q.when(this);
          _resolved = true;
        }
        else if (config.getById) {
          _resolved = false;
          _promise = resource.get(config.getById).$promise.then(function (response) {
            delete response.$promise;
            delete response.$resolved;
            angular.extend(that, response);
            _resolved = true;
            return that;
          });
        }
        else if (config.getOne) {
          _resolved = false;
          _promise = resource.get(config.getOne).$promise.then(function (response) {
            delete response.$promise;
            delete response.$resolved;
            if (response.data) {
              if (response.data.length > 0) {
                angular.extend(that, response.data[0]);
                _resolved = true;
              }
              else {
                return $q.reject();
              }
            }
            else {
              angular.extend(that, response);
              _resolved = true;
            }
            return that;
          });
        }
        else if (config.createNew) {
          _resolved = false;
          Object.defineProperty(this, '_setPromise', {
            value: function (promise) {
              _promise = promise;
              return promise.then(function (obj) {
                _resolved = true;
                delete that._setPromise;
                return obj;
              });
            },
            configurable: true
          });
          if (typeof config.createNew === 'object') {
            angular.merge(that, config.createNew); // angular.merge is a deep copy, whereas angular.extend is a shallow copy.
          }
        }
        else {
          throw new Error('An Entity must be constructed with either config.getOne, config.getById, config.existing, or config.createNew.');
        }
      }

      // Set Entity's prototype chain.
      var entityBaseProto = new EntityBase(resource);
      if (options.entityPrototype) {
        // Inject the passed in entityPrototype into the prototype chain.
        var InjectedPrototype = options.entityPrototype;
        InjectedPrototype.prototype = entityBaseProto;
        entityBaseProto = new InjectedPrototype(resource);
      }
      Entity.prototype = entityBaseProto;
      Object.defineNonEnumerableProperty(Entity.prototype, 'constructor', Entity);


      function getById(id) {
        if (!id) {
          throw new Error('Cannot load resource because "id" was missing or invalid: ' + id);
        }

        var params = {
          id: id
        };

        return new Entity({
          getById: params
        });
      }

      function getOne(params) {
        return new Entity({
          getOne: params
        });
      }

      function fromExisting(obj) {
        return new Entity({
          existing: obj
        });
      }

      function get(parameters) {
        function handleResponse(response) {
          var responseData = response.data;
          response.data = [];
          response = angular.extend({}, response); // Get rid of the inherited $resource prototype.
          angular.forEach(responseData, function (entity) {
            response.data.push(fromExisting(entity));
          });
          return response;
        }
        return resource.get(parameters).$promise.then(handleResponse);
      }

      function create(data) {
        return new Entity({
          createNew: data || true
        });
      }

      this.get = get;
      this.getById = getById;
      this.getOne = getOne;
      this.fromExisting = fromExisting;
      this.create = create;
    }

    return ResourceService;
  });

})();
