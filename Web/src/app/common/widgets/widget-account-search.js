(function () {
  'use strict';

  angular.module('hp.portal.common.widgets.widgetAccountSearch', [
      'hp.portal.home.webservices-v2'
    ])
    .controller('AccountSearchWidgetCtrl', function ($scope, $location, UserService, RolesHelper, Roles) {
      $scope.$watch('search.email', function () {
        $scope.accountSearchForm.$submitted = false;
      });
      $scope.searchAccounts = function (email) {
        UserService.get({
          qs: 'email=="' + email + '"'
        }).then(function success(response) {
          if (response.data.length > 0) {
            // We only take the first record since email is unique and data will only ever contain one element.
            var user = response.data[0];
            user.isAdmin = RolesHelper.hasRole(user, Roles.COMPANY_ADMIN);
            user.isReseller = RolesHelper.hasRole(user, Roles.RESELLER);
            user.isActive = user.status === 'ACTIVE' || user.status === 'CREATED';
            return user;
          }
          return null;
        }, function error() {
          return null;
        }).then(function success(user) {
          $scope.user = user;
        });
      };
      $scope.goToAccount = function () {
        $location.path('/accounts/' + $scope.user.accountId);
      };
      $scope.createAccount = function (email) {
        $location.path('/accounts/add-account').search('email', email);
      };
    });
})();
