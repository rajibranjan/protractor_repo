(function () {
  'use strict';

  angular.module('hp.portal.common.widgets', [
    'hp.portal.common.widgets.widgetUsersGraph',
    'hp.portal.common.widgets.widgetPullPrint',
    'hp.portal.common.widgets.widgetAccountSearch'
  ])

  .run(function (Preferences) {
      Preferences.$promise.then(function (preferences) {
        var types = preferences.getAllByType('widget');
        if (types !== undefined) {
          types.forEach(function (widgetPref) {
            // Cleanup all old widget preferences.
            // We could find a better way to cleanup ALL widget preferences for ALL users, and not need this code.....
            widgetPref.$delete();
          });
        }
      });
    })
    .controller('WidgetsCtrl', function ($scope, Roles, RolesHelper) {
      $scope.widgets = [{
        templateUrl: 'app/common/widgets/widget-templates/widget-pullprint.html'
      }];

      if (RolesHelper.selfHasRole(Roles.HP_ADMIN)) {
        $scope.widgets.length = 0;
        $scope.widgets.push({
          templateUrl: 'app/common/widgets/widget-templates/widget-account-search.html'
        });
      }
      // TODO Added RolesHelper.hasAnyRole that will allow us to make this call cleaner.
      // SDK change needs to make it through gerrit.
      else if (RolesHelper.selfHasRole(Roles.HP_RESELLER) || RolesHelper.selfHasRole(Roles.HP_ADMIN)) {
        $scope.widgets.length = 0;
        $scope.widgets.push({
          templateUrl: 'app/common/widgets/widget-templates/widget-account-search.html'
        });
      }
      else if (RolesHelper.selfHasRole(Roles.RESELLER)) {
        $scope.widgets.length = 0;
      }
      else if (RolesHelper.selfHasRole(Roles.COMPANY_ADMIN)) {
        $scope.widgets.push({
          templateUrl: 'app/common/widgets/widget-templates/widget-users-graph.html'
        });
        $scope.widgets.push({
          templateUrl: 'app/common/widgets/widget-templates/widget-devicemanager.html'
        });
      }
    });
})();
