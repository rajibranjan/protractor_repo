(function () {
  'use strict';

  angular.module('hp.portal.common.pagination.itemsPerPage', [])

  .directive('hpItemsPerPage', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/common/pagination/items-per-page.html',
      scope: {
        pageableResource: '='
      }
    };
  });
})();
