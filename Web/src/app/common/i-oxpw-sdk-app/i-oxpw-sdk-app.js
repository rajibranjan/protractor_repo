(function () {
  'use strict';

  angular.module('iOxpwSdkApp', [
    'init'
  ])

  .factory('PortalApiRootUrl', function ($q, serverURL) {
    var val = {
      url: serverURL,
      $promise: $q.resolve(val)
    };
    return val;
  })

  .constant('OfferingFqName', 'hp-portal')

  .factory('resourceURL', function (cdnURL) {
    return cdnURL;
  })

  .factory('activeProfileList', function (profile) {
    return profile;
  })

  .factory('sdkLocaleMap', function (localeMap) {
    return localeMap;
  })

  .factory('sdkAllowedLocales', function (allowedLocales) {
    return allowedLocales;
  });

})();
