(function () {
  'use strict';

  angular.module('hp.portal.common.forms.metaTags', [])

  .directive('hpMetaTagsForm', function () {
    return {
      restrict: 'E',
      scope: {
        metaTags: '=',
        availableTags: '=',
        freeForm: '='
      },
      templateUrl: 'app/common/forms/meta-tags.html',
      link: function (scope) {

        var removeDuplicateMetaTags = function () {
          scope.metaTags = scope.metaTags.filter(function (metaTag, index) {
            return scope.metaTags.indexOf(metaTag) === index;
          });
        };

        scope.resetForm = function () {
          scope.editing = false;
          scope.adding = false;
          scope.oldValue = '';
          scope.newValue = '';
        };

        scope.edit = function (metaTag) {
          scope.resetForm();
          scope.editing = true;
          scope.oldValue = metaTag;
          scope.newValue = metaTag;
        };

        scope.save = function () {
          var index = scope.metaTags.indexOf(scope.oldValue);
          scope.metaTags[index] = scope.newValue;
          removeDuplicateMetaTags();
          scope.resetForm();
        };

        scope.remove = function () {
          var index = scope.metaTags.indexOf(scope.oldValue);
          scope.metaTags.splice(index, 1);
          scope.resetForm();
        };

        scope.add = function () {
          if (scope.adding && !scope.editing && scope.newValue) {
            scope.metaTags.push(scope.newValue);
            removeDuplicateMetaTags();
            scope.resetForm();
          }
          else {
            scope.resetForm();
            scope.adding = true;
          }
        };

        scope.toggle = function (metaTag) {
          if (scope.availableTags.indexOf(metaTag) === -1) {
            return; // Can't toggle an unavailable tag
          }

          var index = scope.metaTags.indexOf(metaTag);
          if (index === -1) {
            scope.metaTags.push(metaTag);
          }
          else {
            scope.metaTags.splice(index, 1);
          }
        };
      }
    };
  });

})();
