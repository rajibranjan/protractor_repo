(function () {
  'use strict';

  angular.module('hp.portal.common.forms.userForm', [
    'hp.portal.common.webservices',
    'hp.portal.home.users.userStatus',
    'common.utils.roles',
  ])

  .directive('hpUserForm', function ($compile, LinkParser, Self, UserStatus, Locales, Roles, Globals) {
    return {
      restrict: 'A',
      scope: {
        user: '=',
        showEmail: '=',
        showLocales: '=',
        showStatus: '=',
        showRoles: '=',
        isAdding: '=',
        isEmailOptInDisplayed: '='
      },
      templateUrl: 'app/common/forms/user-form.html',
      link: function (scope) {
        scope.isSelf = (LinkParser.getSelfId(Self) === LinkParser.getSelfId(scope.user));
        scope.self = Self;
        scope.showEmailTracking = true;
        var removeWatch = scope.$watch('user', function () {
          if (typeof scope.user !== 'undefined') {
            scope.hasStatusCreated = scope.user.status === UserStatus.CREATED;
            if (typeof scope.user.account !== 'undefined') {
              if (scope.user.account.country === 'US' || scope.user.account.country === 'CA') {
                scope.showEmailTracking = false;
              }
            }
            removeWatch();
          }
        });

        var locales = Locales.getAll();
        scope.locales = {};
        angular.forEach(locales, function (value) {
          scope.locales[value.lang] = value.name;
        });

        var isHPAdmin = Globals.permissions.isHPAdmin;
        var isReseller = (Globals.permissions.isReseller && !isHPAdmin) || false;
        // var isCompanyAdmin = Globals.permissions.isCompanyAdmin && !isReseller;

        scope.roles = [{
          id: 'CompanyAdmin',
          show: true,
          value: Roles.COMPANY_ADMIN
        }, {
          id: 'Reseller',
          show: isReseller,
          value: Roles.RESELLER
        }, {
          id: 'OfferingAdmin',
          show: isHPAdmin,
          value: Roles.OFFERING_ADMIN
        }, {
          id: 'HPAdmin',
          show: (isHPAdmin && scope.isAdding),
          value: Roles.HP_ADMIN
        }, {
          id: 'HPReseller',
          show: isHPAdmin,
          value: Roles.HP_RESELLER
        }]; // TODO: Use some kind of Roles endpoint to get array of possible roles?

        if (isReseller) {
          scope._showRoles = false;

          if (scope.isAdding) {
            // When resellers create users, they get a fixed set of roles, so don't let the user even see/modify them.
            scope.user.roles = [Roles.COMPANY_ADMIN, Roles.RESELLER];
          }
        }
        else {
          scope._showRoles = true;
        }

        scope.disableEmail = !(isHPAdmin || scope.isAdding);
      }
    };
  });
})();
