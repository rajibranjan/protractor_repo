(function () {
  'use strict';

  angular.module('hp.portal.common.forms.companyContact', [
    'hp.portal.common.webservices'
  ])

  .directive('hpCompanyContactField', function ($compile, UserService, LinkParser) {
    return {
      restrict: 'A',
      scope: {
        company: '=company',
        uiSelectModel: '=ngModel',
        userChanged: '=onChange'
      },
      templateUrl: 'app/common/forms/company-contact.html',
      compile: function () {
        return function link(scope) {
          var accountId = LinkParser.getSelfId(scope.company);
          var params = {
            qs: 'accountId==' + accountId + ' && role==ROLE_COMPANY_ADMIN && status==ACTIVE'
          };

          UserService.get(params).then(function success(returnedResource) {
            scope.admins = returnedResource.data;
          });
        };
      }
    };
  });
})();
