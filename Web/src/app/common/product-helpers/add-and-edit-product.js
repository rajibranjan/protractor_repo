(function () {
  'use strict';

  angular.module('hp.portal.common.productHelpers.addAndEditProduct', [
    'hp.portal.home.accounts.addProductToAccountModal',
  ])

  .factory('AddAndEditProduct', function ($q, AddProductToAccountModal, ValidateLicenseKey, PageAlerts, ErrorCodeMapper, LinkParser) {
    function addProduct(availableProducts, shoppingCartItems) {
      return editProduct(undefined, availableProducts, shoppingCartItems);
    }

    function editProduct(enrollmentInfo, availableProducts, shoppingCartItems) {
      return AddProductToAccountModal.open({
        products: availableProducts,
        enrollmentInfo: enrollmentInfo,
      }).then(function resolved(updatedEnrollmentInfo) {
        verifyLicenseKey(updatedEnrollmentInfo);

        var index = availableProducts.indexOf(updatedEnrollmentInfo.product);
        if (index >= 0) {
          availableProducts.splice(index, 1);
          shoppingCartItems.push(updatedEnrollmentInfo);
        }
      }, function rejected(rejection) {
        if (rejection.operation === 'delete') {
          var index = shoppingCartItems.indexOf(rejection.enrollmentInfo);
          if (index >= 0) {
            shoppingCartItems.splice(index, 1);
            availableProducts.push(rejection.enrollmentInfo.product);
          }
        }

        return $q.reject();
      });
    }

    function verifyLicenseKey(enrollmentInfo) {
      if (!enrollmentInfo.licenseKey) {
        // If there is no licenseKey, then we are just bypassing license key validation for now.
        return;
      }

      var taskPayload = {
        licenseKey: enrollmentInfo.licenseKey,
        productId: LinkParser.getSelfId(enrollmentInfo.product)
      };

      delete enrollmentInfo.isLicenseKeyValid;
      enrollmentInfo.validating = true;

      ValidateLicenseKey.startTask(taskPayload).$promise.then(function success() {
        enrollmentInfo.isLicenseKeyValid = true;
      }, function error(task) {
        enrollmentInfo.isLicenseKeyValid = false;
        validateLicenseError(task);
      }).finally(function () {
        enrollmentInfo.validating = false;
      });
    }

    function validateLicenseError(task) {
      if (!ErrorCodeMapper.hasErrorCodes(task)) {
        PageAlerts.add({
          type: 'danger',
          msg: 'cAddProductError',
          translateValues: {
            errorCode: (task.httpStatus || 'unknown')
          },
          timeout: PageAlerts.timeout.long
        });
      }
    }

    return {
      addProduct: addProduct,
      editProduct: editProduct
    };
  });
})();
