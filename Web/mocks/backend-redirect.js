'use strict';

var http = require('http');
var https = require('https');

var proxy = process.env.http_proxy.match(/([a-zA-Z\-\.]+):([0-9]{2,6})/); // jshint ignore:line
var proxyHost = proxy[1];
var proxyPort = proxy[2];

// WARNING: Stop right there! Get your hands away from the keyboard!
// Please don't point this to a sensitive stack...like production. Seriously. Off to timeout you go.
// var stackHost = 'www-micro-dev.hpfoghorn.com';
var stackHost = 'www-micro-staging.hpfoghorn.com';
var stackPort = 443;
var localHost = 'localhost';
var localPort = 9000;

function openSocket(cb) {
  // This isn't just any ol' socket...
  // It's an secure socket through a corporate proxy. Yay!
  http.request({
    host: proxyHost,
    port: proxyPort,
    method: 'CONNECT',
    path: stackHost + ':' + stackPort
  })
  .on('connect', function (res, socket) {
    cb(socket);
  })
  .end();
}

function rewriteRequestHeaders(headers) {
  if (headers.referer) {
    headers.referer = headers.referer.replace('http://' + localHost + ':' + localPort, 'https://' + stackHost);
  }
  headers.host = stackHost + ':' + stackPort;
  // GZip gets all up in our business
  if (headers['accept-encoding']) {
    // Let the server know we won't have any of that, mhmmm
    delete headers['accept-encoding'];
  }
}

function rewriteResponseHeaders(headers) {
  // Handle redirects, like a boss
  if (headers.location) {
    headers.location = headers.location.replace('https://' + stackHost, 'http://' + localHost + ':' + localPort);
  }
  // Cookies are host-specific
  var cookie = headers['set-cookie'];
  if (cookie) {
    cookie = cookie.toString();
    cookie = cookie.replace('Domain=.hpfoghorn.com; ', '');
    cookie = cookie.replace('Path=/; Secure', 'Path=/');
    headers['set-cookie'] = cookie;
  }
}

function shouldProxyRequest(req) {
  if (req.url.indexOf('/api/') === 0) {
    // Server will return real, actual data, woohoo!
    return true;
  } else if (req.url.indexOf('/j_spring_security') === 0) {
    // Server will reset user's cookies, redirect to /
    return true;
  } else if (req.url === '/') {
    // Server will redirect to /home or /login based on auth status
    return true;
  }
  return false;
}

module.exports = function (req, res, next) {
  // Rewrite backend beautified URLs
  if (req.url.match(/^\/home\/?/)) {
    req.url = '/index.html';
  } else if (req.url === '/login') {
    req.url = '/login.html';
  }

  // So, you want the server config, do ya?
  if (req.url === '/api/v2/configuration') {
    // Rewritten config is in the mocks folder under a different name
    req.url = '/api/v2/configuration-backend-proxied';
    return next(); // Pass off to the static files handler
  }

  if (shouldProxyRequest(req)) {

    // Open a TLS socket to the stack
    openSocket(function (socket) {

      // Hide all traces that the request was made to localhost
      rewriteRequestHeaders(req.headers);

      // Perform the fancy secure request
      var request = https.request({
        host: stackHost,
        port: stackPort,
        path: req.url,
        method: req.method,
        headers: req.headers,
        socket: socket,
        agent: false
      }, function (response) {
        console.log('New server request:');
        console.log('  method: ' + req.method);
        console.log('  url: ' + req.url);
        console.log('  > piping through to ' + stackHost);
        console.log('  > response: ' + response.statusCode);

        // Ensure the headers look like they're from us
        rewriteResponseHeaders(response.headers);

        // Send back the real status code and headers
        res.writeHead(response.statusCode, response.headers);
        response.setEncoding('utf8');

        // Requests can be piped directly
        response.pipe(res);

        // Once this request is done, destroy the socket
        response.on('end', function () {
          socket.end();
        });
      });

      // Pass through request body
      req.pipe(request);

    });
  }

  // Leave it to the other handlers
  else {
    next();
  }

};
