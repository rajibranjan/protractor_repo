var fs = require('fs');


var applyHeaders = function (response, payload) {
  if (payload.headers) {
    console.log('    > Headers found in payload, applying them to the response.');
    payload.headers.forEach(function (header) {
      console.log('      > ' + header.name + ' => ' + header.value);
      response.setHeader(header.name, header.value);
    });
    delete payload.headers;
  }
};

module.exports = function(req, res, next) {

  // Function that will read a file from the path and write out its data to the given response object.
  var readFile = function(res, path) {
    var file = fs.createReadStream(path);

    var buff = [];
    file.on('data', function (b) {
      buff.push(b);
    });

    file.on('end', function () {
      var content = Buffer.concat(buff);
      content = content.toString('utf8')
      var payload = JSON.parse(content);

      // Allow the mock file to specify headers to include in the response.
      applyHeaders(res, payload);

      // Allow the mock file to specify a response http status code, otherwise default to 200.
      var httpStatus = payload.httpStatus || 200;

      res.writeHead(httpStatus, {
        'Content-Type': 'application/json'
      });
      res.write(JSON.stringify(payload));
    });

    file.on('close', function() {
      res.end();
    });
  };

  var getMockFilePath = function (url, method) {
    var path = ('./mocks/mock_data/' + url)
    .replace('//','/')
    .replace(/%(..)/g, function(match, hex) {
      return String.fromCharCode(parseInt(hex, 16))
    })
    .replace(/[&?]/g,'_');
    path += '.' + method;
    path += '.json';
    return path;
  };

  var mockFilePath = getMockFilePath(req.url, req.method);

  console.log('New server request:');
  console.log('  method:       ' + req.method);
  console.log('  url:          ' + req.url);
  console.log('  mockFilePath: ' + mockFilePath);

  if(req.method === 'DELETE') {
    console.log('  > DELETE requests are always successful, returning 200.');
    res.writeHead(200);
    res.end();
    return;
  }
  // For all PUT and POST methods on the NodeJS Server we will just return the payload of the request on the response
  else if(req.method === 'PUT' || req.method === 'POST') {
    try {
      fs.statSync(mockFilePath);
    } catch (e) {
      // Mock file did not exist. Just response with the data that was POSTed/PUTed.
      console.log('  > Mock file does not exist. Responding with ' + req.method + 'ed data...');

      // When a data event is received, write out that data to the response.
      req.on('data', function(payload) {
        res.setHeader('Content-Type','application/json');
        var status = req.method === 'POST' ? 201 : 200;
        if (req.headers['content-type'].indexOf('application/x-www-form-urlencoded') === 0) {
          console.log('application/x-www-form-urlencoded request... not sure what to do');
        } else if (req.headers['content-type'].indexOf('multipart/form-data;') === 0) {
          console.log('multipart/form-data request... not sure what to do');
        } else {
          payload = JSON.parse(payload);
          if (req.method === 'POST') {
            // Generate a new GUID for the self link
            var found = false;
            if (payload.links === undefined) {
              payload.links = [];
            } else {
              for (var i = 0; i < payload.links.length; i++) {
                if (payload.links[i].rel === 'self') {
                  found = true;
                }
              }
            }
            if (!found) {
              var hrefBase = 'https://localhost:9443/HPStoreWeb/api/v2/something/';
              var randomGuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {var r = Math.random()*16|0,v=c=='x'?r:r&0x3|0x8;return v.toString(16);});
              payload.links.push({
                rel: 'self',
                href: hrefBase + randomGuid
              });
            }
          }
        }
        res.writeHead(status);
        res.write(JSON.stringify(payload));
      });

      // On an end event on the request, end the response
      req.on('end', function() {
        res.end();
      });
      return;
    }
  }

  // For GET requests return a mock JSON file for the requested entity
  fs.stat(mockFilePath, function(err, stat) {
    if (!err) {
      console.log('  > Mock file exists. Returning mock file contents.');
      readFile(res, mockFilePath);
    }
    else {
      console.log('  mock file not found, returning 404');
      res.writeHead(404, { });
      res.end();
    }
  });

};


