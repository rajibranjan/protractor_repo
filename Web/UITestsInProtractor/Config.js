// An example configuration file.
exports.config = {
  //directConnect: true,
	// location of the Selenium JAR file and chromedriver, use these if you installed protractor locally
  seleniumServerJar: '/usr/lib/node_modules/protractor/node_modules/webdriver-manager/selenium/selenium-server-standalone-2.53.1.jar',
  chromeDriver: '/usr/lib/node_modules/protractor/node_modules/webdriver-manager/selenium/chromedriver_2.26',

  baseUrl: 'http://localhost:9000/',

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    'browserName': 'chrome'
	//Check for phantomJS
  },

  // Framework to use. Jasmine is recommended.
  framework: 'jasmine',

  // Spec patterns are relative to the current working directory when
  // protractor is called.
  specs: ['specs/ProductSignUpPageExistingUserLoggedIn.js'],

  // Options to be passed to Jasmine.
  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  }
};
