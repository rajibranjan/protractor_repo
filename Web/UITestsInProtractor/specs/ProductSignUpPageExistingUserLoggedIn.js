describe('License Key text input box is displayed', function() {
	it('should display the License Key input box in the form', function() {
		browser.get('/login.html?noredirect=true&productId=12&licenseKey=#/sign-up-with-product');
		expect(element(by.model('licenseData.licenseKey')).isDisplayed()).toBeTruthy();
	
	//element(by.model('licenseData.licenseKey')).getAttribute('value').then(function(licensekeydata) {
	//	expect(licensekeydata).toEqual('123');
	//	console.log("The value of the licensekey is "+licensekeydata);
	//	});
	});
	it('should allow to input license value',function() {
		element(by.model('licenseData.licenseKey')).sendKeys("12");
		expect(element(by.model('licenseData.licenseKey')).getAttribute('value')).toBe('12');
		element(by.model('licenseData.licenseKey')).getAttribute('value').then(function(licensekeydata) {
      			console.log("The value of entered license key is -"+licensekeydata);
		});
    	});
	
	it('should display license added message on clicking Submit and display Return Home button', function() {
		element(by.id('submitButton')).click();
		expect(element(by.xpath("//div/span[@translate='cSuccessAddingLicense']")).getText()).toEqual("Successfully added a license to your account.");
		//expect(element(by.xpath("//div/a[@id='returnHomeLink']")).isDisplayed()).toBeTruthy();
		var returnHomeLink=element(by.id('returnHomeLink'));  //This we would need to move to a page, that will be the next step - using Page Object Model for Protractor tests
		expect(returnHomeLink.isDisplayed()).toBeTruthy();
	});
	
	it('should redirect to the login screen on clicking Return Home button', function() {
		var returnHomeLink=element(by.xpath('//div/a[@id="returnHomeLink"]'));
		returnHomeLink.click();
		var userNameTextBox = element(by.model("formData.j_username"));
		expect(userNameTextBox.isDisplayed()).toBeTruthy();
		userNameTextBox.sendKeys("CheckingIfItIsIdentified");
	});
});


	
