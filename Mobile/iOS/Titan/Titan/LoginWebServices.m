//
//  LoginWebServices.m
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import "LoginWebServices.h"


#define WS_LOGIN_GET @"/api/v2/login"


@implementation LoginWebServices


+(void)attemptLoginWithUsername:(NSString *)username password:(NSString *)password
                    withSuccess: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSString *credentialsString = [NSString stringWithFormat:@"%@:%@", username, password];
    NSData *base64EncodedCredentialsData = [credentialsString dataUsingEncoding: NSUTF8StringEncoding]; // @"aHBhZG1pbkBlZ2dub2djb29raWUuY29tOkhwYWRtaW4xMjM0NQ==";
    NSString *authenticationString = [NSString stringWithFormat: @"Basic %@", [WebServicesController base64forData: base64EncodedCredentialsData]];
    
    // Set header
    [[[WebServicesController requestOperationManager] requestSerializer] setValue: authenticationString forHTTPHeaderField: @"Authorization"];
    [[[WebServicesController requestOperationManager] requestSerializer] setValue: @"application/json" forHTTPHeaderField: @"Content-Type"];

    // Send request
    [[WebServicesController requestOperationManager] GET: WS_LOGIN_GET
                                              parameters: nil
                                                 success: success
                                                 failure: failure];
}


@end
