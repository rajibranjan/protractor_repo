//
//  WebServicesController.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"


#define WS_SERVER @"https://mfp-pre.hpbizapps.com"

@interface WebServicesController : NSObject

+(AFHTTPRequestOperationManager *)requestOperationManager;

+(NSString*)base64forData:(NSData*)theData;

+(NSString *)replaceValue:(NSString *)valueString withKey:(NSString *)keyToReplace;

@end
