//
//  LoginWebServices.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServicesController.h"


@interface LoginWebServices : WebServicesController

+(void)attemptLoginWithUsername:(NSString *)username password:(NSString *)password
                    withSuccess: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;


@end
