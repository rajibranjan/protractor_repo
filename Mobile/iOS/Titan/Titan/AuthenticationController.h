//
//  AuthenticationController.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuthenticationController : NSObject


+(BOOL)isSessionValid;

+(AuthenticationController *)sharedAuthenticationController;

+(void)setUsername:(NSString *)usernameString password:(NSString *)passwordString;

+(void)updateSession;
+(void)invalidateSession;

+(void)setAuthenticationCookieFromResponse:(NSHTTPURLResponse *)response;

+(void)resetAuthentication;

+(NSString *)username;
+(NSString *)password;
+(NSString *)authenticationToken;


@end
