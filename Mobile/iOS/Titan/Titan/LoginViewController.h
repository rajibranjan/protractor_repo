//
//  LoginViewController.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (strong, nonatomic) IBOutlet UISwitch *saveCredentialsSwitch;

@property (weak, nonatomic) IBOutlet UIView *overlayView;

@end
