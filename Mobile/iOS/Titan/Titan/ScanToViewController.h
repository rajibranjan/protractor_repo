//
//  ScanToViewController.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScanToViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
-(IBAction)scanImage:(id)sender;
@end
