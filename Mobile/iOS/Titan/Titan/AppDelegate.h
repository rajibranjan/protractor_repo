//
//  AppDelegate.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
