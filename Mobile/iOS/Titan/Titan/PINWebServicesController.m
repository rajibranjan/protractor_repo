//
//  PINWebServicesController.m
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import "PINWebServicesController.h"


@implementation PINWebServicesController

+(void)validatePIN:(NSString *)pinString
           success: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
           failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@", WS_SERVER, @"/PIN"];
    
    // Send request
    [[WebServicesController requestOperationManager] GET: urlString
                                              parameters: nil
                                                 success: success
                                                 failure: failure];
}

@end
