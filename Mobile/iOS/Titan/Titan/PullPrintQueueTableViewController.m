//
//  PullPrintQueueTableViewController.m
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import "PullPrintQueueTableViewController.h"

#import "PullPrintWebServices.h"
#import "PullPrintFactory.h"
#import "PullPrintDocument.h"

#import "MBProgressHUD.h"


#define HUD_DELAY 3


@interface PullPrintQueueTableViewController ()

@property(nonatomic, strong)NSMutableArray *pullprintQueue;

@end



@implementation PullPrintQueueTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"PullPrintQueueCell"];

    [self loadDocuments];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)refreshDocuments:(id)sender
{
    [self loadDocuments];
}

-(void)loadDocuments
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo: self.parentViewController.view animated: YES];
    hud.labelText = NSLocalizedString(@"cLoading", nil);
    self.pullprintQueue = [[NSMutableArray alloc] init];
    
    void (^successBlock)(AFHTTPRequestOperation *, id) = ^void(AFHTTPRequestOperation *operation, id responseObject)
    {
        [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];
        
        self.pullprintQueue = [[PullPrintFactory createPullPrintObjectsFromJSON: responseObject] mutableCopy];
        [self.tableView reloadData];
    };
    void (^failureBlock)(AFHTTPRequestOperation *, id) = ^void(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"%@", operation);
        NSLog(@"%@", responseObject);
        
        [MBProgressHUD hideAllHUDsForView: self.parentViewController.view animated: YES];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: @"Refresh Error"
                                                            message: @"There was an error retrieving your Pull Print Queue"
                                                           delegate: nil
                                                  cancelButtonTitle: @"Dismiss"
                                                  otherButtonTitles: nil];
        [alertView show];
        
        self.pullprintQueue = [[PullPrintFactory createPullPrintObjectsFromJSON: nil] mutableCopy];
        [self.tableView reloadData];
    };
    
    [PullPrintWebServices retrievePullPrintDocumentsWithSuccess: successBlock
                                                        failure: failureBlock];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.pullprintQueue count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PullPrintQueueCell" forIndexPath:indexPath];
    PullPrintDocument *currentDocument = [self.pullprintQueue objectAtIndex: indexPath.row];
    cell.textLabel.text = currentDocument.documentName;
    cell.detailTextLabel.text = currentDocument.documentID;
    
    NSString *documentIconName = [NSString stringWithFormat:@"icon-%@", currentDocument.documentExtension];

    UIImage *documentIcon = [UIImage imageNamed: documentIconName];
    
    if( documentIcon != nil )
    {
        cell.imageView.image = documentIcon;
    }
    else
    {
        cell.imageView.image = [UIImage imageNamed:@"icon-default"];
    }
    
    return cell;
}

 
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [PullPrintWebServices deletePullPrintDocumentWithID: [(PullPrintDocument *)[self.pullprintQueue objectAtIndex: indexPath.row] documentID]
                                                withSuccess:^(AFHTTPRequestOperation *operation, id responseObject){
                                                    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo: self.view animated: YES];
                                                    progressHUD.mode = MBProgressHUDModeText;
                                                    progressHUD.labelText = @"Removed Pull Print Queue Item";
                                                    [self performSelector: @selector(hideHUD) withObject: nil afterDelay: HUD_DELAY];
                                                }
                                                failure:^(AFHTTPRequestOperation *operation, id responseObject){
                                                    NSLog(@"failed to delete object");
                                                }];
        [self.pullprintQueue removeObjectAtIndex: indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


-(void)hideHUD
{
    [MBProgressHUD hideHUDForView: self.view animated: YES];
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

@end
