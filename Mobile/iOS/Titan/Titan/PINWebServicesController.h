//
//  PINWebServicesController.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import "WebServicesController.h"

@interface PINWebServicesController : WebServicesController

+(void)validatePIN:(NSString *)pinString
           success: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
           failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

@end
