//
//  PullPrintWebServices.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import "WebServicesController.h"

@interface PullPrintWebServices : WebServicesController

+(void)retrievePullPrintDocumentsWithSuccess: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                     failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

+(void)deletePullPrintDocumentWithID: (NSString *)documentID
                         withSuccess: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                             failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

+(void)uploadPullPrintDocument:(NSData *)documentData
                   withSuccess: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                       failure: (void (^)(AFHTTPRequestOperation *operation, id responseObject))failure;


@end
