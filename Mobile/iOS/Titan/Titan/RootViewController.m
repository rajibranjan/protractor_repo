//
//  RootViewController.m
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import "RootViewController.h"

#import "AuthenticationController.h"
#import "LoginViewController.h"

#import "ADBMobile.h"
#import "LocationController.h"


@interface RootViewController ()
@property(nonatomic, strong) UITabBarController *tabBarController;
@end


@implementation RootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(locationDidUpdate:)
                                                 name: kLocationDidChange
                                               object: nil];
    
    self.tabBarController = [self.storyboard instantiateViewControllerWithIdentifier:@"RootTabBarController"];
    [self.view addSubview: self.tabBarController.view];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    /*
	 * Adobe Tracking - Analytics
	 *
	 * call to trackState:data: for view states report
	 * trackState:data: increments the page view
	 */
	[ADBMobile trackState:@"Home" data:nil];
    
    if( ![AuthenticationController isSessionValid] )
    {
        LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle: nil];
        [self presentViewController: loginViewController animated: YES completion: nil];
    }
}


-(void)locationDidUpdate:(NSNotification *)notification
{
    CLLocation *updatedLocation = (CLLocation *)[notification object];
    static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		/*
		 * Adobe Tracking - Analytics
		 *
		 * trackLocation:data: call to get the location of the current user
		 * because the config file has points of interest in it, the SDK will automatically determine
		 * whether the user falls within a point of interest
		 */
		[ADBMobile trackLocation: updatedLocation data: nil];
	});

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
