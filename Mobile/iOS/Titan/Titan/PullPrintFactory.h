//
//  PullPrintFactory.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PullPrintFactory : NSObject

+(NSArray *)createPullPrintObjectsFromJSON:(NSDictionary *)pullprintDocuments;

@end
