//
//  QRReaderViewController.m
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import "QRReaderViewController.h"

@interface QRReaderViewController ()

@property (weak, nonatomic) IBOutlet UIButton *qrLaunchButton;
@property (weak, nonatomic) IBOutlet UITextView *qrCodeTextView;

@end

@implementation QRReaderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






-(IBAction)loadReader
{
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present the controller
    [self presentViewController: reader animated: YES completion: nil];
}



- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
    {
        break;
    }
    
    NSString *textDataString = symbol.data;
    [self.qrCodeTextView setText: textDataString];
    
    // Do something useful with the barcode image
    //    id image = [info objectForKey: UIImagePickerControllerOriginalImage];
    
    [reader dismissViewControllerAnimated: YES completion: nil];
}


@end
