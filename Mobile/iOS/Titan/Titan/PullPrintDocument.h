//
//  PullPrintDocument.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PullPrintDocument : NSObject

@property(nonatomic, copy)NSString *documentAccess;
@property(nonatomic, copy)NSString *createdBy;
@property(nonatomic, copy)NSString *createdWhen;
@property(nonatomic, copy)NSString *datasource;
@property(nonatomic, copy)NSString *documentID;
@property(nonatomic, copy)NSString *documentExtension;
@property(nonatomic, copy)NSString *documentFileCreatedWhen;
@property(nonatomic, copy)NSString *documentFileModifiedWhen;
@property(nonatomic, copy)NSString *documentFileReadOnly;
@property(nonatomic, copy)NSString *documentFolderID;
@property(nonatomic, copy)NSString *documentIsShared;
@property(nonatomic, copy)NSString *documentLibrary;
@property(nonatomic, copy)NSString *documentModifiedBy;
@property(nonatomic, copy)NSString *documentModifiedWhen;
@property(nonatomic, copy)NSString *documentName;
@property(nonatomic, copy)NSString *documentOwnerID;
@property(nonatomic, copy)NSString *documentSize;
@property(nonatomic, copy)NSString *documentTags;
@property(nonatomic, copy)NSString *documentUploadedBy;
@property(nonatomic, copy)NSString *documentVersion;

@end
