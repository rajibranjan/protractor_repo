//
//  PullPrintQueueTableViewController.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface PullPrintQueueTableViewController : UITableViewController

-(IBAction)refreshDocuments:(id)sender;

@end
