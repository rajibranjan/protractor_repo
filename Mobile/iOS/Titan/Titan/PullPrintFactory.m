//
//  PullPrintFactory.m
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import "PullPrintFactory.h"

#import "PullPrintDocument.h"



@implementation PullPrintFactory

+(NSArray *)createPullPrintObjectsFromJSON:(NSDictionary *)pullprintDocuments
{
    NSMutableArray *pullprintArray = [[NSMutableArray alloc] init];
    
    NSArray *pullprintQueueArray = [pullprintDocuments objectForKey: @"data"];
    for( NSDictionary *currentDocument in pullprintQueueArray){
        PullPrintDocument *newPullPrintDocument = [[PullPrintDocument alloc] init];
        newPullPrintDocument.documentExtension = [currentDocument objectForKey: @"extension"];
        newPullPrintDocument.documentID = [currentDocument objectForKey: @"documentId"];
        newPullPrintDocument.documentName = [currentDocument objectForKey: @"name"];
        [pullprintArray addObject: newPullPrintDocument];
    }
    
    return [[NSArray alloc] initWithArray: [pullprintArray copy]];
}


@end
