//
//  PullPrintFile.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PullPrintFile : NSObject

@property(nonatomic, copy)NSURL *fileName;
@property(nonatomic, copy)NSString *name;
@property(nonatomic, strong)NSData *fileData;

@end
