//
//  PINViewController.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PINViewController : UIViewController <UIKeyInput>

@end
