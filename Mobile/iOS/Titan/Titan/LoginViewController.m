//
//  LoginViewController.m
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import "LoginViewController.h"

#import "LoginWebServices.h"
#import "AuthenticationController.h"
#import "PINViewController.h"

#import "MBProgressHUD.h"


#define WS_TIMEOUT_SECONDS 90



@interface LoginViewController ()

@property(copy, nonatomic)NSString *username;
@property(copy, nonatomic)NSString *password;

@end



@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *savedUsername = [AuthenticationController username];
    NSString *savedPassword = [AuthenticationController password];
    
    // Fill in username
    if( savedUsername != nil && ![savedUsername isEqualToString: @""] )
    {
        self.emailTextField.text = [AuthenticationController username];
    }
    else
    {
        [self.emailTextField becomeFirstResponder];
    }
    
    // Fill in password
    if( savedPassword != nil && ![savedPassword isEqualToString: @""] )
    {
        self.passwordTextField.text = [AuthenticationController password];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)submitLoginInformation:(id)sender
{
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    
    self.username = _emailTextField.text;
    [_emailTextField resignFirstResponder];
    
    self.password = _passwordTextField.text;
    [_passwordTextField resignFirstResponder];
    
    if( [self.saveCredentialsSwitch isOn] )
    {
        [AuthenticationController setUsername: self.username password: self.password];
    }
    else
    {
        [AuthenticationController resetAuthentication];
    }
    
    void (^successBlock)(AFHTTPRequestOperation *, id) = ^void(AFHTTPRequestOperation *operation, id responseObject)
    {
        [AuthenticationController setAuthenticationCookieFromResponse: operation.response];
        
        [MBProgressHUD hideHUDForView: self.view animated:YES];
        
        [self dismissViewControllerAnimated: YES completion: ^{
            [self.overlayView setHidden: YES];
        }];
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *, id) = ^void(AFHTTPRequestOperation *operation, id responseObject)
    {
        [MBProgressHUD hideHUDForView: self.view animated:YES];
        [self.overlayView setHidden: YES];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"cErrorSigninTitle", nil)
                                                            message: NSLocalizedString(@"cErrorSignin", nil)
                                                           delegate: nil
                                                  cancelButtonTitle: NSLocalizedString(@"cDismiss", nil)
                                                  otherButtonTitles: nil];
        [alertView show];
        
    };
    
    [self.overlayView setHidden: NO];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"cLoading", nil);
    
    [LoginWebServices attemptLoginWithUsername: _username
                                      password: _password
                                   withSuccess: successBlock
                                       failure: failureBlock];
}


- (IBAction)signInWithPIN:(id)sender
{
    PINViewController *pinViewController = [[PINViewController alloc] initWithNibName:@"PINViewController" bundle:nil];
    [self presentViewController:pinViewController animated: YES completion: nil];
}


@end
