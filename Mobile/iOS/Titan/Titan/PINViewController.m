//
//  PINViewController.m
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import "PINViewController.h"

#import "PINWebServicesController.h"
#import "MBProgressHUD.h"


@interface PINViewController ()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *accountLabel;
@property (copy, nonatomic) NSString *pinString;
@property (assign, nonatomic) BOOL canDisplayNumPad;
@property (assign, nonatomic) NSUInteger loginAttempts;
@property (weak, nonatomic) IBOutlet UILabel *pinLabel;
@end



@implementation PINViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.pinString = @"";
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.loginAttempts = 0;
    
    [self performSelector: @selector(openKeypad) withObject: nil afterDelay: 1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)openKeypad
{
    self.canDisplayNumPad = YES;
    [self becomeFirstResponder];
}


-(BOOL)canBecomeFirstResponder
{
    if( _canDisplayNumPad )
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
- (void)insertText:(NSString *)text
{
    self.pinString = [NSString stringWithFormat:@"%@%@", _pinString, text];
    if( [_pinString length] == 6 )
    {
        [self checkPIN];
    }
    else if( [_pinString length] < 6 )
    {
        [self updateDisplayPIN];
    }
}


- (BOOL)hasText
{
    return  YES;
}


-(void)deleteBackward
{
    if( [self.pinString length] > 0 )
    {
        self.pinString = [self.pinString substringToIndex: [self.pinString length]-1];
        [self updateDisplayPIN];
    }
}


-(UIKeyboardType) keyboardType
{
	return UIKeyboardTypeNumberPad;
}


-(void)checkPIN
{
    [self resignFirstResponder];
    
    [MBProgressHUD showHUDAddedTo: self.containerView animated: YES];
    
    self.loginAttempts++;
    
    self.pinString = @"";
    
    void(^successBlock)(AFHTTPRequestOperation *, id) = ^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success");
        [MBProgressHUD hideAllHUDsForView: self.containerView animated: YES];
        [self becomeFirstResponder];
        [self updateDisplayPIN];
    };
    
    void(^failureBlock)(AFHTTPRequestOperation *, id) = ^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Failure");
        [MBProgressHUD hideAllHUDsForView: self.containerView animated: YES];
        [self becomeFirstResponder];
        [self updateDisplayPIN];
        
        [self finishPINLogin];
    };
    
    [PINWebServicesController validatePIN: _pinString
                                  success: successBlock
                                  failure: failureBlock];
}


-(void)updateDisplayPIN
{
    // TODO: temp pin display
    _pinLabel.text = @"";
    
    for( NSUInteger index = 0; index < [_pinString length]; index++ )
    {
        _pinLabel.text = [NSString stringWithFormat:@"%@%@", _pinLabel.text, @"*"];
    }
}

-(void)finishPINLogin
{
    [self dismissViewControllerAnimated: YES completion: nil];
}

@end
