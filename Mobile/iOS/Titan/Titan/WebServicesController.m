//
//  WebServicesController.m
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import "WebServicesController.h"





static AFHTTPRequestOperationManager *requestOperationManager = nil;



@implementation WebServicesController

+(AFHTTPRequestOperationManager *)requestOperationManager
{
    if( requestOperationManager == nil )
    {
        requestOperationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL: [NSURL URLWithString: WS_SERVER]];
    }
    
    return requestOperationManager;
}



#pragma mark - Utilities

+ (NSString*)base64forData:(NSData*)theData
{
	
	const uint8_t* input = (const uint8_t*)[theData bytes];
	NSInteger length = [theData length];
	
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
	
	NSInteger i,i2;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
		for (i2=0; i2<3; i2++) {
            value <<= 8;
            if (i+i2 < length) {
                value |= (0xFF & input[i+i2]);
            }
        }
		
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
	NSString *returnString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    return returnString;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// REPLACE VALUE WITH KEY /////////////////////////////////////////////////////////////////////////

+(NSString *)replaceValue:(NSString *)valueString withKey:(NSString *)keyToReplace
{
    if( keyToReplace != nil )
    {
        NSRange startRange = [valueString rangeOfString:@":"];
        
        if( startRange.location != NSNotFound )
        {
            NSMutableString *token = [[valueString substringFromIndex: startRange.location + startRange.length] mutableCopy];
            NSRange endRange       = [token rangeOfString:@":"];
            token                  = [[valueString substringWithRange: NSMakeRange(startRange.location, endRange.location + endRange.length + startRange.length)] mutableCopy];
            
            NSString *newString = [valueString stringByReplacingOccurrencesOfString: token withString: keyToReplace];
            
            return newString;
        }
    }
    
    return valueString; // return nil if keyToReplace is nil
}


@end
