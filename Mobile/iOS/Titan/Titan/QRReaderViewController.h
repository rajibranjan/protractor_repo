//
//  QRReaderViewController.h
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"


@interface QRReaderViewController : UIViewController <ZBarReaderDelegate>

@end
