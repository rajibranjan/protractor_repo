//
//  AuthenticationController.m
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//


#import "AuthenticationController.h"
#import "KeychainItemWrapper.h"

#define kAUTHTOKEN @"AUTHTOKEN"

#define SESSION_TIMEOUT 600 // 10 MINUTES


static AuthenticationController *sharedAuthenticationController = nil;



// TODO: Refresh session token




@interface AuthenticationController()

@property(strong, nonatomic)KeychainItemWrapper *keychainItemWrapper;

@property(copy, nonatomic)NSString *username;
@property(copy, nonatomic)NSString *password;
@property(copy, nonatomic)NSString *authenticationToken;

@property(strong, nonatomic)NSDate *sessionLastUsed;
@end


@implementation AuthenticationController


+(AuthenticationController *)sharedAuthenticationController
{
    if( sharedAuthenticationController == nil )
    {
        sharedAuthenticationController = [[AuthenticationController alloc] init];
        sharedAuthenticationController.keychainItemWrapper = [[KeychainItemWrapper alloc] initWithIdentifier: APP_IDENTIFIER accessGroup: nil];
    }
    
    return sharedAuthenticationController;
}


+(BOOL)isSessionValid
{
    if( sharedAuthenticationController.sessionLastUsed == nil ||
       [sharedAuthenticationController.sessionLastUsed timeIntervalSinceNow] > SESSION_TIMEOUT ||
       sharedAuthenticationController.authenticationToken == nil )
    {
        return NO;
    }
    
    return YES;
}


+(void)setUsername:(NSString *)usernameString password:(NSString *)passwordString
{
    [AuthenticationController sharedAuthenticationController];
    
    sharedAuthenticationController.username = usernameString;
    sharedAuthenticationController.password = passwordString;

    [sharedAuthenticationController.keychainItemWrapper setObject: sharedAuthenticationController.username forKey: (__bridge id)(kSecAttrAccount)];
    [sharedAuthenticationController.keychainItemWrapper setObject: sharedAuthenticationController.password forKey: (__bridge id)(kSecValueData)];
}


+(void)updateSession
{
    sharedAuthenticationController.sessionLastUsed = [NSDate date];
}


+(void)invalidateSession
{
    sharedAuthenticationController.sessionLastUsed     = nil;
    sharedAuthenticationController.authenticationToken = nil;
}


+(void)setAuthenticationCookieFromResponse:(NSHTTPURLResponse *)response
{
    [AuthenticationController sharedAuthenticationController];
    
    NSArray *cookiesArray = [NSHTTPCookie cookiesWithResponseHeaderFields: [response allHeaderFields] forURL: response.URL];
    NSHTTPCookie *authenticationCookie;
    
    for ( authenticationCookie in cookiesArray)
    {
        // V1 || V2
        if( [authenticationCookie.name isEqualToString:@"X-SSO-Token"] || [authenticationCookie.name isEqualToString:@"X-Auth-Token"] )
        {
            sharedAuthenticationController.authenticationToken = authenticationCookie.value;
        }
    }
    
    [AuthenticationController updateSession];
}


+(NSString *)username
{
    [AuthenticationController sharedAuthenticationController];
    
    return [sharedAuthenticationController.keychainItemWrapper objectForKey: (__bridge id)(kSecAttrAccount)];
}


+(NSString *)password
{
    [AuthenticationController sharedAuthenticationController];

    return [sharedAuthenticationController.keychainItemWrapper objectForKey: (__bridge id)(kSecValueData)];
}


+(NSString *)authenticationToken
{
    [AuthenticationController sharedAuthenticationController];
    
    return sharedAuthenticationController.authenticationToken;
}


+(void)resetAuthentication
{
    [AuthenticationController sharedAuthenticationController];
    
    sharedAuthenticationController.username = @"";
    sharedAuthenticationController.password = @"";
    sharedAuthenticationController.authenticationToken = @"";
    sharedAuthenticationController.sessionLastUsed = nil;
    
    [sharedAuthenticationController.keychainItemWrapper resetKeychainItem];
}


@end
