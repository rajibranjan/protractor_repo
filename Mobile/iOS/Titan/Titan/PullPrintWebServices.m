//
//  PullPrintWebServices.m
//  Titan
//
//  Copyright (c) 2014 HP. All rights reserved.
//

#import "PullPrintWebServices.h"
#import "AuthenticationController.h"

#define WS_PULLPRINT_SERVER @"https://mfp-pre.hpbizapps.com"

#define WS_PULLPRINT_ROOT @"/offerings/hppullprint/api/v2"

#define WS_PULLPRINT_DOCUMENTS @"/folders/print/documents"
#define WS_PULLPRINT_DELETE    @"/documents/:documentId:"
#define WS_PULLPRINT_UPLOAD    @"/documents/scan"


static AFHTTPRequestOperationManager *pullPrintRequestOperationManager = nil;


@implementation PullPrintWebServices

+(AFHTTPRequestOperationManager *)requestOperationManager
{
    if( pullPrintRequestOperationManager == nil )
    {
        pullPrintRequestOperationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL: [NSURL URLWithString: WS_PULLPRINT_SERVER]];
    }
    
    return pullPrintRequestOperationManager;
}

+(void)retrievePullPrintDocumentsWithSuccess: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                     failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@", WS_PULLPRINT_SERVER, WS_PULLPRINT_ROOT, WS_PULLPRINT_DOCUMENTS];
    
    // Send request
    [[PullPrintWebServices requestOperationManager] GET: urlString
                                              parameters: nil
                                                 success: success
                                                 failure: failure];
}



+(void)deletePullPrintDocumentWithID: (NSString *)documentID
                         withSuccess: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                             failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSString *deleteEndPoint = [WebServicesController replaceValue: WS_PULLPRINT_DELETE withKey: documentID];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@", WS_PULLPRINT_SERVER, WS_PULLPRINT_ROOT, deleteEndPoint];
    
    // Send request
    [[PullPrintWebServices requestOperationManager] DELETE: urlString
                                                 parameters: nil
                                                    success: success
                                                    failure: failure];
}



+(void)uploadPullPrintDocument: (NSData *)documentData
                   withSuccess: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                       failure: (void (^)(AFHTTPRequestOperation *operation, id responseObject))failure
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@", WS_PULLPRINT_SERVER, WS_PULLPRINT_ROOT, WS_PULLPRINT_UPLOAD];
    
    // Send request
    [[PullPrintWebServices requestOperationManager] POST: urlString
                                              parameters: nil
                               constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
                                {
                                    [formData appendPartWithFileData: documentData
                                                                name: @"file"
                                                            fileName: @"file.something"
                                                            mimeType: @"pdf"];
                               }
                                                 success: success
                                                 failure: failure];
    
    
//    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:@"http://example.com/upload" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        [formData appendPartWithFileURL:[NSURL fileURLWithPath:@"file://path/to/image.jpg"] name:@"file" fileName:@"filename.jpg" mimeType:@"image/jpeg" error:nil];
//    } error:nil];
//    
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    NSProgress *progress = nil;
//    
//    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithStreamedRequest:request progress:&progress completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
//        if (error) {
//            NSLog(@"Error: %@", error);
//        } else {
//            NSLog(@"%@ %@", response, responseObject);
//        }
//    }];
//    
//    [uploadTask resume];
}


@end
