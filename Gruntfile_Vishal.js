// Generated on 2013-11-14 using generator-angular 0.6.0-rc.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

/*jshint camelcase: false */


module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);

  var defaultMiddleware = function (connection, options) {
    var plugins = [];
    var paths = options.base;
    if (!Array.isArray(paths)) {
      paths = [paths];
    }
    paths.forEach(function(path) {
      plugins.push(connection['static'](path));
    });
    plugins.push(require('./mocks/mock-json.js'));
    return plugins;
  };

  grunt.initConfig({
    shell: {
      options: {
        stdout: true,
        failOnError: true
      },
      npmInstall: {
        command: 'npm install'
      },
      bowerInstall: {
        command: 'bower install'
      },
      gitFetch: {
        command: 'git fetch --all'
      },
      gitRebase: {
        command: 'git rebase origin'
      },
      gitReview: {
        command: 'git review'
      },
      gitReviewDraft: {
        command: 'git review -D'
      }
    },
    yeoman: {
      // configurable paths
      app: require('./bower.json').appPath || 'src/app',
      src: 'src',
      test: 'test',
      assets: 'src/assets',
      dist: 'dist',
      titanSdk:'src/bower_components/titan-web-sdk/dist'
    },
    deploy: {
      home: '../../HPStore/target',
      appName: 'HPStoreWeb-1.1.0-SNAPSHOT',
      distLocation: 'WEB-INF/dist'
    },
    watch: {
      less: {
        options: {
          nospawn: true
        },
        files: [
          '<%= yeoman.src %>/styles/**/*.less',
          '<%= yeoman.titanSdk %>/styles/**/*.less'
        ],
        tasks: ['less:dev']
      },
      styles: {
        files: ['<%= yeoman.src %>/styles/{,*/}*.css'],
        tasks: ['copy:styles', 'autoprefixer']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= yeoman.src %>/**/*.html',
          '!<%= yeoman.src %>/bower_components/**/*.html',// Exclude HTML files in the bower_components folder
          '<%= yeoman.src %>/styles/**/*.less',
          '{.tmp,<%= yeoman.app %>}/**/*.js',
          '<%= yeoman.titanSdk %>/**/*',
          '<%= yeoman.src %>/**/i18n/*.json',
          '<%= yeoman.assets %>/**/*.{png,jpg,jpeg,gif,webp,svg}',
          'mocks/mock_data/**/*'
        ]
      }
    },
    autoprefixer: {
      options: ['last 1 version'],
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside. Do this to test other browsers and access via the VM's IP address.
        hostname: 'localhost',
        livereload: 35729,
        middleware: defaultMiddleware
      },
      livereload: {
        options: {
          open: 'http://localhost:9000/#/dashboard',
          base: [
            '.tmp',
            '<%= yeoman.src %>'
          ]
        }
      },
      backend: {
        options: {
          middleware: function (connection, options) {
            var handlers = defaultMiddleware(connection, options);
            return [require('./mocks/backend-redirect.js')].concat(handlers);
          },
          open: 'http://localhost:9000/#/dashboard',
          base: [
            '.tmp',
            '<%= yeoman.src %>'
          ]
        }
      },
      test: {
        options: {
          port: 9001,
          base: [
            '.tmp',
            'test',
            '<%= yeoman.src %>'
          ]
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= yeoman.dist %>',
          keepalive: true,
          livereload: false
        }
      }
    },
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/*',
            '!<%= yeoman.dist %>/.git*',
            '<%= yeoman.app %>/coverage/*.json'
          ]
        }]
      },
      deploy: {
        options : { force:true},
        files: [{
          dot: true,
          src: [
            '<%= deploy.home %>/<%= deploy.appName %>/<%= deploy.distLocation %>'
          ]
        }]
      },
      server: '.tmp'
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      dev: [
        'Gruntfile.js',
        'src/**/i18n/*.json',
        'src/app/**/*.js',
        'src/app/**/*.json'
      ],
      test: [
        'test/**/*.js'
      ]
    },

    jsbeautifier: {
      options: {
        js: {
          indentSize: 2,
          endWithNewline: true,
          spaceAfterAnonFunction: true,
          braceStyle: 'end-expand'
        },
        html: {
          indentSize: 2,
          endWithNewline: true,
          wrapLineLength: 0
        },
        css: {
          fileTypes: ['.less'],
          indentSize: 2,
          endWithNewline: true
        }
      },
      dev: {
        src: [
          '<%= yeoman.src %>/app/**/*.js',
          '<%= yeoman.src %>/app/**/*.html',
          '<%= yeoman.src %>/styles/**/*.less'
        ]
      },
      test: {
        src: [
          '<%= yeoman.test %>/app/**/*.js'
        ]
      }
    },

    sortJSON: {
      src: [
        '<%= yeoman.src %>/**/i18n/*.json'
      ],
      options: {
        spacing: 2
      }
    },
    rev: {
      dist: {
        files: {
          src: [
            '<%= yeoman.dist %>/scripts/**/*.js',
            '<%= yeoman.dist %>/styles/{,*/}*.css',
            '<%= yeoman.dist %>/assets/*/*.{png,jpg,jpeg,gif,webp,svg}',
            '<%= yeoman.dist %>/app/*/i18n/*.json'
          ]
        }
      }
    },
    useminPrepare: {
      html: [
        '<%= yeoman.src %>/index.html',
        '<%= yeoman.src %>/login.html',
        '<%= yeoman.src %>/eula.html'
      ],
      options: {
        dest: '<%= yeoman.dist %>'
      }
    },
    usemin: {
      html: ['<%= yeoman.dist %>/{,*/}*.html'],
      html2: ['<%= yeoman.dist %>/{,*/}*.html'],
      css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
      options: {
        assetsDirs: ['<%= yeoman.dist %>'],
        patterns: {
          html2: [
            [/^.*(app\/common\/i18n\/locale.*.json).*$/gm, 'Looking for locale.json files to rev...']
          ]
        }
      }
    },

    htmlmin: {
      template: {
        collapseBooleanAttributes: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true,
        removeComments: true, // Only if you don't use comment directives!
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true
      },

      dist: {
        options: {
          /*removeCommentsFromCDATA: true,
          // https://github.com/yeoman/grunt-usemin/issues/44
          //collapseWhitespace: true,
          collapseBooleanAttributes: true,
          removeAttributeQuotes: true,
          removeRedundantAttributes: true,
          useShortDoctype: true,
          removeEmptyAttributes: true,
          removeOptionalTags: true*/
        },
        files: [{
          expand: true,
          cwd: '<%= yeoman.src %>',
          src: '*.html',
          dest: '<%= yeoman.dist %>'
        }]
      }
    },
    // Put files not handled in other tasks here
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.src %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            'assets/**/*.{png,jpg,gif,webp,svg,ico}',
            'fonts/*',
            'app/**/i18n/*',
            'lib/deploy/**/*.js'
          ]
        },
        {
          // SDK Fonts and images
          expand: true,
          cwd: '<%= yeoman.titanSdk %>',
          dest: '<%= yeoman.dist %>',
          src: [
            'assets/**/*.{png,jpg,gif,webp,svg,ico}',
            'styles/fonts/{,*/}*.{eot,svg,ttf,woff}'
          ]
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: '<%= yeoman.dist %>/images',
          src: [
            'generated/*'
          ]
        }, {
          // Iconfonts
          expand: true,
          cwd: '<%= yeoman.src %>/bower_components/font-awesome/fonts',
          dest: '<%= yeoman.dist %>/fonts/',
          src: '{,*/}*.{eot,svg,ttf,woff,woff2}'
        }
        ]
      },
      styles: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.src %>/styles',
          dest: '.tmp/styles/',
          src: '{,*/}*.css'
        },
        {
          // SDK Fonts and images
          expand: true,
          cwd: '<%= yeoman.titanSdk %>',
          dest: '.tmp/',
          src: [
            'assets/**/*.{png,jpg,gif,webp,svg,ico}',
            'styles/fonts/{,*/}*.{eot,svg,ttf,woff}'
          ]
        }, {
          // Font Awesome
          expand: true,
          cwd: '<%= yeoman.src %>/bower_components/font-awesome/fonts',
          dest: '.tmp/fonts/',
          src: '{,*/}*.{eot,svg,ttf,woff,woff2}'
        }]
      },
      deploy: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>',
          src: [ '**' ],
          dest: '<%= deploy.home %>/<%= deploy.appName %>/<%= deploy.distLocation %>'
        }]
      },
      jsp: {
        files: [{
          expand: true,
          cwd: '.tmp/processedJSP/',
          src: ['*.html'],
          dest: '<%= yeoman.dist %>',
          rename: function(dest,src) {
            return dest + '/' + src.replace(/\.html$/, '.jsp');
          }
        }]
      },
      buildDev: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>',
          src: [ '**/*' ],
          dest: '<%= yeoman.dist %>/app'
        },
        {
          expand: true,
          cwd: '<%= yeoman.src %>',
          src: ['*.html'],
          dest: '<%= yeoman.dist %>'
        },
        {
          expand: true,
          cwd: '<%= yeoman.src %>/bower_components',
          src: ['**/*'],
          dest: '<%= yeoman.dist %>/bower_components'
        },
        {
          expand: true,
          cwd: '<%= yeoman.src %>/lib',
          src: ['**/*'],
          dest: '<%= yeoman.dist %>/lib'
        },
        {
          expand: true,
          dot: true,
          cwd: '<%= yeoman.src %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            'assets/**/*.{png,jpg,gif,webp,svg,ico}',
            'fonts/*',
            'app/**/i18n/*'
          ]
        },
        {
          // SDK Fonts and images
          expand: true,
          cwd: '<%= yeoman.titanSdk %>',
          dest: '<%= yeoman.dist %>',
          src: [
            'assets/**/*.{png,jpg,gif,webp,svg,ico}',
            'styles/fonts/{,*/}*.{eot,svg,ttf,woff}'
          ]
        },
        {
          // Iconfonts
          expand: true,
          cwd: '<%= yeoman.src %>/bower_components/font-awesome/fonts',
          dest: '<%= yeoman.dist %>/fonts/',
          src: '{,*/}*.{eot,svg,ttf,woff,woff2}'
        }]
      }
    },
    replace: {
      convertToJSP: {
        options: {
          processTemplates: false
        },
        replacements: [{
          // This `from` regex matches these:
          //   <img src="foo/bar.png">
          //   <script src="foo/bar.js"></script>
          // But will ignore these:
          //   <img src="http://foo.com/bar.png">
          //   <script src="//foo.com/bar.js"></script>
          from: / src="(?:(?!\/\/)(?!http:))/g,
          to: ' src="${cdnURL}'
        },{
          // This `from` regex matches these:
          //   <a href="foo/bar">
          //   <link href="foo/bar.css" rel="stylesheet">
          // But will ignore these:
          //   <a ng-href="foo/bar">
          //   <a href="http://foo.com/bar">
          //   <a href="/">
          //   <link href="//foo.com/bar.css" rel="stylesheet">
          from: / href="(?:(?!\/\/)(?!http:)(?!\/"))/g,
          to: ' href="${cdnURL}'
        },{
          from: '<!doctype',
          to: function() {
            return '<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>\n<!doctype';
          }
        }],
        src: 'dist/*.html',
        dest: '.tmp/processedJSP/'
      }
    },
    karma: {
      unit: {
        configFile: 'karma.conf.js',
        singleRun: true,
        browsers: ['PhantomJS']
      }
    },
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat/scripts',
          src: '**/*.js',
          dest: '.tmp/concat/scripts'
        }]
      }
    },
    uglify: {
      options: {
        //For debugging uglify/ngmin problems umcomment line below
        //mangle: false,
        preserveComments: function (nodeModule, comment) {
          // preserve all comments with the word "license" in it.
          return (comment.value.indexOf('license') !== -1 || comment.value.indexOf('License') !== -1);
        }
      }
    },
    less: {
      dev: {
        options: {},
        files: {
          // target.css file: source.less file
          '.tmp/styles/home.css': '<%= yeoman.src %>/styles/app/home/home.less',
          '.tmp/styles/login.css': '<%= yeoman.src %>/styles/app/login/login.less',
          '.tmp/styles/eula.css': '<%= yeoman.src %>/styles/app/eula/eula.less'
        }
      },
      dist: {
        options: {
          cleancss: true
        },
        files: {
          // target.css file: source.less file
          '<%= yeoman.dist %>/styles/home.css': '<%= yeoman.src %>/styles/app/home/home.less',
          '<%= yeoman.dist %>/styles/login.css': '<%= yeoman.src %>/styles/app/login/login.less',
          '<%= yeoman.dist %>/styles/eula.css': '<%= yeoman.src %>/styles/app/eula/eula.less'
        }
      }
    },
    strip_code: {
      options: {
        start_comment: 'test-code',
        end_comment: 'end-test-code'
      },
      files: {
        src: [
          '<%= yeoman.dist %>/{,*/}*.html',
          '<%= yeoman.dist %>/**/*.js',
          '.tmp/**/*.js'
        ]
      }
    },

    concat: {
      templates: {
        files: [
          {
            dest: '.tmp/concat/scripts/index.js',
            src: [
              '.tmp/concat/scripts/index.js',
              '.tmp/templates/index-templates.js'
            ]
          },
          {
            dest: '.tmp/concat/scripts/login.js',
            src: [
              '.tmp/concat/scripts/login.js',
              '.tmp/templates/login-templates.js'
            ]
          },
          {
            dest: '.tmp/concat/scripts/eula.js',
            src: [
              '.tmp/concat/scripts/eula.js',
              '.tmp/templates/eula-templates.js'
            ]
          }
        ]
      }
    },


    ngtemplates: {
      home: {
        options: {
          module: 'hp.portal.home.homeApp',
          htmlmin: '<%= htmlmin.template %>'
        },
        cwd: '<%= yeoman.src %>',
        src: [
          'app/home/**/*.html',
          'app/common/**/*.html',
          'lib/**/*.html'
        ],
        dest: '.tmp/templates/index-templates.js'
      },
      login: {
        options: {
          module: 'hp.common.login.loginApp',
          htmlmin: '<%= htmlmin.template %>'
        },
        cwd: '<%= yeoman.src %>',
        src: [
          'app/login/**/*.html',
          'app/common/**/*.html',
          'lib/**/*.html'
        ],
        dest: '.tmp/templates/login-templates.js'
      },
      eula: {
        options: {
          module: 'hp.portal.eula.eulaApp',
          htmlmin: '<%= htmlmin.template %>'
        },
        cwd: '<%= yeoman.src %>',
        src: [
          'app/eula/**/*.html',
          'app/common/**/*.html',
          'app/login/header/header.html',
          'lib/**/*.html'
        ],
        dest: '.tmp/templates/eula-templates.js'
      }
    },
	protractor: {
  options: {
    // Location of your protractor config file
    configFile: "prot-test/protractor-conf.js",
 
    // Do you want the output to use fun colors?
    noColor: false,
 
    // Set to true if you would like to use the Protractor command line debugging tool
    // debug: true,
 
    // Additional arguments that are passed to the webdriver command
    args: { }
  },
  e2e: {
    options: {
      // Stops Grunt process if a test fails
      keepAlive: false
    }
  },
  continuous: {
    options: {
      keepAlive: true
    }
  }
},

    svgIcons: {
      dev: {
        files: {
          '.tmp/assets/icons.svg': '<%= yeoman.titanSdk %>/assets/svg-icons/*.svg'
        }
      },
      dist: {
        files: {
          '<%= yeoman.dist %>/assets/icons.svg': '<%= yeoman.titanSdk %>/assets/svg-icons/*.svg'
        }
      }
    }

  });

  // Load NPM tasks
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-sort-json');
  grunt.loadNpmTasks('grunt-text-replace');
  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-jsbeautifier');
  grunt.loadNpmTasks('hp-grunt-svg-icons');


  // Register tasks
  grunt.registerTask('update', [
    'shell:npmInstall',
    'shell:bowerInstall'
  ]);

  grunt.registerTask('sort-json', [
    'sortJSON'
  ]);

  grunt.registerTask('beautify', [
    'jsbeautifier'
  ]);

  grunt.registerTask('convertToJSP', [
    'replace:convertToJSP',
    'copy:jsp'
  ]);

  grunt.registerTask('review', function (option) {
    grunt.task.run([
      'jsbeautifier',
      'jshint:dev',
      'sortJSON',
      'shell:gitFetch',
      'shell:gitRebase',
      'test'
    ]);

    if (option === 'draft') {
      grunt.task.run(['shell:gitReviewDraft']);
    }
    else {
      grunt.task.run(['shell:gitReview']);
    }
  });
grunt.registerTask('e2e-test', ['connect:test', 'protractor:e2e']);
  grunt.registerTask('server', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build:dist', 'connect:dist:keepalive']);
    }

    if (target === 'build:dev') {
      return grunt.task.run(['build:dev', 'connect:dist:keepalive']);
    }

    if (target !== 'backend') {
      target = 'livereload';
    }

    grunt.task.run([
      'jshint:dev',
      'clean:server',
      'less:dev',
      'copy:styles',
      'svgIcons:dev',
      'autoprefixer',
      'connect:' + target,
      'watch'
    ]);
  });

  grunt.registerTask('build', [
    'jsbeautifier:dev',
    'jshint:dev',
    'clean:dist',
    'sortJSON',
    'useminPrepare',
    'less:dist',
    'copy:styles',
    'htmlmin',
    'autoprefixer',
    'concat',
    'ngtemplates',
    'concat:templates',
    'ngAnnotate',
    'copy:dist',
    'svgIcons:dist',
    'strip_code',
    'cssmin',
    'uglify',
    'rev',
    'usemin',
    'convertToJSP',
    'clean:deploy',
    'copy:deploy'
  ]);

  grunt.registerTask('build:dist', [
    'jsbeautifier:dev',
    'jshint:dev',
    'clean:dist',
    'sortJSON',
    'useminPrepare',
    'less:dist',
    'copy:styles',
    'htmlmin',
    'autoprefixer',
    'concat',
    'ngtemplates',
    'concat:templates',
    'ngAnnotate',
    'copy:dist',
    'svgIcons:dist',
    'cssmin',
    'uglify',
    'rev',
    'usemin'
  ]);

  /* This task will build and deploy the code without minification, obfuscation, etc. */
  grunt.registerTask('build:dev', [
    'jsbeautifier:dev',
    'jshint:dev',
    'clean:dist',
    'sortJSON',
    'less:dist',
    'svgIcons:dist',
    'autoprefixer',
    'copy:buildDev',
    'strip_code',
    'convertToJSP',
    'clean:deploy',
    'copy:deploy'
  ]);

  grunt.registerTask('test', [
    'jsbeautifier:test',
    'jshint:test',
    'clean:server',
    'less:dev',
    'copy:styles',
    'autoprefixer',
    'connect:test',
    'karma'
  ]);

  grunt.registerTask('default', [
    'test',
    'build'
  ]);
};

